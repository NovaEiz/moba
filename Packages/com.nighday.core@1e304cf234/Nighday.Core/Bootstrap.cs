using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Core.Loading;
using Nighday.Core.Settings;
using Nighday.General.TaskExtentions;
using Nighday.Web;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Nighday.Core {
public class Bootstrap {

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
	public static void Init() {
		UnityEngine.Application.runInBackground = true;

		var asyncCoroutineRunner = AsyncCoroutineRunner.Instance;
	}

	[Serializable]
	public class UserData : IData {
		public int _id;
		public string name;

		public string Name {
			set {
				name = value;
				OnNameChanged?.Invoke(name);
			}
		}

		public Action<string> OnNameChanged;
	}
	private struct AuthRequestData {
		public string deviceId;
		public string buildVersion;
	}
	private struct AuthResultData : IData {
		public UserData userData;
	}
	private struct CodeData {
		public int code;
	}

	private World AppWorld;
	private DataManager DataManager;

	private LoadingSystem _loadingSystem;

	private bool _initialized;

	private async Task<Application.Settings> LoadSettings() {
		AsyncOperationHandle<Application.Settings> asyncOperation = default;
		try {
			asyncOperation = Addressables.LoadAssetAsync<Application.Settings>("Settings");
		}
		catch (Exception e) {
			Debug.LogError("Нет Settings в Addressables");
			throw;
		}
		await asyncOperation.Task;

		try {
			AppWorld.DataManager.AddComponentData(asyncOperation.Result);
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}

		return asyncOperation.Result;
	}
	
	/// <summary>
	/// Этот метод надо вызывать в самом начале бутстрапа конкретного проекта 
	/// </summary>a
	/// <returns></returns>
	public async Task<World> Initialize() {
		try {
			var asyncCoroutineRunner = AsyncCoroutineRunner.Instance;
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}

		Application.Settings settings = null;
		try {
			
		AppWorld = new World();
		App.World = AppWorld;
		DataManager = AppWorld.DataManager;

//#if !UNITY_SERVER
		AddLoadingView();

		AddCanvasGeneral();

		_loadingSystem.SetState(LoadingState.CheckUpdates);
		settings = await LoadSettings();

		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}
//#endif
		
		HeadServerSettings headServerSettings = null;
		try {
			headServerSettings = settings.GetComponent<HeadServerSettings>();
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}
		
//#if !UNITY_SERVER
		try {
			await CheckUpdates();

			_loadingSystem.SetState(LoadingState.ServerConnection);

			await Auth(headServerSettings);

			_loadingSystem.SetState(LoadingState.Loading);
			await ArchitectureInitialize();

			_initialized = true;
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}
//#else
//		_initialized = true;
//#endif

		return AppWorld;
	}

	private async Task Auth(HeadServerSettings headServerSettings) {
		try {

			var deviceId = SystemInfo.deviceUniqueIdentifier;
	#if UNITY_EDITOR
			deviceId += "_UNITY_EDITOR";
	#endif
			var authRequestData = new AuthRequestData {
				deviceId = deviceId,
				buildVersion = UnityEngine.Application.version
			};

			var isDone = false;
			while (!isDone) {
				string res = null;

				var url = $"http://{headServerSettings.Host}:{headServerSettings.Port}/auth";

				res = await request.post(url,
										JsonUtility.ToJson(authRequestData));

				if (res == null) {
					Debug.LogError("Не удалось подключиться к головному серверу. Повтор через 3 секунды.");

					await new WaitForSeconds(3f);
					continue;
				}

				var codeData = JsonUtility.FromJson<CodeData>(res);

				switch (codeData.code) {
					case 1:
						var userData = JsonUtility.FromJson<AuthResultData>(res).userData;
						DataManager.AddComponentData(userData);
						
						isDone = true;
						break;
					case 2:
						_loadingSystem.SetState(LoadingState.MessageANeedAnApplicationUpdate);

						while (true) {
							await new WaitForSeconds(1000f);
						}

						break;
					default:
						Debug.LogError("Сервер отклонил запрос авторизации! Повтор через 5 секунд.");
						await Task.Delay(5000);
						continue;
						break;
				}
			}
		
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}
	}

	private async Task CheckUpdates() {

	}

	private async Task ArchitectureInitialize() {
		await SetupSharedObject();
		// С этого момента готовы все базовые элементы архитектуры
	}

	private async Task SetupSharedObject() {
		GameObject obj = null;
		while (obj == null) {
			obj = GameObject.Find("Shared");
			await Task.Yield();
		}
		GameObject.DontDestroyOnLoad(obj.transform.parent);
		
		DataManager.AddComponentObjectByName("Shared",  obj.transform);
	}
	private async Task AddCanvasGeneral() {
		GameObject obj = null;
		while (obj == null) {
			obj = GameObject.Find("Canvas - General");
			await Task.Yield();
		}

		DataManager.AddComponentObjectByName("Canvas - General", obj.GetComponent<Canvas>());
	}
	private async Task AddLoadingView() {
		_loadingSystem = AppWorld.CreateSystem<LoadingSystem>();
		while (!_initialized || !_loadingSystem.Created) {
			await Task.Delay(50);
		}
		//_loadingSystem.LoadingView.Hide();
	}
}
}
