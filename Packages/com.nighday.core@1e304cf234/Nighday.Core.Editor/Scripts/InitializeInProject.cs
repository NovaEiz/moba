using System;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

namespace Nighday.Core.Editor {
public class InitializeInProject {
	
	public const string BasePath = "Packages/com.nighday.core/";
	public const string ResourcesPath = BasePath + "Runtime Resources/";
	
	[MenuItem("Nighday/Core/InitBasePrefabs")]
	public static void InitBasePrefabs() {
		var group = CreateCoreGroup();
		
		SetPrefabLoadingView(group);
	}

	[MenuItem("Nighday/Core/Select `Bootstrap` prefab for The Scene")]
	public static void SelectBootstrapPrefab() {
		var assetName = "Bootstrap";
		var assetPath = ResourcesPath + assetName + ".prefab";
		Selection.activeObject = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
	}

	[MenuItem("Nighday/Core/Select `Lobby` prefab for The Scene")]
	public static void SelectLobbyrefab() {
		var assetName = "Lobby";
		var assetPath = ResourcesPath + assetName + ".prefab";
		Selection.activeObject = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
	}

	[MenuItem("Nighday/Core/Select `Game` prefab for The Scene")]
	public static void SelectGamePrefab() {
		var assetName = "Game";
		var assetPath = ResourcesPath + assetName + ".prefab";
		Selection.activeObject = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
	}

	private static AddressableAssetGroup CreateCoreGroup() {
		var groupName = "Core";
	    AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
		var group = addressableAssetSettings.FindGroup(groupName);
		if (group == null) {
			group = addressableAssetSettings.CreateGroup(
				groupName,
				false,
				false,
				false,
				null
			);
		}
		
		return group;
	}
	
	private static void SetPrefabLoadingView(AddressableAssetGroup group) {
        var assetName = "LoadingView";
        var assetPath = ResourcesPath + assetName + ".prefab";
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
        if (prefab == null) {
            throw new Exception("Нет префаба " + assetName + " в пакете Nighday.Core в пути = " + assetPath);
        }
        var guid = AssetDatabase.AssetPathToGUID(assetPath);
        AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
        var asset = addressableAssetSettings.CreateOrMoveEntry(guid, group);
        asset.address = assetName;
        EditorUtility.SetDirty(group);
	}
	
}
}