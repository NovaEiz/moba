namespace Nighday.Core.Settings {

public interface IHeadServerSettingsGetter {

	HeadServerSettings HeadServerSettings { get; }

}
}