using Nighday.Application;
using UnityEngine;

namespace Nighday.Core.Settings {
[CreateAssetMenu(fileName = "HeadServerSettings", menuName = "Nighday/Core/HeadServerSettings", order = 0)]
public class HeadServerSettings : SettingsComponent {

	[SerializeField] private string _host;
	[SerializeField] private ushort _port;

	public string Host => _host;
	public ushort Port => _port;

}
}