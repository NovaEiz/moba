using UnityEngine;
using UnityEngine.UI;

namespace Nighday.Core.Loading {

public class LoadingView : MonoBehaviour {

	[SerializeField] private Camera _camera;

	public virtual void SetState(LoadingState state) {
		
	}

	protected void DisableCamera() {
		_camera.gameObject.SetActive(false);
	}
	protected void EnableCamera() {
		_camera.gameObject.SetActive(true);
	}
	public virtual void Hide() {
		DisableCamera();
		gameObject.SetActive(false);
	}
	public virtual void Show() {
		EnableCamera();
		gameObject.SetActive(true);
	}

}
}