using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

using Nighday.General.EntitiesExtentions;

namespace Nighday.Core.Loading {

public enum LoadingState {
	CheckUpdates,
	ServerConnection,
	Loading,
	MessageANeedAnApplicationUpdate
}

public class LoadingSystem : ComponentSystem {

	private LoadingView _loadingView;

	public LoadingView LoadingView => _loadingView;

	private Action _setStateQueueActions;

	public void SetState(LoadingState state) {
		if (_loadingView == null) {
			_setStateQueueActions += () => {
				_loadingView.SetState(state);
			};
			return;
		}
		_loadingView.SetState(state);
	}
	
	public override async Task OnCreate() {
		try {
			Canvas canvasGeneral = null;

			while (AsyncCoroutineRunner.Instance == null) {
				await Task.Yield();
			}

			while (canvasGeneral == null) {
				await Task.Yield();
				canvasGeneral = DataManager
					.GetComponentObjectByName<Canvas>("Canvas - General");
			}
			foreach (Transform itemTr in canvasGeneral.transform) {
				_loadingView = itemTr.GetComponent<LoadingView>();
				if (_loadingView != null) {
					break;
				}
			}

			if (_loadingView == null) {
				var asyncOperation = Addressables.LoadAssetAsync<GameObject>("LoadingView");
				await asyncOperation.Task;
		
				_loadingView = GameObject.Instantiate(asyncOperation.Result.GetComponent<LoadingView>(), canvasGeneral.transform);
			}

			_setStateQueueActions?.Invoke();
			_setStateQueueActions = null;
			
			DataManager.AddComponentObject(_loadingView);
		}
		catch (Exception e) {
			Debug.LogError("LoadingSystem OnCreate 1 e = " + e);
		}
		
		Enabled = false;
	}

	
	public override void OnUpdate() {
		
	}
	
}

}
