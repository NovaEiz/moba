using System;
using UnityEngine;
using UnityEngine.UI;

namespace Nighday.Matchmaking.Client.UI {
public class MatchmakingLobbyClientView : MonoBehaviour {

	public enum State {
		Passive,
		Searching,
		GameFound,
		WaitingGameStart,
		Connected,
		Disconnected,
		Hidden
	}

	private State _state;

	public void SetState(State state) {
		if (!UnityEngine.Application.isPlaying) {
			Start();
		}
		_changeState?.Invoke(state);
	}
	
	protected virtual void AddStateToChange(State state, Transform stateTransform) {
		if (stateTransform == null) {
			return;
		}
		_changeState += (newState) => {
			if (newState == state) {
				stateTransform.gameObject.SetActive(true);
			} else {
				stateTransform.gameObject.SetActive(false);
			}
		};
	}

	[Header("Buttons")]
	[SerializeField] private Button _playButton;
	[SerializeField] private Button _cancelSearchButton;
	[SerializeField] private Button _acceptGameButton;
	[SerializeField] private Button _disconnectFromGameButton;
	[SerializeField] private Button _reconnectToGameButton;
	[SerializeField] private Button _leaveGameButton;

	public Button PlayButton => _playButton;
	public Button CancelSearchButton => _cancelSearchButton;
	public Button AcceptGameButton => _acceptGameButton;
	public Button DisconnectFromGameButton => _disconnectFromGameButton;
	public Button ReconnectToGameButton => _reconnectToGameButton;
	public Button LeaveGameButton => _leaveGameButton;

	[Header("States")]
	[SerializeField] private Transform _passiveState;
	[SerializeField] private Transform _searchingState;
	[SerializeField] private Transform _gameFoundState;
	[SerializeField] private Transform _connectedState;
	[SerializeField] private Transform _disconnectedState;
	
	private Action<State> _changeState;

	private void Start() {
		_changeState = null;
		
		AddStateToChange(stateTransform: _passiveState, state: State.Passive);
		AddStateToChange(stateTransform: _searchingState, state: State.Searching);
		AddStateToChange(stateTransform: _gameFoundState, state: State.GameFound);
		AddStateToChange(stateTransform: _connectedState, state: State.Connected);
		AddStateToChange(stateTransform: _disconnectedState, state: State.Disconnected);
	}
	
}
}