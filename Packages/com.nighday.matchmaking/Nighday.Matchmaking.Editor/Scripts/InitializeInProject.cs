using System;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

namespace Nighday.Matchmaking.Editor {
public class InitializeInProject {
	
	public const string BasePath = "Packages/com.nighday.matchmaking/Nighday.Matchmaking.Client.UI/";
	public const string ResourcesPath = BasePath + "Runtime Resources/";
	
	[MenuItem("Nighday/Matchmaking/InitBasePrefabs")]
	public static void InitBasePrefabs() {
		var group = CreateMatchmakingGroup();
		
		SetPrefabMatchmakingLobbyClientView(group);
	}
	
	private static AddressableAssetGroup CreateMatchmakingGroup() {
		var groupName = "Matchmaking";
	    AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
		var group = addressableAssetSettings.FindGroup(groupName);
		if (group == null) {
			group = addressableAssetSettings.CreateGroup(
				groupName,
				false,
				false,
				false,
				null
			);
		}

		return group;
	}
	
	private static void SetPrefabMatchmakingLobbyClientView(AddressableAssetGroup group) {
        var assetName = "MatchmakingLobbyClientView";
        var assetPath = ResourcesPath + assetName + ".prefab";
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
        if (prefab == null) {
            throw new Exception("Нет префаба " + assetName + " в пакете Nighday.Matchmaking");
        }
        var guid = AssetDatabase.AssetPathToGUID(assetPath);
        AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
        var asset = addressableAssetSettings.CreateOrMoveEntry(guid, group);
        asset.address = assetName;
        EditorUtility.SetDirty(group);
	}
	
	private static void SetPrefabMatchmakingToLobbyToGameButtonView(AddressableAssetGroup group) {
        var assetName = "Matchmaking_SwitchingBetweenScenesButtonView";
        var assetPath = ResourcesPath + assetName + ".prefab";
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
        if (prefab == null) {
            throw new Exception("Нет префаба " + assetName + " в пакете Nighday.Matchmaking");
        }
        var guid = AssetDatabase.AssetPathToGUID(assetPath);
        AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
        var asset = addressableAssetSettings.CreateOrMoveEntry(guid, group);
        asset.address = assetName;
        EditorUtility.SetDirty(group);
	}
	
	private static void SetPrefabMatchmakingSearchButtonView(AddressableAssetGroup group) {
        var assetName = "Matchmaking_SearchButtonView";
        var assetPath = ResourcesPath + assetName + ".prefab";
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
        if (prefab == null) {
            throw new Exception("Нет префаба " + assetName + " в пакете Nighday.Matchmaking");
        }
        var guid = AssetDatabase.AssetPathToGUID(assetPath);
        AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
        var asset = addressableAssetSettings.CreateOrMoveEntry(guid, group);
        asset.address = assetName;
        EditorUtility.SetDirty(group);
	}
	
	private static void SetPrefabModeSelectionView(AddressableAssetGroup group) {
        var assetName = "Matchmaking_ModeSelectionView";
        var assetPath = ResourcesPath + assetName + ".prefab";
        var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
        if (prefab == null) {
            throw new Exception("Нет префаба " + assetName + " в пакете Nighday.Matchmaking");
        }
        var guid = AssetDatabase.AssetPathToGUID(assetPath);
        AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
        var asset = addressableAssetSettings.CreateOrMoveEntry(guid, group);
        asset.address = assetName;
        EditorUtility.SetDirty(group);
	}
	
}
}