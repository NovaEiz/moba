using Nighday.Application;
using UnityEngine;

namespace Nighday.Matchmaking.General {
[CreateAssetMenu(fileName = "MatchmakingSettings", menuName = "Nighday/Matchmaking/MatchmakingSettings", order = 0)]
public class MatchmakingSettings : SettingsComponent {

	[SerializeField] private ushort _port;
	[SerializeField] private Vector2Int _gamePortRange;
	
	public ushort Port => _port;
	public Vector2Int GamePortRange => _gamePortRange;
	
	/*
	private bool _portReceived;
	public ushort Port {
		get {
			if (!_portReceived) {
				_portReceived = true;
				
				var testCheckValue = System.Convert.ToBoolean(PlayerPrefs.GetInt("TestCheckValue"));
				testCheckValue = !testCheckValue;
				PlayerPrefs.SetInt("TestCheckValue", System.Convert.ToInt32(testCheckValue));

				if (testCheckValue) {
					_port++;
				}
			}
			
			return (ushort)_port;
		}
	}
	 */

}
}
