using System;
using Unity.Collections;
using Unity.Entities;

namespace Nighday.Matchmaking.General {

[Serializable]
public struct GameContacts {
	public string host;
	public ushort port;
}
[Serializable]
public struct GameDataJson {
	public string playersData;
}

[Serializable]
public struct CodeData {
	public int code;
}
[Serializable]
public struct PlayerData {
	public int _id;
	public string data;
}

public struct GameData : IComponentData {
	public FixedString64 Host;
	public ushort Port;
}



}
