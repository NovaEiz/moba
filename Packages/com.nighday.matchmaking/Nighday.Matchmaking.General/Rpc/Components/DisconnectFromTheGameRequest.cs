using Unity.Burst;
using Unity.NetCode;
using Unity.Networking.Transport;

namespace Nighday.Matchmaking.General {

[BurstCompile]
public struct DisconnectFromTheGameRequest : IRpcCommand {

	public int UserId;

	public void Serialize(ref DataStreamWriter writer) {
		writer.WriteInt(UserId);
	}

	public void Deserialize(ref DataStreamReader reader) {
		UserId = reader.ReadInt();
	}

	//[BurstCompile]
	//private static void InvokeExecute(ref RpcExecutor.Parameters parameters) {
		//RpcExecutor.ExecuteCreateRequestComponent<DisconnectFromTheGameRequest>(ref parameters);
	//}

	//static PortableFunctionPointer<RpcExecutor.ExecuteDelegate> InvokeExecuteFunctionPointer =
	//	new PortableFunctionPointer<RpcExecutor.ExecuteDelegate>(InvokeExecute);

	//public PortableFunctionPointer<RpcExecutor.ExecuteDelegate> CompileExecute() {
	//	return InvokeExecuteFunctionPointer;
	//}
}

//public class DisconnectFromTheGameRequestSystem : RpcCommandRequestSystem<DisconnectFromTheGameRequest> {


//}

}