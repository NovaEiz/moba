using System;
using Unity.Burst;
using Unity.NetCode;
using Unity.Networking.Transport;

namespace Nighday.Matchmaking.General {

[BurstCompile]
public struct UserAuthorizationInGameRequestResult : IRpcCommand {

	public bool Value;

	public void Serialize(ref DataStreamWriter writer) {
		writer.WriteByte(Convert.ToByte(Value));
	}

	public void Deserialize(ref DataStreamReader reader) {
		Value = Convert.ToBoolean(reader.ReadByte());
	}

	//[BurstCompile]
	//private static void InvokeExecute(ref RpcExecutor.Parameters parameters) {
	//	RpcExecutor.ExecuteCreateRequestComponent<UserAuthorizationInGameRequestResult>(ref parameters);
	//}

	//static PortableFunctionPointer<RpcExecutor.ExecuteDelegate> InvokeExecuteFunctionPointer =
	//	new PortableFunctionPointer<RpcExecutor.ExecuteDelegate>(InvokeExecute);

	//public PortableFunctionPointer<RpcExecutor.ExecuteDelegate> CompileExecute() {
	//	return InvokeExecuteFunctionPointer;
	//}
}

//public class UserAuthorizationInGameRequestResultSystem : RpcCommandRequestSystem<UserAuthorizationInGameRequestResult> {


//}

}