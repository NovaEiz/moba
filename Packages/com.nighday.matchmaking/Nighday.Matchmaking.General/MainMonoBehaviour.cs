using System;
using System.Collections.Generic;
using UnityEngine;

namespace Nighday.Matchmaking.General {
public class MainMonoBehaviour : MonoBehaviour {
	static MainMonoBehaviour _instance;

	public static MainMonoBehaviour Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new GameObject("MainMonoBehaviour")
					.AddComponent<MainMonoBehaviour>();
			}

			return _instance;
		}
	}

	private List<Action> _callbacksList = new List<Action>();
	public void AddAction(Action callback) {
		_callbacksList.Add(callback);
	}

	private void LateUpdate() {
		foreach (var itemAction in _callbacksList) {
			itemAction();
		}
		_callbacksList.Clear();
	}

}
}