using Unity.Entities;

namespace Nighday.Matchmaking.General {

public struct UserId : IComponentData {
	public int Value;
}

}