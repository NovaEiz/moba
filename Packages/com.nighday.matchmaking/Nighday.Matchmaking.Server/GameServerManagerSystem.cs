using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Core.Settings;
using Nighday.General.Other.SimpleJSON;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.General;
using Nighday.Web;
using UnityEngine;
using World = Unity.Entities.World;

namespace Nighday.Matchmaking.Server {
public class GameServerManagerSystem : ComponentSystem {

	public event Action<ushort, string, Action<World>> OnCreateGame;
	
	private List<ushort> _busyPorts = new List<ushort>();
	private ushort _minPort => (ushort)(_matchmakingSettings.GamePortRange.x);//15000;
	private ushort _maxPort => (ushort)(_matchmakingSettings.GamePortRange.y);//15999;

	private void FreePort(ushort port) {
		var len = _busyPorts.Count;
		for (int i = 0; i < len; i++) {
			if (_busyPorts[i] == port) {
				_busyPorts.RemoveAt(i);
				return;
			}
		}
	}

	private ushort GetFreePort() {
		for (var port = _minPort; port<_maxPort; port++) {
			var checkRes = true;
			foreach (var busyPort in _busyPorts) {
				if (port == busyPort) {
					checkRes = false;
					break;
				}
			}

			if (!checkRes) {
				continue;
			}
			
			_busyPorts.Add(port);
			
			return port;
		}

		throw new Exception("Нет свободных портов");
		return 0;
	}
	
	public async Task FinishGame(World world) {
		// TODO: надо записать в базе данных что игра закончена
		world.QuitUpdate = true;
		
		foreach (var gameWorldByPort in _gameWorldsByPort) {
			if (gameWorldByPort.Value.Name == world.Name) {
				FreePort(gameWorldByPort.Key);
				break;
			}
		}
		await new WaitForUpdate();
		world.EntityManager.CompleteAllJobs();
		await new WaitForUpdate();
		world.Dispose();
	}

	private void CreateGame(ushort port, string gameModeName, Action<World> callback) {
		OnCreateGame(port, gameModeName, callback);
	}

	private struct AuthRequestData {
		public ushort port;
	}

	private struct AuthResultData {
		public int code;
	}

	private HeadServerSettings _headServerSettings;
	private MatchmakingSettings _matchmakingSettings;
	
	private string Address => _headServerSettings.Host + ":" + _headServerSettings.Port;

	public override async Task OnCreate() {
		var settings = DataManager.GetComponentData<Settings>();
		_matchmakingSettings = settings.GetComponent<MatchmakingSettings>();
		_headServerSettings = settings.GetComponent<HeadServerSettings>();
	
		await Auth(_headServerSettings);

		InitRoutes();
		
		Debug.Log("Авторизация GSM выполнена. MatchmakingSettings.Port = " + _matchmakingSettings.Port);
	}

	private async Task Auth(HeadServerSettings _headServerSettings) {
		var authResult = default(AuthResultData);

		var authRequestData = new AuthRequestData {
			port = _matchmakingSettings.Port
		};

		try {
			while (authResult.code != 1) {
				var url = "http://" + Address + "/gsm";
				var authResultJson = await request.post(
										url,
										JsonUtility.ToJson(authRequestData)
									);
				if (string.IsNullOrEmpty(authResultJson)) {
					await Task.Delay(2000);
					continue;
				}
				authResult = JsonUtility.FromJson<AuthResultData>(authResultJson);
			
				await Task.Yield();
			}
		}
		catch (Exception e) {
			Debug.LogError("e = " + e);
		}
	}

	private struct SetDataRequest {
		public int code;
		public string gameData;
		public GameContacts gameContacts;
		public string gameModeName;
	}
	
	private struct CreateGameResponse {
		public int code;
		public ushort port;
	}
	
	private Dictionary<ushort, World> _gameWorldsByPort = new Dictionary<ushort, World>();

	private Express _app;
	
	private async Task InitRoutes() {
		var app = new Express();
		_app = app;
		
		// Запрос на создание игры
		app.post("/games/", async (req, res) => {
			// Создать игру, а потом отправить данные на главный сервер об этой игре
			try {
				ushort port = GetFreePort();

				var createGameResponse = new CreateGameResponse {
					code = 1,
					port = port
				};

				if (!_gameWorldsByPort.TryGetValue(port, out World world_)) {
					_gameWorldsByPort.Add(port, null);
				} else {
					_gameWorldsByPort[port] = null;
				}
				
				res.Send(JsonUtility.ToJson(createGameResponse));
			}
			catch (Exception e) {
				Debug.LogError("Запрос на создание игры: e = " + e);
				res.Send(JsonUtility.ToJson(new CodeData{code = 404}));
			}
		});
		
		// Установить данные для игры
		app.put("/games/data/", async (req, res) => {
			try {
				res.Send(
					JsonUtility.ToJson(new CodeData {code = 1})
				);
				
				var data = req.body;
				var setDataRequest = JsonUtility.FromJson<SetDataRequest>(data);

				var port = setDataRequest.gameContacts.port;
				

				var gameDataJsonNode = JSON.Parse(data)["gameData"];

				var gameDataStr = gameDataJsonNode.ToString();

				MainMonoBehaviour.Instance.AddAction(() => {
					CreateGame(port, setDataRequest.gameModeName, (world) => {
						_gameWorldsByPort[port] = world;
				
						var matchmakingServerSystem = world.GetExistingSystem<MatchmakingGameServerSystem>();
						matchmakingServerSystem.OnGameDataToInitialize(gameDataStr);
					});
				});
				

				
			}
			catch (Exception e) {
				Debug.LogError("/games/data/ e = " + e);
				
				res.Send(
					JsonUtility.ToJson(new CodeData {code = 2})
				);
			}
		});
		
		app.post("/games/:game_port/data/", (req, res) => {
			res.Send("/games/4 data !");
		});

		app.listen(_matchmakingSettings.Port);
	}

	public override void OnUpdate() {
		
	}

	public override void OnDestroy() {
		_app.Dispose();
	}

	private Func<string, string> GetMapNameByModeNameAction;
	public void SetMapNameByModeNameAction(Func<string, string> action) {
		GetMapNameByModeNameAction += action;
	}
	
	public string GetMapNameByModeName(string modeName) {
		var name = GetMapNameByModeNameAction?.Invoke(modeName);

		return name;
	}
	
}
}