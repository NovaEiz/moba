using Nighday.Matchmaking.General;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.Matchmaking.Server {
[DisableAutoCreation]
public class UserAuthorizationInGameServerSystem : ComponentSystem {
	
	private struct NeedAuthorizeTag : IComponentData {}

	private bool _isConnected;
	public bool IsConnected => _isConnected;

	private Entity _connectionEntity;

	public void SetGhostReceiveSystemComponentType<T>() where T : struct, IComponentData {
		RequireSingletonForUpdate<T>();
	}

	//TODO: Может пригодится этот метод потом
	/*
	public void Connect() {
		EntityManager.CreateEntity(typeof(NeedAuthorizeTag));
	}
	*/
	
	protected override void OnCreate() {
		RequireSingletonForUpdate<GameData>();
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<SendRpcCommandRequestComponent>()
			.ForEach((Entity entity,
					ref UserAuthorizationInGameRequest component,
					ref ReceiveRpcCommandRequestComponent receiveRpcCommandRequestComponent
					) => {
						EntityManager.AddComponentData(receiveRpcCommandRequestComponent.SourceConnection, new NetworkStreamInGame());

						var req = EntityManager.CreateEntity();
						EntityManager.AddComponentData(req, new UserAuthorizationInGameRequestResult {
							
						});
						EntityManager.AddComponentData(req, new SendRpcCommandRequestComponent {
							TargetConnection = receiveRpcCommandRequestComponent.SourceConnection
						});

						EntityManager.AddComponentData(receiveRpcCommandRequestComponent.SourceConnection, new UserId {
							Value = component.UserId
						});

						EntityManager.DestroyEntity(entity);
					});
	}

	
}

}