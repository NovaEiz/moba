using Nighday.Matchmaking.General;
using Unity.Entities;
using Unity.NetCode;

namespace Nighday.Matchmaking.Server {

[DisableAutoCreation]
public class GameServerNetworkSystem : ComponentSystem {

	private struct UpdateTag : IComponentData {}

	private NetworkStreamReceiveSystem _networkStreamReceiveSystem;

	private bool _listening;
	public bool Listening => _listening;

	protected override void OnCreate() {
		_networkStreamReceiveSystem = World.GetExistingSystem<NetworkStreamReceiveSystem>();
		RequireSingletonForUpdate<GameData>();
		RequireSingletonForUpdate<UpdateTag>();

		EntityManager.CreateEntity(typeof(UpdateTag));
	}

	protected override void OnUpdate() {
		EntityManager.DestroyEntity(GetSingletonEntity<UpdateTag>());

		var gameData = GetSingleton<GameData>();

		var networkEndPoint = Unity.Networking.Transport.NetworkEndPoint.AnyIpv4;
		networkEndPoint.Port = gameData.Port;
		
		_listening = _networkStreamReceiveSystem.Listen(networkEndPoint);
	}

	
}
}