using Unity.Entities;
using Unity.NetCode;

namespace Nighday.Matchmaking.Server {
public class MatchmakingGameServerBootstrap {

	public void WorldInitialization(World world) {
		
		// Groups

		var ghostPredictionSystemGroup = world.GetExistingSystem<GhostPredictionSystemGroup>();

		var simulationSystemGroup = world.GetExistingSystem<ServerSimulationSystemGroup>();
		var gameServerSystemGroup = world.GetExistingSystem<GameServerSystemGroup>();

		simulationSystemGroup.AddSystemToUpdateList(gameServerSystemGroup);

		//===
		// Systems
		
		var matchmakingServerSystem = world.CreateSystem<MatchmakingGameServerSystem>();
		gameServerSystemGroup.AddSystemToUpdateList(matchmakingServerSystem);
		
		gameServerSystemGroup.AddSystemToUpdateList(world.CreateSystem<UserAuthorizationInGameServerSystem>());
		gameServerSystemGroup.AddSystemToUpdateList(world.CreateSystem<GameServerNetworkSystem>());
		
		//===
		// Init systems
		
		//===
		// Sort systems
		
		ghostPredictionSystemGroup.SortSystems();
		gameServerSystemGroup.SortSystems();
		simulationSystemGroup.SortSystems();

	}
	
}
}