using System;
using Unity.Entities;

namespace Nighday.Matchmaking.Server {

[DisableAutoCreation]
public class MatchmakingGameServerSystem : ComponentSystem {

	private Action<string> _onGameDataToInitialize;

	protected override void OnUpdate() {
		
	}

	public virtual void OnGameDataToInitialize(string gameData) {
		_onGameDataToInitialize?.Invoke(gameData);
	}

	public void SetOnGameDataToInitializeAction(Action<string> onGameDataToInitialize) {
		_onGameDataToInitialize = onGameDataToInitialize;
	}
	
}
}