using System;
using Nighday.Application;

namespace Nighday.Matchmaking.Client {

using GameReadyData = MatchmakingLobbyClientSystem.GameReadyData;

//public partial class MatchmakingLobbyClientSystem {

	public class MatchmakingLobbyClientData : IData {

#region Events

		/// <summary>
		/// Поиск начат и это подтверждено сервером
		/// </summary>
		public Action OnSearchStarted;

		/// <summary>
		/// Поиск отменен и это подтверждено сервером
		/// </summary>
		public Action OnSearchCanceled;

		/// <summary>
		/// Игра найдена
		/// </summary>
		public Action OnGameFound;

		/// <summary>
		/// Для получения актуальных данных поиска.
		/// Например, для отображения присоединенных игроков в этом поиске
		/// </summary>
		public Action OnSearchDataReceived;

		/// <summary>
		/// Игра создана
		/// </summary>
		public Action<GameReadyData> OnGameCreated;

		/// <summary>
		/// Переподключено к игре
		/// </summary>
		public Action<GameReadyData> OnGameReconnected;

		/// <summary>
		/// Игра загружена (инициализирована)
		/// </summary>
		public Action OnGameLoaded;

		/// <summary>
		/// Игра начала загружаться
		/// </summary>
		public Action<GameReadyData> OnLoadingGame;

		/// <summary>
		/// Отключено от игры
		/// </summary>
		public Action OnGameDisconnected;

		/// <summary>
		/// Покинул игру / Выход из игры
		/// </summary>
		public Action OnLeftGame;

		/// <summary>
		/// Игра принята
		/// </summary>
		public Action OnGameAccepted;

#endregion

	}
//}

}