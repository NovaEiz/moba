using Nighday.Matchmaking.General;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.Matchmaking.Client {

[DisableAutoCreation]
public class GameClientNetworkSystem : ComponentSystem {


	private struct NeedConnectTag : IComponentData {}

	private NetworkStreamReceiveSystem _networkStreamReceiveSystem;

	private bool _isConnected;
	public bool IsConnected => _isConnected;

	private Entity _connectionEntity;

	protected override void OnCreate() {
		_networkStreamReceiveSystem = World.GetExistingSystem<NetworkStreamReceiveSystem>();
		RequireSingletonForUpdate<GameData>();
		RequireSingletonForUpdate<NeedConnectTag>();
		
		EntityManager.CreateEntity(typeof(NeedConnectTag));

	}

	protected override void OnUpdate() {

		EntityManager.DestroyEntity(GetSingletonEntity<NeedConnectTag>());

		var gameData = GetSingleton<GameData>();

		ushort port = gameData.Port;
		string host = gameData.Host.ToString();
		
		var networkEndPoint = Unity.Networking.Transport.NetworkEndPoint.Parse(host, port);
		
		_connectionEntity = _networkStreamReceiveSystem.Connect(networkEndPoint);

		Debug.Log("Client: Подключился к серверу игры ConnectionEntity = " + _connectionEntity);
	}

	
}
}