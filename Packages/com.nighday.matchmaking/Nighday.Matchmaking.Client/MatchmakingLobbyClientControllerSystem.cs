using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client.UI;
using Nighday.Matchmaking.General;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Nighday.Matchmaking.Client {


/// <summary>
/// Для каждого проекта создается свой подобный класс, как и View.
/// </summary>
public class MatchmakingLobbyClientControllerSystem : ComponentSystem {

	public enum State {
		LoadingGame,
		GameLoaded
	}

	public Action<State> OnChangeState;

	private MatchmakingLobbyClientView _matchmakingLobbyClientView;
	private MatchmakingLobbyClientSystem _matchmakingLobbyClientSystem;

	public override async Task OnCreate() {
		await LoadMatchmakingLobbyClientView();

		InitButtonEvents();
		
		RequireSingletonForUpdate<MatchmakingLobbyClientData>();
	}
	public override void OnUpdate() {
		var matchmakingLobbyClientData = World.DataManager.GetComponentData<MatchmakingLobbyClientData>();
		_matchmakingLobbyClientSystem = World.GetExistingSystem<MatchmakingLobbyClientSystem>();
		
		InitSubscribes(matchmakingLobbyClientData);

		Enabled = false;
	}

	private async Task LoadMatchmakingLobbyClientView() {
		var asyncOperation = Addressables.LoadAssetAsync<GameObject>("MatchmakingLobbyClientView");
		await asyncOperation.Task;

		Transform matchmakingParentTr = null;
		while (matchmakingParentTr == null) {
			var obj = DataManager.GetComponentObjectByName<Canvas>("Canvas - General");
			if (obj != null) {
				matchmakingParentTr = obj.transform;
			}
			await Task.Yield();
		}

		var instance = GameObject.Instantiate(asyncOperation.Result, matchmakingParentTr);
		_matchmakingLobbyClientView = instance.GetComponent<MatchmakingLobbyClientView>();
	}
	

	private void InitSubscribes(MatchmakingLobbyClientData matchmakingLobbyClientData) {
		matchmakingLobbyClientData.OnSearchStarted += () => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.Searching);
		};
		matchmakingLobbyClientData.OnSearchCanceled += () => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.Passive);
		};
		matchmakingLobbyClientData.OnGameFound += () => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.GameFound);
		};
		matchmakingLobbyClientData.OnGameAccepted += () => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.WaitingGameStart);
		};
		matchmakingLobbyClientData.OnLoadingGame += (gameReadyData) => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.Hidden);
		};
		matchmakingLobbyClientData.OnGameLoaded += () => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.Connected);
		};
		matchmakingLobbyClientData.OnGameDisconnected += async () => {
			// Удалить сцену Game
			await SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Game"));
			
			DataManager.GetGameObject("Lobby").SetActive(true);
			
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.Disconnected);
		};
		matchmakingLobbyClientData.OnGameReconnected += (gameReadyData) => {

		};
		matchmakingLobbyClientData.OnLeftGame += () => {
			_matchmakingLobbyClientView.SetState(MatchmakingLobbyClientView.State.Passive);
		};
		
		matchmakingLobbyClientData.OnSearchDataReceived += () => {
			
		};
	}
	private void InitButtonEvents() {
		_matchmakingLobbyClientView.PlayButton.onClick.AddListener(() => {
			OnClickPlayButton(_matchmakingLobbyClientView.PlayButton);
		});
		_matchmakingLobbyClientView.CancelSearchButton.onClick.AddListener(() => {
			OnClickCancelSearchButton(_matchmakingLobbyClientView.CancelSearchButton);
		});
		_matchmakingLobbyClientView.AcceptGameButton.onClick.AddListener(() => {
			OnClickAcceptGameButton(_matchmakingLobbyClientView.AcceptGameButton);
		});
		_matchmakingLobbyClientView.DisconnectFromGameButton.onClick.AddListener(() => {
			OnClickDisconnectFromGame(_matchmakingLobbyClientView.DisconnectFromGameButton);
		});
		_matchmakingLobbyClientView.ReconnectToGameButton.onClick.AddListener(() => {
			OnClickReconnectToGame(_matchmakingLobbyClientView.ReconnectToGameButton);
		});
		_matchmakingLobbyClientView.LeaveGameButton.onClick.AddListener(() => {
			OnClickLeaveGame(_matchmakingLobbyClientView.LeaveGameButton);
		});
	}

	private async Task OnClickPlayButton(Button button) {
		button.interactable = false;

		var gameModeDataJson = "{\"playersAmount\":1,\"gameModeName\":\"Default Location\"}";
			
		var result = await _matchmakingLobbyClientSystem.Play(gameModeDataJson);

		if (result.code == MatchmakingLobbyClientSystem.EnterToTheLobbyRequestData.ResultCode.OK) {
			var data = result.data;
		}
		
		button.interactable = true;
	}
	
	private async Task OnClickCancelSearchButton(Button button) {
		button.interactable = false;

		var resultCode = await _matchmakingLobbyClientSystem.CancelSearch();

		if (resultCode == MatchmakingLobbyClientSystem.LeaveTheLobbyRequestData.ResultCode.OK) {
			
		}
		
		button.interactable = true;
	}
	
	private async Task OnClickAcceptGameButton(Button button) {
		button.interactable = false;

		var resultCode = await _matchmakingLobbyClientSystem.AcceptGame();

		if (resultCode == MatchmakingLobbyClientSystem.AcceptTheGameRequestData.ResultCode.OK) {
			
		}
		
		button.interactable = true;
	}

	private async Task OnClickDisconnectFromGame(Button button) {
		button.interactable = false;

		var resultCode = await _matchmakingLobbyClientSystem.DisconnectFromGame();

		if (resultCode == MatchmakingLobbyClientSystem.DisconnectFromTheGameRequestData.ResultCode.OK) {
			
		}
		
		button.interactable = true;
	}
	
	private async Task OnClickReconnectToGame(Button button) {
		_matchmakingLobbyClientView.LeaveGameButton.interactable = false;
		button.interactable = false;

		var resultCode = await _matchmakingLobbyClientSystem.ReconnectToGame();

		if (resultCode == MatchmakingLobbyClientSystem.ReconnectFromTheGameRequestData.ResultCode.OK) {
			
		}
		
		_matchmakingLobbyClientView.LeaveGameButton.interactable = true;
		button.interactable = true;
	}

	private async Task OnClickLeaveGame(Button button) {
		_matchmakingLobbyClientView.ReconnectToGameButton.interactable = false;
		button.interactable = false;

		var resultCode = await _matchmakingLobbyClientSystem.LeaveGame();

		if (resultCode == MatchmakingLobbyClientSystem.LeaveTheGameRequestData.ResultCode.OK) {
			
		}
		
		button.interactable = true;
		_matchmakingLobbyClientView.ReconnectToGameButton.interactable = true;
	}

	
	
}
}
