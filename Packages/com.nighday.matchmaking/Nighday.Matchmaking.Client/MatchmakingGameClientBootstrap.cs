using Unity.Entities;
using Unity.NetCode;

namespace Nighday.Matchmaking.Client {
public class MatchmakingGameClientBootstrap {

	public void WorldInitialization(World world) {
		
		// Groups

		var ghostPredictionSystemGroup = world.GetExistingSystem<GhostPredictionSystemGroup>();

		var simulationSystemGroup = world.GetExistingSystem<ClientSimulationSystemGroup>();
		var gameClientSystemGroup = world.GetExistingSystem<GameClientSystemGroup>();

		simulationSystemGroup.AddSystemToUpdateList(gameClientSystemGroup);

		//===
		// Systems
		
		var userAuthorizationInGameClientSystem = world.CreateSystem<UserAuthorizationInGameClientSystem>();
		gameClientSystemGroup.AddSystemToUpdateList(userAuthorizationInGameClientSystem);
		
		var gameClientNetworkSystem = world.CreateSystem<GameClientNetworkSystem>();
		gameClientSystemGroup.AddSystemToUpdateList(gameClientNetworkSystem);
		
		//===

		//===
		// Init systems
		
		//===
		// Sort systems
		
		ghostPredictionSystemGroup.SortSystems();
		gameClientSystemGroup.SortSystems();
		simulationSystemGroup.SortSystems();

	}
	
}
}