using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Core.Settings;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.General;
using Nighday.Web;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.Matchmaking.Client {
public partial class MatchmakingLobbyClientSystem : ComponentSystem {

	private bool _isQuit;
	public override void OnDestroy() {
		_isQuit = true;
	}

#region Persistent fields
	
	private Unity.Entities.World _world;
	public Unity.Entities.World World => _world;

	private EnterToTheLobbyRequestData.ResultData _enterToTheLobbyResultData;

	private MatchmakingLobbyClientData _matchmakingLobbyClientData;

#endregion
	
#region Messages Data
	
	public struct EnterToTheLobbyRequestData {
		public int userId;
		public string gameModeDataJson;

		public enum ResultCode {
			OK = 1,
			ERROR = 2
		}
		
		public struct ResultData {
			[Serializable]
			public class Data {
				public int lobbyId;
				public string gameModeName;
			}

			public Data data;
		}
	}

	public struct LeaveTheLobbyRequestData {
		public int userId;

		public enum ResultCode {
			OK = 1,
			ERROR = 2
		}
	}

	public struct AcceptTheGameRequestData {
		public int userId;

		public enum ResultCode {
			OK = 1,
			ERROR = 2
		}
	}
	public struct DisconnectFromTheGameRequestData {
		public enum ResultCode {
			OK = 1,
			ERROR = 2
		}
	}
	public struct ReconnectFromTheGameRequestData {
		public int userId;
		
		public enum ResultCode {
			OK = 1,
			ERROR = 2
		}
	}
	public struct LeaveTheGameRequestData {
		public int userId;
		
		public enum ResultCode {
			OK = 1,
			ERROR = 2
		}
	}
	
	private struct WaitRequestData {
		public int userId;
	}
	
	public struct GameReadyData {
		public GameContacts gameContacts;
		public string gameModeName;
	}
	
#endregion
	
#region Actions

	private bool _searchCanceled;
	
	private async Task SearchingAsync() {
		
		//===
		/*
		if (codeData.code != 1) {
			matchmakingView.SetState(MatchmakingView.State.Passive);
			Debug.LogError("Error EnterToTheLobbyRequestData");
			OnEnterTheGameSearchEvent?.Invoke(false);

			return;
		}
			
		OnEnterTheGameSearchEvent?.Invoke(true);


		matchmakingView.SetState(MatchmakingView.State.GameSearchInProgress);
		*/
		
		
		string receivedData = null;
		
		var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();

		var isDone = false;
			
		var waitRequestData = new WaitRequestData {
			userId = userData._id
		};

		CodeData codeData = default;

		var url = Address + "/lobbies/" + _enterToTheLobbyResultData.data.lobbyId + "/wait";

		while (!isDone && !_isQuit) {
			var webReq = request.postSync(url, 
											JsonUtility.ToJson(waitRequestData), 
											true, 
											20 * 1000);
			while (!webReq.IsDone) {
				await Task.Yield();
				
				if (_searchCanceled || _isQuit) {
					_searchCanceled = false;
					webReq.WebRequest.Abort();

					return;
				}
			}
			
			receivedData = webReq.Result;

			if (receivedData == null) {
				await new WaitForSeconds(1f);

				continue;	
			}

			codeData = JsonUtility.FromJson<CodeData>(receivedData);

			// Lobby is filled
			if (codeData.code == 55) {
				//matchmakingView.SetState(MatchmakingView.State.GameFound);

				_matchmakingLobbyClientData.OnGameFound?.Invoke();
			}
			// Игра создана, пришли ее данные
			else if (codeData.code == 56) {
				isDone = true;
					
				//Состояние ожидания перед канвасом загрузки сцены игры
				//matchmakingView.SetState(MatchmakingView.State.WaitResponseForEnterTheGameSearch);
					
				var gameReadyData = JsonUtility.FromJson<GameReadyData>(receivedData);

				await LoadGame(gameReadyData);
			}
		}
	}
	
	/// <summary>
	/// Отправить запрос для поиска игры
	/// </summary>
	/// <param name="gameModeDataJson">В объекте должны быть поля: playersAmount(int) и gameModeName(string)</param>
	/// <returns></returns>
	public async Task<(EnterToTheLobbyRequestData.ResultData data, EnterToTheLobbyRequestData.ResultCode code)> Play(string gameModeDataJson) {
		var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();
		var enterToTheLobbyRequestData = new EnterToTheLobbyRequestData {
			userId = userData._id,
			gameModeDataJson = gameModeDataJson
			/*
			gameOptions = new GameOptions {
				playersAmount = selectModeViewData.playersAmount,
				mapName = selectModeViewData.mapName
			}
			 */
		};
		var url = Address + "/lobbies";
		var res = await request.post(url, 
									JsonUtility.ToJson(enterToTheLobbyRequestData));

		if (res == null) {
			return (default, EnterToTheLobbyRequestData.ResultCode.ERROR);
		}
		var codeData = JsonUtility.FromJson<CodeData>(res);


		EnterToTheLobbyRequestData.ResultData enterToTheLobbyResultData = default;
			
		if (codeData.code != 1) {
			_matchmakingLobbyClientData.OnSearchCanceled?.Invoke();
		} else {
			enterToTheLobbyResultData = JsonUtility.FromJson<EnterToTheLobbyRequestData.ResultData>(res);
			_enterToTheLobbyResultData = enterToTheLobbyResultData;
			
			SearchingAsync();
			
			_matchmakingLobbyClientData.OnSearchStarted?.Invoke();
		}

		return (enterToTheLobbyResultData, (EnterToTheLobbyRequestData.ResultCode)codeData.code);
		
	}
	
	/// <summary>
	/// Отменить поиск
	/// </summary>
	public async Task<LeaveTheLobbyRequestData.ResultCode> CancelSearch() {
		var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();
			
		var leaveTheLobbyRequestData = new LeaveTheLobbyRequestData {
			userId = userData._id
		};

		var url = Address + "/lobbies/" + _enterToTheLobbyResultData.data.lobbyId + "/leave";
		var res = await request.put(url, 
									JsonUtility.ToJson(leaveTheLobbyRequestData));

		var codeData = JsonUtility.FromJson<CodeData>(res);

		if (codeData.code == 1) {
			_searchCanceled = true;
			_matchmakingLobbyClientData.OnSearchCanceled?.Invoke();
		} else {
			
		}
		
		return (LeaveTheLobbyRequestData.ResultCode)codeData.code;
		
	}
	
	/// <summary>
	/// Принять игру
	/// </summary>
	public async Task<AcceptTheGameRequestData.ResultCode> AcceptGame() {
		var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();

		var sendData = new AcceptTheGameRequestData {
			userId = userData._id
		};
		
		var url = Address + "/lobbies/" + _enterToTheLobbyResultData.data.lobbyId + "/accept";
		var res = await request.post(url, 
									JsonUtility.ToJson(sendData));

		var codeData = JsonUtility.FromJson<CodeData>(res);
		if (codeData.code == 1) {
			_matchmakingLobbyClientData.OnGameAccepted?.Invoke();
		} else {
			
		}
		
		return (AcceptTheGameRequestData.ResultCode)codeData.code;
	}
	
	/// <summary>
	/// Отключиться от игры
	/// </summary>
	public async Task<DisconnectFromTheGameRequestData.ResultCode> DisconnectFromGame() {
		var resultCode = 0;
		try {

			var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();

			var req = World.EntityManager.CreateEntity();
			World.EntityManager.AddComponentData(req, new DisconnectFromTheGameRequest {
				UserId = userData._id
			});
			World.EntityManager.AddComponentData(req, new SendRpcCommandRequestComponent());

			/*
			await new WaitForUpdate();
				
			// Удалить сцену Game
			await SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName("Game"));
				
			DataManager.GetGameObject("Lobby").SetActive(true);
	
			*/

			await new WaitForUpdate();
			_world.QuitUpdate = true;
			await new WaitForUpdate();
			_world.Dispose();

			_matchmakingLobbyClientData.OnGameDisconnected?.Invoke();

			resultCode = 1;
		}
		catch (Exception e) {
			Debug.LogError(e);
		}

		return (DisconnectFromTheGameRequestData.ResultCode)resultCode;
	}
	
	/// <summary>
	/// Переподключиться к игре
	/// </summary>
	public async Task<ReconnectFromTheGameRequestData.ResultCode> ReconnectToGame() {
		var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();

		var sendData = new ReconnectFromTheGameRequestData {
			userId = userData._id
		};
		
		var url = Address + "/games/reconnect";
		var res = await request.put(url, 
									JsonUtility.ToJson(sendData));
			
		var codeData = JsonUtility.FromJson<CodeData>(res);

		if (codeData.code == 1) {
			var gameReadyData = JsonUtility.FromJson<GameReadyData>(res);

			_matchmakingLobbyClientData.OnGameReconnected?.Invoke(gameReadyData);
			_matchmakingLobbyClientData.OnLoadingGame?.Invoke(gameReadyData);

			await LoadGame(gameReadyData, true);
				
			_matchmakingLobbyClientData.OnGameLoaded?.Invoke();
		}
		
		return (ReconnectFromTheGameRequestData.ResultCode)codeData.code;
	}
	
	/// <summary>
	/// Покинуть игру
	/// </summary>
	public async Task<LeaveTheGameRequestData.ResultCode> LeaveGame() {
		var userData = DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>();

		var sendData = new LeaveTheGameRequestData {
			userId = userData._id
		};
		
		var url = Address + "/games/leave";
		var res = await request.put(url, 
									JsonUtility.ToJson(sendData));
			
		var codeData = JsonUtility.FromJson<CodeData>(res);

		_matchmakingLobbyClientData.OnLeftGame?.Invoke();
		return (LeaveTheGameRequestData.ResultCode)LeaveTheGameRequestData.ResultCode.OK;
	}
	
	public Func<string, ushort, string, bool, Task<Unity.Entities.World>> OnCreateGame;
	
	private async Task<Unity.Entities.World> CreateGame(GameReadyData gameReadyData, string host, ushort port, string gameModeName, bool reconnected) {
		
		_matchmakingLobbyClientData.OnGameCreated?.Invoke(gameReadyData);
		_matchmakingLobbyClientData.OnLoadingGame?.Invoke(gameReadyData);
		//== await LoadGame(gameReadyData);
		var world = await OnCreateGame(host, port, gameModeName, reconnected);
		_matchmakingLobbyClientData.OnGameLoaded?.Invoke();
		return world;
	}

	private async Task LoadGame(MatchmakingLobbyClientSystem.GameReadyData gameReadyData, bool reconnected = false) {
		_world = await CreateGame(gameReadyData,
								gameReadyData.gameContacts.host + "",
								gameReadyData.gameContacts.port,
								gameReadyData.gameModeName,
								reconnected
					);
	}
	
#endregion
	
	private HeadServerSettings _headServerSettings;
	private MatchmakingSettings _matchmakingSettings;
	
	public string Address => "http://" + _headServerSettings.Host + ":" + _headServerSettings.Port;


	public override async Task OnCreate() {
		var settings = DataManager.GetComponentData<Settings>();
		_matchmakingSettings = settings.GetComponent<MatchmakingSettings>();
		_headServerSettings = settings.GetComponent<HeadServerSettings>();
		Debug.Log("Address = " + Address);
		
		_matchmakingLobbyClientData = new MatchmakingLobbyClientData();
		base.World.DataManager.AddComponentData(_matchmakingLobbyClientData);
		
		Enabled = false;
	}
	public override void OnUpdate() {
		
	}

	private Func<string, string> GetMapNameByModeNameAction;
	public void SetMapNameByModeNameAction(Func<string, string> action) {
		GetMapNameByModeNameAction += action;
	}
	
	public string GetMapNameByModeName(string modeName) {
		return GetMapNameByModeNameAction?.Invoke(modeName);
	}
	
}
}
