using Nighday.Matchmaking.General;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.Matchmaking.Client {
[DisableAutoCreation]
public class UserAuthorizationInGameClientSystem : ComponentSystem {
	
	private struct NeedAuthorizeTag : IComponentData {}
	private struct AuthSendedTag : IComponentData {}

	private bool _isConnected;
	public bool IsConnected => _isConnected;

	private Entity _connectionEntity;

	public void SetGhostReceiveSystemComponentType<T>() where T : struct, IComponentData {
		RequireSingletonForUpdate<T>();
	}

	/*
	public void Connect() {
		EntityManager.CreateEntity(typeof(NeedAuthorizeTag));
	}
	*/
	
	protected override void OnCreate() {
		RequireSingletonForUpdate<NetworkStreamConnection>();
		RequireSingletonForUpdate<NetworkIdComponent>();
		RequireSingletonForUpdate<UserId>();
		RequireSingletonForUpdate<GameData>();
		RequireSingletonForUpdate<NeedAuthorizeTag>();

		EntityManager.CreateEntity(typeof(NeedAuthorizeTag));
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<AuthSendedTag>()
			.WithAll<NeedAuthorizeTag>()
			.ForEach((Entity entity
					) => {
						var userIdEntity = GetSingletonEntity<UserId>();
						var userId = GetSingleton<UserId>().Value;

						var connectionEntity = GetSingletonEntity<NetworkIdComponent>();

						var reqEntity = EntityManager.CreateEntity();
						EntityManager.AddComponentData(reqEntity, new UserAuthorizationInGameRequest {
							UserId = userId
						});
						EntityManager.AddComponentData(reqEntity, new SendRpcCommandRequestComponent {
							TargetConnection = connectionEntity
						});

						//EntityManager.DestroyEntity(entity);
						EntityManager.AddComponentData(entity, new AuthSendedTag());
						
						EntityManager.AddComponentData(connectionEntity, new UserId {
							Value = userId
						});
						EntityManager.DestroyEntity(userIdEntity);

						//EntityManager.AddComponentData(connectionEntity, new NetworkStreamInGame());
					});
		
		Entities
			.WithNone<SendRpcCommandRequestComponent>()
			.ForEach((Entity entity,
					ref UserAuthorizationInGameRequestResult component,
					ref ReceiveRpcCommandRequestComponent receiveRpcCommandRequestComponent
					) => {

						EntityManager.AddComponentData(receiveRpcCommandRequestComponent.SourceConnection, new NetworkStreamInGame());

						EntityManager.DestroyEntity(entity);
						
						EntityManager.DestroyEntity(GetSingletonEntity<NeedAuthorizeTag>());
					});
	}

	
}

}