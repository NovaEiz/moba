using System;
using System.Collections.Generic;
using Nighday.Application;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Nighday.BuildSettings.Editor {

public class BuildSettingsItem : ScriptableObject {

	public string Name;
	public BuildTargetGroup BuildTargetGroup;
	public BuildTarget BuildTarget;
	
	public BuildType BuildType;
	public bool DevelopmentBuild;

	public int DefinesMaskValue;
	[SerializeField] public UnityEngine.Object Settings;
	
	public bool Enabled;
	public bool Active;
	
	private BuildPlayerOptions _buildPlayerOptions;
	private VisualElement _root;

	[SerializeField] private List<string> _defineArray = new List<string>();

	private string _nameBeforeChanged;

	public Action OnNameChanged;

	private void NameValueChangedCallback(ChangeEvent<string> changeEvent) {
		Name = changeEvent.newValue;
		
		OnNameChanged?.Invoke();
	}
	private void NameFocusInEventCallback(FocusEventBase<FocusInEvent> changeEvent) {
		_nameBeforeChanged = Name;
	}
	private void NameFocusOutEventCallback(FocusEventBase<FocusOutEvent> changeEvent) {
		var nameChanged = Name;
		Name = _nameBeforeChanged;

		Undo.RecordObject(this, "BuildSettingsItem. Name value changed");
		AssetDatabase.StartAssetEditing();
		Name = nameChanged;
		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	
	private void DevelopmentBuildValueChangedCallback(ChangeEvent<bool> changeEvent) {
		Undo.RecordObject(this, "BuildSettingsItem. DevelopmentBuild value changed");

		AssetDatabase.StartAssetEditing();
		
		DevelopmentBuild = changeEvent.newValue;

		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	private void BuildTargetGroupValueChangedCallback(ChangeEvent<Enum> changeEvent) {
		Undo.RecordObject(this, "BuildSettingsItem. BuildTargetGroup value changed");

		AssetDatabase.StartAssetEditing();
		
		BuildTargetGroup = (BuildTargetGroup)changeEvent.newValue;

		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	private void BuildTargetValueChangedCallback(ChangeEvent<Enum> changeEvent) {
		Undo.RecordObject(this, "BuildSettingsItem. BuildTarget value changed");

		AssetDatabase.StartAssetEditing();
		
		BuildTarget = (BuildTarget)changeEvent.newValue;

		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	private void BuildTypeValueChangedCallback(ChangeEvent<Enum> changeEvent) {
		Undo.RecordObject(this, "BuildSettingsItem. BuildType value changed");

		AssetDatabase.StartAssetEditing();
		
		BuildType = (BuildType)changeEvent.newValue;

		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	private void DefinesMaskValueChangedCallback(ChangeEvent<int> changeEvent) {
		Undo.RecordObject(this, "BuildSettingsItem. DefinesMaskValue value changed");

		AssetDatabase.StartAssetEditing();
		
		DefinesMaskValue = changeEvent.newValue;

		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	private void SettingsObjectFieldValueChangedCallback(ChangeEvent<UnityEngine.Object> changeEvent) {
		Undo.RecordObject(this, "BuildSettingsItem. Settings value changed");

		AssetDatabase.StartAssetEditing();
		
		Settings = changeEvent.newValue;

		AssetDatabase.StopAssetEditing();
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
	}
	
	private void OnDestroy() {
		UnregisterValuesChanged();
	}

	public void UnregisterValuesChanged() {
		if (_root == null) {
			return;
		}
		var len = _root.childCount;
		for (int i = 0; i < len; i++) {
			var item = _root[i];
			switch (item.name) {
				case "Name":
					var textFieldLabel = _root[i].GetFirstOfType<TextField>();
					textFieldLabel.value = Name;
					
					textFieldLabel.UnregisterCallback<FocusInEvent>(NameFocusInEventCallback);
					textFieldLabel.UnregisterCallback<FocusOutEvent>(NameFocusOutEventCallback);
					textFieldLabel.UnregisterValueChangedCallback(NameValueChangedCallback);
					break;
				case "DevelopmentBuild":
					var developmentBuildToggle = _root[i].GetFirstOfType<Toggle>();
					developmentBuildToggle.value = DevelopmentBuild;

					developmentBuildToggle.UnregisterValueChangedCallback(DevelopmentBuildValueChangedCallback);
					break;
			}
		}
	}

	public void RegisterValuesChanged() {
		var len = _root.childCount;
		for (int i = 0; i < len; i++) {
			var item = _root[i];
			switch (item.name) {
				case "Name":
					var textFieldLabel = _root[i].GetFirstOfType<TextField>();
					textFieldLabel.value = Name;
					
					textFieldLabel.RegisterCallback<FocusInEvent>(NameFocusInEventCallback);
					textFieldLabel.RegisterCallback<FocusOutEvent>(NameFocusOutEventCallback);
					textFieldLabel.RegisterValueChangedCallback(NameValueChangedCallback);
					break;
				case "DevelopmentBuild":
					var developmentBuildToggle = _root[i].GetFirstOfType<Toggle>();
					developmentBuildToggle.value = DevelopmentBuild;

					developmentBuildToggle.RegisterValueChangedCallback(DevelopmentBuildValueChangedCallback);
					break;
				case "BuildTargetGroup":
					_root.RemoveAt(i);
					var buildTargetGroupEnumField = new EnumField("BuildTargetGroup", BuildTargetGroup);
					buildTargetGroupEnumField.name = item.name;
					
					_root.Insert(i, buildTargetGroupEnumField);

					buildTargetGroupEnumField.RegisterValueChangedCallback(BuildTargetGroupValueChangedCallback);
					break;
				case "BuildTarget":
					_root.RemoveAt(i);
					var buildTargetEnumField = new EnumField("BuildTarget", BuildTarget);
					buildTargetEnumField.name = item.name;

					_root.Insert(i, buildTargetEnumField);
					
					buildTargetEnumField.RegisterValueChangedCallback(BuildTargetValueChangedCallback);
					break;
				case "BuildType":
					_root.RemoveAt(i);
					var buildTypeEnumField = new EnumField("BuildType", BuildType);
					buildTypeEnumField.name = item.name;

					_root.Insert(i, buildTypeEnumField);
					
					buildTypeEnumField.RegisterValueChangedCallback(BuildTypeValueChangedCallback);
					break;
				case "Defines":
					if (_defineArray == null) {
						_defineArray = new List<string>();
					}
					_root.RemoveAt(i);
					var definesMaskField = new MaskField("Defines", _defineArray, DefinesMaskValue);
					definesMaskField.name = item.name;

					_root.Insert(i, definesMaskField);
					
					definesMaskField.RegisterValueChangedCallback(DefinesMaskValueChangedCallback);
					break;
				case "SettingsObjectField":
					_root.RemoveAt(i);
					var settingsObjectField = new ObjectField("Settings");
					settingsObjectField.name = item.name;
					settingsObjectField.objectType = typeof(Settings);
					settingsObjectField.value = Settings;

					_root.Insert(i, settingsObjectField);
					
					settingsObjectField.RegisterValueChangedCallback(SettingsObjectFieldValueChangedCallback);
					break;
			}
		}
	}
	
	public void SetRoot(VisualElement root) {
		_root = root;
	}
	
	public void SetDefines(List<string> defineArray) {
		_defineArray = defineArray;
	}


}
}