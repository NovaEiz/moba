using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Nighday.BuildSettings.Editor {

[UnityEngine.CreateAssetMenu(fileName = "BuildManagerSettings", menuName = "Nighday/BuildSettings/BuildManagerSettings", order = 0)]
public class BuildManagerSettings : ScriptableObject {
	
	[Header("Server Machine")]
	[SerializeField] private string _host;
	[SerializeField] private string _login;
	//194.67.108.149
	
	[Header("Game Server")]
	[SerializeField] private string _directoryPathOnServer;
	[SerializeField] private string _httpGitUrl;
	[SerializeField] private string _localPathToServer;
	[Tooltip(("Example: beta"))]
	[SerializeField] private string _remoteParametersNpmRun;
	[SerializeField] private string _localParametersNpmRun;
	
	[Header("Other")]
	[Space]
	[SerializeField] private string _pathToBuilds;
	
	[Space]
	[SerializeField] private List<string> _defines = new List<string>();
	
	public string Host => _host;
	public string Login => _login;

	public string DirectoryPathOnServer => _directoryPathOnServer;
	public string HttpGitUrl => _httpGitUrl;
	public string LocalPathToServer => _localPathToServer;
	public string RemoteParametersNpmRun => _remoteParametersNpmRun;
	public string LocalParametersNpmRun => _localParametersNpmRun;

	public string PathToBuilds => _pathToBuilds;
	public List<string> Defines => _defines;


		

}
}
