using UnityEditor;
using UnityEngine;

namespace Nighday.BuildSettings.Editor {
public class BuildManagerEditor : EditorWindow {

	private BuildManager _target;
	
	[MenuItem("Nighday/Utilities/Open BuildManager _%#>")]
	public static void ShowWindow() {
		var window = GetWindow<BuildManagerEditor>();
		window.titleContent = new GUIContent("Game Building");
		window.minSize = new Vector2(250, 50);
	}
	
	private void OnEnable() {
		_target = GetGameBuildSettingsManager();
		_target.SetRoot(rootVisualElement);
		_target.OnEnable();
	}

	private void OnDisable() {
		_target.OnDisable();
	}

	private BuildManager GetGameBuildSettingsManager() {
		var manager = BuildManager.GetOrCreate();
		return manager;
	}
	
}
}