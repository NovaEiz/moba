using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.Build.Reporting;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.Events;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Debug = UnityEngine.Debug;

namespace Nighday.BuildSettings.Editor {

[UnityEngine.CreateAssetMenu(fileName = "BuildManager", menuName = "Nighday/BuildSettings/BuildManager", order = 0)]
public class BuildManager : ScriptableObject {
	
	private static string _currentProjectEditorResourcesPath = "Editor/Resources/";
	private static string _gameBuildingManagerPath => _currentProjectEditorResourcesPath + "/GameBuilding";
	private static string _gameBuildingItemsPath => _gameBuildingManagerPath + "/Items";
	
	public const string BasePath = "Packages/com.nighday.buildsettings/";
	public const string ResourcesPath = BasePath + "Editor Resources/";

	[SerializeField] private BuildManagerSettings _buildManagerSettings;
	[SerializeField] private List<BuildSettingsItem> _items = new List<BuildSettingsItem>();
	
	[SerializeField] private int _setGameBuildingItemIndex = -1;
	[SerializeField] private int _setGameBuildingItemIndexPrevious = -1;

	private BuildManagerSettings BuildManagerSettings {
		get {
			if (_buildManagerSettings == null) {
				_buildManagerSettings = GetSettings();
			}
			return _buildManagerSettings;
		}
	}
	
	private VisualElement _root;
	private VisualElement _rootItem;
	private ListView _listViewItems;
	private ListView _listEnablesViewItems;
	private ListView _listEnablesRunBuildViewItems;
	private ListView _listEnablesUploadBuildViewItems;
	
	private ListView _listSetGameBuildingItemToggleViewItems;
	
	private ToolbarToggle _activeItemToolbarToggle;
	[HideInInspector] [SerializeField] private BuildSettingsItem _activeItemBuildSettingsItem;

	[HideInInspector] [SerializeField] private bool _setActiveNewItem;

	private static string GameBuildingManagerPath {
		get {
			var fullPath = Path.Combine(UnityEngine.Application.dataPath, _gameBuildingManagerPath);
			if (!Directory.Exists(fullPath)) {
				Directory.CreateDirectory(fullPath);
			}
			var path = "Assets/" + _gameBuildingManagerPath;
			return path;
		}
	}

	private static string GameBuildingItemsPath {
		get {
			var fullPath = Path.Combine(UnityEngine.Application.dataPath, _gameBuildingItemsPath);
			if (!Directory.Exists(fullPath)) {
				Directory.CreateDirectory(fullPath);
			}
			var path = "Assets/" + _gameBuildingItemsPath;
			return path;
		}
	}
	
	public static BuildManager GetOrCreate() {
		var path = Path.Combine(GameBuildingManagerPath, "BuildManager.asset");
		var manager = AssetDatabase.LoadAssetAtPath<BuildManager>(path);
		if (manager == null) {
			var asset = ScriptableObject.CreateInstance<BuildManager>();
			AssetDatabase.StartAssetEditing();

			AssetDatabase.CreateAsset(asset, path);

			manager = asset;

			manager._buildManagerSettings = manager.BuildManagerSettings;
			
			AssetDatabase.StopAssetEditing();
			EditorUtility.SetDirty(asset);

			AssetDatabase.SaveAssets();
		}

		return manager;
	}

	public void OnEnable() {
		Undo.undoRedoPerformed -= UndoRedoPerformedEvent;
		Undo.undoRedoPerformed += UndoRedoPerformedEvent;
	}

	public void OnDisable() {
		Undo.undoRedoPerformed -= UndoRedoPerformedEvent;
	}

	private void UndoRedoPerformedEvent() {
		//AssetDatabase.SaveAssets();
		RedrawItemList();
	}
	
	private static BuildManagerSettings GetSettings() {
		var path = Path.Combine(GameBuildingManagerPath, "BuildManagerSettings.asset");
		var settings = AssetDatabase.LoadAssetAtPath<BuildManagerSettings>(path);
		if (settings == null) {
			var asset = ScriptableObject.CreateInstance<BuildManagerSettings>();

			AssetDatabase.CreateAsset(asset, path);

			EditorUtility.SetDirty(asset);

			settings = asset;
		}

		return settings;
	}

	private BuildSettingsItem CreateGameBuildingItem() {
		BuildSettingsItem item = null;
		AssetDatabase.StartAssetEditing();
		try {

			var index = _items.Count;
			var path = Path.Combine(GameBuildingItemsPath, "GameBuildingItem_" + index + ".asset");

			item = ScriptableObject.CreateInstance<BuildSettingsItem>();

			Undo.RegisterCreatedObjectUndo(item, "CreateGameBuildingItem 1");

			AssetDatabase.CreateAsset(item, path);
			
			Undo.RegisterCompleteObjectUndo(this, "CreateGameBuildingItem 1");

			//Undo.RegisterCreatedObjectUndo(item, "CreateGameBuildingItem 1");

			//Undo.DestroyObjectImmediate(item);

			item.Name = "GameBuilding default name";
			
			EditorUtility.SetDirty(item);
			
			_items.Add(item);
			EditorUtility.SetDirty(this);


			if (_activeItemToolbarToggle != null && _activeItemBuildSettingsItem != null) {
				_activeItemToolbarToggle.value = false;
				SetActiveItem(_activeItemBuildSettingsItem, false);
			}

			var toolbarToggle = AddItemToListViewItems(item);
			toolbarToggle.value = true;

			SetActiveItem(item);

			_activeItemToolbarToggle = toolbarToggle;
			_activeItemBuildSettingsItem = item;
			
			AssetDatabase.SaveAssets();
		}
		catch (Exception e) {
			Debug.LogError("e = " + e);
		}

		AssetDatabase.StopAssetEditing();
		
		EditorUtility.SetDirty(item);
		EditorUtility.SetDirty(this);

		AssetDatabase.SaveAssets();
		
		return item;
	}


	public void SetRoot(VisualElement root) {
		_root = root;

		var mainVisualElementPrefab = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(ResourcesPath + "UI/Main.uxml");
		mainVisualElementPrefab.CloneTree(_root);
		
		var itemVisualElementPrefab = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>(ResourcesPath + "UI/Templates/Item.uxml");
		var itemVisualElement = itemVisualElementPrefab.CloneTree().hierarchy[0];
		_rootItem = itemVisualElement;
		_root.Q<VisualElement>("ItemInfo").Add(itemVisualElement);
		
		
		ToolbarMenuInitialization();
		ListViewItemsInitialization();
	}

	private void ToolbarMenuInitialization() {
		var toolbarMenuQuery = _root.Query<ToolbarMenu>();
		var toolbarMenuFile = toolbarMenuQuery.AtIndex(0);
		var toolbarMenuBuild = toolbarMenuQuery.AtIndex(1);
		var toolbarMenuSettings = toolbarMenuQuery.AtIndex(2);
		var toolbarMenuServer = toolbarMenuQuery.AtIndex(3);
			
		toolbarMenuFile.menu.AppendAction("Add Item", (dropDownMenuItem) => {
			CreateGameBuildingItem();
		});
			
		toolbarMenuBuild.menu.AppendAction("Build", (dropDownMenuItem) => {
			var len = _items.Count;
			for (int i = 0; i < len; i++) {
				var item = _items[i];
				if (item.Enabled) {
					var buildPlayerOptions = GetBuildPlayerOptions(item);

					SetGameBuildingSettings(item, ref buildPlayerOptions);
					AddressableAssetSettings.BuildPlayerContent();// Строить Addressables надо до того, как измениться список Defies. Потому что после установки defines начинается асинхронная компиляция. А Addressables не собирается пока идет компиляция в редакторе.

					{
						var defines = GetDefines(item);
						SetDefines(defines, item.BuildTargetGroup);
					}

					Build(buildPlayerOptions);

					var index = i;
					
					_setGameBuildingItemIndexPrevious = index;
						
					//== Закончена работа по созданию билда	
				}
			}
			
			//Вернуть настройик, которые установлены как выбранные в редакторе
			if (_setGameBuildingItemIndex != -1) {
				var item2 = _items[_setGameBuildingItemIndex];

				var buildPlayerOptions2 = GetBuildPlayerOptions(item2);
				SetGameBuildingSettings(item2, ref buildPlayerOptions2);

				var defines = GetDefines(item2);
				SetDefines(defines, item2.BuildTargetGroup);
				
				_setGameBuildingItemIndexPrevious = _setGameBuildingItemIndex;
			}

		});


		toolbarMenuSettings.menu.AppendAction("Settings", (dropDownMenuItem) => {
			Selection.activeObject = BuildManagerSettings;
		});
		
		
		toolbarMenuServer.menu.AppendAction("Run Local Server", (dropDownMenuItem) => {
			var localPathToServer = _buildManagerSettings.LocalPathToServer;
			var pathFile = GetPathToBuilds() + "/temporary.scpt";
			
			var process = new Process();

			var command = "";
		
			if (!File.Exists(pathFile)) {
				var file = File.Create(pathFile);
				file.Close();
			}

			var login = BuildManagerSettings.Login;
			var host = BuildManagerSettings.Host;

			var address = login + "@" + host;
			
			//var command3 = "npm run " + BuildManagerSettings.ParametersNpmRun;

			var command1 = "cd " + localPathToServer + " && npm run " + BuildManagerSettings.LocalParametersNpmRun;
		
			var lines = new string[] {
				"tell application \"Terminal\"",
				"do script \"" + command1 + "\"",
				//"do script \"" + commandCopyBuild + "\"",
				"activate",
				"end tell",
			};
			File.WriteAllLines(pathFile, lines);

			process.StartInfo.FileName = "osascript";

			command = pathFile;

			process.StartInfo.Arguments = command;

			process.Start();
		});

		toolbarMenuServer.menu.AppendAction("Run Remote Server", (dropDownMenuItem) => {
			var projectDirectoryOnRemoteServer
				= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);
			
			var dirName = UnityEngine.Application.productName.ToLower() + ".server";

			var projectPath = Path.Combine(projectDirectoryOnRemoteServer, dirName);

			var pathFile = GetPathToBuilds() + "/temporary.scpt";

			var process = new Process();

			var command = "";
		
			if (!File.Exists(pathFile)) {
				var file = File.Create(pathFile);
				file.Close();
			}

			var login = BuildManagerSettings.Login;
			var host = BuildManagerSettings.Host;

			var address = login + "@" + host;
			
			var command3 = "npm run " + BuildManagerSettings.RemoteParametersNpmRun;

			var command1 = "ssh " + address + " 'mkdir -p " + projectPath + " && cd " + projectPath + " && " + command3 + "'";
		
			var lines = new string[] {
				"tell application \"Terminal\"",
				"do script \"" + command1 + "\"",
				//"do script \"" + commandCopyBuild + "\"",
				"activate",
				"end tell",
			};
			File.WriteAllLines(pathFile, lines);

			process.StartInfo.FileName = "osascript";

			command = pathFile;

			process.StartInfo.Arguments = command;

			process.Start();
		});
		
		toolbarMenuServer.menu.AppendAction("Run Remote Server through `pm2`", (dropDownMenuItem) => {
			var projectDirectoryOnRemoteServer
				= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);
			
			var dirName = UnityEngine.Application.productName.ToLower() + ".server";

			var projectPath = Path.Combine(projectDirectoryOnRemoteServer, dirName);

			var pathFile = GetPathToBuilds() + "/temporary.scpt";

			var process = new Process();

			var command = "";
		
			if (!File.Exists(pathFile)) {
				var file = File.Create(pathFile);
				file.Close();
			}

			var login = BuildManagerSettings.Login;
			var host = BuildManagerSettings.Host;

			var address = login + "@" + host;
			var command3 = "pm2 start src/index.js --watch --ignore-watch='node_modules'";
			var command1 = "ssh " + address + " 'mkdir -p " + projectPath + " && cd " + projectPath + " && " + command3 + "'";
		
			var lines = new string[] {
				"tell application \"Terminal\"",
				"do script \"" + command1 + "\"",
				//"do script \"" + commandCopyBuild + "\"",
				"activate",
				"end tell",
			};
			File.WriteAllLines(pathFile, lines);

			process.StartInfo.FileName = "osascript";

			command = pathFile;

			process.StartInfo.Arguments = command;

			process.Start();
		});
		toolbarMenuServer.menu.AppendAction("Kill process Remote Server through `pm2`", (dropDownMenuItem) => {
			var projectDirectoryOnRemoteServer
				= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);
			
			var dirName = UnityEngine.Application.productName.ToLower() + ".server";

			var projectPath = Path.Combine(projectDirectoryOnRemoteServer, dirName);

			var pathFile = GetPathToBuilds() + "/temporary.scpt";

			var process = new Process();

			var command = "";
		
			if (!File.Exists(pathFile)) {
				var file = File.Create(pathFile);
				file.Close();
			}

			var login = BuildManagerSettings.Login;
			var host = BuildManagerSettings.Host;

			var address = login + "@" + host;
			var command3 = "pm2 delete src/index.js";

			var command1 = "ssh " + address + " 'mkdir -p " + projectPath + " && cd " + projectPath + " && " + command3 + "'";
		
			var lines = new string[] {
				"tell application \"Terminal\"",
				"do script \"" + command1 + "\"",
				//"do script \"" + commandCopyBuild + "\"",
				"activate",
				"end tell",
			};
			File.WriteAllLines(pathFile, lines);

			process.StartInfo.FileName = "osascript";

			command = pathFile;

			process.StartInfo.Arguments = command;

			process.Start();
		});

		toolbarMenuServer.menu.AppendAction("Clone git Server on Remote Server", (dropDownMenuItem) => {
			var projectDirectoryOnRemoteServer
				= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);
			
			var pathFile = GetPathToBuilds() + "/temporary.scpt";

			var process = new Process();

			var command = "";
		
			if (!File.Exists(pathFile)) {
				var file = File.Create(pathFile);
				file.Close();
			}

			var login = BuildManagerSettings.Login;
			var host = BuildManagerSettings.Host;

			var address = login + "@" + host;

			var repName = Path.GetFileNameWithoutExtension(BuildManagerSettings.HttpGitUrl);
			
			var command3 = "git clone " + BuildManagerSettings.HttpGitUrl;
			var command4 = "cd " + repName;
			var command5 = "npm install";
			
			var command1 = "ssh " + address + " 'mkdir -p " + projectDirectoryOnRemoteServer + " && cd " + projectDirectoryOnRemoteServer + " && " + command3 + " && " + command4 + " && " + command5 + "'";
		
			var lines = new string[] {
				"tell application \"Terminal\"",
				"do script \"" + command1 + "\"",
				//"do script \"" + commandCopyBuild + "\"",
				"activate",
				"end tell",
			};
			File.WriteAllLines(pathFile, lines);

			process.StartInfo.FileName = "osascript";

			command = pathFile;

			process.StartInfo.Arguments = command;

			process.Start();
			
		});

		toolbarMenuServer.menu.AppendAction("Pull git Server on Remote Server", (dropDownMenuItem) => {
			var projectDirectoryOnRemoteServer
				= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);

			var dirName = UnityEngine.Application.productName.ToLower() + ".server";

			var projectPath = Path.Combine(projectDirectoryOnRemoteServer, dirName);

			var pathFile = GetPathToBuilds() + "/temporary.scpt";

			var process = new Process();

			var command = "";
		
			if (!File.Exists(pathFile)) {
				var file = File.Create(pathFile);
				file.Close();
			}

			var login = BuildManagerSettings.Login;
			var host = BuildManagerSettings.Host;

			var address = login + "@" + host;
			
			var command2 = "git pull";

			var command1 = "ssh " + address + " 'cd " + projectPath + " && " + command2 + "'";
		
			var lines = new string[] {
				"tell application \"Terminal\"",
				"do script \"" + command1 + "\"",
				//"do script \"" + commandCopyBuild + "\"",
				"activate",
				"end tell",
			};
			File.WriteAllLines(pathFile, lines);

			process.StartInfo.FileName = "osascript";

			command = pathFile;

			process.StartInfo.Arguments = command;

			process.Start();
			
		});

	}

	private IEnumerator BuildPlayerContentAddressable(Action callback) {
		while (EditorApplication.isUpdating || EditorApplication.isCompiling) {
			yield return null;
		}

		callback();
	}

	private void ListViewItemsInitialization() {
		if (_root == null) {
			return;
		}

		var listViews = _root.Query<ListView>();
		
		var listEnablesUploadBuildViewItems = listViews.AtIndex(0);
		_listEnablesUploadBuildViewItems = listEnablesUploadBuildViewItems;
		
		var listEnablesRunBuildViewItems = listViews.AtIndex(1);
		_listEnablesRunBuildViewItems = listEnablesRunBuildViewItems;
		
		var listEnablesViewItems = listViews.AtIndex(2);
		_listEnablesViewItems = listEnablesViewItems;
		
		var listSetGameBuildingItemToggleViewItems = listViews.AtIndex(3);
		_listSetGameBuildingItemToggleViewItems = listSetGameBuildingItemToggleViewItems;
		_listSetGameBuildingItemToggleViewItems.Clear();

		var listViewItems = listViews.AtIndex(4);
		_listViewItems = listViewItems;
		_listViewItems.Clear();
		_listEnablesViewItems.Clear();
		_listEnablesRunBuildViewItems.Clear();
		_listEnablesUploadBuildViewItems.Clear();
		
		var len = _items.Count;
		for (int i = 0; i < len; i++) {
			var item = _items[i];
			if (item == null) {
				return;
			}
			/*if (item == null) {
				_items.RemoveAt(i);
				i--;
				len--;
				continue;
			}*/
			AddItemToListViewItems(item);
		}
	}
	

	private void UploadBuildToServer(BuildSettingsItem item) {
		var buildPlayerOptions = CreateBuildPlayerOptions(item);
		var fromPath = buildPlayerOptions.locationPathName;
		var toPathOnServer = Path.GetDirectoryName(GetBuildLocalPath(item));
		if (item.BuildTarget == BuildTarget.StandaloneLinux64) {
			fromPath = Path.GetDirectoryName(fromPath);
			toPathOnServer = Path.GetDirectoryName(toPathOnServer);
		}

		var projectDirectoryOnRemoteServer
			= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);

		toPathOnServer = Path.Combine(projectDirectoryOnRemoteServer, toPathOnServer);
		
		fromPath = fromPath.Replace(" ", "\\\\ ");
		fromPath = fromPath.Replace(")", "\\\\)");
		fromPath = fromPath.Replace("(", "\\\\(");
		//var split = toPathOnServer.Split('/').ToList();
		//split.RemoveAt(split.Count-1);

		//toPathOnServer = string.Join("/", split);
		
		var pathFile = GetPathToBuilds() + "/temporary.scpt";

		var process = new Process();

		var command = "";
		
		if (!File.Exists(pathFile)) {
			var file = File.Create(pathFile);
			file.Close();
		}

		var login = BuildManagerSettings.Login;
		var host = BuildManagerSettings.Host;

		var address = login + "@" + host;
			
		var commandCreateFolder = "ssh " + address + " 'mkdir -p " + toPathOnServer + "'";
		//var command = "scp -r " + pathFrom + " root@" + _addressServer + ":" + pathTo;
		var commandCopyBuild = "scp -r " + fromPath + " " + address + ":" + toPathOnServer;
		
		var lines = new string[] {
			"tell application \"Terminal\"",
			"do script \"" + commandCreateFolder + " && " + commandCopyBuild + "\"",
			//"do script \"" + commandCopyBuild + "\"",
			"activate",
			"end tell",
		};
		File.WriteAllLines(pathFile, lines);

		process.StartInfo.FileName = "osascript";

		command = pathFile;

		process.StartInfo.Arguments = command;

		//var path = Path.Combine(dir, "Terminal.app/Contents/MacOS/Terminal");
		//var path = Path.Combine(dir, "Terminal");
		//process.StartInfo.FileName = "/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal";
		process.Start();
	}

	private string GetPathToBuilds() {
		var pathToBuilds = BuildManagerSettings.PathToBuilds;
		if (string.IsNullOrEmpty(pathToBuilds)) {
			var path = UnityEngine.Application.dataPath;
			path = path.Substring(0, path.Length - ("Assets".Length + 1));
			pathToBuilds = Path.Combine(path, "Builds");
		}

		return pathToBuilds;
	}
	
	private void RunBuild(BuildSettingsItem item) {
		var buildPath = GetBuildPath(item);
		var pathFile = GetPathToBuilds() + "/temporary.scpt";

		buildPath = buildPath.Replace(" ", "\\\\ ");
		buildPath = buildPath.Replace(")", "\\\\)");
		buildPath = buildPath.Replace("(", "\\\\(");
		if (item.BuildTarget == BuildTarget.StandaloneOSX) {
			buildPath += "/Contents/MacOS/" + UnityEngine.Application.productName;
		}

		var process = new Process();
		//process.StartInfo.UseShellExecute = true;
		
		//command = "-e 'tell application \"Terminal\" to do script \"" + command + "\" '";

		var command = "";
		var commandTerminal = buildPath;
		
		command = "-e 'tell application \"Terminal\"' ";
		
		command += "-e 'set newVar to \"" + commandTerminal + "\"' ";
		command += "-e 'activate' ";
		//command += "-e 'do shell script \"" + commandCreateFolder + "\"' ";
		//command += "-e 'do script \"" + commandSendDirectories + "\"' ";
		//command += "-e 'end tell' > /dev/null"; //  > /dev/null - для закрытия консоли
		//command += "-e 'do script \"\" & newVar & \"\"' ";
		command += "-e \"do script newVar\" ";

		command += "-e 'end tell'"; //  > /dev/null - для закрытия консоли

		if (!File.Exists(pathFile)) {
			var file = File.Create(pathFile);
			file.Close();
		}
		
		var lines = new string[] {
			"tell application \"Terminal\"",

			/*
			"if application \"Terminal\" is running then",
			"tell application \"Terminal\"",
			"do script \"" + buildPath + "\"",
			"activate",
			"	end tell",
			"else",
			"tell application \"Terminal\"",
			"activate",
			"	end tell",
			"end if",
			 */
			
			"do script \"" + buildPath + "\"",

			"activate",
			//"do shell script \"" + buildPath + "\"",
			
			//"set T to do script \"echo $$\" --> btw, there's the pid number of the window's sub-shell",
			//"set W to the id of window 1 where its tab 1 = T",
			//"close window id W",
			//"end tell"
			"end tell",
			//"tell application \"Terminal\" to close"
			//"tell application \"Terminal\" to quit",

		};
		//lines = new string[] {
		//	"tell application \"" + buildPath + "\" to activate",
		//};
		File.WriteAllLines(pathFile, lines);

		process.StartInfo.FileName = "osascript";

		command = pathFile;

		process.StartInfo.Arguments = command;

		//var path = Path.Combine(dir, "Terminal.app/Contents/MacOS/Terminal");
		//var path = Path.Combine(dir, "Terminal");
		//process.StartInfo.FileName = "/Applications/Utilities/Terminal.app/Contents/MacOS/Terminal";
		process.Start();
	}
	
	private ToolbarToggle AddItemToListViewItems(BuildSettingsItem item) {
		var toolbarToggle = new ToolbarToggle {
			label = item.Name
		};
		toolbarToggle.AddToClassList("testClass");
		
		toolbarToggle.RegisterCallback<FocusInEvent>(e => {
			toolbarToggle.value = true;

			_activeItemToolbarToggle.value = false;
			SetActiveItem(_activeItemBuildSettingsItem , false);

			_activeItemToolbarToggle = toolbarToggle;

			SetActiveItem(item);
		});
		toolbarToggle.RegisterCallback<FocusOutEvent>(e => {
		});

		_listViewItems.Add(toolbarToggle);

		if (_activeItemBuildSettingsItem == null) {
			if (_listViewItems.childCount == 1) {
				toolbarToggle.value = true;

				_activeItemToolbarToggle = toolbarToggle;
				SetActiveItem(item);
			}
		} else if (_activeItemBuildSettingsItem == item) {
			toolbarToggle.value = true;

			_activeItemToolbarToggle = toolbarToggle;
			SetActiveItem(item);
		}

		item.OnNameChanged = () => {
			toolbarToggle.label = item.Name;
		};
		
		
		
			
		//===
		
		var buttonUploadBuild = new Button {
			
		};
		buttonUploadBuild.AddToClassList("testClass");

		buttonUploadBuild.text = "Upload";
		_listEnablesUploadBuildViewItems.Add(buttonUploadBuild);
		
		buttonUploadBuild.clicked += () => {
			UploadBuildToServer(item);
		};


		if (item.BuildTarget == BuildTarget.StandaloneLinux64) {
			if (!File.Exists(GetBuildPath(item))) {
				buttonUploadBuild.visible = false;
			}
		} else
		if (item.BuildTarget == BuildTarget.StandaloneOSX) {
			buttonUploadBuild.visible = false;
		} else {
			buttonUploadBuild.visible = false;
		}
		
		//===
			
		//===
		
		var buttonRunBuild = new Button {
			
		};
		//buttonRunBuild.AddToClassList("testClass");
		
		var parent = new VisualElement();
		parent.AddToClassList("testClass");
		parent.AddToClassList("classInRow");
		parent.Add(buttonRunBuild);

		_listEnablesRunBuildViewItems.Add(parent);
		
		if (item.BuildTarget == BuildTarget.Android) {
			if (!File.Exists(GetBuildPath(item))) {
				parent.visible = false;
			} else {
				buttonRunBuild.text = "Run on Remote Device";
			
				buttonRunBuild.clicked += () => {
					
				};
			}
		} else 
		if (item.BuildTarget == BuildTarget.StandaloneLinux64) {
			if (!File.Exists(GetBuildPath(item))) {
				parent.visible = false;
			} else {
				buttonRunBuild.text = "Run on Server";

				buttonRunBuild.clicked += () => {
					var projectDirectoryOnRemoteServer
						= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);

					var dirName = UnityEngine.Application.productName.ToLower() + ".server";

					var projectPath = projectDirectoryOnRemoteServer;//Path.Combine(projectDirectoryOnRemoteServer, dirName);
					
					var buildLocalPath = GetBuildLocalPath(item);
					var pathFile = GetPathToBuilds() + "/temporary.scpt";

					buildLocalPath = buildLocalPath.Replace(" ", "\\\\ ");
					buildLocalPath = buildLocalPath.Replace(")", "\\\\)");
					buildLocalPath = buildLocalPath.Replace("(", "\\\\(");
					//if (item.BuildTarget == BuildTarget.StandaloneOSX) {

					//	buildLocalPath += "/Contents/MacOS/TinyTanks";
					//}

					var process = new Process();
					var command = "";

					if (!File.Exists(pathFile)) {
						var file = File.Create(pathFile);
						file.Close();
					}

					var buildPathOnServer = Path.Combine(projectPath, buildLocalPath);

					var login = BuildManagerSettings.Login;
					var host = BuildManagerSettings.Host;
					var address = login + "@" + host;
			
					var command2 = "git pull";

					var command1 = "ssh " + address + " '" + buildPathOnServer + "'";
		
					var lines = new string[] {
						"tell application \"Terminal\"",
						"do script \"" + command1 + "\"",
						//"do script \"" + commandCopyBuild + "\"",
						"activate",
						"end tell",
					};
					
					File.WriteAllLines(pathFile, lines);

					process.StartInfo.FileName = "osascript";

					command = pathFile;

					process.StartInfo.Arguments = command;

					process.Start();
				};
				
				
				var buttonRunBuildOnRemoteHostThroughPm2 = new Button {
			
				};

				var buttonKillBuildOnRemoteHostThroughPm2 = new Button {
			
				};
				parent.Add(buttonRunBuildOnRemoteHostThroughPm2);
				parent.Add(buttonKillBuildOnRemoteHostThroughPm2);

				buttonRunBuildOnRemoteHostThroughPm2.clicked += () => {
					var projectDirectoryOnRemoteServer
						= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);

					var dirName = UnityEngine.Application.productName.ToLower() + ".server";

					var projectPath = projectDirectoryOnRemoteServer;//Path.Combine(projectDirectoryOnRemoteServer, dirName);
					
					var buildLocalPath = GetBuildLocalPath(item);
					var pathFile = GetPathToBuilds() + "/temporary.scpt";

					buildLocalPath = buildLocalPath.Replace(" ", "\\\\ ");
					buildLocalPath = buildLocalPath.Replace(")", "\\\\)");
					buildLocalPath = buildLocalPath.Replace("(", "\\\\(");
					//if (item.BuildTarget == BuildTarget.StandaloneOSX) {

					//	buildLocalPath += "/Contents/MacOS/TinyTanks";
					//}

					var process = new Process();
					var command = "";

					if (!File.Exists(pathFile)) {
						var file = File.Create(pathFile);
						file.Close();
					}

					var buildPathOnServer = Path.Combine(projectPath, buildLocalPath);

					var login = BuildManagerSettings.Login;
					var host = BuildManagerSettings.Host;
					var address = login + "@" + host;
			
					var command2 = "git pull";

					var command1 = "ssh " + address + " 'pm2 start " + buildPathOnServer + "'";
		
					var lines = new string[] {
						"tell application \"Terminal\"",
						"do script \"" + command1 + "\"",
						//"do script \"" + commandCopyBuild + "\"",
						"activate",
						"end tell",
					};
					
					File.WriteAllLines(pathFile, lines);

					process.StartInfo.FileName = "osascript";

					command = pathFile;

					process.StartInfo.Arguments = command;

					process.Start();
				};
				buttonKillBuildOnRemoteHostThroughPm2.clicked += () => {
					var projectDirectoryOnRemoteServer
						= Path.Combine(BuildManagerSettings.DirectoryPathOnServer, UnityEngine.Application.productName);

					var dirName = UnityEngine.Application.productName.ToLower() + ".server";

					var projectPath = projectDirectoryOnRemoteServer;//Path.Combine(projectDirectoryOnRemoteServer, dirName);
					
					var buildLocalPath = GetBuildLocalPath(item);
					var pathFile = GetPathToBuilds() + "/temporary.scpt";

					buildLocalPath = buildLocalPath.Replace(" ", "\\\\ ");
					buildLocalPath = buildLocalPath.Replace(")", "\\\\)");
					buildLocalPath = buildLocalPath.Replace("(", "\\\\(");
					//if (item.BuildTarget == BuildTarget.StandaloneOSX) {

					//	buildLocalPath += "/Contents/MacOS/TinyTanks";
					//}

					var process = new Process();
					var command = "";

					if (!File.Exists(pathFile)) {
						var file = File.Create(pathFile);
						file.Close();
					}

					var buildPathOnServer = Path.Combine(projectPath, buildLocalPath);

					var login = BuildManagerSettings.Login;
					var host = BuildManagerSettings.Host;
					var address = login + "@" + host;
			
					var command2 = "git pull";

					var command1 = "ssh " + address + " 'pm2 delete " + buildPathOnServer + "'";
		
					var lines = new string[] {
						"tell application \"Terminal\"",
						"do script \"" + command1 + "\"",
						//"do script \"" + commandCopyBuild + "\"",
						"activate",
						"end tell",
					};
					
					File.WriteAllLines(pathFile, lines);

					process.StartInfo.FileName = "osascript";

					command = pathFile;

					process.StartInfo.Arguments = command;

					process.Start();
				};

			}
		} else 
		if (item.BuildTarget == BuildTarget.StandaloneOSX) {
			if (!Directory.Exists(GetBuildPath(item))) {
				parent.visible = false;
			} else {
				buttonRunBuild.text = "Run on Mac";

				buttonRunBuild.clicked += () => {
					RunBuild(item);
				};
			}
		}

		var toggle = new Toggle {
			value = item.Enabled
		};
		_listEnablesViewItems.Add(toggle);

		toggle.RegisterValueChangedCallback((changeEvent) => {
			Undo.RecordObject(item, "BuildSettingsItem. Enabled value changed");

			AssetDatabase.StartAssetEditing();
		
			item.Enabled = changeEvent.newValue;

			AssetDatabase.StopAssetEditing();
			EditorUtility.SetDirty(item);

			AssetDatabase.SaveAssets();
		});
		
		//===
		
		var setGameBuildingItemToggle = new Toggle {
			value = item.Active
		};

		var index = _listSetGameBuildingItemToggleViewItems.childCount;

		setGameBuildingItemToggle.RegisterValueChangedCallback((changeEvent) => {
			if (_setActiveNewItem) {
				_setActiveNewItem = false;
				return;
			} else 
			if (!changeEvent.newValue) {
				_setGameBuildingItemIndex = -1;
				return;
			}
			Undo.RecordObject(this, "GameBuildingManager. Set BuildSettingsItem in editor");

			AssetDatabase.StartAssetEditing();

			if (_setGameBuildingItemIndex != -1) {
				_setActiveNewItem = true;

				((Toggle)_listSetGameBuildingItemToggleViewItems[_setGameBuildingItemIndex]).value = false;

				var previousActiveItem = _items[_setGameBuildingItemIndex];
				
				previousActiveItem.Active = false;
				
				EditorUtility.SetDirty(previousActiveItem);
			}
			
			_setGameBuildingItemIndex = index;
			setGameBuildingItemToggle.value = true;

			item.Active = true;

			var needSetGameBuildingSettings = _setGameBuildingItemIndexPrevious != _setGameBuildingItemIndex;
			_setGameBuildingItemIndexPrevious = _setGameBuildingItemIndex;
			
			AssetDatabase.StopAssetEditing();
			EditorUtility.SetDirty(this);
			
			EditorUtility.SetDirty(item);

			AssetDatabase.SaveAssets();

			if (needSetGameBuildingSettings) {
				var buildPlayerOptions = GetBuildPlayerOptions(item);
				
				var defines = GetDefines(item);
				SetDefines(defines, item.BuildTargetGroup);
				
				SetGameBuildingSettings(item, ref buildPlayerOptions);
			}
		});

		setGameBuildingItemToggle.value = _setGameBuildingItemIndex == index;
		
		_listSetGameBuildingItemToggleViewItems.Add(setGameBuildingItemToggle);

		return toolbarToggle;
	}
	
	private void SetActiveItem(BuildSettingsItem item, bool state = true) {
		if (state) {
			_activeItemBuildSettingsItem = item;
			item.SetRoot(_rootItem);
			item.SetDefines(BuildManagerSettings.Defines);
			item.RegisterValuesChanged();
		} else {
			item.UnregisterValuesChanged();
		}
	}
	
	private void RedrawItemList() {
		ListViewItemsInitialization();
	}

#region For Build

	private void CopyGameSettingsToResources(BuildSettingsItem item) {
		if (item.Settings == null) {
			Debug.LogError("Нет настроек в BuildSettingsItem = " + item);
			return;
		}
		
		var assetPath = UnityEditor.AssetDatabase.GetAssetPath(item.Settings);

		var settingsGroup = CreateSettingsGroup();
		SetSettingsToAddressables(assetPath, settingsGroup);
	}
	
	private static AddressableAssetGroup CreateSettingsGroup() {
		var groupName = "Settings";
		AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;
		var group = addressableAssetSettings.FindGroup(groupName);
		if (group == null) {
			group = addressableAssetSettings.CreateGroup(
				groupName,
				false,
				false,
				false,
				null
			);
		}

		return group;
	}
	
	private static void SetSettingsToAddressables(string assetPath, AddressableAssetGroup group) {
		var assetName = "Settings";
		var guid = AssetDatabase.AssetPathToGUID(assetPath);
		AddressableAssetSettings addressableAssetSettings = AddressableAssetSettingsDefaultObject.Settings;

		var entries = group.entries;
		foreach (var item in entries) {
			if (item.address == assetName) {
				group.RemoveAssetEntry(item);
				break;
			}
		}
		
		var asset = addressableAssetSettings.CreateOrMoveEntry(guid, group);
		asset.address = assetName;
		EditorUtility.SetDirty(group);
	}
	

	private void SetGameBuildingSettings(BuildSettingsItem item, ref BuildPlayerOptions buildPlayerOptions) {
		CopyGameSettingsToResources(item);
		
		EditorUserBuildSettings.SwitchActiveBuildTarget(
			buildPlayerOptions.targetGroup,
			buildPlayerOptions.target);

		SetScenes(ref buildPlayerOptions);
	}

	private List<string> GetScenesList() {
		var scenesList = new List<string>();
		var scenes = EditorBuildSettings.scenes;
		foreach (var scene in scenes) {
			scenesList.Add(scene.path);
		}

		return scenesList;
	}

	private void SetScenes(ref BuildPlayerOptions buildPlayerOptions) {
		var scenesList = GetScenesList();
		
		var scenes = EditorBuildSettings.scenes;
		foreach (var scene in scenes) {
			var res = false;
			foreach (var item in scenesList) {
				if (item == scene.path) {
					res = true;
					break;
				}
			}
			scene.enabled = res;
		}
		EditorBuildSettings.scenes = scenes;

		buildPlayerOptions.scenes = scenesList.ToArray();
	}

	private void Build(BuildPlayerOptions buildPlayerOptions) {
		BuildReport  report  = BuildPipeline.BuildPlayer(buildPlayerOptions);
		BuildSummary summary = report.summary;

		if (summary.result == BuildResult.Succeeded)
		{
			Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
		}

		if (summary.result == BuildResult.Failed)
		{
			Debug.Log("Build failed");
		}
	}
	private BuildPlayerOptions GetBuildPlayerOptions(BuildSettingsItem buildSettingsItem) {
		var buildPlayerOptions = CreateBuildPlayerOptions(buildSettingsItem);

		return buildPlayerOptions;
	}
	
	private BuildPlayerOptions CreateBuildPlayerOptions(BuildSettingsItem item) {
		var buildTargetGroup = item.BuildTargetGroup;
		var buildTarget = item.BuildTarget;
		var developmentBuild = item.DevelopmentBuild;
		var buildType = item.BuildType;
		
		BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
		buildPlayerOptions.options = BuildOptions.None;

		if (developmentBuild) {
			buildPlayerOptions.options |= BuildOptions.Development;
		}

		if (buildType == BuildType.ClientAndServer) {
			
		} else
		if (buildType == BuildType.Server) {
			buildPlayerOptions.options |= BuildOptions.EnableHeadlessMode;
		} else
		if (buildType == BuildType.Client) {
			
		}
		
		buildPlayerOptions.locationPathName = GetBuildPath(item);
		
		buildPlayerOptions.targetGroup  = buildTargetGroup;
		buildPlayerOptions.target  = buildTarget;

		return buildPlayerOptions;
	}

	private string GetProductName(BuildSettingsItem item) {
		var buildType = item.BuildType;

		var productName = UnityEngine.Application.productName;
		
		if (buildType == BuildType.ClientAndServer) {
			
		} else
		if (buildType == BuildType.Server) {
			productName += "_GameServer";
		} else
		if (buildType == BuildType.Client) {
			
		}

		return productName;
	}

	private string GetBuildLocalPath(BuildSettingsItem item) {
		var buildTarget = item.BuildTarget;
		var productName = GetProductName(item);
		var infoInNameBuild = item.Name;
			
		var version = PlayerSettings.bundleVersion;

		var localPath = version;

		var ext = "";
		
		var plusInfo = " (" + infoInNameBuild + ")";

		switch (buildTarget) {
			case BuildTarget.StandaloneOSX:
				productName += " " + version + plusInfo;
					
				ext = ".app";
				break;
			case BuildTarget.StandaloneLinux64:

				ext = "/" + productName;
				//ext += ".x86_64";
				break;
			case BuildTarget.Android:
				productName += " " + version + plusInfo;
					
				ext = ".apk";
				break;
		}
		
		localPath = Path.Combine(localPath, productName);
		
		localPath += ext;

		return localPath;
	}

	private string GetBuildPath(BuildSettingsItem item) {
		var buildLocalPath = GetBuildLocalPath(item);
				
		var resultPath = Path.Combine(GetPathToBuilds(), buildLocalPath);

		return resultPath;
	}

	private string GetDefines(BuildSettingsItem buildSettingsItem) {
		var buildType = buildSettingsItem.BuildType;

		var defines = "";

		if (buildType == BuildType.ClientAndServer) {
		
		} else
		if (buildType == BuildType.Server) {
			//defines += "UNITY_SERVER;";
		} else
		if (buildType == BuildType.Client) {
			defines += "UNITY_CLIENT;";
		}
		

		return defines;
	}
	private void SetDefines(string defines, BuildTargetGroup buildTargetGroup) {
		PlayerSettings.SetScriptingDefineSymbolsForGroup(
			buildTargetGroup,
			defines);
	}
	
	

#endregion
	
}
}
