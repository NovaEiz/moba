namespace Nighday.BuildSettings.Editor {

public enum BuildType {
	ClientAndServer = (1 << 0),
	Client  = (1 << 1),
	Server = (1 << 2)
}

}