using System;
using Unity.Entities;
using UnityEngine;

namespace Nighday.General.EntitiesExtentions {

public class WaitGameObjectIsConvertFinishedAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public Action<Entity> OnFinished;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		OnFinished?.Invoke(entity);
	}

}

}