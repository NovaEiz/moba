using System;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Serialization;

namespace Nighday.General.EntitiesExtentions {

public class ConvertToEntityInWorld : ConvertToEntity {

	[SerializeField] private bool _autoConvert;
	[SerializeField] private bool _withAutoConvertChildren;
	
	public World World;

	private Action _onUpdate;
	
	// Необходимая плашка! Awake method
	private void Awake() {}

	private void Update() {
		if (_autoConvert) {
			if (World == null) {
#if UNITY_EDITOR
				Debug.Log("Не установлен мир для конвертации! name = " + name);
#endif
			} else {
				AutoConvert();
			}
		}
	}

	private void ConvertGameObject() {
		_autoConvert = false;

		try {
			var waitGameObjectIsConvertFinishedAuthoring =
				gameObject.AddComponent<WaitGameObjectIsConvertFinishedAuthoring>();
			waitGameObjectIsConvertFinishedAuthoring.OnFinished = (entity) => {

				Destroy(waitGameObjectIsConvertFinishedAuthoring);
				Destroy(this);

				OnConvertFinished?.Invoke(entity);
			};

			//TODO: не завершается конвертация при первом входе в игру

			var system = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<ConvertToEntitySystem>();
			system.AddToBeConverted(World, this);
		}
		catch (Exception e) {
			Debug.Log("Ошибка. Наверно потому что в родителе _withAutoConvertChildren == true, а в этом нет компонента WaitGameObjectIsConvertFinishedAuthoringж; name = " + name);
		}
	}

	private void SetWorldToChildren(bool withConvert = false) {
		var convertToEntityInWorldInChilds = transform.GetComponentsInChildren<ConvertToEntityInWorld>();
		foreach (var convertToEntityInWorldInChild in convertToEntityInWorldInChilds) {
			convertToEntityInWorldInChild.World = World;
			if (withConvert) {
				convertToEntityInWorldInChild.ConvertGameObject();
			}
		}
	}

	public void AutoConvert() {
		if (_withAutoConvertChildren) {
			OnDestroyed += () => {
				SetWorldToChildren(true);
			};
		}
		
		ConvertGameObject();
	}
	public void Convert() {
		ConvertGameObject();
		
		SetWorldToChildren();
	}

	private void OnDestroy() {
		OnDestroyed?.Invoke();
	}

	public Action<Entity> OnConvertFinished;
	public Action OnDestroyed;

}

}