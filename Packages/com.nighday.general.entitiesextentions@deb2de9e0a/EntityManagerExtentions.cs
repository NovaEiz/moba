using Unity.Collections;
using Unity.Entities;

namespace Nighday.General.EntitiesExtentions {

public static class EntityManagerExtentions {

	public static void CopyBufferElementData<T>(this EntityManager EntityManager, Entity from, Entity to) 
		where T : struct, IBufferElementData
	{
		if (EntityManager.HasComponent<T>(from)) {
			if (EntityManager.HasComponent<T>(to)) {
				var buffer = EntityManager.AddBuffer<T>(to);

				var array = EntityManager.GetBuffer<T>(from).ToNativeArray(Allocator.TempJob);
				buffer.AddRange(array);
				array.Dispose();
			} else {
				var buffer = EntityManager.GetBuffer<T>(to);

				var array = EntityManager.GetBuffer<T>(from).ToNativeArray(Allocator.TempJob);
				buffer.Clear();
				buffer.AddRange(array);
				array.Dispose();
			}
		}
	}
	public static void CopySharedComponentData<T>(this EntityManager EntityManager, Entity from, Entity to) 
		where T : struct, ISharedComponentData
	{
		if (EntityManager.HasComponent<T>(from)) {
			if (EntityManager.HasComponent<T>(to)) {
				EntityManager.SetSharedComponentData(
					to,
					EntityManager.GetSharedComponentData<T>(from)
				);
			} else {
				EntityManager.AddSharedComponentData(
					to,
					EntityManager.GetSharedComponentData<T>(from)
				);
			}
		}
	}
	public static void CopyComponentData<T>(this EntityManager EntityManager, Entity from, Entity to) 
		where T : struct, IComponentData
	{
		if (EntityManager.HasComponent<T>(from)) {
			if (EntityManager.HasComponent<T>(to)) {
				EntityManager.SetComponentData(
					to,
					EntityManager.GetComponentData<T>(from)
				);
			} else {
				EntityManager.AddComponentData(
					to,
					EntityManager.GetComponentData<T>(from)
				);
			}
		}
	}
	public static void CopyComponentDataIfNotExists<T>(this EntityManager EntityManager, Entity from, Entity to) 
		where T : struct, IComponentData
	{
		if (EntityManager.HasComponent<T>(from)) {
			if (EntityManager.HasComponent<T>(to)) {
				return;
			} else {
				EntityManager.AddComponentData(
					to,
					EntityManager.GetComponentData<T>(from)
				);
			}
		}
	}
	
}
}

