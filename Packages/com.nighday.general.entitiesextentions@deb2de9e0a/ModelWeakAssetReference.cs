using Nighday.General.Other;
using Unity.Entities;
using UnityEngine.AddressableAssets;
using Nighday.General.AddressablesExtentions;

namespace Nighday.General.EntitiesExtentions {

[GenerateAuthoringComponent]
public struct ModelWeakAssetReference : IComponentData {
	public WeakAssetReference WeakAssetReference;
}

}