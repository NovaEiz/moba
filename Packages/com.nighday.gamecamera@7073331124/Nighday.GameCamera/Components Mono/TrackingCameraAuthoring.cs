using Nighday.GameCamera;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Nighday.GameCamera {

public class TrackingCameraAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	private Camera _camera => GetComponent<Camera>();

	public void Convert(Entity entity, EntityManager EntityManager, GameObjectConversionSystem conversionSystem) {
		EntityManager.AddComponentData(entity, new TrackingCamera {
			RotationAngles = new float2(60, 0),
			Distance = 7
		});
		EntityManager.AddComponentObject(entity, _camera);
	}
}

}