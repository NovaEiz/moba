using Unity.Entities;
using Unity.Mathematics;

namespace Nighday.GameCamera {

public struct TrackingCamera : IComponentData {

	public Entity Target;
	public float Distance;
	public float2 RotationAngles;

}

}