using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Nighday.GameCamera {

[DisableAutoCreation]
public class TrackingCameraSystem : ComponentSystem {

	private struct TrackingCameraAuthorized : IComponentData {
		
	}

	protected override void OnUpdate() {
		Enabled = false;
		return;
		Entities
			.WithNone<TrackingCameraAuthorized>()
			.WithAll<TrackingCamera>()
			.ForEach((Entity               entity,
					UnityEngine.Camera camera,
					ref TrackingCamera trackingCamera
					) => {
						if (trackingCamera.Target.Equals(Entity.Null)) {
							return;
						}
						//trackingCamera.RotationAngles = new float2(60, 45);
						//trackingCamera.Distance = 7;

						EntityManager.AddComponentObject(entity, camera.transform);

						EntityManager.AddComponentData(entity, new CopyTransformToGameObject());
						
						//EntityManager.AddComponentData(entity, new Parent {
						//	Value = unitTrackingCamera.Target
						//});
						EntityManager.AddComponentData(entity, new Translation {Value   = float3.zero});
						EntityManager.AddComponentData(entity, new Rotation {Value      = float4.zero});
						//EntityManager.AddComponentData(entity, new LocalToParent {Value = float4x4.zero});

						var localToWorld = new LocalToWorld {
							Value = EntityManager.GetComponentData<LocalToWorld>(trackingCamera.Target).Value
						};
						EntityManager.AddComponentData(entity, localToWorld);
						
						EntityManager.AddComponentData(entity, new TrackingCameraAuthorized());
					});

		Entities
			.WithAll<TrackingCamera, TrackingCameraAuthorized>()
			.ForEach((Entity entity,
					ref TrackingCamera unitTrackingCamera,
					ref Translation translation, 
					ref Rotation rotation
					) => {
						if (!EntityManager.HasComponent<LocalToWorld>(unitTrackingCamera.Target)) {
							return;
						}
						var targetLocalToWorld = EntityManager.GetComponentData<LocalToWorld>(unitTrackingCamera.Target);

						if (unitTrackingCamera.RotationAngles.x > 89.9f) {
							unitTrackingCamera.RotationAngles.x = 89.9f;
						} else if (unitTrackingCamera.RotationAngles.x < 10) {
							unitTrackingCamera.RotationAngles.x = 10;
						}

						if (unitTrackingCamera.Distance < 2) {
							unitTrackingCamera.Distance = 2;
						} else if (unitTrackingCamera.Distance > 50) {
							unitTrackingCamera.Distance = 50;
						}
						
						var angles = unitTrackingCamera.RotationAngles;
						var rotationInSphere = (quaternion)Quaternion.Euler(angles.x, angles.y, 0);

						var direction = math.rotate(rotationInSphere, new float3(0, 0, -1));
						var localPosition = direction * unitTrackingCamera.Distance;
						
						var position = targetLocalToWorld.Position + localPosition;


						var directionCameraLookAt = targetLocalToWorld.Position - position;
						directionCameraLookAt = math.normalize(directionCameraLookAt);
						
						translation.Value = position;
						rotation.Value = quaternion.LookRotation(directionCameraLookAt, new float3(0, 1, 0));

						if (Input.GetKeyDown(KeyCode.LeftArrow)) {
							unitTrackingCamera.RotationAngles.y -= 1;
						} else if (Input.GetKeyDown(KeyCode.RightArrow)) {
							unitTrackingCamera.RotationAngles.y += 1;
						}

						if (Input.GetKeyDown(KeyCode.UpArrow)) {
							unitTrackingCamera.RotationAngles.x += 1;
						} else if (Input.GetKeyDown(KeyCode.DownArrow)) {
							unitTrackingCamera.RotationAngles.x -= 1;
						}

						if (Input.GetKeyDown(KeyCode.Z)) {
							unitTrackingCamera.Distance -= 1;
						} else if (Input.GetKeyDown(KeyCode.X)) {
							unitTrackingCamera.Distance += 1;
						}
					});
	}
}
}