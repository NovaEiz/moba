(function() {

    var WindowUtils = {
        GetWindowColors: function () {
            return new WindowColors(GetWindowColors());
        }
    };

    function WindowColors(colors) {
        this.colors = colors;

        this.GetBackgroundColor = function () {
            return this.colors[0];
        };
        this.GetTitleColors = function () {
            return Array(this.colors[1], this.colors[2], this.colors[3]);
        };
        this.GetBodyColor = function () {
            return this.colors[4];
        };
        this.GetBottomBarColors = function () {
            return Array(this.colors[5], this.colors[6], this.colors[7]);
        };

    }

    function Color(r, g, b, a) {
        var newRGBColor = new RGBColor();
        newRGBColor.red = r;
        newRGBColor.green = g;
        newRGBColor.blue = b;
        if (a) {
            newRGBColor.alpha = a;
        }
        return newRGBColor;
    }

    function GetWindowColors() {
        return Array(
            Color(0, 0, 0, 127),
            Color(37, 140, 245),//header
            Color(3, 92, 178),
            Color(3, 56, 150),
            Color(16, 92, 229),//back
            Color(9, 82, 199),
            Color(37, 140, 245),//bottom bar
            Color(4, 74, 147)
        );
    }

    return WindowUtils;
})();