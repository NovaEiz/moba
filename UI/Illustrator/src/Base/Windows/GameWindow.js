(function() {

    var math = app.require("math");
    var utils = app.require("utils");


    function GameWindow(size, colors, document) {
        var borders = [3, 3, 3, 4];

        var titleHeight = 117;
        var bottomBarHeight = 23;

        var minHeight = titleHeight + bottomBarHeight + borders[1] + borders[3] + 1;
        var minWidht = 10 + borders[0] + borders[2];

        var shearAngle = -5;

        var documentSize = size;

        if (documentSize[1] < minHeight) {
            documentSize[1] = minHeight;
        }
        if (documentSize[0] < minWidht) {
            documentSize[0] = minWidht;
        }


        //var offsetXOnMaxHeight = GetOffsetXOnMaxHeight(documentSize, shearAngle);
        var figureSizeInitial = documentSize;//Array(documentSize[0]-offsetXOnMaxHeight,documentSize[1]);

        var windowContentRect = Array(
            borders[0],
            borders[3],
            figureSizeInitial[0] - borders[0] - borders[2],
            figureSizeInitial[1] - borders[1] - borders[3]
        );

        var bodyHeight = windowContentRect[3] - titleHeight - bottomBarHeight;


        var backgroundBorderRadius = 4;
        var insideBorderRadius = 2;

        var figureGroup = document.groupItems.add();

        var shape1 = CreateBackgroundFigure(figureSizeInitial, figureGroup, backgroundBorderRadius, colors.GetBackgroundColor());

        var titleHeight = 117;
        var bottomBarHeight = 23;
        var bodyHeight = windowContentRect[3] - titleHeight - bottomBarHeight;


        var titleRect = Array(0, windowContentRect[3] - titleHeight, windowContentRect[2], titleHeight);
        CreateTitle(windowContentRect, titleRect, figureGroup, insideBorderRadius, colors.GetTitleColors());

        var bodyRect = Array(0, 24, windowContentRect[2], bodyHeight);
        CreateBody(windowContentRect, bodyRect, figureGroup, 0, colors.GetBodyColor());
        var bottomBarRect = Array(0, 0, windowContentRect[2], 24);
        CreateBottomBar(windowContentRect, bottomBarRect, figureGroup, insideBorderRadius, colors.GetBottomBarColors());

        this.baseWindow = {
            document: document
        };
    }

    GameWindow.GetMinimumSize = function () {
        var borders = [3, 3, 3, 4];

        var titleHeight = 117;
        var bottomBarHeight = 23;

        var minHeight = titleHeight + bottomBarHeight + borders[1] + borders[3] + 1;
        var minWidht = 10 + borders[0] + borders[2];

        var resultSize = Array(minWidht, minHeight);
        return resultSize;
    };

    function CreateTitle(parentRect, figureRect, figureGroup, borderRadius, colors) {
        parentRect = utils.Rect.AddTwoRects(parentRect, figureRect);
        figureRect[0] = 0;
        figureRect[1] = 0;

        var bottomLineHeight = 9;
        var bottomBorderHeight = 4;
        var generalHeight = figureRect[3] - bottomLineHeight - bottomBorderHeight;

        var generalRect = utils.Rect.CopyRect(figureRect);
        generalRect[1] = bottomLineHeight + bottomBorderHeight;
        generalRect[3] = generalHeight;
        CreateTitleGeneral(parentRect, generalRect, figureGroup, borderRadius, colors[0]);

        var bottomLineRect = utils.Rect.CopyRect(figureRect);
        bottomLineRect[1] = bottomBorderHeight;
        bottomLineRect[3] = bottomLineHeight;
        CreateTitleBottomLine(parentRect, bottomLineRect, figureGroup, borderRadius, colors[1]);

        var bottomBorderRect = utils.Rect.CopyRect(figureRect);
        bottomBorderRect[1] = 0;
        bottomBorderRect[3] = bottomBorderHeight;
        CreateTitleBottomBorder(parentRect, bottomBorderRect, figureGroup, borderRadius, colors[2]);
    }

    function CreateTitleGeneral(parentRect, rectFigure, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = true;

        shapePath.fillColor = color;

        var pathPoints = shapePath.pathPoints;

        pathPoints[1].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[2].selected = PathPointSelection.ANCHORPOINT;

        utils.roundAnyCorner(Array(shapePath), borderRadius);

        pathPoints[1].selected = PathPointSelection.NOSELECTION;
        pathPoints[2].selected = PathPointSelection.NOSELECTION;

        return shapePath;
    }

    function CreateTitleBottomLine(parentRect, rectFigure, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = true;

        shapePath.fillColor = color;

        return shapePath;
    }

    function CreateTitleBottomBorder(parentRect, rectFigure, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);


        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = true;

        shapePath.fillColor = color;

        return shapePath;
    }

    function CreateBody(parentRect, figureRect, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, figureRect);


        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = false;

        shapePath.fillColor = color;
    }

    function CreateBottomBar(parentRect, figureRect, figureGroup, borderRadius, colors) {
        parentRect = utils.Rect.AddTwoRects(parentRect, figureRect);
        figureRect[0] = 0;
        figureRect[1] = 0;

        var bottomTopBorderHeight = 3;
        var bottomBottomBorderHeight = 1;
        var generalHeight = figureRect[3] - bottomTopBorderHeight - bottomBottomBorderHeight;

        var generalRect = utils.Rect.CopyRect(figureRect);
        generalRect[1] = generalHeight + bottomBottomBorderHeight;
        generalRect[3] = bottomTopBorderHeight;
        CreateBottomBarTopBorder(parentRect, generalRect, figureGroup, borderRadius, colors[0]);

        var bottomBorderRect = utils.Rect.CopyRect(figureRect);
        bottomBorderRect[1] = 0;
        bottomBorderRect[3] = bottomBottomBorderHeight;

        bottomBorderRect[3] += 5;
        CreateBottomBarBottomBorder(parentRect, bottomBorderRect, figureGroup, borderRadius, colors[2]);

        var bottomLineRect = utils.Rect.CopyRect(figureRect);
        bottomLineRect[1] = bottomBottomBorderHeight;
        bottomLineRect[3] = generalHeight;
        CreateBottomBarGeneral(parentRect, bottomLineRect, figureGroup, borderRadius, colors[1]);
    }


    function CreateBottomBarTopBorder(parentRect, rectFigure, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = true;

        shapePath.fillColor = color;

        return shapePath;
    }

    function CreateBottomBarGeneral(parentRect, rectFigure, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = true;

        shapePath.fillColor = color;

        var pathPoints = shapePath.pathPoints;

        pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[3].selected = PathPointSelection.ANCHORPOINT;

        utils.roundAnyCorner(Array(shapePath), borderRadius);

        pathPoints[0].selected = PathPointSelection.NOSELECTION;
        pathPoints[3].selected = PathPointSelection.NOSELECTION;

        return shapePath;
    }

    function CreateBottomBarBottomBorder(parentRect, rectFigure, figureGroup, borderRadius, color) {
        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(utils.Rect.GetPathPointsFromRect(rect));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = true;

        shapePath.fillColor = color;

        var pathPoints = shapePath.pathPoints;

        pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[3].selected = PathPointSelection.ANCHORPOINT;

        utils.roundAnyCorner(Array(shapePath), borderRadius);

        pathPoints[0].selected = PathPointSelection.NOSELECTION;
        pathPoints[3].selected = PathPointSelection.NOSELECTION;

        return shapePath;
    }

    function CreateBackgroundFigure(size, figureGroup, borderRadius, color) {
        var shapePath = figureGroup.pathItems.add();
        shapePath.setEntirePath(Array(
            Array(0, 0),
            Array(0, 0 + size[1]),
            Array(0 + size[0], 0 + size[1]),
            Array(0 + size[0], 0)
        ));

        shapePath.closed = true;
        shapePath.stroked = false;
        shapePath.filled = false;
        shapePath.opacity = 35;

        if (!color) {
            color = new RGBColor;
            color.red = 0;
            color.green = 0;
            color.blue = 0;
        }
        shapePath.fillColor = color;

        var pathPoints = shapePath.pathPoints;
        pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[1].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[2].selected = PathPointSelection.ANCHORPOINT;
        pathPoints[3].selected = PathPointSelection.ANCHORPOINT;

        utils.roundAnyCorner(Array(shapePath), borderRadius);

        pathPoints[0].selected = PathPointSelection.NOSELECTION;
        pathPoints[1].selected = PathPointSelection.NOSELECTION;
        pathPoints[2].selected = PathPointSelection.NOSELECTION;
        pathPoints[3].selected = PathPointSelection.NOSELECTION;

        return shapePath;
    }

    return GameWindow;
})();
