(function() {

    var BoomerangArrow = app.require("Shapes").BoomerangArrow;

    function BoomerangArrowIcon(rect, document, options) {
        //var colors = ButtonColors.GetYellow();
        
        var rect = Array(rect[0] + 1, rect[1] + 1, rect[2] - 2, rect[3] - 2);
        
        var groupItem = document.groupItems.add();
        
        var boomerangArrow = new BoomerangArrow(rect, groupItem);
    
        this.groupItems = [groupItem];
        
        //ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("PlayButton"), playButton.baseButton.document);
        
        var colors = options.colors;

        boomerangArrow.shape.AddPreProcessing(function() {

            boomerangArrow.shape.pathItem.stroked = true;
            boomerangArrow.shape.pathItem.strokeWidth = options.strokeWidth;

            boomerangArrow.shape.pathItem.fillColor = colors.GetMainFillColor();

            boomerangArrow.shape.pathItem.borderColor = colors.GetMainBorderColor();

        });
        boomerangArrow.shape.AddExecute(function() {
            
        });


        boomerangArrow.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(boomerangArrow.shape.pathItem), options.angleRadius);
        });

        this.boomerangArrow = boomerangArrow;

        this.PreProcessing = function() {
            this.boomerangArrow.shape.PreProcessing();
        };
        this.PostProcessing = function() {
            this.boomerangArrow.shape.PostProcessing();
        };
        this.Execute = function() {
            this.boomerangArrow.shape.Execute();
        };
    }

    /*
    CrossIcon.ColorsArrayToCrossIconColors = function(colors) {
        var colors = new CrossIcon.Colors(colors);
        return colors;
    };
     */
    BoomerangArrowIcon.BaseSize = BoomerangArrow.BaseSize;

    BoomerangArrowIcon.Colors = function BoomerangArrowIconColors(
        mainFillColor,
        mainBorderColor
    ) {
        this.colors = Array(mainFillColor, mainBorderColor);
        
        this.GetMainFillColor = function () {
            return this.colors[0];
        };
        this.GetMainBorderColor = function () {
            return this.colors[1];
        };
    };
    
    return BoomerangArrowIcon;
})();