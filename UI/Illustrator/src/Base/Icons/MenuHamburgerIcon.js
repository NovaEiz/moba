(function() {

    var MenuHamburger = app.require("Shapes").MenuHamburger;

    function MenuHamburgerIcon(rect, document, options) {
        //var colors = ButtonColors.GetYellow();
        
        var rect = Array(rect[0] + 1, rect[1] + 1, rect[2] - 2, rect[3] - 2);
        
        var groupItem = document.groupItems.add();
        
        var menuHamburger = new MenuHamburger(rect, groupItem);
    
        this.groupItems = [groupItem];
        
        //ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("PlayButton"), playButton.baseButton.document);
        
        var colors = options.colors;
        
        var shape = menuHamburger.shape;

        shape.AddPreProcessing(function() {

            shape.pathItem.stroked = true;
            shape.pathItem.strokeWidth = options.strokeWidth;

            shape.pathItem.fillColor = colors.GetMainFillColor();

            shape.pathItem.borderColor = colors.GetMainBorderColor();

        });
        shape.AddExecute(function() {
            
        });


        shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(shape.pathItem), options.angleRadius);
        });

        this.menuHamburger = menuHamburger;
        this.shape = shape;

        this.PreProcessing = function() {
            this.shape.PreProcessing();
        };
        this.PostProcessing = function() {
            this.shape.PostProcessing();
        };
        this.Execute = function() {
            this.shape.Execute();
        };
    }

    /*
    CrossIcon.ColorsArrayToCrossIconColors = function(colors) {
        var colors = new CrossIcon.Colors(colors);
        return colors;
    };
     */
    MenuHamburgerIcon.BaseSize = MenuHamburger.BaseSize;

    MenuHamburgerIcon.Colors = function MenuHamburgerIconColors(
        mainFillColor,
        mainBorderColor
    ) {
        this.colors = Array(mainFillColor, mainBorderColor);
        
        this.GetMainFillColor = function () {
            return this.colors[0];
        };
        this.GetMainBorderColor = function () {
            return this.colors[1];
        };
    };
    
    return MenuHamburgerIcon;
})();