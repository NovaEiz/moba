(function() {

    var CrookedCross = app.require("Shapes").CrookedCross;

    function CrookedCrossIcon(rect, document, options) {
        //var colors = ButtonColors.GetYellow();
        
        var crossShadowRect = rect;
        var rect = Array(rect[0], rect[1] + 3, rect[2] - 3, rect[3] - 3);
        
        var groupItem = document.groupItems.add();
        
        var crossShadow = new CrookedCross(crossShadowRect, groupItem);
        
        var cross = new CrookedCross(rect, groupItem);
    
        this.groupItems = [groupItem];
        
        //ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("PlayButton"), playButton.baseButton.document);
        
        var centerPoint = Array(crossShadowRect[0] + crossShadowRect[2]/2, crossShadowRect[1] + crossShadowRect[3]/2);
        
        var colors = options.colors;

        crossShadow.shape.AddPreProcessing(function() {
            crossShadow.shape.pathItem.fillColor = colors.GetShadowColor();
            crossShadow.shape.pathItem.opacity = 80;

        });
        cross.shape.AddPreProcessing(function() {
            cross.shape.pathItem.stroked = true;

            cross.shape.pathItem.fillColor = colors.GetMainFillColor();

            cross.shape.pathItem.borderColor = colors.GetMainBorderColor();

        });
        crossShadow.shape.AddExecute(function() {
            math.SelectAllVerticesInShape(crossShadow.shape.pathItem);

            utils.RotateSelectedPointsAroundAPoint(-45, centerPoint, document);

            math.UnselectAllVerticesInShape(crossShadow.shape.pathItem);
        });
        cross.shape.AddExecute(function() {
            math.SelectAllVerticesInShape(cross.shape.pathItem);

            utils.RotateSelectedPointsAroundAPoint(-45, centerPoint, document);

            math.UnselectAllVerticesInShape(cross.shape.pathItem);
        });

        

        crossShadow.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(crossShadow.shape.pathItem), options.angleRadius);
        });
        cross.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(cross.shape.pathItem), options.angleRadius);
        });

        this.cross = cross;
        this.crossShadow = crossShadow;

        this.PreProcessing = function() {
            this.cross.shape.PreProcessing();
            this.crossShadow.shape.PreProcessing();
        };
        this.PostProcessing = function() {
            this.cross.shape.PostProcessing();
            this.crossShadow.shape.PostProcessing();
        };
        this.Execute = function() {
            this.cross.shape.Execute();
            this.crossShadow.shape.Execute();
        };
    }

    /*
    CrossIcon.ColorsArrayToCrossIconColors = function(colors) {
        var colors = new CrossIcon.Colors(colors);
        return colors;
    };
     */

    CrookedCrossIcon.Colors = function CrossIconColors(
        shadowColor,
        mainFillColor,
        mainBorderColor
    ) {
        this.colors = Array(shadowColor, mainFillColor, mainBorderColor);

        this.GetShadowColor = function () {
            return this.colors[0];
        };
        this.GetMainFillColor = function () {
            return this.colors[1];
        };
        this.GetMainBorderColor = function () {
            return this.colors[2];
        };
    };
    
    return CrookedCrossIcon;
})();