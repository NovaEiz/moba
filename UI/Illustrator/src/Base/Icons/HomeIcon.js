(function() {

    var HomeShape = app.require("Shapes").Home;

    function HomeIcon(rect, document, options) {
        //var colors = ButtonColors.GetYellow();
        
        var rect = Array(rect[0], rect[1] + 3, rect[2] - 3, rect[3] - 3);
        
        var groupItem = document.groupItems.add();
        
        var homeShape = new HomeShape(rect, groupItem);
    
        this.groupItems = [groupItem];
        
        //ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("PlayButton"), playButton.baseButton.document);
        
        var colors = options.colors;

        homeShape.shape.AddPreProcessing(function() {
            homeShape.shape.pathItem.stroked = true;
            homeShape.shape.pathItem.strokeWidth = options.strokeWidth;

            homeShape.shape.pathItem.fillColor = colors.GetMainFillColor();

            homeShape.shape.pathItem.borderColor = colors.GetMainBorderColor();

        });
        homeShape.shape.AddExecute(function() {
            
        });

        
        homeShape.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(homeShape.shape.pathItem), options.angleRadius);
        });

        this.homeShape = homeShape;

        this.PreProcessing = function() {
            this.homeShape.shape.PreProcessing();
        };
        this.PostProcessing = function() {
            this.homeShape.shape.PostProcessing();
        };
        this.Execute = function() {
            this.homeShape.shape.Execute();
        };
    }

    /*
    CrossIcon.ColorsArrayToCrossIconColors = function(colors) {
        var colors = new CrossIcon.Colors(colors);
        return colors;
    };
     */

    HomeIcon.Colors = function CrossIconColors(
        mainFillColor,
        mainBorderColor
    ) {
        this.colors = Array(mainFillColor, mainBorderColor);
        
        this.GetMainFillColor = function () {
            return this.colors[0];
        };
        this.GetMainBorderColor = function () {
            return this.colors[1];
        };
    };
    
    return HomeIcon;
})();