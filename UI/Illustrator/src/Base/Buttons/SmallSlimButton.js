(function() {

    var GameButton = app.require("./GameButton.js", $.fileName);

    function SmallSlimButton(rect, document, options) {
        var options = {
            topElementHeight: 4,
            bottomElementHeight: 5.5,
            bottomMarginShadow: 6.5,
            borders: Array(2.5,2.5,2.5,4),
            colors: options.colors
        };

        this.gameButton = new GameButton(rect, document, options);
    }

    return SmallSlimButton;

})();