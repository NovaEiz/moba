(function() {

    var GameButton = app.require("./GameButton.js", $.fileName);

    function LargeButton(rect, document, options, withShadow) {
        var options = {
            topElementHeight: 8,
            bottomElementHeight: 10,
            bottomMarginShadow: 5.5,
            borders: Array(2.5, 2.5, 2.5, 6),
            colors: options.colors
        };

        this.gameButton = new GameButton(rect, document, options, withShadow);
    }
    
    return LargeButton;

})();