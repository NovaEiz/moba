(function() {

    var GameButton = app.require("./GameButton.js", $.fileName);

    function MediumButton(rect, document, options) {
        var options = {
            topElementHeight: 7,
            bottomElementHeight: 9,
            bottomMarginShadow: 3.5,
            borders: Array(2.5,2.5,2.5,5),
            colors: options.colors
        };

        this.gameButton = new GameButton(rect, document, options);
    }

    return MediumButton;

})();