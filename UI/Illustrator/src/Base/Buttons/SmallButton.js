(function() {

    var GameButton = app.require("./GameButton.js", $.fileName);

    function SmallButton(rect, document, options) {
        var options = {
            topElementHeight: 7,
            bottomElementHeight: 6,
            bottomMarginShadow: 6.5,
            borders: Array(1.5,1.5,1.5,4),
            colors: options.colors
        };

        this.gameButton = new GameButton(rect, document, options);
    }

    return SmallButton;

})();