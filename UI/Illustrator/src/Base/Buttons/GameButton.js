(function() {

    var utils = app.require("utils");
    var math = app.require('math');
    var Shapes = app.require('Shapes');

    function GameButton(rect, document, options, withShadow) {
        if (withShadow === undefined) {
            withShadow = true;
        }
        var likeTransparentWhite = !withShadow;

        var borders = options.borders;//[2, 1, 2, 6];

        var shadowAngleRadius = 8;
        var bordersAngleRadius = 8;
        var topElementAngleRadius = 7;
        var bottomElementAngleRadius = 7;
        var middleElementAngleRadius = 7;

        // Settings

        var bottomMarginShadow = options.bottomMarginShadow;//5;
        if (!withShadow) {
            bottomMarginShadow = 0;
        }
        var topElementHeight = options.topElementHeight;//8;
        var bottomElementHeight = options.bottomElementHeight;//10;

        var documentRect = Array(
            0,
            0,
            document.width,
            document.height
        );
        if (likeTransparentWhite) {
            rect[0] += 2;
            rect[1] += 2;
            rect[2] -= 4;
            rect[3] -= 4;
        }
        var bordersRect = Array(
            rect[0],
            rect[1] + bottomMarginShadow,
            rect[2],
            rect[3] - bottomMarginShadow
        );
        var shadowRect = Array(
            rect[0] + 1,
            rect[1],
            rect[2] - 1,
            bottomMarginShadow + borders[3]*4
        );
        var contentRect = Array(
            bordersRect[0] + borders[0],
            bordersRect[1] + borders[3],
            bordersRect[2] - borders[0] - borders[2],
            bordersRect[3] - borders[1] - borders[3]
        );
        var topElementRect = Array(
            contentRect[0],
            contentRect[1] + (contentRect[3] - topElementHeight) - (middleElementAngleRadius * 2),
            contentRect[2],
            topElementHeight + (middleElementAngleRadius * 2)
        );
        var middleElementRect = Array(
            contentRect[0],
            contentRect[1] + bottomElementHeight,
            contentRect[2],
            contentRect[3] - (topElementHeight + bottomElementHeight)
        );
        var bottomElementRect = Array(
            contentRect[0],
            contentRect[1],
            contentRect[2],
            bottomElementHeight
        );
        //alert("topElementRect = " + utils.Rect.ToString(topElementRect));
        // Creating

        var layer = document.layers.add();

        var colors = options.colors;
        
        this.groupItems = [];
        
        
        if (withShadow) {
            this.groupItems.push(CreateShadowFigure(documentRect, shadowRect, layer, [shadowAngleRadius], colors.GetShadowColor()));
        }
        
        
        if (!likeTransparentWhite) {
            this.groupItems.push(CreateBordersFigure(documentRect, bordersRect, layer, [bordersAngleRadius], colors.GetBordersElementColor(), likeTransparentWhite));

            this.groupItems.push(CreateTopElementFigure(documentRect, topElementRect, layer, [topElementAngleRadius], colors.GetTopElementColor()));
            this.groupItems.push(CreateBottomElementFigure(documentRect, bottomElementRect, layer, [bottomElementAngleRadius], colors.GetBottomElementColor()));
            this.groupItems.push(CreateMiddleElementFigure(documentRect, middleElementRect, layer, [middleElementAngleRadius], colors.GetMiddleElementColor()));
        } else {
            this.groupItems.push(CreateTopElementFigure(documentRect, bordersRect, layer, [topElementAngleRadius], colors.GetTopElementColor(), 30));

            this.groupItems.push(CreateBordersFigure(documentRect, bordersRect, layer, [bordersAngleRadius], colors.GetBordersElementColor(), likeTransparentWhite));
        }
        
        this.document = document;

        this.PreProcessing = function() {
            utils.PreProcessingGroups(this.groupItems);
        };
        this.PostProcessing = function() {
            utils.PostProcessingGroups(this.groupItems);
        };
        this.Execute = function() {
            utils.ExecuteGroups(this.groupItems);
        };
        
        this.contentRect = contentRect;
    }

    function CreateShadowFigure(parentRect, rectFigure, layer, bordersRadius, color) {
        var groupItem = layer.groupItems.add();

        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var box = new Shapes.Box(rect, groupItem);

        box.shape.AddPreProcessing(function() {
            box.shape.pathItem.fillColor = color;

            box.shape.pathItem.opacity = 50;
        });
        box.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(box.shape.pathItem), bordersRadius[0]);
        });

        return groupItem;
    }
    function CreateBordersFigure(parentRect, rectFigure, layer, bordersRadius, color, likeTransparentWhite) {
        var groupItem = layer.groupItems.add();

        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var box = new Shapes.Box(rect, groupItem);

        box.shape.AddPreProcessing(function() {
            if (likeTransparentWhite) {
                //box.shape.pathItem.fillColor = utils.Color(0,0,0,0);
                box.shape.pathItem.strokeColor = color;
                box.shape.pathItem.stroked = true;
                box.shape.pathItem.filled = false;
                box.shape.pathItem.strokeWidth = 3;
                //box.shape.pathItem.strokeAlignment = StrokeAlignment.INSIDE_ALIGNMENT;
            } else {
                box.shape.pathItem.fillColor = color;
            }
        });
        box.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(box.shape.pathItem), bordersRadius[0]);
        });

        return groupItem;
    }
    function CreateTopElementFigure(parentRect, rectFigure, layer, bordersRadius, color, opacity) {
        if (opacity === undefined) {
            opacity = 100;
        }
        var groupItem = layer.groupItems.add();

        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var box = new Shapes.Box(rect, groupItem);

        box.shape.AddPreProcessing(function() {
            box.shape.pathItem.fillColor = color;
            box.shape.pathItem.opacity = opacity;
        });
        box.shape.AddPostProcessing(function() {
            var pathPoints = box.shape.pathItem.pathPoints;
            pathPoints[1].selected = PathPointSelection.ANCHORPOINT;
            pathPoints[2].selected = PathPointSelection.ANCHORPOINT;

            utils.roundAnyCorner(Array(box.shape.pathItem), bordersRadius[0]);

            pathPoints[1].selected = PathPointSelection.NOSELECTION;
            pathPoints[2].selected = PathPointSelection.NOSELECTION;
        });

        return groupItem;
    }
    function CreateBottomElementFigure(parentRect, rectFigure, layer, bordersRadius, color) {
        var groupItem = layer.groupItems.add();

        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var box = new Shapes.Box(rect, groupItem);

        box.shape.AddPreProcessing(function() {
            box.shape.pathItem.fillColor = color;
        });
        box.shape.AddPostProcessing(function() {
            var pathPoints = box.shape.pathItem.pathPoints;
            pathPoints[0].selected = PathPointSelection.ANCHORPOINT;
            pathPoints[3].selected = PathPointSelection.ANCHORPOINT;

            utils.roundAnyCorner(Array(box.shape.pathItem), bordersRadius[0]);

            pathPoints[0].selected = PathPointSelection.NOSELECTION;
            pathPoints[3].selected = PathPointSelection.NOSELECTION;
        });

        return groupItem;
    }
    function CreateMiddleElementFigure(parentRect, rectFigure, layer, bordersRadius, color) {
        var groupItem = layer.groupItems.add();

        var rect = utils.Rect.AddTwoRects(parentRect, rectFigure);

        var box = new Shapes.Box(rect, groupItem);

        box.shape.AddPreProcessing(function() {
            box.shape.pathItem.fillColor = color;
        });
        box.shape.AddPostProcessing(function() {
            var pathPoints = box.shape.pathItem.pathPoints;
            pathPoints[2].selected = PathPointSelection.ANCHORPOINT;

            utils.roundAnyCorner(Array(box.shape.pathItem), bordersRadius[0]);

            pathPoints[2].selected = PathPointSelection.NOSELECTION;
        });

        return groupItem;
    }

    return GameButton;

})();