(function() {

    var utils = app.require("utils");
    var Color = utils.Color;

    var ButtonUtils = {
        GetGrayColors: function () {
            return new Colors(GetGray());
        },
        GetMagentaColors: function () {
            return new Colors(GetMagenta());
        },
        GetYellowColors: function () {
            return new Colors(GetYellow());
        },
        GetRedColors: function () {
            return new Colors(GetRed());
        },
        GetGreenColors: function () {
            return new Colors(GetGreen());
        },
        GetBlueColors: function () {
            return new Colors(GetBlue());
        },
        GetTransparentWhiteColor: function () {
            return new Colors(GetTransparentWhite());
        }
    };
    function Colors(colors) {
        this.colors = colors;

        this.GetShadowColor = function () {
            return this.colors[0];
        };
        this.GetBordersElementColor = function () {
            return this.colors[0];
        };
        this.GetTopElementColor = function () {
            return this.colors[1];
        };
        this.GetMiddleElementColor = function () {
            return this.colors[2];
        };
        this.GetBottomElementColor = function () {
            return this.colors[3];
        };

    }
    
    function GetTransparentWhite() {
        return Array(
            Color(255, 255, 255, 255),
            Color(255, 255, 255, 100),
            Color(255, 255, 255, 100),
            Color(255, 255, 255, 100)
        );
    }
    function GetBlue() {
        return Array(
            Color(0, 0, 0, 127),
            Color(21, 151, 255),
            Color(35, 113, 255),
            Color(9, 73, 240)
        );
    }
    function GetGreen() {
        return Array(
            Color(0, 0, 0, 127),
            Color(0, 255, 1),
            Color(0, 219, 8),
            Color(0, 143, 39)
        );
    }
    function GetGray() {
        return Array(
            Color(0, 0, 0, 127),
            Color(80, 76, 103),
            Color(52, 61, 80),
            Color(38, 43, 59)
        );
    }
    function GetMagenta() {
        return Array(
            Color(0, 0, 0, 127),
            Color(106, 94, 126),
            Color(50, 60, 80),
            Color(38, 44, 67)
        );
    }
    function GetYellow() {
        return Array(
            Color(0, 0, 0, 127),
            Color(248, 238, 69),
            Color(237, 196, 8),
            Color(162, 85, 41)
        );
    }
    function GetRed() {
        return Array(
            Color(0, 0, 0, 127),
            Color(240, 92, 90),
            Color(221, 52, 44),
            Color(183, 15, 76)
        );
    }

    return ButtonUtils;
})();

/*

(function() {

    var ButtonColors = {
        GetMagenta: GetMagentaColors,
        GetYellow: GetYellowColors,
        GetRed: GetRedColors
    };

    function ButtonColors(colors) {
        this.colors = colors;

        this.GetBackgroundColor = function() {
            return this.colors[0];
        };
        this.GetTitleColors = function() {
            return Array(this.colors[1], this.colors[2], this.colors[3]);
        };
        this.GetBodyColor = function() {
            return this.colors[4];
        };
        this.GetBottomBarColors = function() {
            return Array(this.colors[5], this.colors[6], this.colors[7]);
        };

    }


    function GetRedColors() {
        var backColor = new RGBColor;
        backColor.red = 0;
        backColor.green = 0;
        backColor.blue = 0;

        var topColor = new RGBColor;
        topColor.red = 240;
        topColor.green = 92;
        topColor.blue = 90;

        var bottomColor = new RGBColor;
        bottomColor.red = 183;
        bottomColor.green = 15;
        bottomColor.blue = 76;

        var centerColor = new RGBColor;
        centerColor.red = 221;
        centerColor.green = 52;
        centerColor.blue = 44;

        var colors = Array(topColor, bottomColor, centerColor, backColor);

        return colors;
    }

    function GetMagentaColors() {
        var backColor = new RGBColor;
        backColor.red = 0;
        backColor.green = 0;
        backColor.blue = 0;

        var topColor = new RGBColor;
        topColor.red = 106;
        topColor.green = 94;
        topColor.blue = 126;

        var bottomColor = new RGBColor;
        bottomColor.red = 41;
        bottomColor.green = 44;
        bottomColor.blue = 60;

        var centerColor = new RGBColor;
        centerColor.red = 50;
        centerColor.green = 60;
        centerColor.blue = 80;

        var colors = Array(topColor, bottomColor, centerColor, backColor);

        return colors;
    }

    function GetYellowColors() {
        var backColor = new RGBColor;
        backColor.red = 0;
        backColor.green = 0;
        backColor.blue = 0;

        var topColor = new RGBColor;
        topColor.red = 248;
        topColor.green = 238;
        topColor.blue = 69;

        var bottomColor = new RGBColor;
        bottomColor.red = 162;
        bottomColor.green = 85;
        bottomColor.blue = 41;

        var centerColor = new RGBColor;
        centerColor.red = 237;
        centerColor.green = 196;
        centerColor.blue = 8;

        var colors = Array(topColor, bottomColor, centerColor, backColor);

        return colors;
    }

    return ButtonUtils;
    
})();
 */