(function() {

    var Box = app.require("Shapes").Box;

    function RepresentationFightersTeamPanel(rect, document, options) {
        var bottomMarginMainShape = options.bottomMarginMainShape;
        var shadowRect = Array(rect[0], rect[1], rect[2], rect[3] - bottomMarginMainShape);

        var rect = Array(shadowRect[0], shadowRect[1] + bottomMarginMainShape, shadowRect[2], shadowRect[3]);
        
        var groupItem = document.groupItems.add();

        var boxShadow = new Box(shadowRect, groupItem);
        var box = new Box(shadowRect, groupItem);
    
        this.groupItems = [groupItem];
        
        //ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("PlayButton"), playButton.baseButton.document);

        var colors = options.colors;

        box.shape.AddPreProcessing(function() {
            box.shape.pathItem.stroked = true;

            box.shape.pathItem.fillColor = colors.GetMainFillColor();
        });
        box.shape.AddExecute(function() {

        });
        box.shape.AddPostProcessing(function() {
            var pathPoints = box.shape.pathItem.pathPoints;
            var len = pathPoints.length;

            for (var d = 0; d < len; d++) {
                var pathPoint = pathPoints[d];
                var anchor = pathPoint.anchor;
                anchor[1] += bottomMarginMainShape;
                pathPoint.anchor = anchor;

                var leftDirection = pathPoint.leftDirection;
                leftDirection[1] += bottomMarginMainShape;
                pathPoint.leftDirection = leftDirection;

                var rightDirection = pathPoint.rightDirection;
                rightDirection[1] += bottomMarginMainShape;
                pathPoint.rightDirection = rightDirection;
            }
            utils.RoundAnyCorner(Array(box.shape.pathItem), options.angleRadius);
        });

        boxShadow.shape.AddPreProcessing(function() {
            boxShadow.shape.pathItem.stroked = true;

            boxShadow.shape.pathItem.fillColor = colors.GetMainShadowColor();
        });
        boxShadow.shape.AddExecute(function() {

        });
        boxShadow.shape.AddPostProcessing(function() {
            utils.RoundAnyCorner(Array(boxShadow.shape.pathItem), options.angleRadius);
        });

        this.box = box;
        this.boxShadow = boxShadow;
        this.shape = box.shape;

        this.PreProcessing = function() {
            this.box.shape.PreProcessing();
            this.boxShadow.shape.PreProcessing();
        };
        this.PostProcessing = function() {
            this.box.shape.PostProcessing();
            this.boxShadow.shape.PostProcessing();
        };
        this.Execute = function() {
            this.box.shape.Execute();
            this.boxShadow.shape.Execute();
        };
    }

    /*
    CrossIcon.ColorsArrayToCrossIconColors = function(colors) {
        var colors = new CrossIcon.Colors(colors);
        return colors;
    };
     */

    RepresentationFightersTeamPanel.Colors = function RepresentationFightersTeamPanelColors(
        mainFillColor,
        mainBorderColor
    ) {
        this.colors = Array(mainFillColor, mainBorderColor);
        
        this.GetMainFillColor = function () {
            return this.colors[0];
        };
        this.GetMainShadowColor = function () {
            return this.colors[1];
        };
    };
    
    return RepresentationFightersTeamPanel;
})();