//@include "../Utils/ImageExportUtility.js"
//@include "./../Utils/RoundAnyCorner.js"


var ShearShapes = //@include "./../Utils/ShearShapes.js";

var ImagePaths = //@include "../ImagePaths.js";

var CrossIcon = //@include "./CrossIcon.js";

var math = //@include "../Utils/math/index.js";

Create_CrossIcon();
function Create_CrossIcon() {
    var size = Array(53,53);
    var rect = Array(0, 0, size[0], size[1]);
    //var colors = ButtonColors.GetYellow();

    var document = app.documents.add(null, size[0], size[1]);
    var groupItem = document.groupItems.add();
    
    var crossIcon = new CrossIcon(Array(0,0,size[0],size[1]), groupItem, document);

    crossIcon.cross.shape.AddPostProcessing(function() {
        RoundAnyCorner(Array(crossIcon.cross.shape.pathItem), 2);
    });
    crossIcon.crossShadow.shape.AddPostProcessing(function() {
        RoundAnyCorner(Array(crossIcon.crossShadow.shape.pathItem), 2);
    });

    crossIcon.PreProcessing();

    var saveSelection = document.selection;
    document.selection = null;
    document.selection = null;

    math.SelectAllVerticesInShape(crossIcon.cross.shape.pathItem);
    math.SelectAllVerticesInShape(crossIcon.crossShadow.shape.pathItem);

    math.RotateSelectedPointsAroundAPoint(-45, Array(rect[2]/2, rect[3]/2), document);

    ShearShapes.ByAngle(-5, document);

    math.UnselectAllVerticesInShape(crossIcon.crossShadow.shape.pathItem);
    math.UnselectAllVerticesInShape(crossIcon.cross.shape.pathItem);

    crossIcon.Execute();
    crossIcon.PostProcessing();
    //ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("PlayButton"), playButton.baseButton.document);
    
}


