(function() {

    //var folderPath = "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/UI/Illustrator/Images";

    var list = {
        "BackButton": "/Lobby/BackButton",
        "HomeButton": "/Lobby/HomeButton",
        "UserButton": "/Lobby/UserButton",
        "Profile_NameInputButton": "/Lobby/Profile_NameInputButton",
        "PlayButton": "/Lobby/PlayButton",
        "LeaveButton": "/Lobby/LeaveButton",
        "ReconnectButton": "/Lobby/ReconnectButton",
        "SwitchLobbyGame": "/Lobby/SwitchLobbyGame",
        "ModeButton": "/Lobby/ModeButton",
        "MenuButton": "/Lobby/MenuButton",
        "WindowCloseButton": "/Windows/WindowCloseButton",
        "Window": "/Windows/Window",
        
        "RepresentationFightersRivals": "/Game/Representation/RepresentationFightersRivals",
        "RepresentationFightersAllies": "/Game/Representation/RepresentationFightersAllies",
        "RadialLight": "/Game/Representation/RadialLight"
    };
    
    var ImagePaths = {};
    
    ImagePaths.GetPathByKey = function(key) {
        var dirName = (new File($.fileName)).parent + "";
        var dirNameSplit = dirName.split('/');

        dirNameSplit.splice(dirNameSplit.length-3, 3);
        dirName = dirNameSplit.join('/');
        dirName += "/Assets/Runtime/StarFights//Runtime Resources/UI";

        var fullPath = dirName + list[key];
        return fullPath;
    };
    
    return ImagePaths;
})();