//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ButtonUtils = app.require("../../Base/Buttons/ButtonUtils.js", $.fileName);
var LargeButton = app.require("../../Base/Buttons/LargeButton.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths.js", $.fileName);
var config = app.require("../../config.js", $.fileName);

var BoomerangArrowIcon = app.require("../../Base/Icons/BoomerangArrowIcon.js", $.fileName);

Create_BackButton();
function Create_BackButton() {
    var documentSize = Array(295,130);

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), config.shearAngle);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeight, documentSize[1]);


    var buttonColors = ButtonUtils.GetMagentaColors();

    var buttonRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var button = new LargeButton(buttonRect, document, {
        colors: buttonColors
    });

    var boomerangArrowIconColors = new BoomerangArrowIcon.Colors(
        utils.Color(255,255,255),
        utils.Color(0,0,0)
    );
    var boomerangArrowIconSize = BoomerangArrowIcon.BaseSize;
    var boomerangArrowIconRect = Array(
        buttonRect[2] - boomerangArrowIconSize[0] - 65,
        buttonRect[3]/2 - boomerangArrowIconSize[1]/2,
        boomerangArrowIconSize[0],
        boomerangArrowIconSize[1]
    );
    var boomerangArrowIcon = new BoomerangArrowIcon(boomerangArrowIconRect, document, {
        colors: boomerangArrowIconColors,
        angleRadius: 2,
        strokeWidth: 3
    });
    
    var gameButton = button.gameButton;

    gameButton.PreProcessing();
    gameButton.Execute();
    boomerangArrowIcon.PreProcessing();
    boomerangArrowIcon.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(gameButton.groupItems);
    shearPoints.AddGroupItems(boomerangArrowIcon.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    gameButton.PostProcessing();
    boomerangArrowIcon.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("BackButton"), document);

}
