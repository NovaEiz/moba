//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ButtonUtils = app.require("../../Base/Buttons/ButtonUtils.js", $.fileName);
var LargeButton = app.require("../../Base/Buttons/LargeButton.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths.js", $.fileName);
var config = app.require("../../config.js", $.fileName);

var HomeIcon = app.require("../../Base/Icons/HomeIcon.js", $.fileName);

Create_HomeButton();
function Create_HomeButton() {
    var documentSize = Array(200,130);

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), config.shearAngle);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeight, documentSize[1]);


    var buttonColors = ButtonUtils.GetMagentaColors();

    var buttonRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var button = new LargeButton(buttonRect, document, {
        colors: buttonColors
    });

    var homeIconColors = new HomeIcon.Colors(
        utils.Color(255,255,255),
        utils.Color(0,0,0)
    );
    var homeIconSize = Array(100, 75);
    var homeIconRect = Array(
        buttonRect[2]/2 - homeIconSize[0]/2,
        buttonRect[3]/2 - homeIconSize[1]/2,
        homeIconSize[0],
        homeIconSize[1]
    );
    var homeIcon = new HomeIcon(homeIconRect, document, {
        colors: homeIconColors,
        angleRadius: 2,
        strokeWidth: 3
    });
    
    var gameButton = button.gameButton;

    gameButton.PreProcessing();
    gameButton.Execute();
    homeIcon.PreProcessing();
    homeIcon.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(gameButton.groupItems);
    shearPoints.AddGroupItems(homeIcon.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    gameButton.PostProcessing();
    homeIcon.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("HomeButton"), document);

}
