//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ButtonUtils = app.require("../../Base/Buttons/ButtonUtils.js", $.fileName);
var LargeButton = app.require("../../Base/Buttons/LargeButton.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths.js", $.fileName);
var config = app.require("../../config.js", $.fileName);

Create_SwitchLobbyGame();
function Create_SwitchLobbyGame() {
    var documentSize = Array(140,54);

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), config.shearAngle);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeight, documentSize[1]);
    
    
    var buttonColors = ButtonUtils.GetTransparentWhiteColor();

    var buttonRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var button = new LargeButton(buttonRect, document, {
        colors: buttonColors
    }, false);

    var gameButton = button.gameButton;

    gameButton.PreProcessing();
    gameButton.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(gameButton.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    gameButton.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("SwitchLobbyGame"), document);
    
}
