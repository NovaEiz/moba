//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ButtonUtils = app.require("../../Base/Buttons/ButtonUtils.js", $.fileName);
var SmallSlimButton = app.require("../../Base/Buttons/SmallSlimButton.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths.js", $.fileName);
var config = app.require("../../config.js", $.fileName);

Create_Profile_NameInputButton();
function Create_Profile_NameInputButton() {
    var documentSize = Array(460,116);

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), config.shearAngle);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeight, documentSize[1]);


    var buttonColors = ButtonUtils.GetGrayColors();

    var buttonRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var button = new SmallSlimButton(buttonRect, document, {
        colors: buttonColors
    });
    
    var gameButton = button.gameButton;

    gameButton.PreProcessing();
    gameButton.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(gameButton.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    gameButton.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("Profile_NameInputButton"), document);

}
