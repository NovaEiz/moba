//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ButtonUtils = app.require("../../Base/Buttons/ButtonUtils.js", $.fileName);
var SmallButton = app.require("../../Base/Buttons/SmallButton.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths.js", $.fileName);
var config = app.require("../../config.js", $.fileName);

var MenuHamburgerIcon = app.require("../../Base/Icons/MenuHamburgerIcon.js", $.fileName);

Create_MenuButton();
function Create_MenuButton() {
    var documentSize = Array(160,90);

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), config.shearAngle);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeight, documentSize[1]);


    var buttonColors = ButtonUtils.GetMagentaColors();

    var buttonRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var button = new SmallButton(buttonRect, document, {
        colors: buttonColors
    });
    
    var gameButtonContentRect = button.gameButton.contentRect;
    var menuHamburgerIconColors = new MenuHamburgerIcon.Colors(
        utils.Color(255,255,255),
        utils.Color(0,0,0)
    );
    var menuHamburgerIconSize = Array(56,45);
    var menuHamburgerIconRect = Array(
        gameButtonContentRect[0] + gameButtonContentRect[2]/2 - menuHamburgerIconSize[0]/2,
        gameButtonContentRect[1] + gameButtonContentRect[3]/2 - menuHamburgerIconSize[1]/2,
        menuHamburgerIconSize[0],
        menuHamburgerIconSize[1]
    );
    
    var menuHamburgerIcon = new MenuHamburgerIcon(menuHamburgerIconRect, document, {
        colors: menuHamburgerIconColors,
        angleRadius: 2,
        strokeWidth: 3
    });

    var gameButton = button.gameButton;

    gameButton.PreProcessing();
    menuHamburgerIcon.PreProcessing();
    gameButton.Execute();
    menuHamburgerIcon.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(gameButton.groupItems);
    shearPoints.AddGroupItems(menuHamburgerIcon.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    gameButton.PostProcessing();
    menuHamburgerIcon.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("MenuButton"), document);

}
