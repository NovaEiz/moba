//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ButtonUtils = app.require("../../Base/Buttons/ButtonUtils.js", $.fileName);
var SmallButton = app.require("../../Base/Buttons/SmallButton.js", $.fileName);
var CrookedCrossIcon = app.require("../../Base/Icons/CrookedCrossIcon.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths", $.fileName, null);
var config = app.require("../../config.js", $.fileName);

Create_WindowCloseButton();
function Create_WindowCloseButton() {
    var documentSize = Array(115,76);


    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), config.shearAngle);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeight, documentSize[1]);


    var buttonColors = ButtonUtils.GetRedColors();

    var buttonRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var button = new SmallButton(buttonRect, document, {
        colors: buttonColors
    });
    
    var buttonShadowHeight = 6;//TODO: get value from button

    var iconSize = Array(48,48);
    var iconRect = Array(documentSize[0]/2 - iconSize[0]/2 - 3, ((documentSize[1] - buttonShadowHeight)/2) - iconSize[1]/2 + buttonShadowHeight, iconSize[0], iconSize[1]);

    var crossIconColors = new CrookedCrossIcon.Colors(
        utils.Color(0,0,0),
        utils.Color(255,255, 255),
        utils.Color(0,0,0),
    );

    var crossIconRect = Array(0,0,figureInitialSize[0],figureInitialSize[1]);
    var crossIcon = new CrookedCrossIcon(iconRect, document, {
        colors: crossIconColors,
        angleRadius: 2
    });

    var gameButton = button.gameButton;

    gameButton.PreProcessing();
    crossIcon.PreProcessing();
    gameButton.Execute();
    crossIcon.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(gameButton.groupItems);
    shearPoints.AddGroupItems(crossIcon.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    gameButton.PostProcessing();
    crossIcon.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("WindowCloseButton"), document);

}
