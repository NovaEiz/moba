//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var WindowUtils = app.require("../../Base/Windows/WindowUtils.js", $.fileName);
var GameWindow = app.require("../../Base/Windows/GameWindow.js", $.fileName);

var ImagePaths = app.require("../../ImagePaths.js", $.fileName);
var config = app.require("../../config.js", $.fileName);

Create_Window();
function Create_Window() {
    //var size = Array(115,116 + 24 + 1 + 3 + 4);
    //var size = Array(1740, 980);
    var size = Array(0, 0);//0;0 - Значит используется минимально возможный размер окна

    var colors = WindowUtils.GetWindowColors();

    var minSize = GameWindow.GetMinimumSize();
    size = minSize;
    
    var document = app.documents.add(null, size[0], size[1]);
    app.executeMenuCommand ('doc-color-rgb');
    
    var window = new GameWindow(size, colors, document);

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("Window"), document);
}