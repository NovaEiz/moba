//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ImagePaths = app.require("../../../ImagePaths.js", $.fileName);
var config = app.require("../../../config.js", $.fileName);

var RepresentationFightersTeamPanel = app.require("../../../Base/Representation/RepresentationFightersTeamPanel.js", $.fileName);

Create_RepresentationFightersRivals();
function Create_RepresentationFightersRivals() {
    var documentSize = Array(1500,294);
    var shearAngle = 15;

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var offsetXOnMaxHeight = math.GetOffsetXOnMaxHeight(Array(documentSize[0],documentSize[1]), shearAngle);
    var offsetXOnMaxHeightAbs = Math.abs(offsetXOnMaxHeight);
    var figureInitialSize = Array(documentSize[0] - offsetXOnMaxHeightAbs, documentSize[1]);

    var boomerangArrowIconColors = new RepresentationFightersTeamPanel.Colors(
        utils.Color(235,50,35),
        utils.Color(0,0,0)
    );
    var boomerangArrowIconRect = Array(
        offsetXOnMaxHeightAbs,
        0,
        figureInitialSize[0],
        figureInitialSize[1]
    );
    var boomerangArrowIcon = new RepresentationFightersTeamPanel(boomerangArrowIconRect, document, {
        colors: boomerangArrowIconColors,
        angleRadius: 2,
        strokeWidth: 3,
        bottomMarginMainShape: 20
    });
    
    boomerangArrowIcon.PreProcessing();
    boomerangArrowIcon.Execute();

    var shearPoints = new utils.ShearPoints(document);

    shearPoints.AddGroupItems(boomerangArrowIcon.groupItems);

    shearPoints.Execute(offsetXOnMaxHeight);

    boomerangArrowIcon.PostProcessing();

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("RepresentationFightersRivals"), document);

}
