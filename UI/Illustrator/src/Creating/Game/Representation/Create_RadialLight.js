//@include "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js"

var math = app.require("math");
var utils = app.require("utils");

var ImagePaths = app.require("../../../ImagePaths.js", $.fileName);
var config = app.require("../../../config.js", $.fileName);

var Shape = app.require('Shapes').Shape;

function RadialLight(rect, groupItem) {

    var shape = new Shape(rect, groupItem);

    shape.AddPreProcessing(function() {
        var pathPoints = utils.Rect.GetPathPointsFromRect(rect);
        shape.pathItem.setEntirePath(pathPoints);
    });

    this.shape = shape;
    this.pathItem = shape.pathItem;
}

Create_RadialLight();
function Create_RadialLight() {
    var documentSize = Array(2500,2500);

    var rayLength = 2500;
    var raysAmount = 20;
    var rayAngle = 9;

    var document = app.documents.add(null, documentSize[0], documentSize[1]);
    app.executeMenuCommand ('doc-color-rgb');

    var groupItem = document.groupItems.add();

    var newCompoundPath = groupItem.compoundPathItems.add();
    
    var offsetToCenter = Array(documentSize[0]/2, documentSize[1]/2);
    
    var stepAngle = 360 / raysAmount;
    for (var i=0; i<raysAmount; i++) {
        var currentRayAngle = stepAngle * i;
        CreateRay(newCompoundPath, rayAngle, currentRayAngle, rayLength, offsetToCenter);
    }

    utils.ImageExportUtility.ExportFileToPNG24(ImagePaths.GetPathByKey("RadialLight"), document);

}

function CreateRay(newCompoundPath, rayAngle, currentRayAngle, rayLength, offsetToCenter) {
    var rayPos1 = math.GetPositionAroundCircleByRadius(rayLength, currentRayAngle);
    var rayPos2 = math.GetPositionAroundCircleByRadius(rayLength, currentRayAngle + rayAngle);

    rayPos1[0] += offsetToCenter[0];
    rayPos1[1] += offsetToCenter[1];
    
    rayPos2[0] += offsetToCenter[0];
    rayPos2[1] += offsetToCenter[1];
    
    var newPath = newCompoundPath.pathItems.add();
    newPath.setEntirePath(Array(
        Array(0 + offsetToCenter[0], 0 + offsetToCenter[1]),
        rayPos1,
        rayPos2
    ));

    newPath.strokeWidth = 3.5;
    newPath.strokeColor = app.activeDocument.swatches[3].color;
    newPath.fillColor = utils.Color(255, 255, 255);

}
