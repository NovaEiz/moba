if (!app.require) {
    app.modules = {};
    
    app.require = function(path, fileName, moduleName_, debug) {
        var tempSplit = path.split('/');
        if (path.indexOf('/') == -1 && path.indexOf('.') == -1) {
            
            var modulesFolderPath = "illustrator_modules/";
            var moduleName = "";
            
            var pathDir = $.fileName;
            var pathDirSplit = pathDir.split('/');
            pathDirSplit.pop();
            pathDir = pathDirSplit.join('/');
            
            pathDir += "/illustrator_modules/";
            if ((tempSplit[tempSplit.length-1].split('.').length < 2)) {
                var newPath = path + "/index.js";
                var newPathDir = pathDir + newPath;

                var file = new File(newPathDir);
                if (!file.exists) {
                    newPath = path + ".js";
                }
                
                moduleName = path;
                
                path = newPath;
            }
            
            path = modulesFolderPath + path;

            return app.require(path, fileName, moduleName);
        }
        
        var requireLikeModule = false;
        if (!fileName) {
            fileName = $.fileName;
            requireLikeModule = true;
        }
        
        var pathSplit = path.split('/');
        var moduleNameFull = pathSplit[pathSplit.length-1];

        var moduleNameFullSplit = moduleNameFull.split('.');
        var moduleNameWithExt = moduleNameFull;

        var moduleName = moduleNameFullSplit[0];
        
        if (moduleName_) {
            moduleName = moduleName_;
        }
        
        pathSplit.pop();
        var pathhWithoutName = pathSplit.join('/') + "/";


        var pathDir = fileName;
        var pathDirSplit = pathDir.split('/');

        pathDirSplit.pop();

        if (pathhWithoutName[0] == '.' && pathhWithoutName[1] == '/') {

            pathhWithoutName = pathhWithoutName.substr(2, pathhWithoutName.length-2);

        } else {
            while (pathhWithoutName[0] == '.' && pathhWithoutName[1] == '.' && pathhWithoutName[2] == '/') {

                pathhWithoutName = pathhWithoutName.substr(3, pathhWithoutName.length-3);

                pathDirSplit.pop();
            }
        }


        pathDir = pathDirSplit.join('/');


        if (tempSplit.length > 1 && (tempSplit[tempSplit.length-1].split('.').length < 2)) {
            var newPath = moduleNameWithExt + "/index.js";
            var newPathDir = pathDir + "/" + newPath;
            var file = new File(newPathDir);
            if (!file.exists) {
                newPath = moduleNameWithExt + ".js";
            }
            newPathDir = pathDir + "/" + newPath;

            moduleNameWithExt = newPath;
        }

        var fullPath = "";
        if (pathhWithoutName.length == 0) {
            fullPath = moduleNameWithExt
        } else {
            fullPath = pathhWithoutName + "/" + moduleNameWithExt
        }

        
        //pathDir = "/Users/novaeiz/Work/Projects/GitLab/NovaEiz.Games/MOBA/MOBA/UI/Illustrator/init.js";


        var modulePath = pathDir + "/" + fullPath;
        
        if (!requireLikeModule) {
            moduleName = modulePath;
        }

        if (!app.modules[moduleName]) {
            var module = eval("//@include \"" + modulePath + "\"");
            if (debug) {
                alert("moduleName = " + moduleName + "; module = " + module);
            }
            app.modules[moduleName] = module;
        }
        return app.modules[moduleName];
    };
    
}