(function() {


    var math = app.require('math');
    var Shape = app.require('./Shape', $.fileName);

    var pathPoints = null;

    var MenuHamburger = function(rect, groupItem) {
        var shape = new Shape(rect, groupItem);
        shape.AddPreProcessing(function() {
            var pathPoints = MenuHamburger.GetPathPoints(rect);
            shape.pathItem.setEntirePath(pathPoints);
        });

        this.shape = shape;
    };

    MenuHamburger.BaseSize =  Array(90,70);
    MenuHamburger.itemHeight = 20;
    MenuHamburger.itemInterval = 5;

    MenuHamburger.GetPathPoints = function pathPoints(rect) {
        var size = MenuHamburger.BaseSize;
        var sizeHalf = Array(size[0]/2, size[1]/2);

        var itemHeight = MenuHamburger.itemHeight;
        var itemInterval = MenuHamburger.itemInterval;
        
        var getPoint = function(x, y) {
            return Array(x, y);
        };
        
        function AddBoxPointsToArray(array, rect) {
            array.push(getPoint(rect[0],rect[1]));
            array.push(getPoint(rect[0] + rect[2],rect[1]));
            array.push(getPoint(rect[0] + rect[2],rect[1] + rect[3]));
            array.push(getPoint(rect[0],rect[1] + rect[3]));
        }
        
        var points = Array();
        AddBoxPointsToArray(points, Array(0,0, size[0], itemHeight));
        AddBoxPointsToArray(points, Array(0, itemHeight + itemInterval, size[0], itemHeight));
        AddBoxPointsToArray(points, Array(0, itemHeight*2 + itemInterval*2, size[0], itemHeight));
        
        var pathPointsFinal = ConvertPointsInRectToOtherRect(points, Array(0,0, size[0], size[1]), rect);

        return pathPointsFinal;
    };

    function ConvertPointsInRectToOtherRect(points, fromRect, toRect) {
        var resultArray = Array();

        var len = points.length;
        for (var i=0; i<len; i++) {
            var point = points[i];
            var xPercent = point[0] / fromRect[2];
            var yPercent = point[1] / fromRect[3];
            var pathPointNewPosition = Array(xPercent * toRect[2], yPercent * toRect[3]);
            resultArray.push(Array(
                pathPointNewPosition[0] + toRect[0],
                pathPointNewPosition[1] + toRect[1]
            ));
        }

        return resultArray;
    }

    return MenuHamburger;
})();