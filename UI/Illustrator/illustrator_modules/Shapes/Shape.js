(function() {

    var Shape = function (rect, groupItem) {
        var pathItem = groupItem.pathItems.add();

        pathItem.closed = true;
        pathItem.stroked = false;
        pathItem.filled = true;

        this.pathItem = pathItem;

        this.pathItem.shape = this;

        var __onPreProcessing = Array();
        var __onExecute = Array();
        var __onPostProcessing = Array();

        this.PreProcessing = function() {
            var len = __onPreProcessing.length;
            for (var i=0; i<len; i++) {
                __onPreProcessing[i]();
            }
            __onPreProcessing = Array();
        };
        this.PostProcessing = function() {
            var len = __onPostProcessing.length;
            for (var i=0; i<len; i++) {
                __onPostProcessing[i]();
            }
            __onPostProcessing = Array();
        };
        this.Execute = function() {
            var len = __onExecute.length;
            for (var i=0; i<len; i++) {
                __onExecute[i]();
            }
            __onExecute = Array();
        };

        this.AddPreProcessing = function(action) {
            __onPreProcessing.push(action);
        };
        this.AddPostProcessing = function(action) {
            __onPostProcessing.push(action);
        };
        this.AddExecute = function(action) {
            __onExecute.push(action);
        };
    };

    return Shape;
})();