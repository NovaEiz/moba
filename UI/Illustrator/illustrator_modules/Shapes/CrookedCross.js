(function() {

    var math = app.require('math');
    var Shape = app.require('./Shape', $.fileName);
    
    var CrookedCross = function(rect, groupItem) {
        var shape = new Shape(rect, groupItem);
        shape.AddPreProcessing(function() {
            var pathPoints = CrookedCross.GetPathPoints(rect);
            shape.pathItem.setEntirePath(pathPoints);
        });
        
        this.shape = shape;
    };

    CrookedCross.BaseSize = Array(99,99);

    CrookedCross.GetPathPoints = function(rect) {
        var size = CrookedCross.BaseSize;
        
        var points = Array(
            Array(30,0),
            Array(30, 34),
            Array(0, 37),
            Array(5, 69),
            Array(32, 68),
            Array(33,98),
            Array(66,95),
            Array(66,66),
            Array(98,64),
            Array(95,28),
            Array(65,32),
            Array(65,0)
        );

        var pathPointsFinal = ConvertPointsInRectToOtherRect(points, Array(0,0, size[0], size[1]), rect);

        return pathPointsFinal;
    };
    
    function ConvertPointsInRectToOtherRect(points, fromRect, toRect) {
        var resultArray = Array();
        
        var len = points.length;
        for (var i=0; i<len; i++) {
            var point = points[i];
            var xPercent = point[0] / fromRect[2];
            var yPercent = point[1] / fromRect[3];
            var pathPointNewPosition = Array(xPercent * toRect[2], yPercent * toRect[3]);
            resultArray.push(Array(
                pathPointNewPosition[0] + toRect[0],
                pathPointNewPosition[1] + toRect[1]
            ));
        }
        
        return resultArray;
    }


    return CrookedCross;
})();