(function() {


    var math = app.require('math');
    var Shape = app.require('./Shape', $.fileName);

    var pathPoints = null;

    var Home = function(rect, groupItem) {
        var shape = new Shape(rect, groupItem);
        shape.AddPreProcessing(function() {
            var pathPoints = Home.GetPathPoints(rect);
            shape.pathItem.setEntirePath(pathPoints);
        });

        this.shape = shape;
    };

    Home.BaseSize =  Array(100,75);

    Home.GetPathPoints = function pathPoints(rect) {
        var size = Home.BaseSize;
        var sizeHalf = Array(size[0]/2, size[1]/2);
        
        var getPoint = function(x, y) {
            return Array(sizeHalf[0] + x, sizeHalf[1] + y);
        };
        
        var points = Array(
            getPoint(-8,-22),
            getPoint(-8, -35),
            getPoint(-26, -35),
            getPoint(-26, -17),
            getPoint(-30.5, -21),
            getPoint(-47,1.5),
            getPoint(0,36),
            getPoint(47,1.5),
            getPoint(30.5, -21),
            getPoint(26, -17),
            getPoint(26, -35),
            getPoint(8, -35),
            getPoint(8,-22)
        );

        var pathPointsFinal = ConvertPointsInRectToOtherRect(points, Array(0,0, size[0], size[1]), rect);

        return pathPointsFinal;
    };

    function ConvertPointsInRectToOtherRect(points, fromRect, toRect) {
        var resultArray = Array();

        var len = points.length;
        for (var i=0; i<len; i++) {
            var point = points[i];
            var xPercent = point[0] / fromRect[2];
            var yPercent = point[1] / fromRect[3];
            var pathPointNewPosition = Array(xPercent * toRect[2], yPercent * toRect[3]);
            resultArray.push(Array(
                pathPointNewPosition[0] + toRect[0],
                pathPointNewPosition[1] + toRect[1]
            ));
        }

        return resultArray;
    }

    return Home;
})();