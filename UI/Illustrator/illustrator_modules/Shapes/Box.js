(function() {

    var math = app.require('math');
    var utils = app.require('utils');
    var Shape = app.require('./Shape', $.fileName);
    
    function Box(rect, groupItem) {
    
        var shape = new Shape(rect, groupItem);

        shape.AddPreProcessing(function() {
            var pathPoints = utils.Rect.GetPathPointsFromRect(rect);
            shape.pathItem.setEntirePath(pathPoints);
        });

        this.shape = shape;
        this.pathItem = shape.pathItem;
    }
    
    return Box;
})();