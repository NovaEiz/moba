(function() {

    var Shapes = {
        Shape: app.require("./Shape.js", $.fileName),
        Box: app.require("./Box.js", $.fileName),
        Cross: app.require("./Cross.js", $.fileName),
        CrookedCross: app.require("./CrookedCross.js", $.fileName),
        Home: app.require("./Home.js", $.fileName),
        BoomerangArrow: app.require("./BoomerangArrow.js", $.fileName),
        MenuHamburger: app.require("./MenuHamburger.js", $.fileName)
    };
    
    return Shapes;
})();