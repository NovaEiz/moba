(function() {


    var math = app.require('math');
    var Shape = app.require('./Shape', $.fileName);

    var BoomerangArrow = function(rect, groupItem) {
        var shape = new Shape(rect, groupItem);
        shape.AddPreProcessing(function() {
            var pathPoints = BoomerangArrow.GetPathPoints(rect);
            shape.pathItem.setEntirePath(pathPoints);
        });

        this.shape = shape;
    };

    BoomerangArrow.BaseSize = Array(60,88);

    BoomerangArrow.GetPathPoints = function(rect) {
        var size = BoomerangArrow.BaseSize;
        var sizeHalf = Array(size[0]/2, size[1]/2);
        
        var getPoint = function(x, y) {
            return Array(sizeHalf[0] + x, sizeHalf[1] + y);
        };
        
        var points = Array(
            getPoint(-28,0),
            getPoint(16,42),
            getPoint(26.5,24.5),
            getPoint(8,0),
            getPoint(26.5,-24.5),
            getPoint(16,-42)
        );

        var pathPointsFinal = ConvertPointsInRectToOtherRect(points, Array(0,0, size[0], size[1]), rect);

        return pathPointsFinal;
    };

    function ConvertPointsInRectToOtherRect(points, fromRect, toRect) {
        var resultArray = Array();

        var len = points.length;
        for (var i=0; i<len; i++) {
            var point = points[i];
            var xPercent = point[0] / fromRect[2];
            var yPercent = point[1] / fromRect[3];
            var pathPointNewPosition = Array(xPercent * toRect[2], yPercent * toRect[3]);
            resultArray.push(Array(
                pathPointNewPosition[0] + toRect[0],
                pathPointNewPosition[1] + toRect[1]
            ));
        }

        return resultArray;
    }

    return BoomerangArrow;
})();