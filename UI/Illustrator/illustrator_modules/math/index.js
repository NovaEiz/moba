(function() {

    var math = {};
    var intersectionUtils = app.require("./intersection.js", $.fileName);
    //===
        
    math.CheckIntersection = intersectionUtils.CheckIntersection;
    math.GetPositionAroundCircleByRadius = function(radius, angle) {
        var radian = angle * 0.0174532925;
        var x = radius *  Math.cos(radian);
        var y = radius *  Math.sin(radian);
        return Array(x,y);
    };
    math.GetOffsetXOnMaxHeight = function(size, angle) {
        var rightSide = true;
        if (angle > 0) {
            rightSide = false;
        }
        
        angle = Math.abs(angle);
        var width = size[0];
        var height = size[1];

        var newPositionAroundCircle = math.GetPositionAroundCircleByRadius(height, 90 - angle);

        newPositionAroundCircle[0] *= 2;
        newPositionAroundCircle[1] *= 2;

        var intersectResult =  math.CheckIntersection(Array(0,0), newPositionAroundCircle, Array(0,height), Array(width*2,height));
        var offsetXOnMaxHeight = intersectResult.point[0];
        
        if (!rightSide) {
            offsetXOnMaxHeight = -offsetXOnMaxHeight;
        }

        return offsetXOnMaxHeight;
    };

    math.GetPositionAroundCircleByRadius = function(radius, angle) {
        var radian = angle * (Math.PI / 180);
        var x = radius *  Math.cos(radian);
        var y = radius *  Math.sin(radian);

        return Array(x,y);
    };
    math.GetMagnitude = function(x, y) {
        return Math.sqrt(x * x + y * y);
    };
    math.GetAngleAroundCircleByPosition = function(position) {
        var bearingRadians = Math.atan2(position[1], position[0]); // get bearing in radians
        var bearingDegrees = bearingRadians * (180.0 / Math.PI); // convert to degrees
        bearingDegrees = (bearingDegrees > 0.0 ? bearingDegrees : (360.0 + bearingDegrees)); // correct discontinuity
        return bearingDegrees;
    };
    math.SelectAllVerticesInShape = function(pathItem) {
        var pathPoints = pathItem.pathPoints;
        var len = pathPoints.length;
        for (var i=0; i<len; i++) {
            var vertex = pathPoints[i];
            vertex.selected = PathPointSelection.ANCHORPOINT;
        }
    };
    math.UnselectAllVerticesInShape = function(pathItem) {
        var pathPoints = pathItem.pathPoints;
        var len = pathPoints.length;
        for (var i=0; i<len; i++) {
            var vertex = pathPoints[i];
            vertex.selected = PathPointSelection.NOSELECTION;
        }
    };

    return math;
})();