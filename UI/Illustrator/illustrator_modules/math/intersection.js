(function() {

    const COLINEAR = intersectResult('colinear');
    const PARALLEL = intersectResult('parallel');
    const NONE = intersectResult('none');

    /**
     * Check how two line segments intersect eachother. Line segments are represented
     * as (x1, y1)-(x2, y2) and (x3, y3)-(x4, y4).
     *
     * @param {number} x1
     * @param {number} y1
     * @param {number} x2
     * @param {number} y2
     * @param {number} x3
     * @param {number} y3
     * @param {number} x4
     * @param {number} y4
     * @return {object} Object describing intersection that looks like
     *    {
     *      type: none|parallel|colinear|intersecting,
     *      point: {x, y} - only defined when type == intersecting
     *    }
     */
    function checkIntersection(p1, p2, p3, p4) {

        var x1 = p1[0];
        var y1 = p1[1];
        var x2 = p2[0];
        var y2 = p2[1];
        var x3 = p3[0];
        var y3 = p3[1];
        var x4 = p4[0];
        var y4 = p4[1];

        const denom = ((y4 - y3) * (x2 - x1)) - ((x4 - x3) * (y2 - y1));
        const numeA = ((x4 - x3) * (y1 - y3)) - ((y4 - y3) * (x1 - x3));
        const numeB = ((x2 - x1) * (y1 - y3)) - ((y2 - y1) * (x1 - x3));

        if (denom == 0) {
            if (numeA == 0 && numeB == 0) {
                return COLINEAR;
            }
            return PARALLEL;
        }

        const uA = numeA / denom;
        const uB = numeB / denom;

        if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
            return intersecting({
                x: x1 + (uA * (x2 - x1)),
                y: y1 + (uA * (y2 - y1))
            });
        }

        return NONE;
    }

    function intersecting(point) {
        const result = intersectResult('intersecting');
        result.point = Array(point.x, point.y);
        return result;
    }

    function intersectResult(type) {
        return {
            type: type
        };
    }
    
    return {
        CheckIntersection: checkIntersection
    }
})();