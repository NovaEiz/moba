(function __utils() {
    var utils = {
        GetPathPointsFromRect: function(rect) {
            return Array(
                Array(rect[0], rect[1]),
                Array(rect[0], rect[1] + rect[3]),
                Array(rect[0] + rect[2], rect[1] + rect[3]),
                Array(rect[0] + rect[2], rect[1])
            );
        },
        AddTwoRects: function(parentRect, targetRect) {
            return Array(
                parentRect[0] + targetRect[0],
                parentRect[1] + targetRect[1],
                targetRect[2],
                targetRect[3]
            );
        },
        CopyRect: function(rect) {
            return Array(rect[0],rect[1],rect[2],rect[3]);
        },
        ToString: function(rect) {
            var str = "rect(x=" + rect[0] + ";y=" + rect[1] + ";width=" + rect[2] + ";height=" + rect[3] + ")";
            return str;
        }
    };
    return utils;
})();