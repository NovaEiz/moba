(function() {

    var ImageExportUtility = {
        ExportFileToPNG24: function (filePath, document) {
            var exportOptions = new ExportOptionsPNG24();
            var type = ExportType.PNG24;
            var fileSpec = new File(filePath);
            exportOptions.antiAliasing = false;
            exportOptions.transparency = true;
            exportOptions.saveAsHTML = true;
            document.exportFile(fileSpec, type, exportOptions);
        }
    };
    
    return ImageExportUtility;
})();