(function() {
    var math = app.require("math");

    var RotateSelectedPointsAroundAPoint = function(angle, point, document) {

        function RunForShape(pathItem) {
            var pathPoints = pathItem.pathPoints;
            var len2 = pathPoints.length;

            for (var d = 0; d < len2; d++) {
                var pathPoint = pathPoints[d];

                var anchor = pathPoint.anchor;

                var localPosition = Array(anchor[0] - point[0], anchor[1] - point[1]);

                var pathPointAngle = math.GetAngleAroundCircleByPosition(localPosition);
                pathPointAngle += angle;
                var newPosition = math.GetPositionAroundCircleByRadius(math.GetMagnitude(localPosition[0], localPosition[1]), pathPointAngle);

                newPosition[0] += point[0];
                newPosition[1] += point[1];

                pathPoint.anchor = newPosition;
                pathPoint.leftDirection = newPosition;
                pathPoint.rightDirection = newPosition;
            }
        }
        
        var selections = document.selection;
        var len = selections.length;
        for (var i=0; i<len; i++) {
            var selection = selections[i];
            if (!selection) {
                continue;
            }

            if (selection.constructor.name == "GroupItem") {
                var group = selection;
                var len3 = group.pathItems.length;
                for (var w = 0; w < len3; w++) {
                    var pathItem = group.pathItems[w];
                    RunForShape(pathItem);
                }
            } else if (selection.constructor.name == "PathItem") {
                var pathItem = selection;
                RunForShape(pathItem);
            }
            
        }
        
    };
    return RotateSelectedPointsAroundAPoint;
})();