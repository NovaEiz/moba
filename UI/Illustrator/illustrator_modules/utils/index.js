(function() {

    var utils = {};

    var ImageExportUtility = app.require("./ImageExportUtility.js", $.fileName);
    var cornersUtils = app.require("./RoundAnyCorner.js", $.fileName);
    //var ShearShapes = app.require("./ShearShapes.js", $.fileName);
    var rectUtils = app.require("./rectUtils.js", $.fileName);
    var ShearPoints = app.require("./ShearPoints.js", $.fileName, null);
    var RotateSelectedPointsAroundAPoint = app.require("./RotateSelectedPointsAroundAPoint.js", $.fileName);

    //===

    utils.RotateSelectedPointsAroundAPoint = RotateSelectedPointsAroundAPoint;

    utils.ShearPoints = ShearPoints;
    utils.ImageExportUtility = ImageExportUtility;
    utils.RoundAnyCorner = cornersUtils.RoundAnyCorner;
    utils.roundAnyCorner = cornersUtils.roundAnyCorner;
    
    utils.Rect = rectUtils;

    utils.Color = function Color(r, g, b, a) {
        var newRGBColor = new RGBColor();
        newRGBColor.red = r;
        newRGBColor.green = g;
        newRGBColor.blue = b;
        if (a) {
            newRGBColor.alpha = a;
        }
        return newRGBColor;
    };
    utils.PreProcessingGroup = function(groupItem) {
        var pathItems = groupItem.pathItems;
        var len = pathItems.length;
        for (var i=0; i<len; i++) {
            pathItems[i].shape.PreProcessing();
        }
    };
    utils.PostProcessingGroup = function(groupItem) {
        var pathItems = groupItem.pathItems;
        var len = pathItems.length;
        for (var i=0; i<len; i++) {
            pathItems[i].shape.PostProcessing();
        }
    };
    utils.ExecuteGroup = function(groupItem) {
        var pathItems = groupItem.pathItems;
        var len = pathItems.length;
        for (var i=0; i<len; i++) {
            pathItems[i].shape.Execute();
        }
    };
    utils.PreProcessingGroups = function(groupItems) {
        var len = groupItems.length;
        for (var i=0; i<len; i++) {
            utils.PreProcessingGroup(groupItems[i]);
        }
    };
    utils.PostProcessingGroups = function(groupItems) {
        var len = groupItems.length;
        for (var i=0; i<len; i++) {
            utils.PostProcessingGroup(groupItems[i]);
        }
    };
    utils.ExecuteGroups = function(groupItems) {
        var len = groupItems.length;
        for (var i=0; i<len; i++) {
            utils.ExecuteGroup(groupItems[i]);
        }
    };
    
    
    return utils;
})();