(function() {

    function ShearPoints(document) {
        document.selection = null;
        document.selection = null;

        this.AddGroupItem = function(groupItem) {
            var pathItems = groupItem.pathItems;
            var len = pathItems.length;
            for (var i=0; i<len; i++) {
                var pathItem = pathItems[i];
                var pathPoints = pathItem.pathPoints;
                var len2 = pathPoints.length;
                for (var d=0; d<len2; d++) {
                    var pathPoint = pathPoints[d];
                    pathPoint.selected = PathPointSelection.ANCHORPOINT;
                }
            }
        };
        this.AddGroupItems = function(groupItems) {
            var len = groupItems.length;
            for (var i=0; i<len; i++) {
                this.AddGroupItem(groupItems[i]);
            }
        };
        
        this.Execute = function(offsetXOnMaxHeight) {
            var height = document.height;

            var shapes = document.selection;

            function RunForShape(shape) {
                var pathPoints = shape.pathPoints;
                var len2 = pathPoints.length;

                for (var d=0; d<len2; d++) {
                    var point = pathPoints[d];

                    var position = point.anchor;
                    var coefByHeight = position[1] / height;
                    position[0] += coefByHeight * offsetXOnMaxHeight;
                    point.anchor = position;
                    point.leftDirection = position;
                    point.rightDirection = position;
                }
            }

            var len = shapes.length;
            for (var i=0; i<len; i++) {
                var group = shapes[i];

                if (!group) {
                    continue;
                }

                if (group.constructor.name == "GroupItem") {
                    var len3 = group.pathItems.length;
                    for (var w=0; w<len3; w++) {
                        var shape = group.pathItems[w];
                        RunForShape(shape);
                    }
                } else
                if (group.constructor.name == "PathItem") {
                    var shape = group;
                    RunForShape(shape);
                }

            }
        };


    }
    
    return ShearPoints;
})();