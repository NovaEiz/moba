(function() {
    var math = app.require("math");

    function GetPositionAroundCircleByRadius(radius, angle) {
        angle += 90;
        var radian = angle * 0.0174532925;
        var x = radius *  Math.cos(radian);
        var y = radius *  Math.sin(radian);
        return Array(x,y);
    }
    function GetOffsetXOnMaxHeight(size, angle) {
        var width = size[0];
        var height = size[1];

        var newPositionAroundCircle = GetPositionAroundCircleByRadius(height, angle);
        newPositionAroundCircle[0] *= 2;
        newPositionAroundCircle[1] *= 2;

        var newPosition =  math.CheckIntersection(Array(0,0), newPositionAroundCircle, Array(0,height), Array(width*2,height));
        var offsetXOnMaxHeight = newPosition[0];

        return offsetXOnMaxHeight;
    }
    var ShearShapesByAngle = function (angle, document) {
        var xOffset = GetOffsetXOnMaxHeight(Array(document.width, document.height), angle);
        return ShearShapesByOffset(xOffset, document);
    };
    var ShearShapesByOffset = function (offsetXOnMaxHeight, document) {
        var height = document.height;

        var shapes = document.selection;

        function RunForShape(shape) {
            var pathPoints = shape.pathPoints;
            var len2 = pathPoints.length;

            for (var d = 0; d < len2; d++) {
                var point = pathPoints[d];

                var position = point.anchor;
                var coefByHeight = position[1] / height;
                position[0] += coefByHeight * offsetXOnMaxHeight;
                point.anchor = position;
                point.leftDirection = position;
                point.rightDirection = position;
            }
        }

        var len = shapes.length;
        for (var i = 0; i < len; i++) {
            var group = shapes[i];

            if (!group) {
                continue;
            }

            if (group.constructor.name == "GroupItem") {
                var len3 = group.pathItems.length;
                for (var w = 0; w < len3; w++) {
                    var shape = group.pathItems[w];
                    RunForShape(shape);
                }
            } else if (group.constructor.name == "PathItem") {
                var shape = group;
                RunForShape(shape);
            }

        }
    };
    
    return {
        ByOffset: ShearShapesByOffset,
        ByAngle: ShearShapesByAngle
    };
})();