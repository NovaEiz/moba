using System.Collections;
using Nighday.Core.Loading;
using UnityEngine;

namespace NovaEiz.ArenaTest.Scripts {
public class StarFightsLoadingView : LoadingView {

	[SerializeField] private Animator _animator;

	public override void Hide() {
		StartCoroutine(HideAsyncIe());
	}

	private IEnumerator HideAsyncIe() {
		base.DisableCamera();

		_animator.Play("Hide");
		do {
			yield return null;
		}
		while (_animator.GetCurrentAnimatorStateInfo(0).IsName("Hide"));
		base.Hide();
	}

	public override void Show() {
		base.Show();
		StartCoroutine(ShowAsyncIe());
	}

	private IEnumerator ShowAsyncIe() {
		_animator.Play("Show");
		do {
			yield return null;
		}
		while (_animator.GetCurrentAnimatorStateInfo(0).IsName("Show"));
	}

	public override void SetState(LoadingState state) {
		_animator.Play(state.ToString(), 1, 0);
	}
	
}
}