using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.EntitiesExtentions;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.General;
using StarFights.Client.Game;
using StarFights.Client.Lobby;
using StarFights.Server.Game;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace NovaEiz.ArenaTest {

public class ArenaTestBootstrap {

	[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
	public static void Initialize() {
		var asyncCoroutineRunner = AsyncCoroutineRunner.Instance;
		var mainMonoBehaviour = MainMonoBehaviour.Instance;
		var arenaTestBootstrap = new ArenaTestBootstrap();
		Debug.Log("init BeforeSplashScreen");

		arenaTestBootstrap.InitializeAsync();
	}
	
	private static string ActiveGameSceneName => Nighday.CoreMOBA.Client.Bootstrap.ActiveGameSceneName;

	private async Task InitializeAsync() {
		var coreRPGBootstrap = new Nighday.CoreMOBA.Core.Bootstrap<
			GameServerBootstrap,
			GameClientBootstrap
		>();

		// Установить настройки для фреймворка CoreRPGBootstrap
		// todo: 
		
		/*
		
		- Установить объект с interface'ом IGameClientBootstrap
		- Установить объект с interface'ом IGameServerBootstrap
		- Надо ли показывать окно для принятия игры игроком (кнопка Accept) в поиске игры, когда игра найдена. Или принятие готовой игры произойдет автоматически
		- Метод для замены кнопки поиска(вместе с состояниями)
		- Нужна ли возможность во время игры скрывать кнопкой игру в лобби и из лобби обратно в игру. 
		- 
		
		*/

		Debug.Log(55);
		// Инициализировать CoreRPGBootstrap
		var world = await coreRPGBootstrap.InitializeAsync();
		Debug.Log(56 + "; world = " + world);

#if !UNITY_SERVER // Это клиент
		//Инициализация лобби на клиенте
		MainMonoBehaviour.Instance.AddAction(() => {
			var lobbyClientBootstrap = new LobbyClientBootstrap();
			lobbyClientBootstrap.SetGetActiveGameSceneNameAction(() => {
				return ActiveGameSceneName;
			});
			lobbyClientBootstrap.Initialize(world);
		});
#endif
		
#if !UNITY_CLIENT // Это сервере
		//Инициализация лобби на сервере
		MainMonoBehaviour.Instance.AddAction(() => {
			new LobbyServerBootstrap().Initialize(world);
		});
#endif
		
	}
	
	
	
}
}