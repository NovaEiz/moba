using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Core.Loading;
using Nighday.CoreMOBA.Client.Game;
using Nighday.CoreMOBA.Game;
using Nighday.General.EntitiesExtentions;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using Nighday.Matchmaking.General;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using World = Unity.Entities.World;
using Unity.NetCode;
using UnityEngine.Playables;
using ClientServerBootstrap = Unity.NetCode.ClientServerBootstrap;


namespace Nighday.CoreMOBA.Client {
public class Bootstrap {
	
	private IGameClientBootstrap _gameClientBootstrap;

	public void SetGameClientBootstrap(IGameClientBootstrap value) {
		_gameClientBootstrap = value;
	}

	private Nighday.Application.World AppWorld;

	private MatchmakingLobbyClientSystem _matchmakingClientSystem;

	private static string _activeGameSceneName;
	public static string ActiveGameSceneName => _activeGameSceneName;

	public async Task Initialize(Nighday.Application.World appWorld) {
		AppWorld = appWorld;
		
		_matchmakingClientSystem = appWorld.CreateSystem<MatchmakingLobbyClientSystem>();

		_matchmakingClientSystem.OnCreateGame += OnCreateGame;
	}
	
	private async Task<World> OnCreateGame(string host, ushort port, string gameModeName, bool reconnected = false) {
		Debug.Log("Client: Connect with game host = " + host + "; port = " + port);
		var worldName = port + "";
		var world = ClientServerBootstrap.CreateClientWorld(World.DefaultGameObjectInjectionWorld, "ClientWorld: " + worldName);

		var mapName = _matchmakingClientSystem.GetMapNameByModeName(gameModeName) + "@Client";
		
		await GameInit(world, host, port, mapName, reconnected);

		return world;
	}

	private async Task GameInit(World world, string host, ushort port, string mapName, bool reconnected) {
		try {
			var loadingView = App.World.GetExistingSystem<LoadingSystem>().LoadingView;
			loadingView.Show();

			await LoadScene(world, mapName);

			var ent = world.EntityManager.CreateEntity();
			world.EntityManager.AddComponentData(ent, new GameData {
				Host = host,
				Port = port
			});

			world.EntityManager.AddComponentData(
				world.EntityManager.CreateEntity(),
				new UserId {
					Value = AppWorld.DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>()._id
				});
		
		
			// ===

			if (!reconnected) {
				var gameStarted = false;
				GameStartedRequestClientSystem.OnGameStarted += () => { gameStarted = true; };
				while (!gameStarted) {
					await new WaitForUpdate();
				}
			}

			GameObject gameCanvasObj = null;
			while (gameCanvasObj == null) {
				gameCanvasObj = App.World.DataManager.GetGameObject("Canvas - Game");
				await new WaitForUpdate();
			}

			var signalReceiver = gameCanvasObj.GetComponent<UnityEngine.Timeline.SignalReceiver>();
			var signalsEnumerator = signalReceiver.GetRegisteredSignals().GetEnumerator();
			signalsEnumerator.MoveNext();
			var signal_1 = signalsEnumerator.Current;
			signalsEnumerator.MoveNext();
			var signal_2 = signalsEnumerator.Current;
			var beginGameReaction = signalReceiver.GetReaction(signal_1);
			var reconnectedToGameReaction = signalReceiver.GetReaction(signal_2);
			if (reconnected) {
				reconnectedToGameReaction.Invoke();
			} else {
				beginGameReaction.Invoke();
			}
			
			loadingView.Hide();
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
		
	}

	private async Task UnloadScene(World world) {
		await new WaitForUpdate();
	}

	private async Task LoadScene(World world, string mapName) {
		try {
			var sceneHandleAsync = Addressables.LoadSceneAsync(mapName, LoadSceneMode.Additive);
			await sceneHandleAsync.Task;
			
			await new WaitForUpdate();

			SceneManager.SetActiveScene(sceneHandleAsync.Result.Scene);

			AppWorld.DataManager.GetGameObject("Lobby").SetActive(false);
			
			var gameObjectsInRootScene = sceneHandleAsync.Result.Scene.GetRootGameObjects();

			var instance = gameObjectsInRootScene[0];//GameObject.Instantiate(prefab, AppWorld.DataManager.GetGameObject("Game").transform);
			instance.name = "Game";
			var convertToEntityInWorld = instance.AddComponent<ConvertToEntityInWorld>();
			convertToEntityInWorld.World = world;
			convertToEntityInWorld.ConversionMode = ConvertToEntity.Mode.ConvertAndInjectGameObject;
			convertToEntityInWorld.Convert();
			
			var res = false;
			convertToEntityInWorld.OnConvertFinished += (entity) => {
				res = true;
			};
			while (!res) {
				await new WaitForUpdate();
			}

			var gameClientBootstrap = new BaseGameClientBootstrap();
			gameClientBootstrap.WorldInitialization(world);

			_gameClientBootstrap.InitializeAsync(world);

			_activeGameSceneName = mapName;
		}
		catch (Exception e) {
			Debug.LogError("e = " + e);
		}
		
	}
	
}
}