using System.Threading.Tasks;
using Unity.Entities;

namespace Nighday.CoreMOBA.Client {
public interface IGameClientBootstrap {
	void InitializeAsync(World world);
}
}