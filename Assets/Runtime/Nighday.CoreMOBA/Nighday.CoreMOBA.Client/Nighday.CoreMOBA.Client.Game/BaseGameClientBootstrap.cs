using Nighday.General.EntitiesExtentions;
using Nighday.Matchmaking.Client;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using System;
using System.Collections;
using Nighday.General.Other;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Nighday.CoreMOBA.Client.Game {

public class BaseGameClientBootstrap {
	
	public void WorldInitialization(World world) {
	try {
		
		new MatchmakingGameClientBootstrap().WorldInitialization(world);

		// Groups

		var ghostPredictionSystemGroup = world.GetExistingSystem<GhostPredictionSystemGroup>();

		var simulationSystemGroup = world.GetExistingSystem<ClientSimulationSystemGroup>();
		var gameClientSystemGroup = world.GetExistingSystem<GameClientSystemGroup>();
		
		//===
		// Systems
		
		var gameClientSystem = world.CreateSystem<GameClientSystem>();
		gameClientSystemGroup.AddSystemToUpdateList(gameClientSystem);
		
		/*
		var rotationCameraClientSystem = world.CreateSystem<RotationCameraClientSystem>();
		gameClientSystemGroup.AddSystemToUpdateList(rotationCameraClientSystem);
		
		
		var setCommandTargetComponentClientSystem = world.CreateSystem<SetCommandTargetComponentClientSystem>();
		gameClientSystemGroup.AddSystemToUpdateList(setCommandTargetComponentClientSystem);
		 */
		
		//===
		// Init systems
		
		//===
		// Sort systems
		
		ghostPredictionSystemGroup.SortSystems();
		gameClientSystemGroup.SortSystems();
		simulationSystemGroup.SortSystems();
		
		//===
		
		
		//var handleAsync1    = Addressables.InitializeAsync();
		//handleAsync1.Completed += (handle1) => {
		//	if (handle1.Status == AsyncOperationStatus.Succeeded) {
				

		//	}
		//};
		
		//TODO: сделать тут!
		//new CoreClientBootstrap().WorldInitialization<GameClientSystemGroup>(world);
	}
	catch (Exception e) {
		Debug.LogError("e = " + e);
	}
	}

	/*
	
	public void GameWorldIsInitialized(World world) {
		var handleAsync    = Addressables.LoadAssetAsync<GameObject>("TestGameScene - Client");
		handleAsync.Completed += (AsyncOperationHandle<GameObject> handle) => {
			if (handle.Status == AsyncOperationStatus.Succeeded) {
				var prefab = handle.Result;

				WaitOneFrame(() => {
					var instance = GameObject.Instantiate(prefab, Nighday.Application.App.World.DataManager.GetGameObject("Game").transform);
					var convertToEntityInWorld = instance.AddComponent<ConvertToEntityInWorld>();
					convertToEntityInWorld.World = world;
					convertToEntityInWorld.ConversionMode = ConvertToEntity.Mode.ConvertAndInjectGameObject;
					convertToEntityInWorld.Convert();
					
					//_gameAuthorizationClientSystem.Connect();
				});
						
			}
		};
	}
	 */
	
	private static IEnumerator WaitOneFrameIe(Action callback) {
		yield return null;
		callback();
	}
	private static void WaitOneFrame(Action callback) {
		var obj = new GameObject();
		obj.AddComponent<LoadSceneMono>().StartCoroutine(WaitOneFrameIe(() => {
			callback();
			GameObject.Destroy(obj);
		}));
	}
	
}
}