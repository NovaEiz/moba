using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.CoreMOBA.Server;
using Nighday.CoreMOBA.Client;
using UnityEngine;
using Bootstrap = Nighday.CoreMOBA.Server.Bootstrap;

namespace Nighday.CoreMOBA.Core {
public class Bootstrap<TServerBootstrap, TClientBootstrap>
	where TServerBootstrap : IGameServerBootstrap
	where TClientBootstrap : IGameClientBootstrap
{

	public async Task<World> InitializeAsync() {
		var bootstrap = new Nighday.Core.Bootstrap();

		var world = await bootstrap.Initialize();
#if !UNITY_CLIENT // Это сервер
		var serverBootstrap = new Server.Bootstrap();
		serverBootstrap.SetGameServerBootstrap(Activator.CreateInstance<TServerBootstrap>());
		try {
			await serverBootstrap.Initialize(world);
		}
		catch (Exception e) {
			Debug.LogError("App. Ошибка на стороне клиента: e = " + e);
		}
#endif
#if !UNITY_SERVER // Это клиент
		var clientBootstrap = new Client.Bootstrap();
		clientBootstrap.SetGameClientBootstrap(Activator.CreateInstance<TClientBootstrap>());
		try {
			await clientBootstrap.Initialize(world);
		}
		catch (Exception e) {
			Debug.LogError("App. Ошибка на стороне клиента: e = " + e);
		}
#endif

		return world;
	}

}
}