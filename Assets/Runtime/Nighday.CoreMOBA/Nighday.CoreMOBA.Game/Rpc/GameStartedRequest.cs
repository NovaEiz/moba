﻿using Unity.Entities;
using Unity.Burst;
using Unity.NetCode;

namespace Nighday.CoreMOBA.Game {

    [BurstCompile]
    public struct GameStartedRequest : IRpcCommand {
        
    }
}
