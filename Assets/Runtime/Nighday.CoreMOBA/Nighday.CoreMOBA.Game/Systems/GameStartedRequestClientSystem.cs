using System;
using Unity.Entities;
using Unity.NetCode;

namespace Nighday.CoreMOBA.Game {

[DisableAutoCreation()]
public class GameStartedRequestClientSystem : ComponentSystem {

	public static event Action OnGameStarted;

	protected override void OnUpdate() {

		Entities
			.WithNone<SendRpcCommandRequestComponent>()
			.ForEach((Entity reqEntity,
					ref GameStartedRequest gameStartedRequest,
					ref ReceiveRpcCommandRequestComponent receiveRpcCommandRequestComponent
					) => {
						EntityManager.DestroyEntity(reqEntity);
				
						OnGameStarted?.Invoke();
						OnGameStarted = null;
			});
	}
}
}