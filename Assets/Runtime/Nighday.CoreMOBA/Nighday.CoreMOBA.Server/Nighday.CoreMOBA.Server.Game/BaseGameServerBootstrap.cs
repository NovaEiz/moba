using System;
using Nighday.Matchmaking.Server;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.CoreMOBA.Server.Game {

public class BaseGameServerBootstrap {

	public void WorldInitialization(World world) {
		try {

			// Это нужно чтобы выполнить основные общие действия для входа и авторизации в игру
			new MatchmakingGameServerBootstrap().WorldInitialization(world);
		
			// Groups

			var ghostPredictionSystemGroup = world.GetExistingSystem<GhostPredictionSystemGroup>();

			var simulationSystemGroup = world.GetExistingSystem<ServerSimulationSystemGroup>();
			var gameServerSystemGroup = world.GetExistingSystem<GameServerSystemGroup>();
			//===
			// Systems
		
			var gameServerSystem = world.CreateSystem<GameServerSystem>(); gameServerSystemGroup.AddSystemToUpdateList(gameServerSystem);
		
			var matchmakingServerSystem = world.GetExistingSystem<MatchmakingGameServerSystem>();
		
			//===
			// Init systems
		
			//matchmakingServerSystem.SetOnGameDataToInitializeAction(gameServerSystem.OnGameDataToInitialize);

			//===
			// Sort systems
		
			ghostPredictionSystemGroup.SortSystems();
			gameServerSystemGroup.SortSystems();
			simulationSystemGroup.SortSystems();
		
			//===
			// Закончена базовая инициализация сервера игры-сессии
		
			// Дальше инициализация сервера конкретной игры-сессии
			//TODO: сделать тут!
			//new CoreServerBootstrap().WorldInitialization<GameServerSystemGroup>(world);
		}
		catch (Exception e) {
			Debug.LogError("e = " + e);
		}
	}

}
}