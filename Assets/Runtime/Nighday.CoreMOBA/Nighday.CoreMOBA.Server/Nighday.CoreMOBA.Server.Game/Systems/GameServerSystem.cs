using System;
using Nighday.General.Other.SimpleJSON;
using Nighday.Matchmaking.General;
using Nighday.Matchmaking.Server;
using Nighday.Online;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace Nighday.CoreMOBA.Server.Game {

[DisableAutoCreation]
public class GameServerSystem : ComponentSystem {

	public struct PlayersInGame : IComponentData {
		public int Value;
	}
	public struct WaitConnectedPlayers : IComponentData {
		public int Value;
	}
	public struct GameStarted : IComponentData {
		
	}
	public struct GameEnd : IComponentData {
		
	}
	private struct GameToEndTimer : IComponentData {
		public float ElapsedTime;
		public float DurationTime;
	}

	private struct PlayerConnectionAuthorizedTag : IComponentData {}

	/*

	private struct WaitTankIsCreatedTag : IComponentData {
		public int UserId;
		public NativeString64 PlayerName;
	}
	 */

	private int _playersAmount;
	private int _needAuthPlayers;

	private EntityQuery _entityQuery;

	protected override void OnCreate() {
		base.OnCreate();

		_entityQuery = EntityManager.CreateEntityQuery(new EntityQueryDesc {
			Any = new[] {
				ComponentType.ReadOnly<PlayerConnectionAuthorizedTag>()
			}
		});

		//RequireForUpdate(_entityQuery);

		RequireSingletonForUpdate<PlayersInGame>();
	}

	protected override void OnUpdate() {
		var deltaTime = Time.DeltaTime;
			
		Entities
			.WithNone<WaitConnectedPlayers>()
			.WithAll<PlayersInGame>()
			.ForEach((Entity entity
					) => {
						EntityManager.AddComponentData(entity, new WaitConnectedPlayers());
					});
		Entities
			.WithAll<GameEnd>()
			.ForEach((Entity entity
					) => {
						EntityManager.DestroyEntity(entity);

						var gameServerManagerSystem = Nighday.Application.App.World.GetSystem<GameServerManagerSystem>();
						gameServerManagerSystem.FinishGame(World);
					});
		Entities
			.WithNone<GameEnd, GameToEndTimer>()
			.WithAll<GameStarted>()
			.ForEach((Entity entity
					) => {
						var array = _entityQuery.ToEntityArray(Allocator.TempJob);
						if (array.Length == 0) {
							EntityManager.AddComponentData(entity, new GameToEndTimer {
								DurationTime = 10
							});
						}
						array.Dispose();
					});
		Entities
			.WithNone<GameEnd>()
			.WithAll<GameStarted>()
			.ForEach((Entity entity,
					ref GameToEndTimer gameToEndTimer
					) => {
						gameToEndTimer.ElapsedTime += deltaTime;

						var array = _entityQuery.ToEntityArray(Allocator.TempJob);
						if (array.Length > 0) {

							EntityManager.RemoveComponent<GameToEndTimer>(entity);
							array.Dispose();
							return;
						}
						array.Dispose();

						if (gameToEndTimer.ElapsedTime >= gameToEndTimer.DurationTime) {

							EntityManager.AddComponentData(entity, new GameEnd());
						}
					});

		
		
		/*
		Entities
			.WithAll<WaitTankIsCreatedTag>()
			.ForEach((Entity reqEntity,
					ref CreateTankSystem.CreateRequest.Result createRequestResult,
					ref WaitTankIsCreatedTag waitTankIsCreatedTag
					) => {
						var tankEntity = createRequestResult.Entity;
				
						EntityManager.AddComponentData(tankEntity, new OwnerUserId {
							Value = waitTankIsCreatedTag.UserId
						});
						EntityManager.AddComponentData(tankEntity, new UnitName {
							Value = waitTankIsCreatedTag.PlayerName
						});
						
						EntityManager.AddComponentData(tankEntity, new SpawnForRebirthSystem.Timer {
							ElapsedTime = 0,
							DurationTime = 0
						});

						var tankStateNext = EntityManager.GetComponentData<TankStateNext>(tankEntity);
						tankStateNext.Value = (int)TankState.State.Dead;
						EntityManager.SetComponentData(tankEntity, tankStateNext);
							
						EntityManager.AddBuffer<MovementInput>(tankEntity);
						
						EntityManager.DestroyEntity(reqEntity);
					});
					
		Entities
			.WithNone<PlayerConnectionAuthorizedTag>()
			.WithAll<NetworkStreamInGame>()
			.ForEach((Entity connectionEntity,
					ref UserId user
					) => {
						var userId = user.Value;

						Entities
							.ForEach((Entity tankEntity,
									ref Tank tank,
									ref OwnerUserId ownerUserId
									) => {
										if (ownerUserId.Value == userId) {
											
											EntityManager.AddComponentData(connectionEntity, new PlayerConnectionAuthorizedTag());

											EntityManager.SetComponentData(connectionEntity, new CommandTargetComponent {
												targetEntity = tankEntity
											});

											_needAuthPlayers--;

											if (_needAuthPlayers == 0) {
												EntityManager.CreateEntity(typeof(GameStarted));
											}
										}
									});
					});
		 */

		Entities
			.ForEach((Entity entity,
					ref PlayersInGame playersInGame,
					ref WaitConnectedPlayers waitConnectedPlayers
					) => {
				var playersInGame_ = playersInGame;
				var waitConnectedPlayers_ = waitConnectedPlayers;
						Entities
							.WithNone<PlayerConnectionAuthorizedTag>()
							.WithAll<NetworkStreamInGame>()
							.ForEach((Entity connectionEntity,
									ref UserId user
									) => {
										//var userId = user.Value;

										EntityManager.AddComponentData(connectionEntity, new PlayerConnectionAuthorizedTag());

										waitConnectedPlayers_.Value++;

										if (playersInGame_.Value == waitConnectedPlayers_.Value) {
											EntityManager.AddComponentData(GetSingletonEntity<PlayersInGame>(), new GameStarted());
										}
									});
						waitConnectedPlayers.Value = waitConnectedPlayers_.Value;
			});
		
	}
	
	/*
	public void OnGameDataToInitialize(string gameDataJson) {
		try {
			var gameData = JSON.Parse(gameDataJson);

			_playersAmount = gameData["gameOptions"]["playersAmount"].AsInt;
			_needAuthPlayers = _playersAmount;
			
			var players = gameData["playersData"].AsArray;
			var len = players.Count;

			for (int i = 0; i < len; i++) {
				var player = players[i];

				var userId = player["_id"].AsInt;
				var playerData = player["data"];
				var playerName = playerData["name"];
				playerName = "UserId = " + userId;
				CreateTank(userId, playerName, playerData);
				
				if (players.Count == 1) {
					CreateTank(-1, "Bot", playerData);//Bot
					CreateTank(-2, "Bot", playerData);//Bot
				}
			}

		}
		catch (Exception e) {
			Debug.LogError("e = " + e);
		}

	}

	private void CreateTank(int userId, string playerName, JSONNode playerData) {
		var reqEntity = EntityManager.CreateEntity();
		EntityManager.AddComponentData(reqEntity, new CreateTankSystem.CreateRequest {
			BodyItemId = 1,
			GunItemId = 1,
		});
		EntityManager.AddComponentData(reqEntity, new WaitTankIsCreatedTag {
			UserId = userId,
			PlayerName = playerName
		});
		
	}
	 */
	
}
}