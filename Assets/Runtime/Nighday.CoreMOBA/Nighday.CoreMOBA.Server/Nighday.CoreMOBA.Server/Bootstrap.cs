using System;
using System.Threading.Tasks;
using Nighday.CoreMOBA.Server.Game;
using Nighday.General.EntitiesExtentions;
using Nighday.Matchmaking.General;
using Nighday.Matchmaking.Server;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

namespace Nighday.CoreMOBA.Server {
public class Bootstrap {
	
	private IGameServerBootstrap _gameServerBootstrap;

	public void SetGameServerBootstrap(IGameServerBootstrap value) {
		_gameServerBootstrap = value;
	}

	private Nighday.Application.World AppWorld;

	private GameServerManagerSystem _gameServerManagerSystem;

	public async Task Initialize(Nighday.Application.World appWorld) {
		AppWorld = appWorld;
		
		_gameServerManagerSystem = appWorld.CreateSystem<GameServerManagerSystem>();

		_gameServerManagerSystem.OnCreateGame += OnCreateGame;
		
		//World world = ClientServerBootstrap.CreateServerWorld(Unity.Entities.World.DefaultGameObjectInjectionWorld, "ServerWorld233");
	}
	
	private void OnCreateGame(ushort port, string gameModeName, Action<World> callback) {
		var worldName = port + "";
		World world = null;//_world;
		
		world = Online.Core.ClientServerBootstrap.CreateServerWorld(Unity.Entities.World.DefaultGameObjectInjectionWorld, worldName);
		
		var baseGameServerBootstrap = new BaseGameServerBootstrap();
		baseGameServerBootstrap.WorldInitialization(world);
		_gameServerBootstrap.InitializeAsync(world);

		var mapName = _gameServerManagerSystem.GetMapNameByModeName(gameModeName) + "@Server";

		LoadScene(world, mapName, () => {
			var ent = world.EntityManager.CreateEntity();
			world.EntityManager.AddComponentData(ent, new GameData {
				Port = port
			});

			callback(world);
		});
	}

	private void LoadScene(Unity.Entities.World world, string mapName, Action callback) {
		GameObject gameServersContainer = AppWorld.DataManager.GetGameObject("GameServersContainer");
		
		if (gameServersContainer == null) {
			gameServersContainer = new GameObject("GameServersContainer");
			GameObject.DontDestroyOnLoad(gameServersContainer);
			AppWorld.DataManager.AddGameObject("GameServersContainer", gameServersContainer);
		}
		var sceneHandleAsync = Addressables.LoadSceneAsync(mapName, LoadSceneMode.Additive);

		if (!sceneHandleAsync.IsValid()) {
			Debug.LogError("LoadScene. Не удалось загрузить сцену");
			return;
		}

		sceneHandleAsync.Completed += (handleAsync_) => {
			//var prefab = handleAsync_.Result;

			var gameObjectsInRootScene = sceneHandleAsync.Result.Scene.GetRootGameObjects();

			var instance = gameObjectsInRootScene[0];//GameObject.Instantiate(prefab, gameServersContainer.transform);
			var convertToEntityInWorld = instance.AddComponent<ConvertToEntityInWorld>();

			convertToEntityInWorld.World = world;
			convertToEntityInWorld.ConversionMode = ConvertToEntity.Mode.ConvertAndDestroy;
			convertToEntityInWorld.Convert();

			convertToEntityInWorld.OnConvertFinished += (entity) => {
				callback();
			};
		};
	}

}
}