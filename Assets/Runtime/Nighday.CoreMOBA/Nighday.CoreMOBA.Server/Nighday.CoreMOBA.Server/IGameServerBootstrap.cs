using System.Threading.Tasks;
using Unity.Entities;

namespace Nighday.CoreMOBA.Server {
public interface IGameServerBootstrap {
	void InitializeAsync(World world);
}
}