using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.CoreMOBA.Server;
using Nighday.General.PhysicsExtentions;
using Nighday.Matchmaking.Server;
using StarFights.GameCore.Attack;
using StarFights.GameCore.CapturingCrystals;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Projectile;
using StarFights.General;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using ComponentSystemGroup = Unity.Entities.ComponentSystemGroup;
using World = Unity.Entities.World;

namespace StarFights.Server.Game {
public class LobbyServerBootstrap {

	private Nighday.Application.World AppWorld;
	private World World;
	
	public static string GetMapNameByModeName(string modeName) {
		var res = _gameModesDatabaseAsset.GetMapNameByModeName(modeName);
		return res;
	}

	private static GameModesDatabaseAsset _gameModesDatabaseAsset;

	private async Task LoadGameModesDatabaseAsset() {
		_gameModesDatabaseAsset = await GameModesDatabaseAsset.LoadAsync();
	}

	public void Initialize(Nighday.Application.World appWorld) {
		LoadGameModesDatabaseAsset();
		
		var gameServerManagerSystem = App.World.GetExistingSystem<GameServerManagerSystem>();
		gameServerManagerSystem.SetMapNameByModeNameAction(GetMapNameByModeName);

	}
	
}
}