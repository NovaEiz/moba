using System;
using System.Threading.Tasks;
using Nighday.CoreMOBA.Game;
using Nighday.CoreMOBA.Server.Game;
using Nighday.General.Other.SimpleJSON;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.General;
using Nighday.Online;
using StarFights.GameCore.CapturingCrystals;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using Unity.Physics;
using UnityEngine;

namespace StarFights.Server.Game {

[UpdateAfter(typeof(CreateHeroSystem))]
[UpdateBefore(typeof(GhostSendSystem))]
[DisableAutoCreation]
public class ReceiveGameDataServerSystem : ComponentSystem {

	private struct PlayerInGame : IComponentData {
		public int UserId;
	}

	private struct GameStartedRequestToPlayerSent : IComponentData {}

	public struct PlayingGameState : IComponentData {
		
	}
	private struct InitializingGameState : IComponentData {
		
	}

	private struct WaitConnectionPlayer : IComponentData {
	}
	
	private struct WaitHeroIsCreatedTag : IComponentData {
		public int UserId;
		public FixedString64 PlayerName;
		public int HeroId;
		public int TeamId;
		public int IndexInTeam;
		public int NetworkId;
	}

	//private GhostDespawnSystem 123 a123sd;
	//private GhostSendSystem 123 a123sd;
	
	private struct PlayerConnectionAuthorizedTag : IComponentData {}

	private int _playersAmount;
	private int _needAuthPlayers;
	
	protected override void OnCreate() {
		RequireSingletonForUpdate<InitializingGameState>();
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
		//RequireSingletonForUpdate<GhostPrefabMetaDataComponent>();

	}

	private int GetNetworkIdByUserId(int userIdValue) {
		var res = -1;
		Entities
			.ForEach((Entity entity,
					ref UserId userId,
					ref NetworkIdComponent networkIdComponent
			) => {
						if (userId.Value == userIdValue) {
							res = networkIdComponent.Value;
						}
			});
		return res;
	}

	protected override void OnUpdate() {
		var gameInitialized = true;
		
		Entities
			.ForEach((Entity reqEntity,
					ref CreateHeroRequest createHeroRequest
					) => {
				gameInitialized = false;

				var networkId = GetNetworkIdByUserId(createHeroRequest.UserId);
				if (networkId == -1) {
					if (createHeroRequest.UserId > 0) {
						return;
					}
				}
				
				CreateHero(createHeroRequest, networkId);
				EntityManager.DestroyEntity(reqEntity);
			});
		Entities
			.ForEach((Entity reqEntity,
					ref CreateHeroSystem.CreateRequest.Result createRequestResult,
					ref WaitHeroIsCreatedTag waitHeroIsCreatedTag
					) => {
				gameInitialized = false;

				var networkId = waitHeroIsCreatedTag.NetworkId;
				
				var heroEntity = createRequestResult.Entity;
				
				EntityManager.SetComponentData(heroEntity, new GhostOwnerComponent {
					NetworkId = networkId
					//NetworkId = EntityManager.GetComponentData<NetworkIdComponent>(reqSrc.SourceConnection).Value
				});

						
				EntityManager.SetComponentData(heroEntity, new OwnerUserId {
					Value = waitHeroIsCreatedTag.UserId
				});
				EntityManager.SetComponentData(heroEntity, new Hero {
					Id = waitHeroIsCreatedTag.HeroId
				});
				EntityManager.SetComponentData(heroEntity, new TeamId {
					Value = waitHeroIsCreatedTag.TeamId
				});
				EntityManager.SetComponentData(heroEntity, new IndexPlayerInTeam {
					Value = waitHeroIsCreatedTag.IndexInTeam
				});
				EntityManager.SetComponentData(heroEntity, new UnitName {
					Value = waitHeroIsCreatedTag.PlayerName
				});
				
				//EntityManager.AddComponentData(heroEntity, new UnitName {
				//	Value = waitHeroIsCreatedTag.PlayerName
				//});
					
				EntityManager.AddComponentData(heroEntity, new DeadState());
				EntityManager.AddComponentData(heroEntity, new SpawnForRebirthSystem.Timer {
					DurationTime = 0
				});

				EntityManager.AddComponentData(heroEntity, new UnitAuthorized());
						
				//var tankStateNext = EntityManager.GetComponentData<TankStateNext>(tankEntity);
				//tankStateNext.Value = (int)TankState.State.Dead;
				//EntityManager.SetComponentData(tankEntity, tankStateNext);
				EntityManager.AddBuffer<HeroInput>(heroEntity);	
						
				EntityManager.DestroyEntity(reqEntity);
				
				
				/*
				EntityManager.AddComponentData(heroEntity, new PhysicsMassOverride {
					IsKinematic = 1
				});
				
				EntityManager.AddComponentData(heroEntity, new PhysicsGravityFactor {
					Value = 1
				});
				 */
				
				
			});
					
		Entities
			.WithNone<PlayerConnectionAuthorizedTag>()
			.WithAll<NetworkStreamInGame>()
			.ForEach((Entity connectionEntity,
					ref UserId user
					) => {
						var userIdValue = user.Value;
						Entities
							.WithNone<WaitConnectionPlayer>()
							.ForEach((Entity waitEntity,
									ref PlayerInGame playerInGame
									) => {
										if (userIdValue == playerInGame.UserId) {
											EntityManager.AddComponentData(waitEntity, new WaitConnectionPlayer());
										}
									});
					});
		Entities
			.ForEach((Entity waitEntity,
					ref PlayerInGame playerInGame,
					ref WaitConnectionPlayer waitConnectionPlayer
					) => {
						gameInitialized = false;
						var targetUserId = playerInGame.UserId;
						
						Entities
							.WithNone<PlayerConnectionAuthorizedTag>()
							.WithAll<NetworkStreamInGame>()
							.ForEach((Entity connectionEntity,
									ref UserId user
									) => {
										var userId = user.Value;

										if (targetUserId != userId) {
											return;
										}

										Entities
											.ForEach((Entity tankEntity,
													ref Hero hero,
													ref OwnerUserId ownerUserId
													) => {
														if (ownerUserId.Value == userId) {
											
															EntityManager.AddComponentData(connectionEntity, new PlayerConnectionAuthorizedTag());

															EntityManager.SetComponentData(connectionEntity, new CommandTargetComponent {
																targetEntity = tankEntity
															});
															
															EntityManager.RemoveComponent<WaitConnectionPlayer>(waitEntity);
														}
													});
									});
					});
		
		
		Entities
			.WithNone<GameStartedRequestToPlayerSent>()
			.WithAll<PlayerConnectionAuthorizedTag>()
			.WithAll<NetworkStreamInGame>()
			.ForEach((Entity connectionEntity,
					ref UserId user
					) => {
				if (!gameInitialized) {
					return;
				}
			
				EntityManager.AddComponentData(connectionEntity, new GameStartedRequestToPlayerSent());

				Send_GameStartedRequest(connectionEntity);

			});
		
		/*
		if (gameInitialized) {
			EntityManager.DestroyEntity(GetSingletonEntity<InitializingGameState>());
			EntityManager.CreateEntity(typeof(PlayingGameState));

			Send_GameStartedRequestToAllConnections();
		}
		 */
	}
	

	private void Send_GameStartedRequestToAllConnections() {
		var gameStartedRequest = new GameStartedRequest();

		Entities
			.WithAll<NetworkStreamConnection>()
			.ForEach((Entity connectionEntity) => {
				var reqEntity = EntityManager.CreateEntity();
				EntityManager.AddComponentData(reqEntity, gameStartedRequest);
				EntityManager.AddComponentData(reqEntity, new SendRpcCommandRequestComponent {
					TargetConnection = connectionEntity
				});
			});
	}

	private void Send_GameStartedRequest(Entity connectionEntity) {
		var gameStartedRequest = new GameStartedRequest();
		
		var reqEntity = EntityManager.CreateEntity();
		EntityManager.AddComponentData(reqEntity, gameStartedRequest);
		EntityManager.AddComponentData(reqEntity, new SendRpcCommandRequestComponent {
			TargetConnection = connectionEntity
		});
	}
	
	public void OnGameDataToInitialize(string gameDataJson) {
		try {
			var gameData = JSON.Parse(gameDataJson);
			var gameModeData = gameData["gameModeData"];
			_playersAmount = gameModeData["playersAmount"].AsInt;
			_needAuthPlayers = _playersAmount;

			var playersAmount = 0;

			var teamType = (GameModeTeamType)gameModeData["gameModeTeamType"].AsInt;
			if (teamType == GameModeTeamType.InTeam) {
				var teams = gameData["gameData"]["teams"].AsArray;
				var tLen = teams.Count;

				for (var t = 0; t < tLen; t++) {
					var team = teams[t];
					var teamId = team["id"];
					var players = team["players"].AsArray;
					var pLen = players.Count;
					
					for (var p = 0; p < pLen; p++) {
						var player = players[p];

						var userId = player["_id"].AsInt;
						var playerData = player["data"];
						var playerName = playerData["name"];
			
						Create_CreateHeroRequest(userId, playerName, teamId, p, playerData);

						var waitEntity = EntityManager.CreateEntity();
						EntityManager.AddComponentData(waitEntity, new PlayerInGame {
							UserId = userId
						});
			
						/*
						if (players.Count == 1) {
							Create_CreateHeroRequest(-1,  "Bot", 2, 0, playerData);//Bot
							Create_CreateHeroRequest(-1,  "Bot", 2, 1, playerData);//Bot
						}
						*/
					}
					
					playersAmount += pLen;
				}
			} else if (teamType == GameModeTeamType.EveryManForHimself) {
				var players = gameData["gameData"]["players"].AsArray;
				var pLen = players.Count;
					
				for (var p = 0; p < pLen; p++) {
					var player = players[p];

					var userId = player["_id"].AsInt;
					var playerData = player["data"];
					var playerName = playerData["name"];
			if (string.IsNullOrEmpty(playerName)) {
				playerName = "Default Name";
			}
					Create_CreateHeroRequest(userId, playerName, p+1, 0, playerData);

#if UNITY_EDITOR
					if (pLen == 1) {
						Create_CreateHeroRequest(-1, "Bot", p+2, 0, playerData);
					}
#endif
					var waitEntity = EntityManager.CreateEntity();
					EntityManager.AddComponentData(waitEntity, new PlayerInGame {
						UserId = userId
					});
					/*
					if (players.Count == 1) {
						Create_CreateHeroRequest(-1,  "Bot", 2, 0, playerData);//Bot
						Create_CreateHeroRequest(-1,  "Bot", 2, 1, playerData);//Bot
					}
					*/

					playersAmount++;
				}
			}
			
			var ent = EntityManager.CreateEntity();
			EntityManager.AddComponentData(ent, new GameServerSystem.PlayersInGame {
				Value = playersAmount
			});

		}
		catch (Exception e) {
			Debug.LogError("e = " + e);
		}

		EntityManager.CreateEntity(typeof(InitializingGameState));
	}
	
	

	private struct CreateHeroRequest : IComponentData {
		public int UserId;
		public FixedString64 PlayerName;
		public int HeroId;
		public int TeamId;
		public int IndexInTeam;
	}

	private void Create_CreateHeroRequest(int userId, string playerName, int teamId, int indexInTeam, JSONNode playerData) {
		var heroId = playerData["selectedHeroId"];

		var ent = EntityManager.CreateEntity();
		EntityManager.AddComponentData(ent, new CreateHeroRequest {
			UserId = userId,
			PlayerName = playerName,
			HeroId = heroId,
			TeamId = teamId,
			IndexInTeam = indexInTeam
		});
	}

	private void CreateHero(CreateHeroRequest createHeroRequest, int networkId) {
		
		var reqEntity = EntityManager.CreateEntity();
		EntityManager.AddComponentData(reqEntity, new CreateHeroSystem.CreateRequest {
			HeroId = createHeroRequest.HeroId
		});
		EntityManager.AddComponentData(reqEntity, new WaitHeroIsCreatedTag {
			UserId = createHeroRequest.UserId,
			PlayerName = createHeroRequest.PlayerName,
			HeroId = createHeroRequest.HeroId,
			TeamId = createHeroRequest.TeamId,
			IndexInTeam = createHeroRequest.IndexInTeam,
			NetworkId = networkId
		});
		
	}
	
}
}