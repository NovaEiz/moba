using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.CoreMOBA.Server;
using Nighday.General.PhysicsExtentions;
using Nighday.Matchmaking.Server;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Attack;
using StarFights.GameCore.CapturingCrystals;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Projectile;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using ComponentSystemGroup = Unity.Entities.ComponentSystemGroup;
using World = Unity.Entities.World;

namespace StarFights.Server.Game {
public class GameServerBootstrap : IGameServerBootstrap {

	private GameServerManagerSystem _gameServerManagerSystem;
	
	public void InitializeAsync(World world) {
		WorldInitialization<GameServerSystemGroup>(world);
	}
	
	public void WorldInitialization<T>(World world) where T : ComponentSystemGroup {
		// Groups

		var ghostPredictionSystemGroup = world.GetExistingSystem<GhostPredictionSystemGroup>();
		var abilityGhostSystemGroup = world.GetExistingSystem<AbilityGhostSystemGroup>();
		//var fixedStepSimulationSystemGroup = world.GetExistingSystem<FixedStepSimulationSystemGroup>();

		//===
		// Systems
		
		//var createHeroSystem = world.CreateSystem<CreateHeroSystem>(); gameServerSystemGroup.AddSystemToUpdateList(createHeroSystem);
		//var createCrystalSystem = world.CreateSystem<CreateCrystalSystem>(); gameServerSystemGroup.AddSystemToUpdateList(createCrystalSystem);

		
		//var throwingCrystalsServerSystem = world.CreateSystem<ThrowingCrystalsServerSystem>(); gameServerSystemGroup.AddSystemToUpdateList(throwingCrystalsServerSystem);
		
		var simulationSystemGroup = world.GetExistingSystem<SimulationSystemGroup>();
		var receiveGameDataServerSystem = world.CreateSystem<ReceiveGameDataServerSystem>();
		simulationSystemGroup.AddSystemToUpdateList(receiveGameDataServerSystem);
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<MoveSpeedInitServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CreateHeroSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CreateCrystalSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ThrowingCrystalsServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<GameCountdownToEndServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<DeterminingCrystalsInTeamForGameEndServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<DamageReceiveServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<LockXZRotationUnitsServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<SpawnForRebirthSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ProjectileLifeServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<HealthServerSystem>());

		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<HeroGhostSystem>());
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<AnimatorHeroGhostSystem>());
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<PickingUpCrystalsServerSystem>());
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<AutoMovementToForwardGhostSystem>());

		//fixedStepSimulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CollisionSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CollisionSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<TriggerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ProjectileCollisionServerSystem>());

		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<AbilityMagazineGhostSystem>());

		abilityGhostSystemGroup.AddSystemToUpdateList(world.CreateSystem<RunAbilityGhostSystem>());
		abilityGhostSystemGroup.AddSystemToUpdateList(world.CreateSystem<MultipleProjectileAbilityCastGhostSystem>());
		abilityGhostSystemGroup.AddSystemToUpdateList(world.CreateSystem<OneShotProjectileAbilityCastGhostSystem>());

		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<RpcUnitNameRequestServerSystem>());

		//===
		//rpc systems
		
		
		//var rpcUnitNameRequestServerSystem = world.CreateSystem<RpcUnitNameRequestServerSystem>(); gameServerSystemGroup.AddSystemToUpdateList(rpcUnitNameRequestServerSystem);
		
		//===
		
		//===
		// Init systems
		var matchmakingGameServerSystem = world.GetExistingSystem<MatchmakingGameServerSystem>();
		
		matchmakingGameServerSystem.SetOnGameDataToInitializeAction(receiveGameDataServerSystem.OnGameDataToInitialize);

		
		//===
		// Sort systems

		ghostPredictionSystemGroup.SortSystems();
		
	}
}
}