using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Matchmaking.Client;
using Nighday.Matchmaking.General;
using Nighday.Web;
using StarFights.GameCore.Ghosts;
using Unity.Entities;
using Unity.NetCode;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using World = Unity.Entities.World;

namespace StarFights.Client.Lobby {

[DisableAutoCreation]
public class HeroesWindowSystem : SystemBase {

	private HeroesWindow _heroesWindow;
	private LobbySelectedHeroSystem _lobbySelectedHeroSystem;
	private MatchmakingLobbyClientSystem _matchmakingLobbyClientSystem;

	private async Task InitializationAsync() {
		//_heroesWindow = await HeroesWindow.LoadAsync();
		//EntityManager.AddComponentObject(_lobbySelectedHeroView.ContainerEntity, _lobbySelectedHeroView);

	}

	private Entity _currentHeroEntity;

	private HeroesWindow_HeroView.State GetStateHeroView(int heroId, int selectedHeroId) {
		if (heroId == selectedHeroId) {
			return HeroesWindow_HeroView.State.Active;
		}

		return HeroesWindow_HeroView.State.Passive;
	}
	
	protected override void OnCreate() {
		var userId = App.World.DataManager.GetComponentData<Nighday.Core.Bootstrap.UserData>()._id;

		_lobbySelectedHeroSystem = World.GetOrCreateSystem<LobbySelectedHeroSystem>();
		_matchmakingLobbyClientSystem = Nighday.Application.App.World.GetExistingSystem<MatchmakingLobbyClientSystem>();
		
		HeroesWindow.OnCreated += (heroesWindow_) => {
			_heroesWindow = heroesWindow_;

			var selectedHeroId = LobbySelectedHeroSystem.GetSavedSelectedHeroId();

			_heroesWindow.SetData(new [] {
				new HeroesWindow_HeroView.HeroViewData {
					Id = 1,
					Name = "Archer 1",
					State = GetStateHeroView(1, selectedHeroId)
				}, 
				new HeroesWindow_HeroView.HeroViewData {
					Id = 2,
					Name = "Archer 2",
					State = GetStateHeroView(2, selectedHeroId)
				} 
			});

			_heroesWindow.OnChooseHero += (heroId_) => {
				var prevHeroId = LobbySelectedHeroSystem.GetSavedSelectedHeroId();
				_lobbySelectedHeroSystem.SetHero(heroId_);

				SendRequest_ChooseHero(userId, heroId_, (result) => {
					if (!result) {
						_lobbySelectedHeroSystem.SetHero(prevHeroId);
					}
				});
			};

			_heroesWindow.Window.ViewCloser.OnClosed += () => {
			};
		};
	}

	private async Task SendRequest_ChooseHero(int userId, int heroId, Action<bool> callback) {
		try {
			var url = _matchmakingLobbyClientSystem.Address + "/";
			url += "users/" + userId + "/selectedHero/" + heroId;
			var reqResult = await request.post(url, "{}", true);
			if (reqResult != null) {
				callback(true);
			} else {
				callback(false);
			}
		}
		catch (Exception e) {
			Debug.LogError(e);
			callback(false);
		}
	}
	
	protected override void OnUpdate() {
	}
	
}
}