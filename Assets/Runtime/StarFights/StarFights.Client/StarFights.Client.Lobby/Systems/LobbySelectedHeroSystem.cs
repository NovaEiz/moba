using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Core.Loading;
using StarFights.GameCore.Ghosts;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.Client.Lobby {

[Unity.Entities.UpdateAfter(typeof(CreateHeroSystem))]
[DisableAutoCreation]
public class LobbySelectedHeroSystem : SystemBase {

	private LobbySelectedHeroView _lobbySelectedHeroView;

	private static string _savedSelectedHeroIdKey = "SavedSelectedHeroId";
	public static int GetSavedSelectedHeroId() {
		var selectedHeroId = PlayerPrefs.GetInt(_savedSelectedHeroIdKey);
		if (selectedHeroId < 1) {
			selectedHeroId = 1;
			PlayerPrefs.SetInt(_savedSelectedHeroIdKey, selectedHeroId);
		}
		return selectedHeroId;
	}
	private void SetSavedSelectedHeroId(int value) {
		PlayerPrefs.SetInt(_savedSelectedHeroIdKey, value);
	}

	private async Task LoadLobbySelectedHeroView() {
		_lobbySelectedHeroView = await LobbySelectedHeroView.Load();
		//EntityManager.AddComponentObject(_lobbySelectedHeroView.ContainerEntity, _lobbySelectedHeroView);
		
		
	}

	private Entity _currentHeroEntity;
	private LoadingSystem _loadingSystem;
	
	protected override void OnCreate() {
		_loadingSystem = App.World.GetExistingSystem<LoadingSystem>();
		
		LoadLobbySelectedHeroView();
		
		RequireSingletonForUpdate<SelectedHeroContainer>();
		//RequireSingletonForUpdate<LobbySelectedHeroView>();
		
		CreateHero(GetSavedSelectedHeroId());
	}

	public void SetHero(int heroId) {
		SetSavedSelectedHeroId(heroId);
		CreateHero(GetSavedSelectedHeroId());
	}

	private struct WaitLoadedHeroView : IComponentData { }

	private bool _isNotFirstLoad;
	
	protected override void OnUpdate() {
		Entities
			.ForEach((Entity reqEntity,
					ref CreateHeroSystem.CreateRequest.Result createRequestResult,
					ref WaitHeroIsCreatedTag waitHeroIsCreatedTag
					) => {
				if (EntityManager.Exists(_currentHeroEntity)) {
					EntityManager.DestroyEntity(_currentHeroEntity);
				}
				var heroEntity = createRequestResult.Entity;
				_currentHeroEntity = heroEntity;

				EntityManager.RemoveComponent<GhostComponent>(heroEntity);
				EntityManager.RemoveComponent<GhostOwnerComponent>(heroEntity);
				EntityManager.RemoveComponent<PredictedGhostComponent>(heroEntity);
				EntityManager.RemoveComponent<GhostTypeComponent>(heroEntity);

				EntityManager.RemoveComponent<PhysicsVelocity>(heroEntity);
				
				//var selectedHeroContainerEntity = GetSingletonEntity<SelectedHeroContainer>();
				var selectedHeroContainerEntity = GetSingletonEntity<SelectedHeroContainer>();
				EntityManager.AddComponentData(heroEntity, new Parent {
					Value = selectedHeroContainerEntity
				});
				EntityManager.AddComponentData(heroEntity, new LocalToParent());
				
				EntityManager.DestroyEntity(reqEntity);

				if (!_isNotFirstLoad) {
					_isNotFirstLoad = true;
					EntityManager.AddComponentData(heroEntity, new WaitLoadedHeroView());
				}

			}).WithStructuralChanges().Run();
		
		Entities
			.WithAll<WaitLoadedHeroView>()
			.ForEach((Entity heroEntity,
					DynamicBuffer<Child> childBuffer
					) => {
						var childArray = childBuffer.ToNativeArray(Allocator.Temp);
						foreach (var child in childArray) {
							if (EntityManager.HasComponent<Animator>(child.Value)) {
								_loadingSystem.LoadingView.Hide();
								EntityManager.RemoveComponent<WaitLoadedHeroView>(heroEntity);
								break;
							}
						}
					}).WithStructuralChanges().Run();
	}
	
	
	
	private struct WaitHeroIsCreatedTag : IComponentData {
	}

	private void CreateHero(int heroId) {
		var reqEntity = EntityManager.CreateEntity();
		EntityManager.AddComponentData(reqEntity, new CreateHeroSystem.CreateRequest {
			HeroId = heroId
		});
		EntityManager.AddComponentData(reqEntity, new WaitHeroIsCreatedTag {
		});
	}
	
}
}