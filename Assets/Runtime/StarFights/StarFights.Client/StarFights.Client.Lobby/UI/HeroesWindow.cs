using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using Nighday.Matchmaking.Client.UI;
using Nighday.Matchmaking.General;
using StarFights.Client.Lobby.UI;
using StarFights.General;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace StarFights.Client.Lobby {

public class HeroesWindow : MonoBehaviour {

	[SerializeField] private HeroesWindow_HeroView _heroViewPrefab;
	[SerializeField] private Transform _container;

	private Window _window;

	public Window Window {
		get {
			if (_window == null) {
				_window = GetComponent<Window>();
			}

			return _window;
		}
	}

	public static event Action<HeroesWindow> OnCreated;

	private static HeroesWindow _instance;

	private GameModesDatabaseAsset _gameModesDatabaseAsset;

	private async Task LoadGameModesDatabaseAsset() {
		_gameModesDatabaseAsset = await GameModesDatabaseAsset.LoadAsync();

		OnCreate();
	}

	public Action<int> OnChooseHero;

	private void ChooseHero(int heroId) {
		OnChooseHero?.Invoke(heroId);
	}

	private void Awake() {
		_instance = this;
		OnCreated?.Invoke(_instance);
		
		OnCreate();//LoadGameModesDatabaseAsset();
	}

	private void OnCreate() {
		
	}

	private void CreateHeroView(HeroesWindow_HeroView.HeroViewData data, Action onClick) {
		var gameModeView = Instantiate(_heroViewPrefab, _container);
		gameModeView.SetData(data);
		gameModeView.OnClick += () => {
			Window.ViewCloser.Close();
			onClick();
			Destroy(gameObject);
		};
	}

	private void OnDisable() {
		Destroy(gameObject);
	}

	public void SetData(HeroesWindow_HeroView.HeroViewData[] dataArray) {
		foreach (var data in dataArray) {
			CreateHeroView(data, () => {
				ChooseHero(data.Id);
			});
		}
	}

}
}