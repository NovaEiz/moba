using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using Nighday.Matchmaking.Client.UI;
using Nighday.Matchmaking.General;
using StarFights.Client.Lobby.UI;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace StarFights.Client.Lobby {

public class HeroesWindow_HeroView : MonoBehaviour {

	public class HeroViewData {
		public int Id;
		public string Name;
		public State State;
	}

	public enum State {
		Passive,
		Active
	}

	[SerializeField] private TextMeshProUGUI _nameText;
	[SerializeField] private GameObject _backgroundActive;
	[SerializeField] private Button _button;

	public void SetName(string value) {
		_nameText.text = value;
	}
	
	public void SetState(State value) {
		switch (value) {
			case State.Passive:
				_backgroundActive.SetActive(false);
				break;
			case State.Active:
				_backgroundActive.SetActive(true);
				break;
		}
	}

	private void Awake() {
		_button.onClick.AddListener(() => {
			OnClick?.Invoke();
		});
	}

	public Action OnClick;

	public void SetData(HeroViewData data) {
		SetName(data.Name);
		SetState(data.State);
	}
	
}
}