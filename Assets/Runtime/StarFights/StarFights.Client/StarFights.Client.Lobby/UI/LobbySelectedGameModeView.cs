using System;
using System.Threading.Tasks;
using StarFights.General;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace StarFights.Client.Lobby {

public class LobbySelectedGameModeView : MonoBehaviour {

	[SerializeField] private TextMeshProUGUI _nameText;
	
	private GameModesDatabaseAsset _gameModesDatabaseAsset;

	private async Task LoadGameModesDatabaseAsset() {
		_gameModesDatabaseAsset = await GameModesDatabaseAsset.LoadAsync();

		_gameModesDatabaseAsset.OnGameModeSelected += (gameMode) => {
			SetData(gameMode);
		};
		
		SetData(_gameModesDatabaseAsset.SelectedGameMode);
	}

	private void Awake() {
		LoadGameModesDatabaseAsset();
	}

	private void SetData(GameModeAsset gameMode) {
		_nameText.text = gameMode.Name;
	}

}
}