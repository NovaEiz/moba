using System;
using System.Threading.Tasks;
using Nighday.Core;
using Nighday.General.AddressablesExtentions;
using Nighday.General.Other.SimpleJSON;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using Nighday.Web;
using StarFights.Client.Lobby.UI;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using App = Nighday.Application.App;

namespace StarFights.Client.Lobby.UI {

public class LobbyView : MonoBehaviour {

	[Header("Buttons")]
	[SerializeField] private Toggle _modeToggle;
	[SerializeField] private Toggle _heroesToggle;
	[SerializeField] private Toggle _profileToggle;
	[SerializeField] private TextMeshProUGUI _profileToggleUserName;

	[Header("Windows")]
	[SerializeField] private WeakAssetReference _modeWindow;
	[SerializeField] private WeakAssetReference _heroesWindow;
	[SerializeField] private WeakAssetReference _profileWindow;

	private MatchmakingLobbyClientSystem _matchmakingLobbyClientSystem;

	private void Awake() {
		_matchmakingLobbyClientSystem = Nighday.Application.App.World.GetExistingSystem<MatchmakingLobbyClientSystem>();

		_modeToggle.GetComponent<IToggleView>().BindLoadWindow((Action<IWindowView> callback) => {
			OpenModeWindow(callback);
		});
		_heroesToggle.GetComponent<IToggleView>().BindLoadWindow((Action<IWindowView> callback) => {
			OpenHeroesWindow(callback);
		});
		_profileToggle.GetComponent<IToggleView>().BindLoadWindow((Action<IWindowView> callback) => {
			OpenProfileWindow(callback);
		});

		string oldName = null;
		Bootstrap.UserData userData = null;
		Action<string> onNameChanged = (name) => {
			_profileToggleUserName.text = name;
		};

		ProfileWindow.OnEndEditEvent += () => {
			SendMessage_ChangeName(userData._id, userData.name, (resultName) => {
				if (string.IsNullOrEmpty(resultName)) {
					userData.Name = oldName;
				} else {
					userData.Name = resultName;
					oldName = userData.name;
				}
			});
		};
		
		if (!App.World.DataManager.HasComponentData<Bootstrap.UserData>()) {
			userData = App.World.DataManager.GetComponentData<Bootstrap.UserData>();
			oldName = userData.name;
			userData.OnNameChanged += onNameChanged;
			_profileToggleUserName.text = userData.name;
		} else {
			WaitAddedUserData((userData_) => {
				userData = userData_;
				oldName = userData.name;
				userData.OnNameChanged += onNameChanged;
				_profileToggleUserName.text = userData.name;
			});
		}
	}

	private async Task SendMessage_ChangeName(int userId, string name, Action<string> callback) {
		try {
			var url = _matchmakingLobbyClientSystem.Address + "/";
			url += "users/" + userId + "/changeName/" + name;
			var reqResult = await request.post(url, "{}", true);
			if (reqResult != null) {
				var resultJsonNode = JSON.Parse(reqResult);
				var okJsonNode = resultJsonNode["ok"];
				if (okJsonNode != null && okJsonNode.AsBool) {
					callback(resultJsonNode["name"]);
				} else {
					callback(null);
				}
			} else {
				callback(null);
			}
		}
		catch (Exception e) {
			Debug.LogError(e);
			callback(null);
		}
	}

	private async Task WaitAddedUserData(Action<Bootstrap.UserData> callback) {
		do {
			await new WaitForUpdate();
		} while (!App.World.DataManager.HasComponentData<Bootstrap.UserData>());

		callback(App.World.DataManager.GetComponentData<Bootstrap.UserData>());
	}
	
	private async Task OpenModeWindow(Action<IWindowView> callback) {
		var window = await LoadWindow(_modeWindow);
		window.ViewCloser.OnClosed += () => {
		};

		callback(window);
	}
	private async Task OpenHeroesWindow(Action<IWindowView> callback) {
		var window = await LoadWindow(_heroesWindow);
		window.ViewCloser.OnClosed += () => {
		};

		callback(window);
	}
	private async Task OpenProfileWindow(Action<IWindowView> callback) {
		var window = await LoadWindow(_profileWindow);
		window.ViewCloser.OnClosed += () => {
		};

		callback(window);
	}
	
	private async Task<IWindowView> LoadWindow(WeakAssetReference weakAssetReference) {
		var asyncOperation = weakAssetReference.GetAssetReference().LoadAssetAsync<GameObject>();
		await asyncOperation.Task;

		var instance = Instantiate(asyncOperation.Result, transform);
		var window = instance.GetComponent<IWindowView>();
		return window;
	}

}
}