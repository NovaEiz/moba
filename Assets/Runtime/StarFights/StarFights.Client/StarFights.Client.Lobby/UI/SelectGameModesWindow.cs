using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using Nighday.Matchmaking.Client.UI;
using Nighday.Matchmaking.General;
using StarFights.Client.Lobby.UI;
using StarFights.General;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace StarFights.Client.Lobby {

public class SelectGameModesWindow : MonoBehaviour {

	[SerializeField] private GameModeView _gameModeViewPrefab;
	[SerializeField] private Transform _container;

	private IViewCloser _viewCloser {
		get {
			return GetComponent<IViewCloser>();
		}
	}

	private GameModesDatabaseAsset _gameModesDatabaseAsset;

	private async Task LoadGameModesDatabaseAsset() {
		_gameModesDatabaseAsset = await GameModesDatabaseAsset.LoadAsync();

		OnCreate();
	}

	private void Awake() {
		LoadGameModesDatabaseAsset();
	}

	private void OnDisable() {
		Destroy(gameObject);
	}

	private void OnCreate() {
		Clear();
		
		var gameModes = _gameModesDatabaseAsset.GameModes;
		var selectedGameMode = _gameModesDatabaseAsset.SelectedGameMode;
		
		var len = gameModes.Length;
		for (var i = 0; i < len; i++) {
			var gameMode = gameModes[i];
			CreateGameModeView(gameMode, selectedGameMode == gameMode);
		}
	}

	private void CreateGameModeView(GameModeAsset gameMode, bool selectedItem) {
		var gameModeView = Instantiate(_gameModeViewPrefab, _container);
		gameModeView.SetData(gameMode);
		gameModeView.OnClick += () => {
			_gameModesDatabaseAsset.SetSelectedGameMode(gameMode);
			_viewCloser.Close();
		};
	}

	private void Clear() {
		foreach (Transform item in _container) {
			Destroy(item.gameObject);
		}
	}
	
}
}