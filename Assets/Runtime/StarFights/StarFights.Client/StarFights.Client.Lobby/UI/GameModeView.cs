using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using Nighday.Matchmaking.Client.UI;
using Nighday.Matchmaking.General;
using StarFights.General;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace StarFights.Client.Lobby {

public class GameModeView : MonoBehaviour{

	[SerializeField] private TextMeshProUGUI _nameText;
	[SerializeField] private Button _button;
	
	public void SetData(GameModeAsset gameMode) {
		_nameText.text = gameMode.Name;
	}

	private void Awake() {
		_button.onClick.AddListener(() => {
			OnClick();
		});
	}

	public Action OnClick;
	
}
}