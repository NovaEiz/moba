using System;
using Nighday.Application;
using Nighday.Core;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace StarFights.Client.Lobby {

public class ProfileWindow : MonoBehaviour {

	[SerializeField] private TMP_InputField _userNameInput ;
	[SerializeField] private TextMeshProUGUI _userNameText;
	[SerializeField] private TextMeshProUGUI _userIdText;
	
	private string _userIdTextForReplace;

	private Bootstrap.UserData _userData;

	private void Awake() {
		_userIdTextForReplace = _userIdText.text;
	}

	private void OnEnable() {
		_userData = App.World.DataManager.GetComponentData<Bootstrap.UserData>();
		_userData.OnNameChanged += OnNameChanged;
		_userIdText.text = _userIdTextForReplace.Replace("{1}", _userData._id.ToString());

		OnNameChanged(_userData.name);

		_userNameInput.onEndEdit.AddListener(OnEndEdit);
	}

	private void OnDisable() {
		_userData.OnNameChanged -= OnNameChanged;
	}

	private void OnNameChanged(string value) {
		_userNameInput.text = value;
		_userNameText.text = value;
	}

	public static Action OnEndEditEvent;
	private void OnEndEdit(string value) {
		_userData.Name = value;
		OnEndEditEvent?.Invoke();
	}

}
}