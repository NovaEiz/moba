using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.TaskExtentions;
using StarFights.General;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace StarFights.Client.Lobby {
public class LobbySelectedHeroView : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler {

	[SerializeField] private RectTransform _draggableContainer;
	[SerializeField] private Rect _area;

	[SerializeField] private Transform _selectedHeroContainer;

	private static LobbySelectedHeroView _instance;

	public static async Task<LobbySelectedHeroView> Load() {
		if (_instance != null) {
			return _instance;
		}
		while (_instance == null) {
			await new WaitForUpdate();
		}

		return _instance;
	}

	private void Awake() {
		_instance = this;
		
		var eventSystemWithoutCanvas = gameObject.AddComponent<EventSystemWithoutCanvas>();
		eventSystemWithoutCanvas.SetComponent(this, _draggableContainer, _area);
	}

	public void OnPointerDown(PointerEventData eventData) {
		
	}

	public void OnPointerUp(PointerEventData eventData) {
		
	}

	public void OnBeginDrag(PointerEventData eventData) {
		
	}

	public void OnDrag(PointerEventData eventData) {
		_selectedHeroContainer.localEulerAngles -= new Vector3(0, eventData.delta.x, 0);
	}

	public void OnEndDrag(PointerEventData eventData) {
		
	}
}
}