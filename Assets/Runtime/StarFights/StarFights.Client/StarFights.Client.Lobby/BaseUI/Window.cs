using System;
using UnityEngine;
using UnityEngine.UI;

namespace StarFights.Client.Lobby.UI {
public class Window : MonoBehaviour, IWindowView {

	[SerializeField] private Button _backButton;
	[SerializeField] private Button _homeButton;

	private void Awake() {
		_backButton.onClick.AddListener(() => {
			gameObject.GetComponent<IViewCloser>().Close();
		});
		_homeButton.onClick.AddListener(() => {
			gameObject.GetComponent<IViewCloser>().Close();
		});
	}

	private IViewOpener _viewOpener;
	private IViewCloser _viewCloser;

	public IViewOpener ViewOpener {
		get {
			if (_viewOpener == null) {
				_viewOpener = gameObject.GetComponent<IViewOpener>();
			}
			return _viewOpener;
		}
	}

	public IViewCloser ViewCloser {
		get {
			if (_viewCloser == null) {
				_viewCloser = gameObject.GetComponent<IViewCloser>();
			}
			return _viewCloser;
		}
	}
}
}