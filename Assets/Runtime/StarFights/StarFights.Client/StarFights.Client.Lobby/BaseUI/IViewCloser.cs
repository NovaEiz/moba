using System;

namespace StarFights.Client.Lobby.UI {
public interface IViewCloser {
	void Close();
	event Action OnClosed;
}
}