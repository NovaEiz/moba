using UnityEngine;

namespace StarFights.Client.Lobby.UI {
[CreateAssetMenu(fileName = "ViewScalerSettingsAsset", menuName = "Nighday/UI Utils/ViewScalerSettingsAsset", order = 0)]
public class ViewScalerSettingsAsset : ScriptableObject {

	[SerializeField] private float _toScale = 0.95f;

	public float ToScale => _toScale;

}
}