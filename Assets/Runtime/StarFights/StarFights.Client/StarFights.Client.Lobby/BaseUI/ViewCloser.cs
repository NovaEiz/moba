using System;
using UnityEngine;

namespace StarFights.Client.Lobby.UI {
public class ViewCloser : MonoBehaviour, IViewCloser {

	//[SerializeField] private Curve _closeAnimation;

	public void Close() {
		gameObject.SetActive(false);
		OnClosed?.Invoke();
	}

	public event Action OnClosed;

}
}