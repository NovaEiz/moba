using System;

namespace StarFights.Client.Lobby.UI {
public interface IToggleView {
	
	/// <summary>
	/// Привязать окно
	/// </summary>
	void BindLoadWindow(Action<Action<IWindowView>> callback);
	
}
}