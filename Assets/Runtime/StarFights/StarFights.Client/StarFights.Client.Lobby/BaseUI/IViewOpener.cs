using System;

namespace StarFights.Client.Lobby.UI {
public interface IViewOpener {
	void Open();
	event Action OnOpened;
}
}