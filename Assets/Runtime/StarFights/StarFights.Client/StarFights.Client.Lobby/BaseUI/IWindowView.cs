using System;

namespace StarFights.Client.Lobby.UI {
public interface IWindowView {
	
	IViewOpener ViewOpener { get; }
	IViewCloser ViewCloser { get; }
	
}
}