using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace StarFights.Client.Lobby.UI {
public class ToggleViewScaler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	[SerializeField] private ToggleViewScalerSettingsAsset _toggleViewScalerSettingsAsset;
	
	[SerializeField] private ToggleView _toggleView;
	[SerializeField] private ViewScaler _viewScaler;

	private void Awake() {
		_toggleView.Toggle.onValueChanged.AddListener((newValue) => {
			if (newValue) {
				ScaleTowards();
			} else {
				ScaleBackwards();
			}
		});
	}

	private void ScaleTowards() {
		_viewScaler.RunScaling(
			_toggleViewScalerSettingsAsset.TowardsAnimationCurveAsset.AnimationCurve,
			_toggleViewScalerSettingsAsset.TowardsDurationTime
		);
	}

	private void ScaleBackwards() {
		_viewScaler.RunScaling(
			_toggleViewScalerSettingsAsset.BackwardsAnimationCurveAsset.AnimationCurve,
			_toggleViewScalerSettingsAsset.BackwardsDurationTime
		);
	}

	public void OnPointerEnter(PointerEventData eventData) {
		if (_toggleView.Toggle.isOn) {
			return;
		}
		ScaleTowards();
	}

	public void OnPointerExit(PointerEventData eventData) {
		if (_toggleView.Toggle.isOn) {
			return;
		}
		ScaleBackwards();
	}
	
}
}