using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace StarFights.Client.Lobby.UI {
public class ButtonViewScaler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	[SerializeField] private ToggleViewScalerSettingsAsset _toggleViewScalerSettingsAsset;
	
	[SerializeField] private ViewScaler _viewScaler;

	private void ScaleTowards() {
		_viewScaler.RunScaling(
			_toggleViewScalerSettingsAsset.TowardsAnimationCurveAsset.AnimationCurve,
			_toggleViewScalerSettingsAsset.TowardsDurationTime
		);
	}

	private void ScaleBackwards() {
		_viewScaler.RunScaling(
			_toggleViewScalerSettingsAsset.BackwardsAnimationCurveAsset.AnimationCurve,
			_toggleViewScalerSettingsAsset.BackwardsDurationTime
		);
	}

	public void OnPointerEnter(PointerEventData eventData) {
		ScaleTowards();
	}

	public void OnPointerExit(PointerEventData eventData) {
		ScaleBackwards();
	}
	
}
}