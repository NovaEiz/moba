﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace StarFights.Client.Lobby.UI {

public class BaseButtonScaler : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	[SerializeField] private AnimationCurve _towardsAnimationCurve;
	[SerializeField] private AnimationCurve _backwardsAnimationCurve;

	[SerializeField] private float _towardsTime = 1f;
	[SerializeField] private float _backwardsTime = 1f;
	[SerializeField] private float _toScale = 0.95f;

	[SerializeField] private Transform _container;

	private float _baseScale;


	private float _moveToScale;

	private Coroutine _coroutine;

	private void Awake() {
		_baseScale = _container.localScale.x;
	}

	public void OnPointerEnter(PointerEventData eventData) {
		//ToScale(_toScale);

		var diff = _toScale - _baseScale;

		var currentScale = _container.localScale.x;

		var minDistance = 0f;

		var minDistanceTime = -1f;
		for (float i = 0; i < 1f; i += 0.05f) {
			var keyValue = _towardsAnimationCurve.Evaluate(i);
			var newScale = diff * keyValue + _baseScale;

			var distance = Mathf.Abs(currentScale - newScale);

			if (distance < minDistance || minDistanceTime < 0) {
				minDistance = distance;
				minDistanceTime = i;
			}
		}

		ToScaleTowards(minDistanceTime);
	}

	public void OnPointerExit(PointerEventData eventData) {
		//ToScale(_baseScale);

		var diff = _toScale - _baseScale;

		var currentScale = _container.localScale.x;

		var minDistance = 0f;

		var minDistanceTime = -1f;
		for (float i = 0; i < 1f; i += 0.05f) {
			var keyValue = _backwardsAnimationCurve.Evaluate(i);
			var newScale = diff * keyValue + _baseScale;

			var distance = Mathf.Abs(currentScale - newScale);

			if (distance < minDistance || minDistanceTime < 0) {
				minDistance = distance;
				minDistanceTime = i;
			}
		}

		ToScaleBackwards(minDistanceTime);
	}

	private void ToScaleBackwards(float time) {
		if (_coroutine != null) {
			StopCoroutine(_coroutine);
		}

		_coroutine = StartCoroutine(ToScaleBackwardsIe(time));
	}

	private IEnumerator ToScaleBackwardsIe(float time) {
		var durationTime = _backwardsTime;
		var elapsedTime = (float)time / 1f * durationTime;

		var diff = _toScale - _baseScale;

		while (elapsedTime < durationTime) {
			var progressTime = elapsedTime / durationTime;
			var keyValue = _backwardsAnimationCurve.Evaluate(progressTime);
			var newScale = diff * keyValue + _baseScale;
			var newLocalScale = Vector3.one * newScale;
			_container.localScale = newLocalScale;

			yield return null;
			elapsedTime += Time.deltaTime;

		}

		_coroutine = null;
	}


	private void ToScaleTowards(float time) {
		if (_coroutine != null) {
			StopCoroutine(_coroutine);
		}

		_coroutine = StartCoroutine(ToScaleTowardsIe(time));
	}

	private IEnumerator ToScaleTowardsIe(float time) {
		var durationTime = _towardsTime;
		var elapsedTime = (float)time / 1f * durationTime;

		var diff = _toScale - _baseScale;

		while (elapsedTime < durationTime) {
			var progressTime = elapsedTime / durationTime;
			var keyValue = _towardsAnimationCurve.Evaluate(progressTime);
			var newScale = diff * keyValue + _baseScale;
			var newLocalScale = Vector3.one * newScale;
			_container.localScale = newLocalScale;

			yield return null;
			elapsedTime += Time.deltaTime;

		}

		_coroutine = null;
	}

	private void ToScale(float scale) {
		_moveToScale = scale;

		if (_coroutine == null) {
			_coroutine = StartCoroutine(ToScaleIe());
		}
	}

	private IEnumerator ToScaleIe() {
		var speed = 1f;
		while (!Mathf.Approximately(_container.localScale.x, _moveToScale)) {
			var step = Time.deltaTime * speed;

			var newScale = Mathf.MoveTowards(_container.localScale.x, _moveToScale, step);
			var newLocalScale = Vector3.one * newScale;
			_container.localScale = newLocalScale;

			yield return null;
		}

		_coroutine = null;
	}

}

}