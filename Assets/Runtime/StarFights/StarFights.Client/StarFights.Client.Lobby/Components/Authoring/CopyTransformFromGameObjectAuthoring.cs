using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.Client.Lobby {

public class CopyTransformFromGameObjectAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(
		Entity entity,
		EntityManager dstManager,
		GameObjectConversionSystem conversionSystem
	) {
		dstManager.AddComponentData(entity, new CopyTransformFromGameObject());
	}

}
}