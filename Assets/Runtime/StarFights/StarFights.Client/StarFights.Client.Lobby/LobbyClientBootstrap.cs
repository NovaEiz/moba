using System;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.General.EntitiesExtentions;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.Client;
using StarFights.GameCore.Ghosts;
using StarFights.General;
using Unity.NetCode;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;
using World = Unity.Entities.World;

namespace StarFights.Client.Lobby {
public class LobbyClientBootstrap {

	private Nighday.Application.World AppWorld;
	private World World;
	
	private string GetMapNameByModeName(string modeName) {
		return _gameModesDatabaseAsset.GetMapNameByModeName(modeName);
	}

	private GameModesDatabaseAsset _gameModesDatabaseAsset;

	private async Task LoadGameModesDatabaseAsset() {
		_gameModesDatabaseAsset = await GameModesDatabaseAsset.LoadAsync();
	}
	
	private Func<string> _GetActiveGameSceneNameAction;

	public void SetGetActiveGameSceneNameAction(Func<string> action) {
		_GetActiveGameSceneNameAction = action;
	}

	public void Initialize(Nighday.Application.World appWorld) {
		LoadGameModesDatabaseAsset();
		
		AppWorld = appWorld;
		
		App.World.GetExistingSystem<MatchmakingLobbyClientSystem>().SetMapNameByModeNameAction(GetMapNameByModeName);

		var matchmakingLobbyClientControllerSystem = appWorld.CreateSystem<MatchmakingLobbyClientControllerSystem>();
		matchmakingLobbyClientControllerSystem.SetGetActiveGameSceneNameAction(() => {
			return _GetActiveGameSceneNameAction();
		});
		
		Setup();
		
		
		World = Nighday.Online.Core.ClientServerBootstrap.CreateClientWorld(Unity.Entities.World.DefaultGameObjectInjectionWorld, "Lobby Client");
		var simulationSystemGroup = World.GetExistingSystem<ClientSimulationSystemGroup>();
		
		simulationSystemGroup.AddSystemToUpdateList(World.CreateSystem<LobbySelectedHeroSystem>());
		simulationSystemGroup.AddSystemToUpdateList(World.CreateSystem<CreateHeroSystem>());

		var viewAssetReferenceLoadClientSystem = World.CreateSystem<ViewAssetReferenceLoadClientSystem>();
		viewAssetReferenceLoadClientSystem.ContainerName = "Lobby";
		simulationSystemGroup.AddSystemToUpdateList(viewAssetReferenceLoadClientSystem);
		
		
		simulationSystemGroup.AddSystemToUpdateList(World.CreateSystem<HeroesWindowSystem>());

		simulationSystemGroup.SortSystems();
		
		LoadLobbyScene();
	}

	protected async Task Setup() {
		MatchmakingLobbyClientSystem matchmakingLobbyClientSystem = null;
		while (matchmakingLobbyClientSystem == null) {
			await Task.Yield();
			
			matchmakingLobbyClientSystem = AppWorld.GetSystem<MatchmakingLobbyClientSystem>();
		}
	}

	
	public async Task LoadLobbyScene() {
		try {
			var asyncOperation = Addressables.LoadSceneAsync("Lobby", LoadSceneMode.Additive);
			await asyncOperation.Task;

			SceneManager.SetActiveScene(asyncOperation.Result.Scene);
		}
		catch (Exception e) {
			Debug.LogError("LoadingSystem LoadLobbyScene Error LoadSceneAsync e = " + e);
			return;
		}

			await new WaitForUpdate();

		var lobby = AppWorld.DataManager.GetGameObject("Lobby");

		var convertToEntityInWorld = lobby.AddComponent<ConvertToEntityInWorld>();
		convertToEntityInWorld.World = World;
		convertToEntityInWorld.ConversionMode = ConvertToEntity.Mode.ConvertAndInjectGameObject;
		convertToEntityInWorld.OnConvertFinished += (ent) => {
			
		};
		convertToEntityInWorld.Convert();

	}
	
}
}