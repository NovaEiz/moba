using System.Threading.Tasks;
using Nighday.Core.Loading;
using Nighday.GameCamera;
using Nighday.General.TaskExtentions;
using Nighday.Matchmaking.General;
using Nighday.Online;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using StarFights.General;
using Unity.Entities;
using Unity.NetCode;
using Unity.Transforms;

namespace StarFights.Client.Game {

[DisableAutoCreation]
public class SetCommandTargetComponentClientSystem : ComponentSystem {
	
	private struct NeedSetTarget : IComponentData {}

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<UserId>();
		RequireSingletonForUpdate<NeedSetTarget>();
		RequireSingletonForUpdate<FightCameraTargetComponent>();
		
		EntityManager.CreateEntity(typeof(NeedSetTarget));
	}

	protected override void OnUpdate() {
		var userId = GetSingleton<UserId>();

		Entities
			.WithAll<Hero>()
			.ForEach((Entity entity,
					ref OwnerUserId ownerUserId
					) => {
						//TODO: Тут можно в будущем добавить клиенту флаг что герой уже нашелся и выбран для передачи данных
						//Nighday.WebNetwork.WebDebug.Log("SetCommandTargetComponentClientSystem: user.Id = " + user.Id + "; tank ownerUserId = " + ownerUserId.Value);

						if (ownerUserId.Value == userId.Value) {
							EntityManager.AddBuffer<HeroInput>(entity);	

							var fightCameraTargetComponentEntity = GetSingletonEntity<FightCameraTargetComponent>();

							DynamicBuffer<Child> childBuffer = default;
							if (!EntityManager.HasComponent<Child>(entity)) {
								childBuffer = EntityManager.AddBuffer<Child>(entity);
							} else {
								childBuffer = EntityManager.GetBuffer<Child>(entity);
							}
							childBuffer.Add(new Child {
								Value = fightCameraTargetComponentEntity
							});
							EntityManager.AddComponentData(fightCameraTargetComponentEntity, new Parent {
								Value = entity
							});
							EntityManager.AddComponentData(fightCameraTargetComponentEntity, new LocalToParent {
							});
							EntityManager.SetComponentData(fightCameraTargetComponentEntity,
															new Translation {
							});
							EntityManager.AddComponentData(fightCameraTargetComponentEntity,
															new CopyTransformWithoutRotationToGameObject());
							
							var commandTargetComponent = GetSingleton<CommandTargetComponent>();
							var trackingCamera = GetSingleton<TrackingCamera>();
							trackingCamera.Target = entity;
							trackingCamera.Distance *= 3f;
							SetSingleton(trackingCamera);

							commandTargetComponent.targetEntity = entity;
							
							SetSingleton(commandTargetComponent);
							
							EntityManager.DestroyEntity(GetSingletonEntity<NeedSetTarget>());

							Task.Factory.StartNew(WaitOneFrame);
						} else {
							if (!EntityManager.HasComponent<HeroInput>(entity)) {
								EntityManager.AddBuffer<HeroInput>(entity);	

							}
						}
					});
	}

	private async Task WaitOneFrame() {
		await new WaitForUpdate();
		
		//Nighday.Application.App.World.DataManager.GetComponentObject<LoadingView>().Hide();
	}
}
}