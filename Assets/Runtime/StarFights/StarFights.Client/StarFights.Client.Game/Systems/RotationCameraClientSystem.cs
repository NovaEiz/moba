using Nighday.GameCamera;
using Nighday.Matchmaking.General;
using Nighday.TouchControllers;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.Client.Game {

[DisableAutoCreation]
[UpdateAfter(typeof(RotationAndScalingTouchSystem))]
public class RotationCameraClientSystem : ComponentSystem {
	

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<UserId>();
		RequireSingletonForUpdate<TrackingCamera>();
	}

	protected override void OnUpdate() {
		var trackingCameraEntity = GetSingletonEntity<TrackingCamera>();
		var trackingCamera = GetSingleton<TrackingCamera>();

		var camera = EntityManager.GetComponentObject<Camera>(trackingCameraEntity);

		/*
		Entities
			.ForEach((Entity entity,
					ref RotationAndScalingTouchSystem.RotationOneFrameEvent rotationOneFrameEvent
					) => {
						trackingCamera.RotationAngles += rotationOneFrameEvent.Offset;
					});
		Entities
			.ForEach((Entity entity,
					ref RotationAndScalingTouchSystem.ScalingOneFrameEvent scalingOneFrameEvent
					) => {
						trackingCamera.Distance += scalingOneFrameEvent.ScaleChangeValue;
					});
		 */

		var cameraEulerAngles = camera.transform.eulerAngles;
		trackingCamera.RotationAngles = new float2(cameraEulerAngles.x, cameraEulerAngles.y);
		
		SetSingleton(trackingCamera);
	}
}
}