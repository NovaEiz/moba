using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Matchmaking.Client;
using StarFights.Client.Game.UI;
using StarFights.GameCore.CapturingCrystals;
using Unity.Entities;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.Client.Game {

[DisableAutoCreation]
public class GameTranslationToLobbyThroughGameResultWindowClientSystem : SystemBase {
	
	private struct EmptyBlank : IComponentData {}

	private MatchmakingLobbyClientSystem _matchmakingLobbyClientSystem;
	
	protected override void OnCreate() {
		_matchmakingLobbyClientSystem = App.World.GetExistingSystem<MatchmakingLobbyClientSystem>();
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<EmptyBlank>()
			.WithAll<GameEnded>()
			.ForEach((Entity entity
					) => {
						EntityManager.AddComponentData(entity, new EmptyBlank());

						SwitchLobbyGameToggleView.ToLobby();
						DisconnectAndLeaveGame();
					}).WithStructuralChanges().Run();
	}

	private async Task DisconnectAndLeaveGame() {
		await _matchmakingLobbyClientSystem.DisconnectFromGame();
		await _matchmakingLobbyClientSystem.LeaveGame();
	}
}
}