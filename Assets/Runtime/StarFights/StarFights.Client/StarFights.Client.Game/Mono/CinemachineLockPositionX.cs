using UnityEngine;
using Cinemachine;
using Unity.Mathematics;

namespace StarFights.Client.Game {

[ExecuteInEditMode] [SaveDuringPlay] [AddComponentMenu("CinemachineLockPositionX")] // Hide in menu
public class CinemachineLockPositionX : CinemachineExtension {

	[SerializeField] private float _fixedPositionX;
	[SerializeField] private float _fixedEulerAngleY = 180;

	protected override void PostPipelineStageCallback(
		CinemachineVirtualCameraBase vcam,
		CinemachineCore.Stage stage, ref CameraState state, float deltaTime) {
		if (stage == CinemachineCore.Stage.Body) {
			var pos = state.RawPosition;
			pos.x = _fixedPositionX;
			state.RawPosition = pos;
		} else if (stage == CinemachineCore.Stage.Aim) {
			var rawOrientation = state.RawOrientation;
			
			var angle = Mathf.RoundToInt(rawOrientation.eulerAngles.y / _fixedEulerAngleY) * _fixedEulerAngleY;
			
			var eulerAngles = rawOrientation.eulerAngles;
			eulerAngles.y = angle;
			state.RawOrientation = Quaternion.Euler(eulerAngles);
		}
	}
}

}