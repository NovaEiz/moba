using System.Threading.Tasks;
using Nighday.Application;
using Nighday.CoreMOBA.Client;
using Nighday.CoreMOBA.Game;
using Nighday.GameCamera;
using Nighday.General.PhysicsExtentions;
using Nighday.Matchmaking.Client;
using Nighday.TouchControllers;
using StarFights.Client.Game.UI;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Attack;
using StarFights.GameCore.CapturingCrystals;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Projectile;
using StarFights.GameCore.Unit;
using Unity.Entities;
using Unity.NetCode;
using ComponentSystemGroup = Unity.Entities.ComponentSystemGroup;
using World = Unity.Entities.World;

namespace StarFights.Client.Game {
public class GameClientBootstrap : IGameClientBootstrap {

	public void InitializeAsync(World world) {
		WorldInitialization<GameClientSystemGroup>(world);
	}
	
	public void WorldInitialization<T>(World world) where T : ComponentSystemGroup {

		// Groups

		var simulationSystemGroup = world.GetExistingSystem<ClientSimulationSystemGroup>();
		var ghostSimulationSystemGroup = world.GetExistingSystem<GhostSimulationSystemGroup>();
		var ghostPredictionSystemGroup = world.GetExistingSystem<GhostPredictionSystemGroup>();
		var abilityGhostSystemGroup = world.GetExistingSystem<AbilityGhostSystemGroup>();
		//var fixedStepSimulationSystemGroup = world.GetExistingSystem<FixedStepSimulationSystemGroup>();
		
		//===
		// Systems
		
		//var changeEquipmentTankSystem = world.CreateSystem<ChangeEquipmentTankSystem>(); gameClientSystemGroup.AddSystemToUpdateList(changeEquipmentTankSystem);
		
		//var heroMovementInputClientSystem = world.CreateSystem<HeroInputClientSystem>(); gameClientSystemGroup.AddSystemToUpdateList(heroMovementInputClientSystem);
		
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<DamageReceiveClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<LeftUntilTheResurrectionClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<GameCountdownToEndClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<MoveSpeedInitServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ProjectileGhostSpawnClassificationClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<GameStartedRequestClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<UnitHealthBarViewControllerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<MineHeroViewControllerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<GameTranslationToLobbyThroughGameResultWindowClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<TeamViewClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CollectedCrystalsViewClientSystem>());

		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<MovementTouchSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<RotationAndScalingTouchSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ViewAssetReferenceLoadClientSystem>());

		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<MovementJoystickControllerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<UnitInitializationClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<RotationCameraClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<SetCommandTargetComponentClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<RotationContainerByTeamClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<TrackingCameraSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<PostSetAnimationStateClientSystem>());

		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<MovementInputClientSystem>());
		
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<AnimatorViewHeroClientSystem>());


		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<HeroGhostSystem>());
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<AnimatorHeroGhostSystem>());
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<HeroInputClientSystem>());
		
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<AutoMovementToForwardGhostSystem>());
		
		ghostPredictionSystemGroup.AddSystemToUpdateList(world.CreateSystem<AttackJoystickInputClientSystem>());
		
		//fixedStepSimulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CollisionSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<CollisionSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<TriggerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ProjectileCollisionServerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<ProjectileLifeServerSystem>());

		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<AbilityMagazineGhostSystem>());

		abilityGhostSystemGroup.AddSystemToUpdateList(world.CreateSystem<RunAbilityGhostSystem>());
		abilityGhostSystemGroup.AddSystemToUpdateList(world.CreateSystem<MultipleProjectileAbilityCastGhostSystem>());
		abilityGhostSystemGroup.AddSystemToUpdateList(world.CreateSystem<OneShotProjectileAbilityCastGhostSystem>());
		
		
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<UnitNameViewControllerSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<RpcUnitNameResultClientSystem>());
		simulationSystemGroup.AddSystemToUpdateList(world.CreateSystem<UnitNameRequestClientSystem>());
		
		//===
		//rpc systems
		
		//var unitNameRequestClientSystem = world.CreateSystem<UnitNameRequestClientSystem>(); gameClientSystemGroup.AddSystemToUpdateList(unitNameRequestClientSystem);
		//var rpcUnitNameResultClientSystem = world.CreateSystem<RpcUnitNameResultClientSystem>(); gameClientSystemGroup.AddSystemToUpdateList(rpcUnitNameResultClientSystem);

		//===
		
		
		//===
		// Init systems
		
		//===
		// Sort systems

		ghostPredictionSystemGroup.SortSystems();
		ghostSimulationSystemGroup.SortSystems();
		simulationSystemGroup.SortSystems();
	}
	
}
}