using Nighday.GameCamera;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Unit;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using Unity.Physics.Systems;
using UnityEngine;

namespace StarFights.Client.Game.UI {

[DisableAutoCreation]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
[UpdateBefore(typeof(GhostSimulationSystemGroup))]
public class MovementInputClientSystem : SystemBase {

	private ClientSimulationSystemGroup _clientSimulationSystemGroup;

	protected override void OnCreate() {
		RequireSingletonForUpdate<TrackingCamera>();
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<MovementJoystickView>();

		_clientSimulationSystemGroup = World.GetExistingSystem<ClientSimulationSystemGroup>();
	}

	protected override void OnUpdate() {
		var trackingCamera = GetSingleton<TrackingCamera>();
		var commandTargetComponent = GetSingleton<CommandTargetComponent>();
		var entity = commandTargetComponent.targetEntity;
		if (!EntityManager.Exists(entity)) {
			return;
		}
		
		var serverTick = _clientSimulationSystemGroup.ServerTick;;

		var movementInputComponent = EntityManager.GetComponentData<MovementInputComponent>(entity);
		
		Entities
			.ForEach((Entity joystickEntity,
					MovementJoystickView movementJoystickView
					) => {
				if (!movementJoystickView.Movement) {
					movementInputComponent.movementAngle = -1f;
					EntityManager.SetComponentData(entity, movementInputComponent);
					return;
				}
				movementInputComponent.movementAngle 
					= movementJoystickView.MovementAngle + trackingCamera.RotationAngles.y;

				EntityManager.SetComponentData(entity, movementInputComponent);
			}).WithStructuralChanges().Run();
		
	}

}
}