using System;
using StarFights.General;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace StarFights.Client.Game.UI {
public class JoystickView : MonoBehaviour, IBeginDragHandler, IPointerUpHandler, IDragHandler, IEndDragHandler, IPointerDownHandler {

	public enum CornersSide {
		LeftCornersSide,
		RightCornersSide,
	}
	
	[SerializeField] protected RectTransform _canvasRect;
	[SerializeField] protected RectTransform _basePart;
	[SerializeField] protected RectTransform _cancelPart;
	[SerializeField] protected RectTransform _movingPart;

	[SerializeField] protected Image _movingPartImage;

	[Header("Settings")]
	[Tooltip("Если true, то по нажатию не перемещает BasePart в позицию курсора")]
	[SerializeField] protected bool _positionFreeze;
	[Tooltip("Если true, то перемещает BasePart за курсором, если курсор вышел за пределы _maxMovingRadius. Работает только если _positionFreeze == false")]
	[SerializeField] protected bool _movingOverMaxRadius;

	[Space]
	[Tooltip("Если true, то используется круглая область для тачей, а не прямоугольная от RectTransform(_basePart)")]
	[SerializeField] protected bool _useRadiusBasePartForTouch;
	
	[Space]
	[Tooltip("Если true, то для джойстика будет использоваться указанная область(_area)")]
	[SerializeField] protected bool _useArea;
	[SerializeField] protected Rect _area;

	protected Vector3 _basePosition;

	protected float _maxMovingRadius;
	protected float _cancelRadius;

	private bool _pointerInCancelArea;
	public bool PointerInCancelArea => _pointerInCancelArea;
	
	protected RectTransform _rectTransform;

	private Vector2 _direction;

	private float _opacityWhenNotActive = 0.5f;

	public float GetAngle() {
		return -Vector2.SignedAngle(Vector2.up, _direction) + 360;
	}
	public float GetDistance() {
		return _direction.magnitude / _maxMovingRadius;
	}
	public Vector2 GetDirection() {
		return _direction;
	}
	
	private void Awake() {
		_maxMovingRadius = _basePart.rect.width/2;
		_cancelRadius = _cancelPart.rect.width/2;
		
		_basePosition = _basePart.localPosition;

		_rectTransform = GetComponent<RectTransform>();

		if (_useArea) {
			var eventSystemWithoutCanvas = gameObject.AddComponent<EventSystemWithoutCanvas>();
			eventSystemWithoutCanvas.SetComponent(this, _canvasRect, _area);
		}

		SetOpacity(_opacityWhenNotActive);
	}

	private void SetOpacity(float value) {
		var color = _movingPartImage.color;
		color.a = value;
		_movingPartImage.color = color;
	}

	protected RectTransform BasePart {
		get {
			/*
			if (_basePart == null) {
				_basePart = transform.GetComponent<RectTransform>();
			}
			 */
			return _basePart;
		}
	}

	protected RectTransform MovingPart => _movingPart;


	private int _pointerId;
	private bool _down;
	private bool _dragging;

	public Action OnDragEvent;
	public Action OnBeginDragEvent;
	public Action OnEndDragEvent;
	public Action OnClickInCancelAreaEvent;
	public Action OnUpWithDirectionEvent;
	public Action OnEnterCancelAreaEvent;
	public Action OnExitCancelAreaEvent;
	
	public Action OnUpdate;
	
	private bool _exitedTheCancelArea;
	
	public void OnBeginDrag(PointerEventData eventData) {
		OnBeginDragEvent?.Invoke();
		_dragging = true;

		SetOpacity(1);
	}

	public void OnDrag(PointerEventData eventData) {
		if (!_down) {
			return;
		}
		if (_pointerId != eventData.pointerId) {
			return;
		}
		
		OnBeginDragEvent?.Invoke();

		var basePosition = _basePart.localPosition;
		Vector3 pointerPosition = GetPosition(eventData.position);

		var offset = pointerPosition - basePosition;

		if (offset.magnitude > _maxMovingRadius) {
			var oldOffset = offset;
			var newOffset = oldOffset.normalized * _maxMovingRadius;
			offset = newOffset;

			if (_movingOverMaxRadius && !_positionFreeze) {
				var forBasePartOffset = oldOffset - newOffset;
				SetJoystickPosition(((Vector3)_basePart.localPosition) + forBasePartOffset);
			}
		}

		if (offset.magnitude < _cancelRadius) {
			if (!_pointerInCancelArea) {
				OnEnterCancelAreaEvent?.Invoke();
				_pointerInCancelArea = true;
			}
		} else {
			_exitedTheCancelArea = true;
			if (_pointerInCancelArea) {
				_pointerInCancelArea = false;
				OnExitCancelAreaEvent?.Invoke();
			}
		}
		
		_direction = offset;

		SetMovingPartPosition(offset);
		
		OnDragEvent?.Invoke();
	}

	public void OnEndDrag(PointerEventData eventData) {
		if (_pointerId != eventData.pointerId) {
			return;
		}
		
		OnEndDragEvent?.Invoke();
		_dragging = false;
		
		SetOpacity(_opacityWhenNotActive);
	}


	private void SetJoystickPosition(Vector3 position) {
		//_basePart.anchoredPosition3D = position;
		_basePart.localPosition = position;
	}
	private void SetMovingPartPosition(Vector3 position) {
		//_movingPart.anchoredPosition3D = position;
		_movingPart.localPosition = position;
	}

	public void OnPointerDown(PointerEventData eventData) {
		if (_useRadiusBasePartForTouch && !_useArea && ((GetPosition(eventData.pressPosition)) - _basePart.localPosition).magnitude > _maxMovingRadius) {
			return;
		}
		
		SetOpacity(1);

		_pointerInCancelArea = true;
		_exitedTheCancelArea = false;
		_down = true;
		_pointerId = eventData.pointerId;

		if (_positionFreeze) {
			OnDrag(eventData);
		} else {
			SetJoystickPosition(GetPosition(eventData.pressPosition));
			SetMovingPartPosition(Vector3.zero);
		}
	}
	
	private Vector3 GetPosition(Vector3 position) {
		RectTransformUtility.ScreenPointToLocalPointInRectangle(
			_rectTransform,
			position,
			null,
			out Vector2 localPoint
		);
		return localPoint;
	}

	public void OnPointerUp(PointerEventData eventData) {
		SetJoystickPosition(_basePosition);
		SetMovingPartPosition(Vector3.zero);
		
		_down = false;

		if (_pointerInCancelArea && !_exitedTheCancelArea) {
			OnClickInCancelAreaEvent?.Invoke();
		} else {
			OnUpWithDirectionEvent?.Invoke();
		}
		
		SetOpacity(_opacityWhenNotActive);
	}

	private void OnDestroy() {
		if (_dragging) {
			_dragging = false;
			OnEndDragEvent?.Invoke();
		}
	}

	private void OnDisable() {
		if (_dragging) {
			_dragging = false;
			OnEndDragEvent?.Invoke();
		}
	}

	private void Update() {
		OnUpdate?.Invoke();
	}

}
}