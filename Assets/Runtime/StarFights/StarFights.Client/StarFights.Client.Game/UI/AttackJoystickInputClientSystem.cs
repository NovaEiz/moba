using Nighday.GameCamera;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Unit;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.Client.Game.UI {

[DisableAutoCreation]
/*

[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
[UpdateAfter(typeof(GhostSimulationSystemGroup))]
*/

[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
[UpdateBefore(typeof(HeroGhostSystem))]
[UpdateBefore(typeof(HeroInputClientSystem))]
public class AttackJoystickInputClientSystem : SystemBase {

	private GhostPredictionSystemGroup m_PredictionGroup;
	private ClientSimulationSystemGroup _clientSimulationSystemGroup;

	protected override void OnCreate() {
		RequireSingletonForUpdate<TrackingCamera>();
		RequireSingletonForUpdate<CommandTargetComponent>();
		
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
		_clientSimulationSystemGroup = World.GetExistingSystem<ClientSimulationSystemGroup>();
	}

	protected override void OnUpdate() {
		var trackingCamera = GetSingleton<TrackingCamera>();
		var commandTargetComponent = GetSingleton<CommandTargetComponent>();
		var entity = commandTargetComponent.targetEntity;

		if (!EntityManager.Exists(entity)) {
			return;
		}

		//_clientSimulationSystemGroup.ServerTick;//
		//var tick = m_PredictionGroup.PredictingTick;
		var tick = _clientSimulationSystemGroup.ServerTick;

		var abilityInputComponent = EntityManager.GetComponentData<AbilityInputComponent>(entity);
		var heroAbilitiesBuffer = EntityManager.GetBuffer<AbilityBufferElement>(entity);
		var heroAbilitiesArray = heroAbilitiesBuffer.ToNativeArray(Allocator.Temp);
		var heroAbilitiesArrayLen = heroAbilitiesArray.Length;

		var attackAngle = abilityInputComponent.attackAngle;
		var abilitySlotIndex = abilityInputComponent.abilitySlotIndex;
		var attackCommandClientTick = abilityInputComponent.attackCommandClientTick;

		Entities
			.ForEach((Entity joystickEntity,
					AttackJoystickView attackJoystickView
					) => {
				if (!attackJoystickView.Attack) {
					return;
				}
				var joystickAttackAngle = attackJoystickView.AttackAngle;

				if (attackJoystickView.AbilitySlotIndex >= heroAbilitiesArrayLen) {
					return;
				}

				var heroAbilitiesBufferElement = heroAbilitiesArray[attackJoystickView.AbilitySlotIndex];
				var abilityEntity = heroAbilitiesBufferElement.AbilityEntity;

				if (EntityManager.HasComponent<AbilityMagazineComponent>(abilityEntity)) {
					var heroMagazineComponent = EntityManager.GetComponentData<AbilityMagazineComponent>(abilityEntity);
					if (heroMagazineComponent.Amount == 0) {
						return;
					}

					heroMagazineComponent.Amount--;
				} else if (EntityManager.HasComponent<AbilityMagazineComponent>(abilityEntity)) {
					//Другая способность. Он Rage, еще будет от Маны
				}

				// Значит атаку производить в сторону ближайшей цели
				if (joystickAttackAngle < 2f) {
					joystickAttackAngle = GetToNearestEnemyTargetGhostAngle();
					if (EntityManager.HasComponent<TurnedByTeamAngleComponent>(entity)) {
						joystickAttackAngle += EntityManager.GetComponentData<TurnedByTeamAngleComponent>(entity).Angle;
					}
				}
				var attackAngle_ = joystickAttackAngle + trackingCamera.RotationAngles.y;

				attackAngle = attackAngle_;
				attackCommandClientTick = tick;
				abilitySlotIndex = attackJoystickView.AbilitySlotIndex;
			}).WithStructuralChanges().Run();
		
		abilityInputComponent.attackAngle = attackAngle;
		abilityInputComponent.attackCommandClientTick = attackCommandClientTick;
		abilityInputComponent.abilitySlotIndex = abilitySlotIndex;
		EntityManager.SetComponentData(entity, abilityInputComponent);
	}
	
	
	private float GetToNearestEnemyTargetGhostAngle() {
		var entity = GetSingleton<CommandTargetComponent>().targetEntity;
		var localToWorld = EntityManager.GetComponentData<LocalToWorld>(entity);
		var teamId = EntityManager.GetComponentData<TeamId>(entity);
		var nearestEnemyTargetPosition = FindNearestEnemyTargetGhostPosition(entity, localToWorld.Position, teamId);
		var toNearestEnemyTargetDirection = nearestEnemyTargetPosition - localToWorld.Position;
		var toNearestEnemyTargetQuaternion = quaternion.LookRotation(toNearestEnemyTargetDirection, new float3(0,1,0));
		var angle = ((Quaternion)(toNearestEnemyTargetQuaternion)).eulerAngles.y;
		return angle + 360;
	}
	
	private float3 FindNearestEnemyTargetGhostPosition(Entity entity, float3 position, TeamId teamId) {
		var targetPosition = float3.zero;
		var targetEntity = Entity.Null;
		var minDistance = 0f;
		Entities
			.WithNone<DeadState>()
			.WithAll<Unit>()
			.ForEach((Entity enemyEntity,
					ref GhostComponent ghostComponent,
					ref LocalToWorld localToWorld,
					ref TeamId enemyTeamId
					) => {
						if (enemyTeamId.Value == teamId.Value) {
							return;
						}

						var currentDistance = math.distance(localToWorld.Position, position);
						if (currentDistance < minDistance || targetEntity.Equals(Entity.Null)) {
							minDistance = currentDistance;
							targetEntity = enemyEntity;
							targetPosition = localToWorld.Position;
						}
					}).Run();

		return targetPosition;
	}

}
}