using System;
using Nighday.Application;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace StarFights.Client.Game.UI {
public class SwitchLobbyGameToggleView : MonoBehaviour {

	public enum State {
		Lobby,
		Game
	}
	
	[SerializeField] private State _state;
	
	[Space]
	[SerializeField] private Button _button;

	private void Awake() {
		_button.onClick.AddListener(() => {
			if (_state == State.Lobby) {
				ToGame();
			} else if (_state == State.Game) {
				ToLobby();
			}
		});
	}

	private static void SetActiveLobbySceneAndGameObject(string name, bool state) {
		if (state) {
			SceneManager.SetActiveScene(SceneManager.GetSceneByName(name));
		}
		var obj = App.World.DataManager.GetGameObject(name);
		obj.SetActive(state);
	}
	private static void SetActiveGameSceneAndGameObject(string name, bool state) {
		if (state) {
			SceneManager.SetActiveScene(SceneManager.GetSceneByName(name));
		}
		var obj = App.World.DataManager.GetGameObject("Game");
		obj.SetActive(state);
	}

	private static string ActiveGameSceneName => Nighday.CoreMOBA.Client.Bootstrap.ActiveGameSceneName;

	public static void ToLobby() {
		SetActiveLobbySceneAndGameObject("Lobby", true);
		SetActiveGameSceneAndGameObject(ActiveGameSceneName, false);
	}

	public static void ToGame() {
		SetActiveGameSceneAndGameObject(ActiveGameSceneName, true);
		SetActiveLobbySceneAndGameObject("Lobby", false);
	}

}
}