using System;
using UnityEngine;

namespace StarFights.Client.Game.UI {
public class AttackJoystickView : MonoBehaviour {
	

	[SerializeField] protected JoystickView _joystickView;

	public JoystickView JoystickView => _joystickView;

	[Space]
	[SerializeField] protected int _abilitySlotIndex;
	public int AbilitySlotIndex => _abilitySlotIndex;

	
	
	private float _attackAngle = -1f;
	public bool Attack => _attackAngle >= 0 || _attackAngle < -2f;
	public float AttackAngle {
		get {
			var angle = _attackAngle;
			_attackAngle = -1;
			return angle;
		}
	}

	private void Awake() {
		var joystickView = JoystickView;

		joystickView.OnBeginDragEvent += () => {
		};
		joystickView.OnDragEvent += () => {
			if (joystickView.PointerInCancelArea) {
			} else {
			}
		};

		joystickView.OnEndDragEvent += () => {
			_attackAngle = _joystickView.GetAngle();
			// Выполнить способность
		};
		joystickView.OnClickInCancelAreaEvent += () => {
			_attackAngle = -3f;
		};
	}
	
	

}
}