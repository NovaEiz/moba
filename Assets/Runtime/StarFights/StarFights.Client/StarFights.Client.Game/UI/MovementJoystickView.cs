using System;
using UnityEngine;

namespace StarFights.Client.Game.UI {
public class MovementJoystickView : MonoBehaviour {

	[SerializeField] protected JoystickView _joystickView;

	public JoystickView JoystickView => _joystickView;
	
	
	private float _movementAngle = -1f;
	public bool Movement => _movementAngle >= 0;
	public float MovementAngle => _movementAngle;

	private bool _joystickActive;

	private void Awake() {
		var joystickView = JoystickView;

		joystickView.OnBeginDragEvent += () => {
			_joystickActive = true;
		};
		joystickView.OnDragEvent += () => {
			if (joystickView.PointerInCancelArea) {
				_movementAngle = -1f;
			} else {
				_movementAngle = _joystickView.GetAngle();
			}
		};

		joystickView.OnEndDragEvent += () => {
			_movementAngle = -1f;
			_joystickActive = false;
		};
		joystickView.OnClickInCancelAreaEvent += () => {
			
		};
	}

#if UNITY_STANDALONE
	private void Update() {
		if (_joystickActive) {
			return;
		}
		var horizontal = 0;
		var vervical = 0;
		if (Input.GetKey(KeyCode.W)) {
			vervical = 1;
		}
		if (Input.GetKey(KeyCode.S)) {
			vervical = -1;
		}
		if (Input.GetKey(KeyCode.A)) {
			horizontal = -1;
		}
		if (Input.GetKey(KeyCode.D)) {
			horizontal = 1;
		}
		if (horizontal != 0 || vervical != 0) {
			_movementAngle = Quaternion.LookRotation(new Vector3(horizontal, 0, vervical)).eulerAngles.y;
		} else {
			_movementAngle = -1;
		}
	}
#endif

}
}