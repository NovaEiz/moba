using System;
using System.Threading.Tasks;
using Nighday.GameCamera;
using Nighday.Matchmaking.General;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Movement;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace StarFights.Client.Game.UI {

[DisableAutoCreation]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class MovementJoystickControllerSystem : ComponentSystem {

	protected override void OnCreate() {
		
		RequireSingletonForUpdate<MovementJoystickView>();
		RequireSingletonForUpdate<TrackingCamera>();
		
		LoadMovementJoystickCursorInWorld();
	}

	private Transform _movementJoystickCursorInWorldTr;
	private Animator _targetAnimator;
	
	private async Task LoadMovementJoystickCursorInWorld() {
		try {
			var asyncOperation = Addressables.LoadAssetAsync<GameObject>("MovementJoystickCursorInWorld");
			await asyncOperation.Task;
			_movementJoystickCursorInWorldTr = GameObject.Instantiate(asyncOperation.Result.GetComponent<Transform>());
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
	}

	private void SetMovementJoystickCursorInWorldByAngle(float3 targetPosition, float distance, float angle) {
		var trackingCamera = GetSingleton<TrackingCamera>();
		
		var rot = quaternion.Euler(new float3(0, math.radians(angle + trackingCamera.RotationAngles.y), 0));
		var offsetByAngle = math.mul(rot, new float3(0,0,1));

		var position = targetPosition + offsetByAngle * distance;

		_movementJoystickCursorInWorldTr.position = position;
	}
	
	private void SetMovementJoystickCursorInWorldByAngle(Transform parent, float distance, float angle) {
		var rot = quaternion.Euler(new float3(0, math.radians(angle), 0));
		//rot = Quaternion.Inverse(parent.rotation) * rot;
		var offsetByAngle = math.mul(rot, new float3(0,0,1));

		var position = offsetByAngle * distance;

		if (_movementJoystickCursorInWorldTr.parent != parent) {
			//_movementJoystickCursorInWorldTr.SetParent(parent.parent, true);
			_movementJoystickCursorInWorldTr.SetParent(parent, true);
		}
		_movementJoystickCursorInWorldTr.position = (Vector3)position + parent.position;
	}

	private bool _dragging;

	protected override void OnUpdate() {
		var movementJoystickViewEntity = GetSingletonEntity<MovementJoystickView>();
		var movementJoystickView = EntityManager.GetComponentObject<MovementJoystickView>(movementJoystickViewEntity);

		var joystickView = movementJoystickView.JoystickView;
		/*
		joystickView.OnDragEvent += () => {
			var eventEntity = EntityManager.CreateEntity(typeof(MovementInputClientSystem.MovementOneFrameEvent));
			var movementOneFrameEvent = new MovementInputClientSystem.MovementOneFrameEvent {
				Angle = joystickView.GetAngle(),
				Direction = joystickView.GetDirection()
			};
			EntityManager.SetComponentData(eventEntity, movementOneFrameEvent);
		};
		 */

		joystickView.OnBeginDragEvent += () => {
			//movementInputClientSystem.GetMovementDistance += joystickView.GetDistance;
			_dragging = true;
			
			if (_targetAnimator != null) {
				_movementJoystickCursorInWorldTr.gameObject.SetActive(true);
			} else {
				var commandTargetComponent = GetSingleton<CommandTargetComponent>();
				if (!EntityManager.Exists(commandTargetComponent.targetEntity)) {
					return;
				}
				var childBuffer =
					EntityManager.GetBuffer<Child>(commandTargetComponent.targetEntity);

				Animator animator = null;
				//float3 animatorEntityPosition = default;
				foreach (var childBufferElement in childBuffer) {
					if (EntityManager.HasComponent<Animator>(childBufferElement.Value)
					&&
						EntityManager.HasComponent<LocalToWorld>(childBufferElement.Value)) {
						animator = EntityManager.GetComponentObject<Animator>(childBufferElement.Value);
						//animatorEntityPosition = EntityManager.GetComponentData<LocalToWorld>(childBufferElement.Value).Position;
						break;
					}
				}

				if (animator != null) {
					_targetAnimator = animator;
				}
			}
		};

		joystickView.OnEndDragEvent += () => {
			_movementJoystickCursorInWorldTr.gameObject.SetActive(false);
			_dragging = false;
		};
		joystickView.OnUpdate += () => {
			if (!_dragging) {
				return;
			}

			MainMonoBehaviour.Instance.AddAction(() => {
				if (_targetAnimator == null) {
					return;
				}
				var animator = _targetAnimator;
				var distance = joystickView.GetDistance();
				var angle = joystickView.GetAngle();
			
				SetMovementJoystickCursorInWorldByAngle(
					animator.transform.position,
					distance,
					angle
				);
			});
		};



		Enabled = false;
	}
	
	
}
}