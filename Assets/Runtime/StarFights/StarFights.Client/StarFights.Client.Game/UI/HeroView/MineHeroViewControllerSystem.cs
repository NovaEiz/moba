using System.Collections;
using Nighday.Matchmaking.General;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Hero;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.Client.Game {

[DisableAutoCreation]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
[UpdateAfter(typeof(GhostSimulationSystemGroup))]
[UpdateBefore(typeof(BuildPhysicsWorld))]
public class MineHeroViewControllerSystem : SystemBase {

	private Camera _camera;

	protected override void OnCreate() {
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<GameCameraComponent>();
	}

	private IEnumerator CreateHeroMagazineView(Entity unitEntity) {
		if (_camera == null) {
			_camera = EntityManager.GetComponentObject<Camera>(GetSingletonEntity<GameCameraComponent>());
		}
		
		var asyncOperation = Addressables.LoadAssetAsync<GameObject>("HeroMagazineView");

		var gameCanvasObj = Nighday.Application.App.World.DataManager.GetGameObject("Canvas - Game");
		var gameCanvasTr = gameCanvasObj.GetComponent<Transform>();
		var gameViewTr = gameCanvasTr.Find("GameViewContainer/GameView");

		yield return asyncOperation;
		
		var heroMagazineView = GameObject.Instantiate(asyncOperation.Result.GetComponent<HeroMagazineView>(), gameViewTr);
		EntityManager.AddComponentObject(unitEntity, heroMagazineView);
		EntityManager.RemoveComponent<Creating>(unitEntity);
		
		while (!EntityManager.HasComponent<Child>(unitEntity)) {
			yield return null;
		}
		
		var childBuffer = EntityManager.GetBuffer<Child>(unitEntity);
		var childArray = childBuffer.ToNativeArray(Allocator.Temp);
		foreach (var child in childArray) {
			if (EntityManager.HasComponent<Transform>(child.Value)) {
				heroMagazineView.SetTarget(EntityManager.GetComponentObject<Transform>(child.Value));
				heroMagazineView.SetCamera(_camera);
				break;
			}
		}
	}

	private struct Creating : IComponentData {
		
	}

	protected override void OnUpdate() {
		var commandTargetComponent = GetSingleton<CommandTargetComponent>();
		var entity = commandTargetComponent.targetEntity;

		if (!EntityManager.Exists(entity)) {
			return;
		}

		if (!EntityManager.HasComponent<HeroMagazineView>(entity)) {
			if (!EntityManager.HasComponent<Creating>(entity)) {
				EntityManager.AddComponentData(entity, new Creating());
				MainMonoBehaviour.Instance.StartCoroutine(CreateHeroMagazineView(entity));
			}
			return;
		}

		var abilitiesBuffer = EntityManager.GetBuffer<AbilityBufferElement>(entity);
		var abilityEntity = abilitiesBuffer[0].AbilityEntity;
		
		var abilityMagazineComponent = EntityManager.GetComponentData<AbilityMagazineComponent>(abilityEntity);
		
		var heroMagazineView = EntityManager.GetComponentObject<HeroMagazineView>(entity);
		
		heroMagazineView.SetFrameData(abilityMagazineComponent.Amount,
									abilityMagazineComponent.MaxAmount,
									abilityMagazineComponent.RechargeProgress);
		
	}
}
}