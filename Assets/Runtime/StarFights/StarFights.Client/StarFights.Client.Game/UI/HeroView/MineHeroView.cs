using UnityEngine;

namespace StarFights.Client.Game {
public class MineHeroView : MonoBehaviour {

	[SerializeField] private HeroMagazineView _heroMagazineView;
	
	public HeroMagazineView HeroMagazineView => _heroMagazineView;

	
}
}