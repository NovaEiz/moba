using UnityEngine;
using UnityEngine.UI;

namespace StarFights.Client.Game {
public class HeroMagazine_PatronItemView : MonoBehaviour {

	public enum State {
		Available,
		Recharge
	}

	private State _state;

	public void SetState(State value) {
		_state = value;

		switch (_state) {
			case State.Available:
				_availableState.gameObject.SetActive(true);
				_rechargeState.gameObject.SetActive(false);
				break;
			case State.Recharge:
				_availableState.gameObject.SetActive(false);
				_rechargeState.gameObject.SetActive(true);
				break;
		}
	}

	[SerializeField] private Transform _availableState;
	[SerializeField] private Transform _rechargeState;
	
	[SerializeField] private Image _rechargeProgressImage;


	public void SetRechargeProgress(float progress) {
		_rechargeProgressImage.fillAmount = progress;
	}
	
}
}