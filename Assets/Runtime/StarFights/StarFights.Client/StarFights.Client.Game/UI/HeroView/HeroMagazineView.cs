using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace StarFights.Client.Game {
public class HeroMagazineView : MonoBehaviour {

	[SerializeField] private HeroMagazine_PatronItemView _prefab;
	[SerializeField] private Transform _container;
	
	private List<HeroMagazine_PatronItemView> _items = new List<HeroMagazine_PatronItemView>();

	private int _maxPatrons;
	
	public void SetFrameData(int amount, int maxAmount, float rechargeProgress) {
		SetMaxPatrons(maxAmount);
		
		var len = _items.Count;
		for (var i = 0; i < len; i++) {
			var item = _items[i];

			if (i+1 == amount + 1) {
				item.SetState(HeroMagazine_PatronItemView.State.Recharge);
				item.SetRechargeProgress(rechargeProgress);
			} else if (i+1 > amount) {
				item.SetState(HeroMagazine_PatronItemView.State.Recharge);
				item.SetRechargeProgress(0);
			} else {
				item.SetState(HeroMagazine_PatronItemView.State.Available);
			}
		}
	}
	public void SetMaxPatrons(int value) {
		_maxPatrons = value;

		if (CheckNeedChanges()) {
			Rebuild();
		}
	}

	/// <summary>
	/// Проверить нужны ли изменения
	/// </summary>
	private bool CheckNeedChanges() {
		if (_items.Count != _maxPatrons) {
			return true;
		}

		return false;
	}
	
	private void Rebuild() {
		if (_items.Count > _maxPatrons) {
			while (_items.Count > _maxPatrons) {
				Destroy(_items[_items.Count-1]);
			}
		} else if (_items.Count < _maxPatrons) {
			while (_items.Count < _maxPatrons) {
				_items.Add(CreateItem());
			}
		}
	}

	private HeroMagazine_PatronItemView CreateItem() {
		var item = Instantiate(_prefab, _container);
		return item;
	}

	public void SetPosition(Vector3 position) {
		position.y -= 16;
		transform.position = position;
	}
	
	private Transform _target;
	public void SetTarget(Transform value) {
		_target = value;
	}
	private Camera _camera;
	public void SetCamera(Camera value) {
		_camera = value;
		CinemachineCore.CameraUpdatedEvent.AddListener((cinemachineBrain) => {
			UpdatePosition();
		});
	}

	private void UpdatePosition() {
		var worldPosition = _target.position + new Vector3(0, 2.5f, 0);
		var position = _camera.WorldToScreenPoint(worldPosition);
		SetPosition(position);
	}
	
}
}