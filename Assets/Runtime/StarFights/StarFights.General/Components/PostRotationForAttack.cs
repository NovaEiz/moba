using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.General {

[GenerateAuthoringComponent]
public struct PostRotationForAttack : IComponentData {
	[GhostField]
	[HideInInspector] public float Angle;
}
}