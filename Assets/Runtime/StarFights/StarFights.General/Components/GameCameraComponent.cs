using Unity.Entities;

namespace StarFights.General {

public struct GameCameraComponent : IComponentData {
	public Entity Entity;
}
}