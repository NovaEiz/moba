using Unity.Animation;
using Unity.Animation.Hybrid;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

public struct LastInteractionWithWorld : IComponentData {
	public float ElapsedTime;
}

}