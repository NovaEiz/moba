using System;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

[GenerateAuthoringComponent]
public struct ParentForChild : IComponentData {

	public Entity Value;

}
}