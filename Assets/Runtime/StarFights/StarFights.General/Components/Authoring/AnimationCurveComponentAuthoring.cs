using Unity.Animation;
using Unity.Animation.Hybrid;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

[RequiresEntityConversion]
public class AnimationCurveComponentAuthoring : MonoBehaviour, IConvertGameObjectToEntity {
	[SerializeField] private AnimationCurveAsset _animationCurveAsset;
    
	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentData(entity, new AnimationCurveComponent
		{
			AnimationCurve = _animationCurveAsset.AnimationCurve.ToAnimationCurveBlobAssetRef()
		});
	}
}

}