using System;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

public class ChildAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		var parentForChild = dstManager.GetComponentData<ParentForChild>(entity);

		var parentEntity = parentForChild.Value;

		DynamicBuffer<Child> buffer = default;
		if (!dstManager.HasComponent<Child>(parentEntity)) {
			if (!dstManager.Exists(parentEntity)) {
				Debug.LogError("Нет parentEntity! gameObject.name = " + gameObject.name + "; entity = " + entity);

			}
			buffer = dstManager.AddBuffer<Child>(parentEntity);
		} else {
			buffer = dstManager.GetBuffer<Child>(parentEntity);
		}

		buffer.Add(new Child {
			Value = entity
		});

		dstManager.RemoveComponent<ParentForChild>(entity);
	}

}
}