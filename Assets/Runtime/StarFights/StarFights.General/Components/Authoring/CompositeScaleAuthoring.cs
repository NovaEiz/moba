using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

public class CompositeScaleAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		if (!dstManager.HasComponent<CompositeScale>(entity)) {
			
			dstManager.AddComponentData(entity, new CompositeScale {
				Value = new float4x4 {
					c0 = new float4(1,0,0,0),
					c1 = new float4(0,1,0,0),
					c2 = new float4(0,0,1,0),
					c3 = new float4(0,0,0,1)
				}
			});
		}
	}

}
}