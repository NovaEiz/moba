using System;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

public class ChildBufferAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		if (!dstManager.HasComponent<Child>(entity)) {
			dstManager.AddBuffer<Child>(entity);
		}
	}

}
}