using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.General {

public class GameCameraComponentAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private Camera _camera;

	public void Convert(
		Entity entity,
		EntityManager dstManager,
		GameObjectConversionSystem conversionSystem
	) {
		dstManager.AddComponentData(entity, new GameCameraComponent {});
		dstManager.AddComponentObject(entity, _camera);
	}

}
}