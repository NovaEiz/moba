using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

namespace StarFights.General {

[UnityEngine.ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class CopyTransformWithScaleToGameObjectSystem : JobComponentSystem
{
	
	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform
	{
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		public void Execute(int index, TransformAccess transform)
		{
			var value = LocalToWorlds[index];
			transform.position = value.Position;
			transform.rotation = new quaternion(value.Value);

			transform.localScale = value.Value.GetScale();
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate() {
		m_TransformGroup = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly(typeof(CopyTransformWithScaleToGameObject)),
				ComponentType.ReadOnly<LocalToWorld>(),
				typeof(UnityEngine.Transform)
			},
			None = new[] {
				ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(),
			}
		});
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms
		{
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}


[UnityEngine.ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class CopyTransformWithScaleWithIgnoreRotationToGameObjectSystem : JobComponentSystem
{
	
	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform
	{
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		public void Execute(int index, TransformAccess transform)
		{
			var value = LocalToWorlds[index];
			transform.position = value.Position;

			transform.localScale = value.Value.GetScale();
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate()
	{
		m_TransformGroup = GetEntityQuery(ComponentType.ReadOnly(typeof(CopyTransformWithScaleToGameObject)), ComponentType.ReadOnly<LocalToWorld>(), ComponentType.ReadOnly<IgnoreCopyRotationToGameObject>(), typeof(UnityEngine.Transform));
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms
		{
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}



}