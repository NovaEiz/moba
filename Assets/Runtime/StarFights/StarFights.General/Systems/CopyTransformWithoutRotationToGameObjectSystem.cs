using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Jobs;

namespace StarFights.General {

[UnityEngine.ExecuteAlways]
[UpdateInGroup(typeof(TransformSystemGroup))]
[UpdateAfter(typeof(EndFrameLocalToParentSystem))]
public class CopyTransformWithoutRotationToGameObjectSystem : JobComponentSystem
{
	
	[BurstCompile]
	struct CopyTransforms : IJobParallelForTransform
	{
		[DeallocateOnJobCompletion]
		[ReadOnly] public NativeArray<LocalToWorld> LocalToWorlds;

		public void Execute(int index, TransformAccess transform)
		{
			var value = LocalToWorlds[index];
			transform.position = value.Position;
		}
	}

	EntityQuery m_TransformGroup;

	protected override void OnCreate()
	{
		m_TransformGroup = GetEntityQuery(ComponentType.ReadOnly(typeof(CopyTransformWithoutRotationToGameObject)), ComponentType.ReadOnly<LocalToWorld>(), typeof(UnityEngine.Transform));
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps)
	{
		var transforms = m_TransformGroup.GetTransformAccessArray();
		var copyTransformsJob = new CopyTransforms
		{
			LocalToWorlds = m_TransformGroup.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out inputDeps),
		};

		return copyTransformsJob.Schedule(transforms, inputDeps);
	}
}
}