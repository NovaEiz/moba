using Unity.Mathematics;

namespace StarFights.General {

public static class float4x4Extentions {
	public static float3 GetScale(this float4x4 matrix) => new float3(
		math.length(matrix.c0.xyz),
		math.length(matrix.c1.xyz),
		math.length(matrix.c2.xyz)
	);
}

}