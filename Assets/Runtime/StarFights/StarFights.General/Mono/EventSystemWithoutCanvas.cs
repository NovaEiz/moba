using System;
using System.Collections;
using Nighday.General.Other;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace StarFights.General {
public class EventSystemWithoutCanvas : MonoBehaviour {

	public void SetComponent<T>(T component, RectTransform rectTransform, Rect rect = default)
		where T : IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler
	{
		_rectTransform = rectTransform;
		_canvasScaler = _rectTransform.GetComponentInParent<CanvasScaler>();

		_iPointerDownHandler = component;
		_iPointerUpHandler = component;
		_iBeginDragHandler = component;
		_iDragHandler = component;
		_iEndDragHandler = component;

		if (!rect.Equals(default)) {
			StartCoroutine(RectInitIe());

			_rect = rect;
			_useRect = true;
		}
	}

	private IEnumerator RectInitIe() {
		yield return null;
		if ((int)_rect.width == 0) {
			_rect.width = GetPosition(new Vector3(Screen.width, 0, 0)).x;

		} else {
			_rect.width = _rect.width / _canvasScaler.referenceResolution.x *
						GetPosition(new Vector3(Screen.width, 0, 0)).x;
			
			if (_canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight) {
				//_rect.width *= _canvasScaler.GetComponent<Canvas>().scaleFactor;
			}

		}
		if ((int)_rect.height == 0) {
			//rect.height = rectTransform.rect.height;//GetPosition(new Vector3(0, rectTransform.rect.height, 0)).y;
			_rect.height = GetPosition(new Vector3(0, Screen.height, 0)).y;
		} else {
			if (_canvasScaler.screenMatchMode == CanvasScaler.ScreenMatchMode.MatchWidthOrHeight) {
				//_rect.height *= _canvasScaler.GetComponent<Canvas>().scaleFactor;
			}

			_rect.height = _rect.height / _canvasScaler.referenceResolution.y *
							GetPosition(new Vector3(0, Screen.height, 0)).y;
		}
	}
	
	private RectTransform _rectTransform;
	private CanvasScaler _canvasScaler;

	private IPointerDownHandler _iPointerDownHandler;
	private IPointerUpHandler _iPointerUpHandler;
	private IBeginDragHandler _iBeginDragHandler;
	private IDragHandler _iDragHandler;
	private IEndDragHandler _iEndDragHandler;

	private IPointerDownHandler IPointerDownHandler => _iPointerDownHandler;
	private IPointerUpHandler IPointerUpHandler => _iPointerUpHandler;
	private IBeginDragHandler IBeginDragHandler => _iBeginDragHandler;
	private IDragHandler IDragHandler => _iDragHandler;
	private IEndDragHandler IEndDragHandler => _iEndDragHandler;

	private Vector3 _lastPosition;
	private bool _dragging;
	private bool _down;

	private bool _useRect;
	private Rect _rect;

	private Vector3 GetPosition(Vector3 position) {
		RectTransformUtility.ScreenPointToLocalPointInRectangle(
			_rectTransform,
			position,
			null,
			out Vector2 localPoint
		);
		return localPoint;
	}
	
	private void Update() {
		var position = GetPosition(Input.mousePosition);
		
		
		if (_dragging && Input.GetMouseButton(0) && _lastPosition != position) {
			var data = new PointerEventData(EventSystem.current);
			data.pressPosition = position;
			data.position = position;
			data.delta = position - _lastPosition;
			IDragHandler.OnDrag(data);

			_lastPosition = data.position;
		}
		if (_down && !_dragging && Input.GetMouseButton(0)) {
			var data = new PointerEventData(EventSystem.current);
			data.delta = default;
			data.pressPosition = position;
			data.position = position;
			IBeginDragHandler.OnBeginDrag(data);
			
			_dragging = true;
			
			_lastPosition = position;
		}
		if (Input.GetMouseButtonUp(0) && _dragging) {
			var data = new PointerEventData(EventSystem.current);
			data.position = position;
			data.pressPosition = position;
			IEndDragHandler.OnEndDrag(data);

			_dragging = false;

			_lastPosition = default;
		}
		
		if (Input.GetMouseButtonDown(0) && _rectTransform.rect.Contains(position)
			&&
			!UIExt.CheckPointOnOverUI(Input.mousePosition)
			) {
			if (_useRect && !_rect.Contains(position)) {
				return;
			}
			
			var data = new PointerEventData(EventSystem.current);
			data.position = position;
			data.pressPosition = position;
			IPointerDownHandler.OnPointerDown(data);

			_lastPosition = data.position;

			_down = true;
		}
		if (Input.GetMouseButtonUp(0) && _down) {
			_down = false;
			
			var data = new PointerEventData(EventSystem.current);
			data.position = position;
			IPointerUpHandler.OnPointerUp(data);
		}
	}
	
#if UNITY_EDITOR

	private void OnValidate() {
		if (_rectTransform != null) {
			//_rectTransform.ForceUpdateRectTransforms();
		}
	}

#endif

}
}