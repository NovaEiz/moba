namespace StarFights.General {

public enum GameModeTeamType {
	EveryManForHimself,
	InTeam
}

}