using System;
using System.Threading.Tasks;
using Nighday.General.TaskExtentions;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace StarFights.General {

[CreateAssetMenu(fileName = "GameModesDatabaseAsset", menuName = "StarFights/GameModesDatabaseAsset", order = 0)]
public class GameModesDatabaseAsset : ScriptableObject {

	[SerializeField] private GameModeAsset[] _gameModes;

	public GameModeAsset[] GameModes => _gameModes;

	private GameModeAsset _selectedGameMode;

	public GameModeAsset SelectedGameMode => _selectedGameMode;
	
	private string _playerPrefsKey = "SavedModeName";

	public void SetSelectedGameMode(GameModeAsset gameMode) {
		PlayerPrefs.SetString(_playerPrefsKey, gameMode.Name);
		_selectedGameMode = gameMode;
		OnGameModeSelected?.Invoke(_selectedGameMode);
	}

	private void OnCreate() {
		var savedModeName = PlayerPrefs.GetString(_playerPrefsKey);
		var gameModes = GameModes;
		var len = gameModes.Length;
		for (var i = 0; i < len; i++) {
			var gameMode = gameModes[i];
			if (gameMode.Name == savedModeName) {
				_selectedGameMode = gameMode;
				return;
			}
		}

		SetSelectedGameMode(gameModes[0]);
	}

	private static GameModesDatabaseAsset _instance;
	private static bool _loading;

	public static async Task<GameModesDatabaseAsset> LoadAsync() {
		try {

			if (_instance != null) {
				return _instance;
			}

			if (_loading) {
				while (_instance == null) {
					await new WaitForUpdate();
				}
				return _instance;
			}

			_loading = true;
			var asyncOperation = Addressables.LoadAssetAsync<ScriptableObject>("GameModesDatabaseAsset");
			await asyncOperation.Task;
			_instance = (GameModesDatabaseAsset)asyncOperation.Result;
			_instance.OnCreate();
			return _instance;
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
		return null;
	}

	public Action<GameModeAsset> OnGameModeSelected;

	public string GetMapNameByModeName(string modeName) {
		foreach (var gameMode in _gameModes) {
			if (gameMode.Name == modeName) {
				return gameMode.MapName;
			}
		}

		return null;
	}

}
}