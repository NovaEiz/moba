using UnityEngine;

namespace StarFights.General {

[CreateAssetMenu(fileName = "GameModeAsset", menuName = "StarFights/GameModeAsset", order = 0)]
public class GameModeAsset : ScriptableObject {
	
	[SerializeField] private string _name;
	[SerializeField] private string _mapName;

	[Space]
	[SerializeField] private GameModeTeamType _teamType;
	
	/// <summary>
	/// Делится на количество команд для получения количества игроков в команде
	/// </summary>
	[SerializeField] private ushort _playersAmount;
	[SerializeField] private ushort _playersInTeamAmount;

	public string Name => _name;
	public string MapName => _mapName;
	
	public GameModeTeamType TeamType => _teamType;
	public ushort PlayersAmount => _playersAmount;
	public ushort PlayersInTeamAmount => _playersInTeamAmount;

}
}