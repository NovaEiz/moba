﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.General.PhysicsExtentions {

public struct TriggerEventBufferElement : IBufferElementData {
    public Entity WithEntity;
}
}
