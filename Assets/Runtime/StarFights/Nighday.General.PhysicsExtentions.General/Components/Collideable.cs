﻿using Unity.Entities;

namespace Nighday.General.PhysicsExtentions {

[GenerateAuthoringComponent]
public struct Collideable : IComponentData {
    public Entity MainEntity;
}
}
