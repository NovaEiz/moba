﻿using Unity.Entities;
using Unity.NetCode;

namespace Nighday.General.PhysicsExtentions {

[GenerateAuthoringComponent]
public struct GhostScale : IComponentData {
	[GhostField(Quantization=100)]
	public float Value;
}

}