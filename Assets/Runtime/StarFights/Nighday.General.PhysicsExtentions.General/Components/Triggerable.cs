﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.General.PhysicsExtentions {

[GenerateAuthoringComponent]
public struct Triggerable : IComponentData {
    public Entity MainEntity;
}
}
