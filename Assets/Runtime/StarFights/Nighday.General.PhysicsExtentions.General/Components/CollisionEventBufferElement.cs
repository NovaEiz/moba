﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.General.PhysicsExtentions {

public struct CollisionEventBufferElement : IBufferElementData {
    public Entity WithEntity;
}
}
