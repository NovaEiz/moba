﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.General.PhysicsExtentions {

public struct ProjectileAttackedTargetsForLifeBufferElement : IBufferElementData {
    public Entity TargetEntity;
}

}
