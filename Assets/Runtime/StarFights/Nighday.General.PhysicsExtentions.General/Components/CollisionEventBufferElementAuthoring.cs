﻿using Unity.Entities;
using UnityEngine;

namespace Nighday.General.PhysicsExtentions {

public class CollisionEventBufferElementAuthoring : MonoBehaviour, IConvertGameObjectToEntity {
    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
        dstManager.AddBuffer<CollisionEventBufferElement>(entity);
    }
}
}
