﻿
using Nighday.General.PhysicsExtentions;
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.NetCode;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

namespace Nighday.General.PhysicsExtentions {


[UpdateAfter(typeof(AbilityGhostSystemGroup))]
[UpdateAfter(typeof(BuildPhysicsWorld))]
[DisableAutoCreation]
//[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
//[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
public class TriggerSystem : JobComponentSystem {
	private BuildPhysicsWorld buildPhysicsWorldSystem;
	private StepPhysicsWorld stepPhysicsWorldSystem;

	protected override void OnCreate() {
		base.OnCreate();
		
		buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
		stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		Entities
			.ForEach((Entity entity,
					DynamicBuffer<TriggerEventBufferElement> triggerEventBuffer
					) => {
				triggerEventBuffer.Clear();
					}).Run();
		
		var inputDeps2 = new TriggerableJob {
			TriggerableGroup = GetComponentDataFromEntity<Triggerable>(),
			TriggerEventBufferGroup = GetBufferFromEntity<TriggerEventBufferElement>()
		}.Schedule(stepPhysicsWorldSystem.Simulation,
					ref buildPhysicsWorldSystem.PhysicsWorld, inputDeps);
		return inputDeps2;
	}

	[BurstCompile]
	private struct TriggerableJob : ITriggerEventsJob {
		public ComponentDataFromEntity<Triggerable> TriggerableGroup;
		public BufferFromEntity<TriggerEventBufferElement> TriggerEventBufferGroup;

		public void UpdateTriggerable(Entity entity, Entity collider, TriggerEvent triggerEvent) {
			if (TriggerableGroup.HasComponent(entity)) {
				var triggerable = TriggerableGroup[entity];
				if (triggerable.MainEntity.Equals(Entity.Null)) {
					TriggerEventBufferGroup[entity].Add(new TriggerEventBufferElement{
						WithEntity = collider							
					});
				} else {
					TriggerEventBufferGroup[triggerable.MainEntity].Add(new TriggerEventBufferElement{
						WithEntity = collider							
					});
				}
			}
		}

		public void Execute(TriggerEvent triggerEvent) {
			var entityA = triggerEvent.EntityA;
			var entityB = triggerEvent.EntityB;
                
			UpdateTriggerable(entityA, entityB, triggerEvent);
			UpdateTriggerable(entityB, entityA, triggerEvent);
		}
	}
}
}

/*
using Unity.Burst;
using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Systems;

namespace Nighday.General.PhysicsExtentions {

[UpdateAfter(typeof(EndFramePhysicsSystem))]
[DisableAutoCreation]
public class TriggerSystem : JobComponentSystem {
	private BuildPhysicsWorld buildPhysicsWorldSystem;
	private StepPhysicsWorld stepPhysicsWorldSystem;

	protected override void OnCreate() {
		base.OnCreate();
		
		buildPhysicsWorldSystem = World.GetOrCreateSystem<BuildPhysicsWorld>();
		stepPhysicsWorldSystem = World.GetOrCreateSystem<StepPhysicsWorld>();
	}

	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var inputDeps2 = new TriggerableJob {
			TriggerableGroup = GetComponentDataFromEntity<Triggerable>()
		}.Schedule(stepPhysicsWorldSystem.Simulation, ref buildPhysicsWorldSystem.PhysicsWorld, inputDeps);
		return inputDeps2;
	}

	[BurstCompile]
	private struct TriggerableJob : ITriggerEventsJob {
		public ComponentDataFromEntity<Triggerable> TriggerableGroup;

		public void UpdateTriggerable(Entity entity, Entity collider, TriggerEvent triggerEvent) {
			if (TriggerableGroup.HasComponent(entity)) {
				var triggerable = TriggerableGroup[entity];
				triggerable.TriggerEntity = collider;
				TriggerableGroup[entity] = triggerable;
			}
		}

		public void Execute(TriggerEvent triggerEvent) {
			var entityA = triggerEvent.Entities.EntityA;
			var entityB = triggerEvent.Entities.EntityB;
                
			UpdateTriggerable(entityA, entityB, triggerEvent);
			UpdateTriggerable(entityB, entityA, triggerEvent);
		}
	}
}
}
*/