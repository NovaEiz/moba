using Nighday.General.AddressablesExtentions;
using StarFights.GameCore.Movement;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ghosts {
public class ViewWeakAssetReferenceAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private WeakAssetReference _weakAssetReference;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		if (dstManager.World.GetExistingSystem<ClientSimulationSystemGroup>() != null) {
			dstManager.AddComponentData(entity, new ViewWeakAssetReference {
				WeakAssetReference = _weakAssetReference
			});
		}
	}
}

}