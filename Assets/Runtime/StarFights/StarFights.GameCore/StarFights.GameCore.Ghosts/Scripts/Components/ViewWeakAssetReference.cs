using Nighday.General.AddressablesExtentions;
using Unity.Entities;

namespace StarFights.GameCore.Ghosts {
public struct ViewWeakAssetReference : IComponentData {
	public WeakAssetReference WeakAssetReference;
}
}