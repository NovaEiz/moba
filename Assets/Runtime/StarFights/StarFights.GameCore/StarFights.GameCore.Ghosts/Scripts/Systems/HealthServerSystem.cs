using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ghosts {

[DisableAutoCreation]
[UpdateInGroup(typeof(SimulationSystemGroup))]
public class HealthServerSystem : SystemBase {

	private struct HealthInited : IComponentData { }

	private struct WaitBeforeNextRegenerateStep : IComponentData {
		public float ElapsedTime;
	}

	private float _waitForActiveRegeneration = 5f;
	private float _waitForNextRegenerateStep = 1.5f;

	protected override void OnCreate() {
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	protected override void OnUpdate() {
		var deltaTime = Time.DeltaTime;
		
		Entities
			.WithNone<HealthInited>()
			.ForEach((Entity entity,
					ref Health health
					) => {
						health.Init();
						EntityManager.AddComponentData(entity, new HealthInited());
						EntityManager.AddComponentData(entity, new LastInteractionWithWorld());
					}).WithStructuralChanges().Run();
		Entities
			.WithNone<DeadState>()
			.WithNone<WaitBeforeNextRegenerateStep>()
			.WithAll<HealthInited>()
			.ForEach((Entity entity,
					ref LastInteractionWithWorld lastInteractionWithWorld,
					ref Health health
					) => {
						if (lastInteractionWithWorld.ElapsedTime >= _waitForActiveRegeneration) {
							EntityManager.AddComponentData(entity, new WaitBeforeNextRegenerateStep());

						}
						lastInteractionWithWorld.ElapsedTime += deltaTime;
					}).WithStructuralChanges().Run();
		Entities
			.WithNone<DeadState>()
			.WithAll<HealthInited>()
			.ForEach((Entity entity,
					ref LastInteractionWithWorld lastInteractionWithWorld,
					ref Health health,
					ref WaitBeforeNextRegenerateStep waitBeforeNextRegenerateStep
					) => {
				if (lastInteractionWithWorld.ElapsedTime < _waitForActiveRegeneration) {
					EntityManager.RemoveComponent<WaitBeforeNextRegenerateStep>(entity);
					return;
				}
				if (waitBeforeNextRegenerateStep.ElapsedTime >= _waitForNextRegenerateStep) {
					waitBeforeNextRegenerateStep.ElapsedTime = 0;

					health.Value += 500;
					if (health.Value > health.MaxValue) {
						health.Value = health.MaxValue;
					}
				}

				waitBeforeNextRegenerateStep.ElapsedTime += deltaTime;
			}).WithStructuralChanges().Run();
	}
}
}