using Nighday.GameCamera;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Movement;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Ghosts {

[DisableAutoCreation]
[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
[UpdateBefore(typeof(HeroGhostSystem))]
public class HeroInputClientSystem : ComponentSystem {

	private GhostPredictionSystemGroup m_PredictionGroup;
	private ClientSimulationSystemGroup _clientSimulationSystemGroup;
	
	protected override void OnCreate() {
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<TrackingCamera>();

		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
		_clientSimulationSystemGroup = World.GetExistingSystem<ClientSimulationSystemGroup>();
	}
	
	protected override void OnUpdate() {
		Entities
			.ForEach((Entity entity,
					ref CommandTargetComponent commandTargetComponent
					) => {
						if (commandTargetComponent.targetEntity.Equals(Entity.Null)) {
							return;
						}

						if (!EntityManager.HasComponent<HeroInput>(commandTargetComponent.targetEntity)) {
							return;
						}
						
						var inputBuffer = EntityManager.GetBuffer<HeroInput>(commandTargetComponent.targetEntity);

						var abilityInputComponent = EntityManager.GetComponentData<AbilityInputComponent>(commandTargetComponent.targetEntity);
						var movementInputComponent = EntityManager.GetComponentData<MovementInputComponent>(commandTargetComponent.targetEntity);

						var input = default(HeroInput);
						//input.tick = m_PredictionGroup.PredictingTick;//_clientSimulationSystemGroup.ServerTick;
						input.tick = _clientSimulationSystemGroup.ServerTick;
						input.movementAngle = movementInputComponent.movementAngle;
						
						input.attackAngle = abilityInputComponent.attackAngle;
						input.attackCommandClientGhostTick = abilityInputComponent.attackCommandClientTick;
						input.abilitySlotIndex = abilityInputComponent.abilitySlotIndex;

						inputBuffer.Add(input);
			});
						
	}
	
	/*
	 
	public Func<float> GetAttackAngle;
	public Func<float> GetMovementAngle;
	public Func<float> GetMovementDistance;
	
	private void MoveSelectedUnit(float angle) {
		Entities
			.ForEach((Entity entity,
					ref CommandTargetComponent commandTargetComponent
					) => {
				if (commandTargetComponent.targetEntity.Equals(Entity.Null)) {
					return;
				}
				if (!EntityManager.HasComponent<HeroInput>(commandTargetComponent.targetEntity)) {
					return;
				}
				
				var distance = 1f;

				var movementInputBuffer = EntityManager.GetBuffer<HeroInput>(commandTargetComponent.targetEntity);
						
				var input = default(HeroInput);
				input.tick = _clientSimulationSystemGroup.ServerTick;
				input.movementAngle = -1;
				input.attackAngle = -1;
				input.abilitySlotIndex = -1;
				
				
				if (!EntityManager.HasComponent<DeadState>(commandTargetComponent.targetEntity)) {
					//TODO: заменить 0.1f на радиус маленького изображения круга в UI
					if (distance < 0.1f) {
						
						movementInputBuffer.Add(input);

						return;
					}

					input.movementAngle = angle;

					
					if (GetAttackAngle != null) {
						var heroAbilitiesBuffer =
							EntityManager.GetBuffer<HeroAbilitiesBufferElement>(commandTargetComponent.targetEntity);

						var attackAngle = GetAttackAngle();

						if (attackAngle < 0) {
							attackAngle = GetToNearestEnemyTargetGhostAngle();
						}
						input.attackAngle = attackAngle;
						
						input.abilitySlotIndex = 0;
						
						input.attackCommandClientTick = input.tick;
						
					} else {
						input.abilitySlotIndex = -1;
						input.attackAngle = -1;
					}
				}
				GetAttackAngle = null;

				movementInputBuffer.Add(input);
			});
	}
	
	
	private float GetAngleMobile() {
		var angle = -1f;

		if (GetMovementAngle != null) {
			angle = GetMovementAngle() + 360;
		}
		return angle;
	}

	private float GetAngleStandalone() {
		var angle = -1f;
		
		var horizontal = 0;
		var vertical   = 0;
			
		if (Input.GetKey("a"))
			horizontal -= 1;
		if (Input.GetKey("d"))
			horizontal += 1;
		if (Input.GetKey("s"))
			vertical -= 1;
		if (Input.GetKey("w"))
			vertical += 1;

		if (horizontal != 0 || vertical != 0) {
			var value     = new float2(horizontal, vertical);
			var direction = math.normalize(value);

			angle = direction.ConvertDirectionToAngle();
		}
		
		return angle;
	}

	private float GetAngle() {
		var angle = -1f;
#if UNITY_IOS || UNITY_ANDROID || true
		angle = GetAngleMobile();
#elif UNITY_STANDALONE
		angle = GetAngleStandalone();
#endif
		if (angle >= 0) {
			angle += 360;

			var trackingCamera = GetSingleton<TrackingCamera>();

			angle += trackingCamera.RotationAngles.y;
		}

		return angle;
	}

 */

	
}
}