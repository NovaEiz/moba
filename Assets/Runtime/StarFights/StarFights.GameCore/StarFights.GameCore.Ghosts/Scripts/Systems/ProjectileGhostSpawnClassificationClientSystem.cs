using System;
using Nighday.GameCamera;
using Nighday.General.MathematicsExtentions;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Networking.Transport.Utilities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Ghosts {

[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
[UpdateAfter(typeof(GhostSpawnClassificationSystem))]
[DisableAutoCreation]
public class ProjectileGhostSpawnClassificationClientSystem : SystemBase {
	protected override void OnCreate() {
		RequireSingletonForUpdate<GhostSpawnQueueComponent>();
		RequireSingletonForUpdate<PredictedGhostSpawnList>();
	}
	protected override void OnUpdate() {
		Entities
			.WithNone<GhostComponent>()
			.WithAll<PredictedGhostComponent>()
			.WithAll<GhostOwnerComponent>()
			.WithoutBurst()
			.ForEach((Entity entity) => {
				EntityManager.DestroyEntity(entity);
			}).WithStructuralChanges().Run();
		
		var spawnListEntity = GetSingletonEntity<PredictedGhostSpawnList>();
		var spawnListFromEntity = GetBufferFromEntity<PredictedGhostSpawn>();
		Dependency = Entities
					.WithAll<GhostSpawnQueueComponent>()
					.WithoutBurst()
					.ForEach((DynamicBuffer<GhostSpawnBuffer> ghosts, DynamicBuffer<SnapshotDataBuffer> data) =>
					{
						var spawnList = spawnListFromEntity[spawnListEntity];
						for (int i = 0; i < ghosts.Length; ++i)
						{
							var ghost = ghosts[i];
							if (ghost.SpawnType == GhostSpawnBuffer.Type.Predicted)
							{
								for (int j = 0; j < spawnList.Length; ++j)
								{
									if (ghost.GhostType == spawnList[j].ghostType && !SequenceHelpers.IsNewer(spawnList[j].spawnTick, ghost.ServerSpawnTick + 5) && SequenceHelpers.IsNewer(spawnList[j].spawnTick + 5, ghost.ServerSpawnTick))
									{
										ghost.PredictedSpawnEntity = spawnList[j].entity;
										spawnList[j] = spawnList[spawnList.Length-1];
										spawnList.RemoveAt(spawnList.Length - 1);
										break;
									}
								}
								ghosts[i] = ghost;
							}
						}
					}).Schedule(Dependency);
	}
}

}