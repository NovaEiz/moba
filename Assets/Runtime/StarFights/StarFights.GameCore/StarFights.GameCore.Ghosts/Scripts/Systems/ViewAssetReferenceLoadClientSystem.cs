using System;
using System.Collections.Generic;
using Nighday.General.AddressablesExtentions;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Projectile;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using Unity.Physics.Systems;
using UnityEngine;
using Unity.Transforms;
using UnityEngine.ResourceManagement.AsyncOperations;
using ComponentSystem = Unity.Entities.ComponentSystem;

namespace StarFights.GameCore.Ghosts {

[DisableAutoCreation]
[UpdateInGroup(typeof(SimulationSystemGroup))]
[UpdateAfter(typeof(ProjectileLifeServerSystem))]
public class ViewAssetReferenceLoadClientSystem : ComponentSystem {

	private Dictionary<string, GameObject> _dictionaryPrefabs = new Dictionary<string, GameObject>();

	private string _containerName = "Game";

	public string ContainerName {
		get => _containerName;
		set => _containerName = value;
	}

	private void GetPrefab(WeakAssetReference weakAssetReference, Action<GameObject> callback) {
		var guid = weakAssetReference.GetGuid().ToString();
		if (_dictionaryPrefabs.TryGetValue(guid, out GameObject prefab)) {
			callback(prefab);
		} else {
			try {
				var handleAsync = weakAssetReference.GetAssetReference().LoadAssetAsync<GameObject>();
				handleAsync.Completed += (AsyncOperationHandle<GameObject> handle) => {
					if (handle.Status == AsyncOperationStatus.Succeeded) {
						if (!_dictionaryPrefabs.ContainsKey(guid)) {
							_dictionaryPrefabs.Add(guid, handle.Result);
						}
						callback(handle.Result);
					}
				};
			}
			catch (Exception e) {
				Debug.LogError(e);
				throw;
			}
		}
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<Parent>()
			.ForEach((Entity entity,
					Transform transform,
					ref GhostClientView ghostClientView
					) => {
				GameObject.Destroy(transform.gameObject);
				EntityManager.DestroyEntity(entity);
					});
		Entities
			.ForEach((Entity entity,
					ref ViewWeakAssetReference viewWeakAssetReference
					) => {
						EntityManager.RemoveComponent<ViewWeakAssetReference>(entity);

						GetPrefab(viewWeakAssetReference.WeakAssetReference, (prefab) => {
							if (!EntityManager.Exists(entity)) {
								return;
							}
							var instance = GameObject.Instantiate(prefab, Nighday.Application.App.World.DataManager.GetGameObject(_containerName).transform);
							var instanceTransform = instance.GetComponent<Transform>();

							var childEntity = EntityManager.CreateEntity(
								typeof(LocalToParent),
								typeof(LocalToWorld),
								typeof(Rotation),
								//typeof(Scale),
								typeof(GhostChildEntityComponent),
								typeof(Translation),
								typeof(GhostClientView)
							);
							EntityManager.AddComponentData(childEntity, new Parent { Value = entity });
								
							DynamicBuffer<Child> childBuffer = default;
							if (!EntityManager.HasComponent<Child>(entity)) {
								childBuffer = EntityManager.AddBuffer<Child>(entity);
							} else {
								childBuffer = EntityManager.GetBuffer<Child>(entity);
							}
							//childBuffer.Add();
								
							EntityManager.AddComponentObject(childEntity, instanceTransform);
								
							var animator = instance.GetComponent<Animator>();
							if (animator != null) {
								EntityManager.AddComponentObject(childEntity, animator);
							}

							EntityManager.AddComponentData(childEntity, new CopyTransformWithScaleToGameObject());
							EntityManager.AddComponentData(entity, new ViewAuthorized());
							
						});
					});
	}

}

}