using System;
using Nighday.General.MathematicsExtentions;
using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Animation;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using AnimationState = StarFights.GameCore.Animation.AnimationState;

namespace StarFights.GameCore.Ghosts {

[DisableAutoCreation]
[UpdateAfter(typeof(GhostSimulationSystemGroup))]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
//[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
/*

[UpdateAfter(typeof(GhostSimulationSystemGroup))]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
 */
public class AnimatorViewHeroClientSystem : SystemBase {
	
	private GhostPredictionSystemGroup m_PredictionGroup;
	
	protected override void OnCreate() {
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
		
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
	}

	protected override void OnDestroy() {
	}

	protected override void OnUpdate() {
		AnimatorClient();
	}

	private void AnimatorClient() {
		var tick = m_PredictionGroup.PredictingTick;
		
		/*
		Entities
			.WithNone<IgnoreCopyRotationToGameObject>()
			.WithAll<GhostClientView>()
			.ForEach((Entity animatorEntity,
					Animator animator
					) => {
				EntityManager.AddComponentData(animatorEntity, new IgnoreCopyRotationToGameObject());
			}).WithStructuralChanges().Run();
		 */
		Entities
			//.WithAll<IgnoreCopyRotationToGameObject>()
			.WithAll<GhostClientView>()
			.ForEach((Entity animatorEntity,
					Animator animator
					) => {
						var entity = EntityManager.GetComponentData<Parent>(animatorEntity).Value;
						var prediction = EntityManager.GetComponentData<PredictedGhostComponent>(entity);

						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}
						
						var unitState = EntityManager.GetComponentData<UnitState>(entity);
						var animatorComponent = EntityManager.GetComponentData<AnimatorComponent>(entity);
						var moveSpeed = EntityManager.GetComponentData<MoveSpeed>(entity);
						var postRotationForAttack = EntityManager.GetComponentData<PostRotationForAttack>(entity);
						var rotation = EntityManager.GetComponentData<Rotation>(entity);
						var compositeScale = EntityManager.GetComponentData<CompositeScale>(entity);
						
						JobViewAnimator(entity,
										animator,
										ref unitState,
										ref animatorComponent,
										moveSpeed,
										prediction,
										postRotationForAttack,
										rotation,
										compositeScale
						);
						
						EntityManager.SetComponentData(entity, unitState);
						EntityManager.SetComponentData(entity, animatorComponent);

					}).WithStructuralChanges().Run();

	}

	private void JobViewAnimator(Entity entity,
								Animator animator,
								ref UnitState unitState,
								ref AnimatorComponent animatorComponent,
								MoveSpeed moveSpeed,
								PredictedGhostComponent prediction,
								PostRotationForAttack postRotationForAttack,
								Rotation rotation,
								CompositeScale compositeScale
	) {

		var animatorTr = animator.transform;

		/*
		var eulerAngles = animatorTr.eulerAngles;
		eulerAngles.y = postRotationForAttack.Angle;
		animatorTr.eulerAngles = eulerAngles;
		*/
		
		var progress = animatorComponent.AnimationProgress;

		animatorComponent.AnimationSpeed = 1;
		var speed = animatorComponent.AnimationSpeed;
 
		var animationState = (AnimationState)unitState.Value;
		
		if (animatorComponent.ClientLastAnimationTickPlayAnimation < animatorComponent.LastAnimationTickPlayAnimation) {
			animatorComponent.ClientLastAnimationTickPlayAnimation = animatorComponent.LastAnimationTickPlayAnimation;
			switch (animationState) {
				case AnimationState.Idle:
					animator.Play("Idle");
					break;
				case AnimationState.Movement:
					break;
				case AnimationState.Dead:
					animator.Play("Death");
					break;
				case AnimationState.Stunned:
					animator.Play("Stunned");
					break;
				case AnimationState.Sleeping:
					animator.Play("Sleeping");
					break;
				case AnimationState.Attack1:
					//if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack1")) {
					animator.Play("Attack1", 0, 0);
					//}
					break;
				case AnimationState.Attack2:
					//if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2")) {
					animator.Play("Attack2", 0, 0);
					//}
					break;
			}
			
		} else {
			//animatorComponent.ClientLastAnimationTickPlayAnimation =
			//	animatorComponent.LastAnimationTickPlayAnimation;
		}

		switch (animationState) {
			case AnimationState.Movement:
				var moveSpeedValue = moveSpeed.GetValueForWorld();
				animator.speed = speed * moveSpeedValue / compositeScale.Value.c0.x;

				if (moveSpeedValue < 4) {
					animator.Play("Walk");
				} else {
					animator.Play("Run");
				}
				break;
			default:
				animator.speed = speed;

				break;
		}

	}
}

}