using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Movement;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using AnimationState = StarFights.GameCore.Animation.AnimationState;

namespace StarFights.GameCore.Ghosts {

[DisableAutoCreation]
//[UpdateBefore(typeof(AbilityGhostSystemGroup))]
[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
public class HeroGhostSystem : SystemBase {

	private struct CreateProjectileData : IComponentData {
		public int NetworkId;
		public int ProjectileId;
		public float3 Position;
		public float3 Direction;
		public GhostProjectileOwner GhostProjectileOwner;
		public TeamId TeamId;
	}

	private struct RunAbilityData : IComponentData {
		public int NetworkId;
		public int AbilityId;
		public float3 Position;
		public quaternion Rotation;
		public GhostProjectileOwner GhostProjectileOwner;
		public TeamId TeamId;
	}


	private GhostPredictionSystemGroup m_PredictionGroup;

	private NativeHashMap<int, Entity> _projectilePrefabsMap;


	private BeginSimulationEntityCommandBufferSystem m_Barrier;

	private bool _isClientWorld;

	private struct NeedInitProjectilePrefabs : IComponentData {

	}

	protected override void OnCreate() {
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
		EntityManager.CreateEntity(typeof(NeedInitProjectilePrefabs));

		_isClientWorld = World.GetExistingSystem<ServerSimulationSystemGroup>() == null;

		m_Barrier = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();

		_projectilePrefabsMap = new NativeHashMap<int, Entity>(32, Allocator.Persistent);
	}

	protected override void OnDestroy() {
		_projectilePrefabsMap.Dispose();
	}


	private void InitProjectilePrefabs() {
		var ghostCollection = GetSingleton<GhostPrefabCollectionComponent>();
		if (!_isClientWorld) {
			var ghostCollectionEntity = GetSingletonEntity<GhostPrefabCollectionComponent>();
			var serverPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollectionEntity);
			for (int ghostId = 0; ghostId < serverPrefabs.Length; ++ghostId) {
				if (EntityManager.HasComponent<Projectile.Projectile>(serverPrefabs[ghostId].Value)) {
					var projectile =
						EntityManager.GetComponentData<Projectile.Projectile>(serverPrefabs[ghostId].Value);

					var prefab = serverPrefabs[ghostId].Value;
					_projectilePrefabsMap.Add(projectile.Id, prefab);
				}
			}
		} else {
			
			var ghostCollectionEntity = GetSingletonEntity<GhostPrefabCollectionComponent>();
			var clientPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollectionEntity)
											.ToNativeArray(Allocator.Temp);
			for (int ghostId = 0; ghostId < clientPrefabs.Length; ++ghostId) {
				if (EntityManager.HasComponent<Projectile.Projectile>(clientPrefabs[ghostId].Value)) {
					var projectile =
						EntityManager.GetComponentData<Projectile.Projectile>(clientPrefabs[ghostId].Value);

					var prefab = clientPrefabs[ghostId].Value;
					prefab = EntityManager.Instantiate(prefab);
					EntityManager.AddComponentData(prefab, default(Prefab));
					EntityManager.AddComponentData(prefab, default(PredictedGhostSpawnRequestComponent));

					_projectilePrefabsMap.Add(projectile.Id, prefab);
				}
			}
		}
	}



	protected override void OnUpdate() {
		Entities
			.WithAll<NeedInitProjectilePrefabs>()
			.ForEach((Entity entity) => {
				EntityManager.DestroyEntity(entity);

				InitProjectilePrefabs();
			}).WithStructuralChanges().Run();

		ActionsGhost();
		//AnimatorGhost();
		//AnimatorClient();

		m_Barrier.AddJobHandleForProducer(Dependency);
	}

	private void ActionsGhost() {
		void RunAbility(EntityCommandBuffer.ParallelWriter commandBuffer_,
								int nativeThreadIndex, RunAbilityData runAbilityData, NativeHashMap<int, Entity> projectilePrefabsMap_) {
			if (runAbilityData.AbilityId == 1) {
				var amountProjectiles = 5;
				var stepAngle = 5;
				var fullAngle = (amountProjectiles-1) *stepAngle;
				var fullAngleHalf = fullAngle/2;
				for (var i = 0; i < amountProjectiles; i++) {
					var rot = math.mul(
						runAbilityData.Rotation, 
						quaternion.Euler(
							new float3(0,math.radians(stepAngle * i - fullAngleHalf),0)
						)
					);
					CreateProjectile(commandBuffer_, nativeThreadIndex, new CreateProjectileData {
						ProjectileId = 1,
						Position = runAbilityData.Position,
						Direction = math.mul(rot, new float3(0, 0, 1)),
						NetworkId = runAbilityData.NetworkId,
						GhostProjectileOwner = runAbilityData.GhostProjectileOwner,
						TeamId = runAbilityData.TeamId
					}, projectilePrefabsMap_);
				}
			} else if (runAbilityData.AbilityId == 2) {
				CreateProjectile(commandBuffer_, nativeThreadIndex, new CreateProjectileData {
					ProjectileId = 2,
					Position = runAbilityData.Position,
					Direction = math.mul(runAbilityData.Rotation, new float3(0, 0, 1)),
					NetworkId = runAbilityData.NetworkId,
					GhostProjectileOwner = runAbilityData.GhostProjectileOwner,
					TeamId = runAbilityData.TeamId
				}, projectilePrefabsMap_);
			}
		}
		Entity CreateProjectile(EntityCommandBuffer.ParallelWriter commandBuffer_,
										int nativeThreadIndex, CreateProjectileData createRequestData, NativeHashMap<int, Entity> projectilePrefabsMap_) {
			var prefabGhostEntity = projectilePrefabsMap_[createRequestData.ProjectileId];

			var instanceEntity = commandBuffer_.Instantiate(nativeThreadIndex, prefabGhostEntity);

			commandBuffer_.SetComponent(nativeThreadIndex, instanceEntity, new GhostOwnerComponent {
				NetworkId = createRequestData.NetworkId
			});

			createRequestData.Position.y += 1;
			commandBuffer_.SetComponent(nativeThreadIndex, instanceEntity, new Translation {
				Value = createRequestData.Position
			});
			commandBuffer_.SetComponent(nativeThreadIndex, instanceEntity, new Rotation {
				Value = quaternion.LookRotation(createRequestData.Direction, new float3(0, 1, 0))
			});
			commandBuffer_.SetComponent(nativeThreadIndex, instanceEntity, new Power {
				Value = 1
			});

			commandBuffer_.SetComponent(nativeThreadIndex, instanceEntity, createRequestData.GhostProjectileOwner);
			commandBuffer_.SetComponent(nativeThreadIndex, instanceEntity, createRequestData.TeamId);

			return instanceEntity;
		}
		
		var tick = m_PredictionGroup.PredictingTick;
		var deltaTime = Time.DeltaTime;

		var commandBuffer = m_Barrier.CreateCommandBuffer().AsParallelWriter();

		var inputBufferFromEntity = GetBufferFromEntity<HeroInput>(true);
		var heroAbilitiesBufferFromEntity = GetBufferFromEntity<AbilityBufferElement>(true);
		var ghostComponentFromEntity = GetComponentDataFromEntity<GhostComponent>(true);
		var moveSpeedFromEntity = GetComponentDataFromEntity<MoveSpeed>(true);
		var ghostOwnerComponentFromEntity = GetComponentDataFromEntity<GhostOwnerComponent>(true);
		var heroMagazineComponentFromEntity = GetComponentDataFromEntity<AbilityMagazineComponent>(true);
		var predictedGhostComponentFromEntity = GetComponentDataFromEntity<PredictedGhostComponent>(true);
		var teamIdFromEntity = GetComponentDataFromEntity<TeamId>(true);
		var runAbilityGhostSystemRunAbilityFromEntity = GetComponentDataFromEntity<RunAbilityGhostSystem.RunAbility>(true);
		
		var isClientWorld = _isClientWorld;

		var projectilePrefabsMap = _projectilePrefabsMap;

		var isA = false;
		var isD = false;
		
		if (!_isClientWorld) {
			if (Input.GetKeyDown(KeyCode.A)) {
				isA = true;
			} else if (Input.GetKeyDown(KeyCode.D)) {
				isD = true;
			}
		}
		
		Entities
			.WithReadOnly(runAbilityGhostSystemRunAbilityFromEntity)
			.WithReadOnly(heroAbilitiesBufferFromEntity)
			//.WithReadOnly(teamIdFromEntity)
			//.WithReadOnly(projectilePrefabsMap)
			.WithReadOnly(inputBufferFromEntity)
			//.WithReadOnly(ghostComponentFromEntity)
			//.WithReadOnly(ghostOwnerComponentFromEntity)
			.WithReadOnly(moveSpeedFromEntity)
			//.WithReadOnly(heroMagazineComponentFromEntity)
			.WithReadOnly(predictedGhostComponentFromEntity)
			.WithAll<Hero.Hero>()
			.WithAll<GhostComponent>()
			.WithAll<GhostOwnerComponent>()
			.WithNone<DeadState>()
			.WithAll<MovementEnabled>()
			.WithAll<HeroInput>()
			.ForEach((Entity entity,
					int nativeThreadIndex,
					ref Translation translation,
					ref Rotation rotation,
					ref PostRotationForAttack postRotationForAttack,
					ref UnitState unitState,
					ref PhysicsVelocity physicsVelocity,
					ref AttackCommandServerTick attackCommandServerGhostTick
					) => {
				var prediction = predictedGhostComponentFromEntity[entity];
						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}

						//var ghostOwnerComponent = ghostOwnerComponentFromEntity[entity];
						//var ghostComponent = ghostComponentFromEntity[entity];
						var moveSpeed = moveSpeedFromEntity[entity];
						var inputBuffer = inputBufferFromEntity[entity];

						if (isA) {
							moveSpeed.Value -= 10;
							commandBuffer.SetComponent(nativeThreadIndex, entity, moveSpeed);
						}
						if (isD) {
							moveSpeed.Value += 10;
							commandBuffer.SetComponent(nativeThreadIndex, entity, moveSpeed);
						}

						HeroInput heroInput;
						if (!inputBuffer.GetDataAtTick(tick, out heroInput)) {
							heroInput.attackAngle = -1;
							heroInput.movementAngle = -1;
						}

#region Movement
				physicsVelocity.Linear = float3.zero;
				
						if (heroInput.movementAngle > 0) {
							var rot = Quaternion.AngleAxis(heroInput.movementAngle, Vector3.up);
							var direction = (float3)(rot * Vector3.forward);
							direction = math.normalize(direction);

							var offset = direction * moveSpeed.GetTotalValue();
							//offset *= deltaTime;
							translation.Value += offset * deltaTime;
							//physicsVelocity.Linear = offset;
							
							//postRotationForAttack.Value = input.angle % 360; //rotation.Value = rot;
							rotation.Value = rot;
							//postRotationForAttack.RealValue = input.angle % 360;

							if (unitState.Value == (int)AnimationState.Idle) {
								unitState.ValueNext = (int)AnimationState.Movement;
							}
							
						} else {
							//physicsVelocity.Linear = float3.zero;

							// Эти 3 строчки выполняются в PostSetAnimationStateClientSystem системе на клиенте
							if (!isClientWorld) {
								if (unitState.Value == (int)AnimationState.Movement) {
									unitState.ValueNext = (int)AnimationState.Idle;
								}
							}
						}

						unitState.angle = heroInput.movementAngle;
#endregion
#region Attack

				//var eulerAngles = ((Quaternion)rotation.Value).eulerAngles;
				
				/*
				var rotationTowardsSpeed = 5;
				postRotationForAttack.Value = Mathf.LerpAngle(
					postRotationForAttack.Value,
					postRotationForAttack.RealValue,
					deltaTime * rotationTowardsSpeed
				);
				 */

				/*
				postRotationForAttack.Angle = Mathf.MoveTowardsAngle(
					postRotationForAttack.Angle,
					((Quaternion)rotation.Value).eulerAngles.y,
					deltaTime * 360
				);
				 */
				
				
				//var heroMagazineComponent = heroMagazineComponentFromEntity[entity];

				//!isClientWorld && 
				if (heroInput.attackAngle > 0 && heroInput.attackCommandClientGhostTick > attackCommandServerGhostTick.Value
				) {
					attackCommandServerGhostTick.Value = heroInput.attackCommandClientGhostTick;
					//&& heroMagazineComponent.Amount > 0) {
					
					// Тут надо дать сигнал способности,
					// а там она сама решит хватате ли heroMagazineComponent.Amount

					{
						var heroAbilitiesBuffer = heroAbilitiesBufferFromEntity[entity];
						if (heroAbilitiesBuffer.Length <= heroInput.abilitySlotIndex) {
							return;
						}

						var abilityEntity = heroAbilitiesBuffer[heroInput.abilitySlotIndex].AbilityEntity;
						if (!runAbilityGhostSystemRunAbilityFromEntity.HasComponent(abilityEntity)) {
							commandBuffer.AddComponent(nativeThreadIndex, abilityEntity,
															new RunAbilityGhostSystem.RunAbility {
																AttackAngle = heroInput.attackAngle,
																tick = tick
															});

							
						}
					}
					return;
				}
				

#endregion

					}).ScheduleParallel();

		//AnimatorGhost();
	}

	/*
	
	private void AnimatorGhost() {
		var deltaTime = Time.fixedDeltaTime;

		var animationClipDataFromEntity = GetComponentDataFromEntity<AnimationClipData>(true);

		Entities
			.WithReadOnly(animationClipDataFromEntity)
			.WithAll<Hero.Hero>()
			.WithAll<GhostComponent>()
			.WithAll<GhostOwnerComponent>()
			.ForEach((Entity entity,
					int nativeThreadIndex,
					ref AnimatorComponent animatorComponent,
					ref PredictedGhostComponent prediction,
					ref HeroAnimationControllerGraph heroAnimationControllerGraph,
					ref UnitState unitState
					) => {
				animatorComponent.AnimationElapsedTime += deltaTime * animatorComponent.AnimationSpeed;
				
				Debug.Log("lol animatorComponent.AnimationElapsedTime = " + animatorComponent.AnimationElapsedTime + "; animatorComponent.ActiveClipLength = " + animatorComponent.ActiveClipLength);
				if (animatorComponent.AnimationElapsedTime > animatorComponent.ActiveClipLength) {
					animatorComponent.AnimationElapsedTime = 0;

					if ((AnimationState)unitState.Value != AnimationState.Movement
						&&
						(AnimationState)unitState.Value != AnimationState.Idle) {
						unitState.ValueNext = (int)AnimationState.Idle;
					}
				}
				animatorComponent.AnimationProgress = animatorComponent.AnimationElapsedTime / animatorComponent.ActiveClipLength;
				
				if (unitState.ValueNext != unitState.Value || heroAnimationControllerGraph.ActiveClipEntity.Equals(Entity.Null)) {
					//unitState.ValuePrevious = unitState.Value;
					unitState.Value = unitState.ValueNext;

					var clipEntity = Entity.Null;

					var animationState = (AnimationState)unitState.Value;
					switch (animationState) {
						case AnimationState.Idle:
							clipEntity = heroAnimationControllerGraph.IdleClipEntity;
							break;
						case AnimationState.Movement:
							clipEntity = heroAnimationControllerGraph.WalkClipEntity;
							break;
						case AnimationState.Dead:
							clipEntity = heroAnimationControllerGraph.DeathClipEntity;
							break;
						case AnimationState.Stunned:
							clipEntity = heroAnimationControllerGraph.StunnedClipEntity;
							break;
						case AnimationState.Attack1:
							clipEntity = heroAnimationControllerGraph.Attack1ClipEntity;
							break;
						case AnimationState.Attack2:
							clipEntity = heroAnimationControllerGraph.Attack2ClipEntity;
							break;
					}

					heroAnimationControllerGraph.ActiveClipEntity = clipEntity;

					var animationClipData = animationClipDataFromEntity[heroAnimationControllerGraph.ActiveClipEntity];

					animatorComponent.ActiveClipLength = animationClipData.Length;
					animatorComponent.AnimationElapsedTime = 0;
					animatorComponent.AnimationProgress = 0;
				}
				
			}).ScheduleParallel();
	}
	 */

	/*
	
	private void AnimatorClient() {
		if (_isClientWorld) {
			Entities
				.WithAll<GhostClientView>()
				.ForEach((Entity animatorEntity,
						Animator animator
						) => {
							var entity = EntityManager.GetComponentData<Parent>(animatorEntity).Value;

							var unitState = EntityManager.GetComponentData<UnitState>(entity);
							var animatorComponent = EntityManager.GetComponentData<AnimatorComponent>(entity);
							
							JobViewAnimator(entity,
											animator,
											ref unitState,
											animatorComponent
							);
							
							EntityManager.SetComponentData(entity, unitState);
						}).WithStructuralChanges().Run();

		}
	}

	private void JobViewAnimator(Entity entity,
								Animator animator,
								ref UnitState unitState,
								AnimatorComponent animatorComponent
	) {
		var progress = animatorComponent.AnimationProgress;

		animatorComponent.AnimationSpeed = 1;
		var speed = animatorComponent.AnimationSpeed;

		if (unitState.Value != unitState.ValuePrevious) {
			
			var animationState = (AnimationState)unitState.Value;
			switch (animationState) {
				case AnimationState.Idle:
					animator.speed = speed;
					animator.Play("Idle", 0, 0);
					break;
				case AnimationState.Movement:
					animator.speed = speed;
					animator.Play("Walk", 0, 0);
					break;
				case AnimationState.Dead:
					animator.speed = speed;
					animator.Play("Death", 0, 0);;
					break;
				case AnimationState.Stunned:
					animator.speed = speed;
					animator.Play("Stunned", 0, 0);
					//animator.Play("Death");
					break;
				case AnimationState.Attack1:
					animator.speed = speed;
					//if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack1")) {
					animator.Play("Attack1", 0, 0);
					//}
					break;
				case AnimationState.Attack2:
					animator.speed = speed;
					//if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Attack2")) {
					animator.Play("Attack2", 0, 0);
					//}
					break;
			}
			
			unitState.ValuePrevious = unitState.Value;
		}
	}
	 */
	
}

}