using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Ability;
using StarFights.GameCore.Animation;
using StarFights.GameCore.Unit;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using AnimationState = StarFights.GameCore.Animation.AnimationState;

namespace StarFights.GameCore.Ghosts {

[DisableAutoCreation]
[UpdateAfter(typeof(HeroGhostSystem))]
[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
public class AnimatorHeroGhostSystem : SystemBase {

	private BeginSimulationEntityCommandBufferSystem m_Barrier;

	private GhostPredictionSystemGroup m_PredictionGroup;

	protected override void OnCreate() {
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
		
		m_Barrier = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
	}

	protected override void OnDestroy() {
	}

	protected override void OnUpdate() {
		AnimatorGhost();
	}

	private void AnimatorGhost() {
		var tick = m_PredictionGroup.PredictingTick;
		var deltaTime = Time.DeltaTime;

		var animationClipDataFromEntity = GetComponentDataFromEntity<AnimationClipData>(true);
		var deadStateFromEntity = GetComponentDataFromEntity<DeadState>(true);
		var commandBuffer = m_Barrier.CreateCommandBuffer().AsParallelWriter();

		var isServerWorld = World.GetExistingSystem<ServerSimulationSystemGroup>() != null;
		
		Entities
			.WithReadOnly(deadStateFromEntity)
			.WithReadOnly(animationClipDataFromEntity)
			//.WithAll<Hero.Hero>()
			//.WithNone<DeadState>()
			.WithAll<GhostComponent>()
			//.WithAll<GhostOwnerComponent>()
			.ForEach((Entity entity,
					int nativeThreadIndex,
					ref AnimatorComponent animatorComponent,
					ref PredictedGhostComponent prediction,
					ref HeroAnimationControllerGraph heroAnimationControllerGraph,
					ref UnitState unitState
					) => {
				if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
					return;
				}

				if (unitState.ValuePrevious != unitState.Value) {
					unitState.ValuePrevious = unitState.Value;
				}

				var needFirstSetActiveClipEntity = heroAnimationControllerGraph.ActiveClipEntity.Equals(Entity.Null);
				if (unitState.ValueNext != unitState.Value || needFirstSetActiveClipEntity) {
					//unitState.ValuePrevious = unitState.Value;
					unitState.Value = unitState.ValueNext;

					if (!needFirstSetActiveClipEntity) {
						animatorComponent.LastAnimationTickPlayAnimation++;

						//animatorComponent.LastPlayAnimationTick = tick;
						//animatorComponent.LastPlayAnimationIndex = unitState.ValueNext;
					}

					var clipEntity = Entity.Null;

					var animationState = (AnimationState)unitState.Value;

					switch (animationState) {
						case AnimationState.Idle:
							clipEntity = heroAnimationControllerGraph.IdleClipEntity;
							break;
						case AnimationState.Movement:
							clipEntity = heroAnimationControllerGraph.WalkClipEntity;
							break;
						case AnimationState.Dead:
							clipEntity = heroAnimationControllerGraph.DeathClipEntity;
							break;
						case AnimationState.Stunned:
							clipEntity = heroAnimationControllerGraph.StunnedClipEntity;
							break;
						case AnimationState.Attack1:
							clipEntity = heroAnimationControllerGraph.Attack1ClipEntity;
							break;
						case AnimationState.Attack2:
							clipEntity = heroAnimationControllerGraph.Attack2ClipEntity;
							break;
					}


					heroAnimationControllerGraph.ActiveClipEntity = clipEntity;

					var animationClipData = animationClipDataFromEntity[heroAnimationControllerGraph.ActiveClipEntity];

					animatorComponent.ActiveClipLength = animationClipData.Length;
					animatorComponent.AnimationElapsedTime = 0;
					animatorComponent.AnimationProgress = 0;
				}

				{
					var animationState = (AnimationState)unitState.Value;

					if (animationState == AnimationState.Dead) {
						if (!deadStateFromEntity.HasComponent(entity)) {
							commandBuffer.AddComponent(nativeThreadIndex, entity, new DeadState());
						}
					} else {
						if (deadStateFromEntity.HasComponent(entity)) {
							commandBuffer.RemoveComponent<DeadState>(nativeThreadIndex, entity);
						}
					}
				}

				animatorComponent.AnimationProgress = animatorComponent.AnimationElapsedTime / animatorComponent.ActiveClipLength;

				animatorComponent.AnimationElapsedTime += deltaTime * animatorComponent.AnimationSpeed;
				
				if (animatorComponent.AnimationProgress >= 1) {
					animatorComponent.AnimationElapsedTime = 0;
					
					if ((AnimationState)unitState.Value != AnimationState.Movement
					&&
						(AnimationState)unitState.Value != AnimationState.Idle
					&&
						(AnimationState)unitState.Value != AnimationState.Dead
					&&
						(AnimationState)unitState.Value != AnimationState.Stunned
					&&
						(AnimationState)unitState.Value != AnimationState.Sleeping) {
						unitState.ValueNext = (int)AnimationState.Idle;
					}
				}
				
				
				

			}).ScheduleParallel();
	}

}

}