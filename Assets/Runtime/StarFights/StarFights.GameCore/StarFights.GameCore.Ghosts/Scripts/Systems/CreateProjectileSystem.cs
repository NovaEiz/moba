using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Movement;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;

namespace StarFights.GameCore.Ghosts {

/*


[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
[DisableAutoCreation]
public class CreateProjectileSystem : ComponentSystem {

	public struct CreateRequest : IComponentData {
		public int ProjectileId;
		public float3 Position;
		public float3 Direction;
		public uint spawnTick;
		public int NetworkId;
		
		public struct Result : IComponentData {
			public Entity Entity;
		}
	}
	
	private GhostPredictionSystemGroup m_PredictionGroup;

	protected override void OnCreate() {
		base.OnCreate();
		
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	protected override void OnUpdate() {
		var currentTick = m_PredictionGroup.PredictingTick;

		Entities
			.ForEach((Entity eventEntity,
					ref CreateRequest createRequest
					) => {
				var predicted = new PredictedGhostComponent {
					PredictionStartTick = createRequest.spawnTick
				};
				if (!GhostPredictionSystemGroup.ShouldPredict(currentTick, predicted)) {
					return;
				}
				
						var entity = Create(createRequest);
						EntityManager.SetComponentData(entity, new Translation {
							Value = createRequest.Position
						});
						EntityManager.SetComponentData(entity, new Rotation {
							Value = quaternion.LookRotation(createRequest.Direction, new float3(0,1,0))
						});
						
						EntityManager.RemoveComponent<CreateRequest>(eventEntity);
						EntityManager.AddComponentData(eventEntity, new CreateRequest.Result {
							Entity = entity
						});
					});
	}

	private Entity Create(CreateRequest createRequest) {
		var prefabGhostEntity = Entity.Null;
		
		var ghostCollection = GetSingleton<GhostPrefabCollectionComponent>();
		var serverPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollection.serverPrefabs);
		for (int ghostId = 0; ghostId < serverPrefabs.Length; ++ghostId) {
			if (EntityManager.HasComponent<Projectile.Projectile>(serverPrefabs[ghostId].Value)) {
				var projectile = EntityManager.GetComponentData<Projectile.Projectile>(serverPrefabs[ghostId].Value);
				if (projectile.Id == createRequest.ProjectileId) {
					prefabGhostEntity = serverPrefabs[ghostId].Value;
					break;
				}
			}
		}
		
		var instanceEntity = EntityManager.Instantiate(prefabGhostEntity);
		
		EntityManager.SetComponentData(instanceEntity, new GhostOwnerComponent {
			NetworkId = createRequest.NetworkId
		});
		EntityManager.AddBuffer<TriggerEventBufferElement>(instanceEntity);
		
		
		return instanceEntity;
	}
	
}

[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
[DisableAutoCreation]
public class CreateProjectileClientSystem : ComponentSystem {

	public struct CreateRequest : IComponentData {
		public int ProjectileId;
		public float3 Position;
		public float3 Direction;
		
		public struct Result : IComponentData {
			public Entity Entity;
		}
	}

	protected override void OnCreate() {
		base.OnCreate();

		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	protected override void OnUpdate() {

		
		Entities
			.ForEach((Entity eventEntity,
					ref CreateRequest createRequest
					) => {
				
						var entity = Create(createRequest);
						EntityManager.SetComponentData(entity, new Translation {
							Value = createRequest.Position
						});
						EntityManager.SetComponentData(entity, new Rotation {
							Value = quaternion.LookRotation(createRequest.Direction, new float3(0,1,0))
						});
						
						EntityManager.RemoveComponent<CreateRequest>(eventEntity);
						EntityManager.AddComponentData(eventEntity, new CreateRequest.Result {
							Entity = entity
						});
					});
	}

	private Entity Create(CreateRequest createRequest) {
		var prefabGhostEntity = Entity.Null;
		
		var ghostCollection = GetSingleton<GhostPrefabCollectionComponent>();
		var serverPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollection.clientPredictedPrefabs).ToNativeArray(Allocator.Temp);
		for (int ghostId = 0; ghostId < serverPrefabs.Length; ++ghostId) {
			if (EntityManager.HasComponent<Projectile.Projectile>(serverPrefabs[ghostId].Value)) {
				var projectile = EntityManager.GetComponentData<Projectile.Projectile>(serverPrefabs[ghostId].Value);
				if (projectile.Id == createRequest.ProjectileId) {
					prefabGhostEntity = serverPrefabs[ghostId].Value;
					break;
				}
			}
		}
		
		var instanceEntity = EntityManager.Instantiate(prefabGhostEntity);
		
		//EntityManager.AddBuffer<TriggerEventBufferElement>(instanceEntity);
		
		EntityManager.AddComponentData(instanceEntity, default(Prefab));
		EntityManager.AddComponentData(instanceEntity, default(PredictedGhostSpawnRequestComponent));

		instanceEntity = EntityManager.Instantiate(instanceEntity);

		
		EntityManager.SetComponentData(instanceEntity, new GhostOwnerComponent {
			NetworkId = GetSingleton<NetworkIdComponent>().Value
		});
		var moveSpeed = EntityManager.GetComponentData<MoveSpeed>(instanceEntity);
		moveSpeed.Init();
		EntityManager.SetComponentData(instanceEntity, moveSpeed);
		
		return instanceEntity;
	}
	
}
 */

}