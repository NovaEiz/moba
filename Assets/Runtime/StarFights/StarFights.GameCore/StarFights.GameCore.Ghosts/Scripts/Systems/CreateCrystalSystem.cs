using System;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using Nighday.General.EntitiesExtentions;
using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Hero;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace StarFights.GameCore.Ghosts {

[UpdateInGroup(typeof(SimulationSystemGroup))]
[UpdateBefore(typeof(Unity.Physics.Systems.BuildPhysicsWorld))]
[UpdateBefore(typeof(Unity.Physics.Systems.ExportPhysicsWorld))]
[DisableAutoCreation]
public class CreateCrystalSystem : ComponentSystem {

	public struct CreateRequest : IComponentData {
		public struct Result : IComponentData {
			public Entity Entity;
		}
	}

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	protected override void OnUpdate() {
		Entities
			.ForEach((Entity eventEntity,
					ref CreateRequest createRequest
					) => {
						var entity = Create(createRequest);
						EntityManager.RemoveComponent<CreateRequest>(eventEntity);
						EntityManager.AddComponentData(eventEntity, new CreateRequest.Result {
							Entity = entity
						});
					});
	}

	private Entity Create(CreateRequest createRequest) {
		var prefabGhostEntity = Entity.Null;

		var ghostCollectionEntity = GetSingletonEntity<GhostPrefabCollectionComponent>();
		var serverPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollectionEntity);
		for (int ghostId = 0; ghostId < serverPrefabs.Length; ++ghostId) {
			if (EntityManager.HasComponent<Crystal.Crystal>(serverPrefabs[ghostId].Value)) {
				prefabGhostEntity = serverPrefabs[ghostId].Value;
				break;
			}
		}

		var instanceEntity = EntityManager.Instantiate(prefabGhostEntity);

		//var instanceEntity = GhostCollectionSystem.CreatePredictedSpawnPrefab(EntityManager, prefabGhostEntity);

		EntityManager.SetComponentData(instanceEntity, new GhostOwnerComponent { NetworkId = -1});
		//EntityManager.AddBuffer<TriggerEventBufferElement>(instanceEntity);

		return instanceEntity;
	}
	
}
}