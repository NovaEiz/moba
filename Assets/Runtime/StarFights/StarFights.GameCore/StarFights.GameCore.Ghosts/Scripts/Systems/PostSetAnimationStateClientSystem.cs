using StarFights.GameCore.Animation;
using StarFights.GameCore.Unit;
using Unity.Entities;

namespace StarFights.GameCore.Ghosts {

/// <summary>
/// Эта система нужна чтобы анимация движения не прерывалось когда юнит еще двинается
/// </summary>
[DisableAutoCreation()]
[UpdateInGroup(typeof(SimulationSystemGroup))]
public class PostSetAnimationStateClientSystem : SystemBase {
	
	protected override void OnUpdate() {
		Entities
			.ForEach((Entity entity,
					ref UnitState unitState
					) => {
						if (unitState.angle > 0) {
					
						} else {
							if (unitState.Value == (int)AnimationState.Movement) {
								unitState.ValueNext = (int)AnimationState.Idle;
							}
						}
					}).WithStructuralChanges().Run();
	}
}
}