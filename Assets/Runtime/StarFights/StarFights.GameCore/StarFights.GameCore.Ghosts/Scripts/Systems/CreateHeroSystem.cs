using System;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using Nighday.General.EntitiesExtentions;
using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Movement;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace StarFights.GameCore.Ghosts {

[UpdateInGroup(typeof(SimulationSystemGroup))]
//[UpdateAfter(typeof(Unity.Physics.Systems.BuildPhysicsWorld))]
//[UpdateAfter(typeof(Unity.Physics.Systems.StepPhysicsWorld))]
[UpdateBefore(typeof(Unity.Physics.Systems.BuildPhysicsWorld))]
[UpdateBefore(typeof(Unity.Physics.Systems.ExportPhysicsWorld))]
//[UpdateBefore(typeof(Unity.Physics.Systems.EndFramePhysicsSystem))]
[DisableAutoCreation]
public class CreateHeroSystem : ComponentSystem {

	public struct CreateRequest : IComponentData {
		public int HeroId;

		public struct Result : IComponentData {
			public Entity Entity;
		}
	}

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	protected override void OnUpdate() {
		Entities
			.ForEach((Entity eventEntity,
					ref CreateRequest createRequest
					) => {
						var entity = Create(createRequest);
						EntityManager.RemoveComponent<CreateRequest>(eventEntity);
						EntityManager.AddComponentData(eventEntity, new CreateRequest.Result {
							Entity = entity
						});
					});
	}

	private Entity Create(CreateRequest createRequest) {
		var prefabGhostEntity = Entity.Null;

		var heroId = createRequest.HeroId;
		
		var ghostCollectionEntity = GetSingletonEntity<GhostPrefabCollectionComponent>();
		var serverPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollectionEntity);
		for (int ghostId = 0; ghostId < serverPrefabs.Length; ++ghostId) {
			if (EntityManager.HasComponent<Hero.Hero>(serverPrefabs[ghostId].Value)) {
				if (EntityManager.GetComponentData<Hero.Hero>(serverPrefabs[ghostId].Value).Id
					== 
					heroId
				) {
					prefabGhostEntity = serverPrefabs[ghostId].Value;
					break;
				}
			}
		}

		var instanceEntity = EntityManager.Instantiate(prefabGhostEntity);

		//var instanceEntity = GhostCollectionSystem.CreatePredictedSpawnPrefab(EntityManager, prefabGhostEntity);
		
		EntityManager.SetComponentData(instanceEntity, new GhostOwnerComponent { NetworkId = -1});
		//EntityManager.AddBuffer<TriggerEventBufferElement>(instanceEntity);
		
		//EntityManager.AddBuffer<HeroSnapshotData>(instanceEntity);
		//var prefabEntity = _gameDatabaseSystem.UnitsDatabaseSystem.GetPrefab(id);
		//var instanceEntity = EntityManager.Instantiate(prefabEntity);
		//if (EntityManager.HasComponent<Unit>(prefabEntity)) {
			//prefabGhostEntity = GetPrefabGhostPrefab<HeroSnapshotData>();
			//CopyComponentsUnit(EntityManager, prefabGhostEntity, instanceEntity);

			//EntityManager.AddBuffer<HeroSnapshotData>(instanceEntity);
		//}

		return instanceEntity;
	}
	

	/*
	private void LoadHeroView(int heroId, Action<HeroAsset> callback) {
		var path = "";
		if (heroId == 1) {
			path = "WarriorHeroAsset";
		} else if (heroId == 2) {
			path = "MageHeroAsset";
		}
		
		var handleAsync = Addressables.LoadAssetAsync<ScriptableObject>(path);
		handleAsync.Completed += (AsyncOperationHandle<ScriptableObject> handle) => {
			if (handle.Status == AsyncOperationStatus.Succeeded) {
				var prefab = handle.Result;
				callback((HeroAsset)prefab);
			}
		};
	}

	private void LoadGameObject(string path, Action<GameObject> callback) {
		var handleAsync    = Addressables.LoadAssetAsync<GameObject>(path);
		handleAsync.Completed += (AsyncOperationHandle<GameObject> handle) => {
			if (handle.Status == AsyncOperationStatus.Succeeded) {
				var prefab = handle.Result;
				callback(prefab);
			}
		};
	}

	private static void CopyGhostComponents(EntityManager EntityManager, Entity from, Entity to) {
		EntityManager.CopyComponentData<GhostComponent>(from, to);
		EntityManager.CopyComponentData<GhostTypeComponent>(from, to);
		EntityManager.CopyComponentData<PredictedGhostComponent>(from, to);
		EntityManager.CopySharedComponentData<SharedGhostTypeComponent>(from, to);
	}
	 */
	/*
	private Entity GetPrefabGhostPrefab<T>() where T : struct, ISnapshotData<T> {
		var ghostCollection = GetSingleton<GhostPrefabCollectionComponent>();
		var ghostId = StarFightsGhostSerializerCollection.FindGhostType<T>();
		var prefab          = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollection.serverPrefabs)[ghostId].Value;
		return prefab;
	}
	*/

	
}
}