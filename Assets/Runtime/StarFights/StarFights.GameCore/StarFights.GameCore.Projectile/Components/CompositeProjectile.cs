using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Projectile {

[GenerateAuthoringComponent]
public struct CompositeProjectile : IComponentData {
	public Entity ProjectileEntity;
}
}