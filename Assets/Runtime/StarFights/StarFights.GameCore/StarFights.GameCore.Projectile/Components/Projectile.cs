using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Projectile {

[GenerateAuthoringComponent]
public struct Projectile : IComponentData {
	[GhostField]
	public int Id;
}
}