using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Projectile {

[GenerateAuthoringComponent]
public struct ProjectileLifeDistance : IComponentData {
	public float Distance;
	[HideInInspector] public float ElapsedDistance;
	[HideInInspector] public float3 PositionPrevious;
	[HideInInspector] public bool Started;
}
}