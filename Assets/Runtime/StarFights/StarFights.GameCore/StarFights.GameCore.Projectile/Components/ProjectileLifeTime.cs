using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Projectile {

[GenerateAuthoringComponent]
public struct ProjectileLifeTime : IComponentData {
	public float Time;
	public float ElapsedTime;
}
}