using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Projectile {

[DisableAutoCreation]
[UpdateInGroup(typeof(SimulationSystemGroup))]
[UpdateAfter(typeof(BuildPhysicsWorld))]
public class ProjectileLifeServerSystem : SystemBase {
	
	private bool _isServerWorld;

	protected override void OnCreate() {
		_isServerWorld = World.GetExistingSystem<ServerSimulationSystemGroup>() != null;
	}

	protected override void OnUpdate() {
		var deltaTime = Time.DeltaTime;
		
		Entities
			.ForEach((Entity entity,
					ref LocalToWorld localToWorld,
					ref Translation translation,
					ref ProjectileLifeDistance projectileLifeDistance
					) => {
						if (!projectileLifeDistance.Started) {
							projectileLifeDistance.Started = true;

							projectileLifeDistance.PositionPrevious = translation.Value;
						} else {
							var positionOffset = localToWorld.Position - projectileLifeDistance.PositionPrevious;
							projectileLifeDistance.PositionPrevious = localToWorld.Position;
							projectileLifeDistance.ElapsedDistance += math.length(positionOffset);
						}

						var needDestroy = false;
						if (projectileLifeDistance.ElapsedDistance >= projectileLifeDistance.Distance) {
							//EntityManager.DestroyEntity(entity);
							needDestroy = true;
						}

						if (needDestroy) {
							if (_isServerWorld) {
								EntityManager.DestroyEntity(entity);
							} else {
								if (EntityManager.HasComponent<Child>(entity)) {
									var childBufferArray = EntityManager.GetBuffer<Child>(entity).ToNativeArray(Allocator.Temp);
											
									var len = childBufferArray.Length;

									var wasChildTransform = false;

									for (var i = 0; i < len; i++) {
										var childEntity = childBufferArray[i].Value;
										if (EntityManager.HasComponent<GhostChildEntityComponent>(childEntity)) {
											if (!EntityManager.HasComponent<Transform>(childEntity)) {
												EntityManager.DestroyEntity(childEntity);
											} else {
												EntityManager.RemoveComponent<Parent>(childEntity);
												wasChildTransform = true;
											}
										}
									}
									
									if (!wasChildTransform) {
										EntityManager.DestroyEntity(entity);
									} else {
										EntityManager.RemoveComponent<Child>(entity);
									}

								}
										
							}
						}
			}).WithStructuralChanges().Run();
		Entities
			.ForEach((Entity entity,
					ref ProjectileLifeTime projectileLifeTime
					) => {
						projectileLifeTime.ElapsedTime += deltaTime;
						
						var needDestroy = false;

						if (projectileLifeTime.ElapsedTime >= projectileLifeTime.Time) {
							//EntityManager.DestroyEntity(entity);
							needDestroy = true;
						}

						if (needDestroy) {
							if (_isServerWorld) {
								EntityManager.DestroyEntity(entity);
							} else {
								if (EntityManager.HasComponent<Child>(entity)) {
									var childBufferArray = EntityManager.GetBuffer<Child>(entity).ToNativeArray(Allocator.Temp);
											
									var len = childBufferArray.Length;

									var wasChildTransform = false;
	
									for (var i = 0; i < len; i++) {
										var childEntity = childBufferArray[i].Value;
										if (EntityManager.HasComponent<GhostChildEntityComponent>(childEntity)) {
											if (!EntityManager.HasComponent<Transform>(childEntity)) {
												EntityManager.DestroyEntity(childEntity);
											} else {
												EntityManager.RemoveComponent<Parent>(childEntity);
												wasChildTransform = true;
											}
										}
									}
	
									if (!wasChildTransform) {
										EntityManager.DestroyEntity(entity);
									} else {
										EntityManager.RemoveComponent<Child>(entity);
									}
								}
									
							}
						}
					}).WithStructuralChanges().Run();
	}
}
}