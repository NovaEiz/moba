using System;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Animation {

[GenerateAuthoringComponent]
public struct HeroAnimationControllerGraph : IComponentData {
	public Entity IdleClipEntity;
	public Entity WalkClipEntity;
	public Entity Attack1ClipEntity;
	public Entity Attack2ClipEntity;
	public Entity DeathClipEntity;
	public Entity StunnedClipEntity;
	public Entity SleepingClipEntity;

	[HideInInspector]
	public Entity ActiveClipEntity;
}
}