using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Animation {

public struct AnimationClipData : IComponentData {
	public float Length;
	public float Speed;
}
}