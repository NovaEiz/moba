using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Animation {

[GenerateAuthoringComponent]
public struct AnimatorComponent : IComponentData {
	public float ActiveClipLength;
	public float AnimationElapsedTime;
	//[GhostField(Quantization=100)]
	public float AnimationProgress;
	[GhostField(Quantization=100)]
	public float AnimationSpeed;
	
	[GhostField]
	public int LastAnimationTickPlayAnimation;
	public int ClientLastAnimationTickPlayAnimation;
	
	[GhostField]
	public uint LastPlayAnimationTick;
	public uint ClientLastPlayAnimationTick;
	[GhostField]
	public int LastPlayAnimationIndex;

}
}