using Unity.Entities;
using UnityEngine;

namespace StarFights.GameCore.Animation {

public class AnimationClipAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private AnimationClip _clip;
	[SerializeField] private float _speed = 1f;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentData(entity, new AnimationClipData {
			Length = _clip.length / _speed
		});
		var buffer = dstManager.AddBuffer<AnimationClipEventDataBufferElement>(entity);

		var events = _clip.events;
		var len = events.Length;
		for (int i = 0; i < len; i++) {
			buffer.Add(new AnimationClipEventDataBufferElement {
				Key = 0,
				TimePoint = events[i].time / _speed
			});
		}
	}
}

}