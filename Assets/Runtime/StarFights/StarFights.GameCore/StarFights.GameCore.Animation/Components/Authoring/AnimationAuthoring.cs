using Unity.Entities;
using UnityEngine;

namespace StarFights.GameCore.Animation {

public class AnimationAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentObject(entity, gameObject.GetComponent<Animator>());
	}
}

}