using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Animation {

public struct AnimationClipEventDataBufferElement : IBufferElementData {
	/// <summary>
	/// Id анимации
	/// </summary>
	public ushort Key;
	
	public float TimePoint;
}
}