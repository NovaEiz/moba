namespace StarFights.GameCore.Animation {
public enum AnimationState {
	Idle = 0,
	Movement = -1,
	Cast = -2,
	Dead = -3,
	Stunned = -4,
	Sleeping = -5,
	Attack1 = 1,
	Attack2 = 2
}
}