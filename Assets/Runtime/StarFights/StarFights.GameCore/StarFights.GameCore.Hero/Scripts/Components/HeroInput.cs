using Unity.NetCode;
using Unity.Networking.Transport;

namespace StarFights.GameCore.Hero {

//[NetCodeDisableCommandCodeGen]
public struct HeroInput : ICommandData {

	public uint tick;

	public uint Tick {
		get => tick;
		set => tick = value;
	}

	public float movementAngle;

	public int abilitySlotIndex;
	public float attackAngle;
	public uint attackCommandClientGhostTick;
}

}