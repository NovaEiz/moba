using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Hero {

[GenerateAuthoringComponent]
public struct Hero : IComponentData {
	[GhostField(Quantization=-1)]
	//[GhostDefaultField(-1)]
	public int Id;
}

}