using StarFights.GameCore.Movement;
using Unity.Entities;
using UnityEngine;

namespace StarFights.GameCore.Hero {

public class HeroInputAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.AddBuffer<HeroInput>(entity);
	}
}

}