using Unity.Entities;
using UnityEngine;

namespace StarFights.GameCore.Movement {

[DisableAutoCreation]
public class MoveSpeedInitServerSystem : ComponentSystem {
	
	private struct MoveSpeedAuthorizedTag : IComponentData {}

	protected override void OnUpdate() {
		Entities
			.WithNone<MoveSpeedAuthorizedTag>()
			.ForEach((Entity entity,
					ref MoveSpeed moveSpeed
					) => {
						moveSpeed.Init();
						//moveSpeed.IncreaseValue += UnityEngine.Random.Range(0, 500);
						EntityManager.AddComponentData(entity, new MoveSpeedAuthorizedTag());
					});
	}
	
}
}