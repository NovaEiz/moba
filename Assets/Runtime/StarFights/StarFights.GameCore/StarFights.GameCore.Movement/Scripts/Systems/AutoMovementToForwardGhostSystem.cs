using Nighday.General.MathematicsExtentions;
using StarFights.GameCore.Unit;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Movement {

[DisableAutoCreation]
[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
//[UpdateInGroup(typeof(SimulationSystemGroup))]
public class AutoMovementToForwardGhostSystem : ComponentSystem {

	private GhostPredictionSystemGroup _ghostPredictionSystemGroup;

	protected override void OnCreate() {
		_ghostPredictionSystemGroup = World.GetExistingSystem<GhostPredictionSystemGroup>();
	}

	protected override void OnUpdate() {
		var tick      = _ghostPredictionSystemGroup.PredictingTick;
		var deltaTime = Time.DeltaTime;
		Entities
			.WithNone<DeadState>()
			.WithAll<AutoMoveToForward>()
			.WithAll<MovementEnabled>()
			.ForEach((Entity entity,
					ref Translation translation,
					ref LocalToWorld localToWorld,
					ref Rotation rotation,
					ref MoveSpeed moveSpeed,
					ref PredictedGhostComponent prediction
					) => {
						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}

						var direction = (float3)(math.mul(rotation.Value, new float3(0,0,1)));
						direction = math.normalize(direction);

						var offset = direction * moveSpeed.GetTotalValue();
						offset *= deltaTime;

						translation.Value += offset;
					});
	}
}
	
}