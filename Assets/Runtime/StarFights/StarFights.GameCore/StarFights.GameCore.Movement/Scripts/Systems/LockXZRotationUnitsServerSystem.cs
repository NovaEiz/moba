using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Movement {

[DisableAutoCreation]
public class LockXZRotationUnitsServerSystem : ComponentSystem {
	
	protected override void OnUpdate() {
		Entities
			.WithAll<Unit.Unit>()
			.ForEach((Entity entity,
					ref PhysicsMass physicsMass,
					ref PhysicsVelocity physicsVelocity
					) => {
				physicsMass.InverseInertia = new float3(0);
				physicsVelocity.Angular = float3.zero;
				//rotation.Value = quaternion.Euler(new float3(0, math.radians(((Quaternion)rotation.Value).eulerAngles.y), 0));
			});
	}
	
}
}