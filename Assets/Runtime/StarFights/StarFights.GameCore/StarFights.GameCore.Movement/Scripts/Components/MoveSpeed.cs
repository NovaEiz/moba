using System;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Movement {

[Serializable]
public struct MoveSpeed : IComponentData {

	[HideInInspector]
	[GhostField(Quantization=100)]
	//[GhostDefaultField(100)]
	public float Value;
	public float BaseValue;
	
	[HideInInspector]
	public float IncreaseValue;
	
	public float IncreaseValueP {
		get { return IncreaseValue; }
		set { IncreaseValue = value;  }
	}
	
	[HideInInspector]
	public float PercentIncreaseValue; //От бафов и предметов

	public float PercentIncreaseValueP {
		get { return PercentIncreaseValue; }
		set { PercentIncreaseValue = value;  }
	}
	
	public void Init() {
		Value = BaseValue;
	}
	
	public float GetValueForWorld() {
		return Value / 100;
	}
	
	private float ConvertValueForWorld(float value) {
		var worldSpeedCoef = 100;
		return value / worldSpeedCoef;
	}

	public float GetTotalValue() {
		var totalValue = Value + IncreaseValue + (Value * PercentIncreaseValue);
		totalValue = ConvertValueForWorld(totalValue);
		return totalValue;
	}
	
}

}