using StarFights.GameCore.Movement;
using Unity.Entities;
using UnityEngine;

namespace StarFights.GameCore.Ghosts {

public class MoveSpeedAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	public MoveSpeed _moveSpeed;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		_moveSpeed.Init();
		dstManager.AddComponentData(entity, _moveSpeed);
	}
}

}