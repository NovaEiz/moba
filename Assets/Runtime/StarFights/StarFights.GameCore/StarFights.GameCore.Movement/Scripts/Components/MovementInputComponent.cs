using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Movement {

[GenerateAuthoringComponent]
public struct MovementInputComponent : IComponentData {
	[HideInInspector] public float movementAngle;
}

}