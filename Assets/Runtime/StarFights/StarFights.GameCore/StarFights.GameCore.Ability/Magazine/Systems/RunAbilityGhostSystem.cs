using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;
using AnimationState = StarFights.GameCore.Animation.AnimationState;

namespace StarFights.GameCore.Ability {

[DisableAutoCreation]
[UpdateInGroup(typeof(AbilityGhostSystemGroup))]
public class RunAbilityGhostSystem : SystemBase {

	public struct RunAbility : IComponentData {
		public uint tick;
		public float AttackAngle;
	}

	private GhostPredictionSystemGroup m_PredictionGroup;
	private BeginSimulationEntityCommandBufferSystem m_Barrier;

	private bool _isClientWorld;
	
	protected override void OnCreate() {
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
		
		m_Barrier = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
		
		_isClientWorld = World.GetExistingSystem<ServerSimulationSystemGroup>() == null;
	}


	protected override void OnUpdate() {
		var tick = m_PredictionGroup.PredictingTick;

		var commandBuffer = m_Barrier.CreateCommandBuffer().AsParallelWriter();

		var inputBufferFromEntity = GetBufferFromEntity<HeroInput>(true);
		
		var deltaTime = Time.DeltaTime;
		Entities
			.ForEach((Entity abilityEntity,
					ref AbilityMagazineComponent heroMagazineComponent,
					ref GhostChildEntityComponent ghostChildEntityComponent,
					ref Parent parent,
					ref RunAbility runAbility
					) => {
				if (heroMagazineComponent.Amount == 0) {
					EntityManager.RemoveComponent<RunAbility>(abilityEntity);
					return;
				}
						var entity = parent.Value;
						
						var prediction = EntityManager.GetComponentData<PredictedGhostComponent>(entity);
						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}

						if (heroMagazineComponent.Amount > 0) {

							heroMagazineComponent.Amount--;
							
							var inputBuffer = inputBufferFromEntity[entity];

							HeroInput heroInput;
							if (!inputBuffer.GetDataAtTick(runAbility.tick, out heroInput)) {
								heroInput.attackAngle = -1;
								heroInput.movementAngle = -1;
							}

							{

								var unitState = EntityManager.GetComponentData<UnitState>(entity);

								if (EntityManager.HasComponent<LastInteractionWithWorld>(entity)) {
									EntityManager.SetComponentData(entity, new LastInteractionWithWorld {
										ElapsedTime = 0
									});
								} else {
									EntityManager.AddComponentData(entity, new LastInteractionWithWorld {
										ElapsedTime = 0
									});
								}
					
								unitState.ValueNext = (int)AnimationState.Attack1;
								unitState.Value = (int)AnimationState.Idle;
								
								EntityManager.SetComponentData(entity, unitState);


								var postRotationForAttack = new PostRotationForAttack {
									Angle = runAbility.AttackAngle % 360
								};
								EntityManager.SetComponentData(entity, postRotationForAttack);
								

								//rotation.Value = quaternion.Euler(new float3(0, math.radians(postRotationForAttack.Value), 0));

								var throwRotation = quaternion.Euler(
									new float3(0, math.radians(postRotationForAttack.Angle), 0)
								);
								//Debug.Log("throwRotation = " + throwRotation);

								EntityManager.AddComponentData(abilityEntity, new CastAbility {
									ThrowRotation = throwRotation
								});

								// Тут создать событие на запуск способности

							}
						}
						
						EntityManager.RemoveComponent<RunAbility>(abilityEntity);
					}).WithStructuralChanges().Run();

		Entities
			.ForEach((Entity entity,
					ref RunAbility runAbility
					) => {
				EntityManager.RemoveComponent<RunAbility>(entity);
			}).WithStructuralChanges().Run();
	}
	
}

}