using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Animation;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[DisableAutoCreation]
[UpdateAfter(typeof(RunAbilityGhostSystem))]
[UpdateInGroup(typeof(AbilityGhostSystemGroup))]
public class MultipleProjectileAbilityCastGhostSystem : CastAbilityAbilityGhostSystemBase {

	private bool _isServerWorld;

	protected override void OnCreate() {
		base.OnCreate();
		
		_isServerWorld = World.GetExistingSystem<ServerSimulationSystemGroup>() != null;
	}


	protected override void OnUpdate() {
		var tick = m_PredictionGroup.PredictingTick;

		var commandBuffer = m_Barrier.CreateCommandBuffer().AsParallelWriter();

		var inputBufferFromEntity = GetBufferFromEntity<HeroInput>(true);

		var deltaTime = Time.DeltaTime;
		Entities
			.ForEach((Entity abilityEntity,
					ref GhostChildEntityComponent ghostChildEntityComponent,
					ref Parent parent,
					ref AbilityProjectile abilityProjectile,
					ref CastAbility castAbility,
					ref MultipleProjectileAbility multipleProjectile
					) => {
				var entity = parent.Value;
						PredictedGhostComponent prediction = EntityManager.GetComponentData<PredictedGhostComponent>(entity);
						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}

						EntityManager.RemoveComponent<CastAbility>(abilityEntity);

						var amountProjectiles = multipleProjectile.Amount;
						var angleDistance = multipleProjectile.AngleDistance;
						
						var localToWorld = EntityManager.GetComponentData<LocalToWorld>(entity);
						var ghostOwnerComponent = EntityManager.GetComponentData<GhostOwnerComponent>(entity);
						var ghostComponent = EntityManager.GetComponentData<GhostComponent>(entity);
						var teamId = EntityManager.GetComponentData<TeamId>(entity);
	
						var fullAngle = (amountProjectiles-1) * angleDistance;
						var fullAngleHalf = fullAngle/2;
						for (var i = 0; i < amountProjectiles; i++) {
							var projectileRotation = math.mul(
								castAbility.ThrowRotation, 
								quaternion.Euler(
									new float3(0,math.radians(angleDistance * i - fullAngleHalf),0)
								)
							);
							
							var projectile = CreateProjectile(new CreateProjectileData {
								ProjectileId = abilityProjectile.ProjectileId,
								Position = localToWorld.Position,
								Direction = math.mul(projectileRotation, new float3(0, 0, 1)),
								NetworkId = ghostOwnerComponent.NetworkId,
								GhostProjectileOwner = new GhostProjectileOwner {
									GhostId = ghostComponent.ghostId,
									GhostEntity = entity
								},
								TeamId = teamId
							});
						}
						
			}).WithStructuralChanges().Run();
	}
	
}

}