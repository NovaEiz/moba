using StarFights.GameCore.Animation;
using StarFights.GameCore.Hero;
using Unity.Entities;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[DisableAutoCreation]
//[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
[UpdateInGroup(typeof(SimulationSystemGroup))]
public class AbilityMagazineGhostSystem : SystemBase {

	private GhostPredictionSystemGroup m_PredictionGroup;

	protected override void OnCreate() {
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
	}

	private float _rechargeDurationSeconds = 2f;
	
	
	protected override void OnUpdate() {
		var tick = m_PredictionGroup.PredictingTick;

		var deltaTime = Time.DeltaTime;
		var elapsedTime = (float)Time.ElapsedTime;
		var stepTime = deltaTime / _rechargeDurationSeconds;

		
		Entities
			.ForEach((Entity entity,
					ref AbilityCooldown abilityCooldown,
					ref AbilityCooldownDuration abilityCooldownDuration,
					ref GhostChildEntityComponent ghostChildEntityComponent,
					ref Parent parent
					) => {
						if (abilityCooldown.Progress >= 1) {
							EntityManager.RemoveComponent<AbilityCooldown>(entity);
							return;
						}
						
						abilityCooldown.ElapsedTime = elapsedTime - abilityCooldown.StartTime;
						abilityCooldown.Progress = abilityCooldown.ElapsedTime / abilityCooldownDuration.Value;

			}).WithStructuralChanges().Run();

		Entities
			.WithNone<AbilityCooldown>()
			.ForEach((Entity entity,
					ref AbilityMagazineComponent heroMagazineComponent,
					ref GhostChildEntityComponent ghostChildEntityComponent,
					ref Parent parent
					) => {

						if (heroMagazineComponent.Amount < heroMagazineComponent.MaxAmount) {
							EntityManager.AddComponentData(entity, new AbilityCooldown {
								StartTime = elapsedTime
							});
						}
					}).WithStructuralChanges().Run();
		
		Entities
			.ForEach((Entity entity,
					ref AbilityCooldown abilityCooldown,
					ref AbilityMagazineComponent heroMagazineComponent,
					ref GhostChildEntityComponent ghostChildEntityComponent,
					ref Parent parent
					) => {

						if (abilityCooldown.Progress >= 1) {
							heroMagazineComponent.Amount++;
							heroMagazineComponent.RechargeProgress = 0f;
							return;
						}
						
						heroMagazineComponent.RechargeProgress = abilityCooldown.Progress;

					}).WithStructuralChanges().Run();

		/*
		
		Entities
			.ForEach((Entity entity,
					ref AbilityCooldown abilityCooldown,
					ref AbilityCooldownDuration abilityCooldownDuration,
					ref GhostChildEntityComponent ghostChildEntityComponent,
					ref Parent parent
					) => {
						PredictedGhostComponent prediction = EntityManager.GetComponentData<PredictedGhostComponent>(parent.Value);
						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}
						if (abilityCooldown.Progress >= 1) {
							EntityManager.RemoveComponent<AbilityCooldown>(entity);
							return;
						}

						abilityCooldown.ElapsedTime += deltaTime;
						abilityCooldown.Progress = abilityCooldown.ElapsedTime / abilityCooldownDuration.Value;
			}).WithStructuralChanges().Run();
		 */


	}
	/*

	protected override void OnUpdate() {
		var tick = m_PredictionGroup.PredictingTick;

		var deltaTime = Time.DeltaTime;
		var stepTime = deltaTime / _rechargeDurationSeconds;
		Entities
			.ForEach((Entity entity,
					DynamicBuffer<HeroAbilitiesBufferElement> heroAbilitiesBuffer,
					ref HeroMagazineComponent heroMagazine,
					ref PredictedGhostComponent prediction
					) => {
						if (!GhostPredictionSystemGroup.ShouldPredict(tick, prediction)) {
							return;
						}
				
						if (heroMagazine.Amount < heroMagazine.MaxAmount) {
							heroMagazine.RechargeProgress += stepTime;
							if (heroMagazine.RechargeProgress >= 1) {
								heroMagazine.RechargeProgress = 0f;
								heroMagazine.Amount++;
							}
						}
					}).WithStructuralChanges().Run();
	}
	 */
	
}

}