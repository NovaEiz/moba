using StarFights.GameCore.Animation;
using StarFights.GameCore.Attack;
using StarFights.GameCore.Hero;
using StarFights.GameCore.Location;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[DisableAutoCreation]
[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
public class CastAbilityAbilityGhostSystemBase : SystemBase {

	protected GhostPredictionSystemGroup m_PredictionGroup;
	protected BeginSimulationEntityCommandBufferSystem m_Barrier;
	protected ClientSimulationSystemGroup _clientSimulationSystemGroup;

	private NativeHashMap<int, Entity> _projectilePrefabsMap;

	protected override void OnCreate() {
		m_PredictionGroup = World.GetOrCreateSystem<GhostPredictionSystemGroup>();
		_clientSimulationSystemGroup = World.GetExistingSystem<ClientSimulationSystemGroup>();

		m_Barrier = World.GetOrCreateSystem<BeginSimulationEntityCommandBufferSystem>();
	}


	protected override void OnUpdate() {

	}
	
	protected struct CreateProjectileData : IComponentData {
		public int NetworkId;
		public int ProjectileId;
		public float3 Position;
		public float3 Direction;
		public GhostProjectileOwner GhostProjectileOwner;
		public TeamId TeamId;
	}
	
	protected Entity CreateProjectile(CreateProjectileData createRequestData) {
		var prefabGhostEntity = GetProjectilePrefab(createRequestData.ProjectileId);
		prefabGhostEntity = GhostCollectionSystem.CreatePredictedSpawnPrefab(EntityManager, prefabGhostEntity);

		var instanceEntity = EntityManager.Instantiate(prefabGhostEntity);//EntityManager.Instantiate(nativeThreadIndex, prefabGhostEntity);

		EntityManager.SetComponentData(instanceEntity, new GhostOwnerComponent {
			NetworkId = createRequestData.NetworkId
		});

		createRequestData.Position.y += 0;
		EntityManager.SetComponentData(instanceEntity, new Translation {
			Value = createRequestData.Position
		});
		EntityManager.SetComponentData(instanceEntity, new Rotation {
			Value = quaternion.LookRotation(createRequestData.Direction, new float3(0, 1, 0))
		});
		EntityManager.SetComponentData(instanceEntity, new Power {
			Value = 1
		});

		EntityManager.SetComponentData(instanceEntity, createRequestData.GhostProjectileOwner);
		EntityManager.SetComponentData(instanceEntity, createRequestData.TeamId);

		return instanceEntity;
	}
	
	private Entity GetProjectilePrefab(int projectileId) {
		//var ghostCollection = GetSingleton<GhostPrefabCollectionComponent>();
		var ghostCollectionEntity = GetSingletonEntity<GhostPrefabCollectionComponent>();
		var prefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollectionEntity);
		for (int ghostId = 0; ghostId < prefabs.Length; ++ghostId) {
			var item = prefabs[ghostId];
			if (EntityManager.HasComponent<Projectile.Projectile>(item.Value)) {
				var projectile =
					EntityManager.GetComponentData<Projectile.Projectile>(item.Value);

				if (projectile.Id == projectileId) {
					return item.Value;
				}
			}
		}
		return Entity.Null;
	}
	
}

}