using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct AbilityMagazineComponent : IComponentData {
	[GhostField]
	public int MaxAmount;
	//[GhostField]
	public int Amount;
	//[GhostField(Quantization=100)]
	public float RechargeProgress;
}

}