using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct MultipleProjectileAbility : IComponentData {
	public int Amount;
	public float AngleDistance;
}

}