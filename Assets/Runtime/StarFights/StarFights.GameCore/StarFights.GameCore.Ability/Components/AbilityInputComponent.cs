using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct AbilityInputComponent : IComponentData {
	[HideInInspector] public int abilitySlotIndex;
	[HideInInspector] public float attackAngle;
	[HideInInspector] public uint attackCommandClientTick;
}
}