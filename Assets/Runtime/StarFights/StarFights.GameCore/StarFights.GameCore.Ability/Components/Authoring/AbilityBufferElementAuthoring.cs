using System;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Ability {

public class AbilityBufferElementAuthoring : MonoBehaviour, IConvertGameObjectToEntity {
	
	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		var buffer = dstManager.AddBuffer<AbilityBufferElement>(entity);

		var heroAbilitiesTempComponent = dstManager.GetComponentData<AbilitiesTempComponent>(entity);
		buffer.Add(new AbilityBufferElement {
			AbilityEntity = heroAbilitiesTempComponent.Ability_1
		});
		buffer.Add(new AbilityBufferElement {
			AbilityEntity = heroAbilitiesTempComponent.Ability_2
		});
		buffer.Add(new AbilityBufferElement {
			AbilityEntity = heroAbilitiesTempComponent.Ability_3
		});

		dstManager.RemoveComponent<AbilitiesTempComponent>(entity);
	}

}
}