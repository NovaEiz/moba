using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct AbilityProjectile : IComponentData {
	public int ProjectileId;
}
}