using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct Ability : IComponentData {
	public int Id;
}
}