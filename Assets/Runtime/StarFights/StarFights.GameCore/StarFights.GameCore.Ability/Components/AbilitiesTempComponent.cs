using System;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct AbilitiesTempComponent : IComponentData {
	public Entity Ability_1;
	public Entity Ability_2;
	public Entity Ability_3;

}

}