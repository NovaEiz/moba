using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct AbilityCooldownDuration : IComponentData {
	public float Value;
}
}