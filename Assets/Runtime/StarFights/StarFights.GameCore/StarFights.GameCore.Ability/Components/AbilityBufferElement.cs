using System;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ability {

public struct AbilityBufferElement : IBufferElementData {
	public Entity AbilityEntity;
	
}

}