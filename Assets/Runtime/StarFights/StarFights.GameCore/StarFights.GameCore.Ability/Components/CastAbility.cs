using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct CastAbility : IComponentData {
	public quaternion ThrowRotation;
}
}