using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct Rage : IComponentData {
	public float Value;
}
}