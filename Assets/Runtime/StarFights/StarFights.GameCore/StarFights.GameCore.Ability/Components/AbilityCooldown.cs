using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Ability {

[GenerateAuthoringComponent]
public struct AbilityCooldown : IComponentData {
	[HideInInspector] public float Progress;
	[HideInInspector] public float ElapsedTime;
	[HideInInspector] public float StartTime;
}
}