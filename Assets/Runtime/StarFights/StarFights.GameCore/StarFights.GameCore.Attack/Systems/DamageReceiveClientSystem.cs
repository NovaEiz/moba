using StarFights.GameCore.Location;
using StarFights.GameCore.Animation;
using StarFights.GameCore.Unit;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Attack {

[AlwaysUpdateSystem]
[DisableAutoCreation]
[UpdateBefore(typeof(Unity.Physics.Systems.BuildPhysicsWorld))]
[UpdateBefore(typeof(Unity.Physics.Systems.ExportPhysicsWorld))]
public class DamageReceiveClientSystem : ComponentSystem {
	
	protected override void OnUpdate() {
		Entities
			.ForEach((Entity eventEntity,
					ref DamageEvent damageEvent
					) => {
						EntityManager.DestroyEntity(eventEntity);
					});
	}
	
}
}