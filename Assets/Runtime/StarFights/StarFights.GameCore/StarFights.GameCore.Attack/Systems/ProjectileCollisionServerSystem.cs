using System.Linq;
using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Location;
using StarFights.GameCore.Projectile;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.Attack {

[DisableAutoCreation]
[UpdateInGroup(typeof(SimulationSystemGroup))]
//[UpdateBefore(typeof(ProjectileLifeServerSystem))]
[UpdateAfter(typeof(BuildPhysicsWorld))]
[UpdateAfter(typeof(TriggerSystem))]
public class ProjectileCollisionServerSystem : SystemBase {

	private bool _isServerWorld;

	protected override void OnCreate() {
		_isServerWorld = World.GetExistingSystem<ServerSimulationSystemGroup>() != null;
	}

	protected override void OnUpdate() {
		Entities
			.ForEach((Entity entity,
					DynamicBuffer<TriggerEventBufferElement> triggerEventBuffer,
					ref Projectile.Projectile compositeProjectile,
					ref GhostProjectileOwner ownerGhost,
					ref TeamId teamId,
					ref Damage damage,
					ref Power power
					) => {
						var triggerEventBufferArray = triggerEventBuffer.ToNativeArray(Allocator.Temp);
						if (triggerEventBuffer.Length > 0) {
							foreach (var triggerEventBufferElementData in triggerEventBufferArray) {
								//var targetGhostId = EntityManager.GetComponentData<GhostComponent>
								
								var needDestroy = false;
								if (!EntityManager.HasComponent<TeamId>(triggerEventBufferElementData.WithEntity)) {
									//if (EntityManager.HasComponent<ProjectileAttackedTargetsForLifeBufferElement>(entity)) {
									//} else {
										needDestroy = true;
									//}
								} else {
									var targetTeamId = EntityManager.GetComponentData<TeamId>
										(triggerEventBufferElementData.WithEntity).Value;
									//if (targetGhostId == ownerGhost.GhostId
									if (targetTeamId == teamId.Value
									) {
										continue;
									} else if (EntityManager.HasComponent<DeadState>(triggerEventBufferElementData.WithEntity)) {
										continue;
									}


									if (EntityManager.HasComponent<ProjectileAttackedTargetsForLifeBufferElement>(entity)) {
										var projectileAttackedTargetsForLifeBuffer = EntityManager.GetBuffer<ProjectileAttackedTargetsForLifeBufferElement>(entity);
									
										var needContinue = false;
										var len = projectileAttackedTargetsForLifeBuffer.Length;
										for (var i = 0; i < len; i++) {
											if (projectileAttackedTargetsForLifeBuffer[i].TargetEntity.Equals(triggerEventBufferElementData.WithEntity)) {
												needContinue = true;
												break;
											}
										}

										if (needContinue) {
											continue;
										}
									
										projectileAttackedTargetsForLifeBuffer.Add(
											new ProjectileAttackedTargetsForLifeBufferElement {
												TargetEntity = triggerEventBufferElementData.WithEntity
											});
									} else {
										needDestroy = true;
									}
								
									var eventEntity = EntityManager.CreateEntity();
									EntityManager.AddComponentData(eventEntity, new DamageEvent {
										SourceGhostEntity = ownerGhost.GhostEntity,
										TargetGhostEntity = triggerEventBufferElementData.WithEntity,
										Damage = damage.Value * power.Value
									});

									if (_isServerWorld) {
										EntityManager.SetComponentData(triggerEventBufferElementData.WithEntity, new LastInteractionWithWorld {
											ElapsedTime = 0
										});
									}
								}

								if (needDestroy) {
									if (_isServerWorld) {
										EntityManager.DestroyEntity(entity);
									} else {
										if (EntityManager.HasComponent<Child>(entity)) {
											var childBufferArray = EntityManager.GetBuffer<Child>(entity).ToNativeArray(Allocator.Temp);
											
											var len = childBufferArray.Length;

											var wasChildTransform = false;

											for (var i = 0; i < len; i++) {
												var childEntity = childBufferArray[i].Value;
												if (EntityManager.HasComponent<GhostChildEntityComponent>(childEntity)) {
													if (!EntityManager.HasComponent<Transform>(childEntity)) {
														EntityManager.DestroyEntity(childEntity);
													} else {
														EntityManager.RemoveComponent<Parent>(childEntity);
														wasChildTransform = true;
													}
												}
											}
										
											if (!wasChildTransform) {
												EntityManager.DestroyEntity(entity);
											} else {
												EntityManager.RemoveComponent<Child>(entity);
											}
										}
									}

									break;
								}
							}
						}
					}).WithStructuralChanges().Run();
	}
	
}
}