using StarFights.GameCore.Location;
using StarFights.GameCore.Animation;
using StarFights.GameCore.Unit;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Attack {

[AlwaysUpdateSystem]
[DisableAutoCreation]
[UpdateBefore(typeof(Unity.Physics.Systems.BuildPhysicsWorld))]
[UpdateBefore(typeof(Unity.Physics.Systems.ExportPhysicsWorld))]
public class DamageReceiveServerSystem : ComponentSystem {
	
	protected override void OnUpdate() {
		Entities
			.ForEach((Entity eventEntity,
					ref DamageEvent damageEvent
					) => {
				if (!EntityManager.HasComponent<DeadState>(damageEvent.TargetGhostEntity)) {
					DamageReceive(damageEvent);
				}
						EntityManager.DestroyEntity(eventEntity);
					});
	}

	private void DamageReceive(DamageEvent damageEvent) {
		var health = EntityManager.GetComponentData<Health>(damageEvent.TargetGhostEntity);
		health.Value -= damageEvent.Damage;
		if (health.Value <= 0) {
			damageEvent.Damage -= -health.Value;
			health.Value = 0;

			var targetUnitState = EntityManager.GetComponentData<UnitState>(damageEvent.TargetGhostEntity);
			targetUnitState.ValueNext = (int)AnimationState.Dead;
			EntityManager.AddComponentData(damageEvent.TargetGhostEntity, targetUnitState);
		}
		
		EntityManager.SetComponentData(damageEvent.TargetGhostEntity, health);

		Send_DamageReceiveRequest(damageEvent);
	}

	private int GetGhostId(Entity entity) {
		return EntityManager.GetComponentData<GhostComponent>(entity).ghostId;
	}
	
	private void Send_DamageReceiveRequest(DamageEvent damageEvent) {
		return;
		var damageReceiveRequest = new DamageReceiveRequest {
			DamageReceiveEvent = new DamageReceiveEvent {
				Damage       = damageEvent.Damage,
				SourceGhostId = GetGhostId(damageEvent.SourceGhostEntity),
				TargetGhostId = GetGhostId(damageEvent.TargetGhostEntity),
				SourceGhostEntity = damageEvent.SourceGhostEntity,
				TargetGhostEntity = damageEvent.TargetGhostEntity
			}
		};

		SendToAllConnections(damageReceiveRequest);
	}

	private void SendToAllConnections(DamageReceiveRequest damageReceiveRequest) {
		Entities
			.WithAll<NetworkStreamConnection>()
			.ForEach((Entity connectionEntity) => {
				var req = EntityManager.CreateEntity();
				EntityManager.AddComponentData(req, damageReceiveRequest);
				EntityManager.AddComponentData(req, new SendRpcCommandRequestComponent {
					TargetConnection = connectionEntity
				});
			});
	}
	
}
}