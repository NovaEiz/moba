using Unity.Entities;

namespace StarFights.GameCore.Attack {

public struct DamageReceiveEvent : IComponentData {
	public float Damage;
	public int SourceGhostId;// Источник события. Юнит который нанес урон
	public int TargetGhostId;// Владелец события. Юнит которому нанесли урон
	public Entity SourceGhostEntity;
	public Entity TargetGhostEntity;
}

}