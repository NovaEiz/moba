using Unity.Burst;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Attack {

[BurstCompile]
public struct DamageReceiveRequest : IRpcCommand {

	public DamageReceiveEvent DamageReceiveEvent;

}

}