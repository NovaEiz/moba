using Unity.Entities;

namespace StarFights.GameCore.Attack {

public struct DamageEvent : IComponentData {
	public Entity SourceGhostEntity;
	public Entity TargetGhostEntity;
	public float Damage;
}
}