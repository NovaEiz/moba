using Unity.Entities;

namespace StarFights.GameCore.Attack {

[GenerateAuthoringComponent]
public struct Damage : IComponentData {
	public float Value;
}
}