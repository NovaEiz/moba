using Unity.Entities;

namespace StarFights.GameCore.Attack {

public struct AttackCommandServerTick : IComponentData {
	public uint Value;
}
}