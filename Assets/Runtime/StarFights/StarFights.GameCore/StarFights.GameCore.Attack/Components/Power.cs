using Unity.Entities;

namespace StarFights.GameCore.Attack {

[GenerateAuthoringComponent]
public struct Power : IComponentData {
	public float Value;
}
}