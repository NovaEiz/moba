using Unity.Burst;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Attack {

[BurstCompile]
public struct AttackRequestResult : IRpcCommand {
	public int GeneratedClientGhostId;
	public int GhostId;
}

}