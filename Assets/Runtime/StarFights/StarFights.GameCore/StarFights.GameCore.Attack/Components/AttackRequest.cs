using Unity.Burst;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Attack {

[BurstCompile]
public struct AttackRequest : IRpcCommand {
	public int GeneratedClientGhostId;
	public float Angle;
	public uint spawnTick;
}

}