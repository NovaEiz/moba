using Unity.Entities;
using Unity.NetCode;
using Nighday.Online;
using Unity.Transforms;

namespace StarFights.GameCore.Unit {

[DisableAutoCreation]
public class UnitInitializationClientSystem : ComponentSystem {
	
	public struct InitializedTag : IComponentData {}

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<InitializedTag>()
			.ForEach((Entity tankEntity,
					ref Unit tank,
					ref OwnerUserId ownerUserId
					) => {
						if (ownerUserId.Value == 0) {
							return;
						}
						EntityManager.AddComponentData(tankEntity, new InitializedTag());

						EntityManager.AddComponentData(tankEntity, new PredictedGhostComponent());
					});
	}
	
}
}