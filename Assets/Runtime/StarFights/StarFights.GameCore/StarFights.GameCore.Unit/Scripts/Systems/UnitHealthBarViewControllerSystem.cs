using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Application;
using Nighday.Matchmaking.General;
using Unity.Entities;
using Unity.NetCode;
using Nighday.Online;
using StarFights.General;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.Unit {

[DisableAutoCreation]
[UpdateBefore(typeof(BuildPhysicsWorld))]
public class UnitHealthBarViewControllerSystem : SystemBase {
	
	private Camera _camera;

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	private IEnumerator CreateMineHeroView(Entity unitEntity) {
		if (_camera == null) {
			_camera = EntityManager.GetComponentObject<Camera>(GetSingletonEntity<GameCameraComponent>());
		}

		var asyncOperation = Addressables.LoadAssetAsync<GameObject>("UnitHealthBarView");

		var gameCanvasObj = App.World.DataManager.GetGameObject("Canvas - Game");
		var gameCanvasTr = gameCanvasObj.GetComponent<Transform>();
		var gameViewTr = gameCanvasTr.Find("GameViewContainer/GameView");
		
		yield return asyncOperation;

		var unitHealthBarView = GameObject.Instantiate(asyncOperation.Result.GetComponent<UnitHealthBarView>(), gameViewTr);
		EntityManager.AddComponentObject(unitEntity, unitHealthBarView);
		EntityManager.RemoveComponent<Creating>(unitEntity);
		
		while (!EntityManager.HasComponent<Child>(unitEntity)) {
			yield return null;
		}
		

		try {

			var childBuffer = EntityManager.GetBuffer<Child>(unitEntity);
			var childArray = childBuffer.ToNativeArray(Allocator.Temp);
			foreach (var child in childArray) {
				if (EntityManager.HasComponent<Transform>(child.Value)) {
					unitHealthBarView.SetTarget(EntityManager.GetComponentObject<Transform>(child.Value));
					unitHealthBarView.SetCamera(_camera);
					break;
				}
			}
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}
	}

	private struct Creating : IComponentData {
		
	}

	protected override void OnUpdate() {
		Entities
			.WithAll<ViewAuthorized>()
			.WithNone<Creating>()
			.WithNone<UnitHealthBarView>()
			.WithAll<Unit>()
			.WithAll<Health>()
			.ForEach((Entity entity
					) => {
						EntityManager.AddComponentData(entity, new Creating());

						MainMonoBehaviour.Instance.StartCoroutine(CreateMineHeroView(entity));
					}).WithStructuralChanges().Run();
		
		Entities
			.ForEach((Entity entity,
					UnitHealthBarView unitHealthBarView,
					ref Health health
					) => {
				unitHealthBarView.SetFrameData(health.Value, health.MaxValue);
			}).WithStructuralChanges().Run();
	}
}
}