using System;
using System.Collections;
using System.Collections.Generic;
using Nighday.Matchmaking.General;
using Unity.Entities;
using Unity.NetCode;
using Nighday.Online;
using StarFights.General;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.Unit {

[DisableAutoCreation]
[UpdateBefore(typeof(BuildPhysicsWorld))]
[UpdateAfter(typeof(GhostSimulationSystemGroup))]
public class UnitNameViewControllerSystem : SystemBase {

	private struct Creating : IComponentData {
		
	}
	
	private Camera _camera;

	protected override void OnCreate() {
		base.OnCreate();
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
	}

	private IEnumerator CreateUnitNameView(Entity unitEntity) {
		if (_camera == null) {
			_camera = EntityManager.GetComponentObject<Camera>(GetSingletonEntity<GameCameraComponent>());
		}

		var asyncOperation = Addressables.LoadAssetAsync<GameObject>("UnitNameView");

		var gameCanvasObj = Nighday.Application.App.World.DataManager.GetGameObject("Canvas - Game");
		var gameCanvasTr = gameCanvasObj.GetComponent<Transform>();
		var gameViewTr = gameCanvasTr.Find("GameViewContainer/GameView");
		
		yield return asyncOperation;

		var unitNameView = GameObject.Instantiate(asyncOperation.Result.GetComponent<UnitNameView>(), gameViewTr);
		EntityManager.AddComponentObject(unitEntity, unitNameView);
		EntityManager.RemoveComponent<Creating>(unitEntity);

		while (!EntityManager.HasComponent<Child>(unitEntity)) {
			yield return null;
		}

		try {

			var childBuffer = EntityManager.GetBuffer<Child>(unitEntity);
			var childArray = childBuffer.ToNativeArray(Allocator.Temp);
			foreach (var child in childArray) {
				if (EntityManager.HasComponent<Transform>(child.Value)) {
					unitNameView.SetTarget(EntityManager.GetComponentObject<Transform>(child.Value));
					unitNameView.SetCamera(_camera);
					break;
				}
			}
		}
		catch (Exception e) {
			Debug.LogError(e);
			throw;
		}
	}

	protected override void OnUpdate() {
		Entities
			.WithAll<ViewAuthorized>()
			.WithNone<Creating>()
			.WithNone<UnitNameView>()
			.WithAll<Unit>()
			.WithAll<UnitName>()
			.ForEach((Entity entity
					) => {
						EntityManager.AddComponentData(entity, new Creating());

						MainMonoBehaviour.Instance.StartCoroutine(CreateUnitNameView(entity));
					}).WithStructuralChanges().Run();
		
/*
		Entities
			.ForEach((Entity entity,
					UnitNameView unitNameView,
					ref LocalToWorld localToWorld
					) => {
						var worldPosition = localToWorld.Position + new float3(0, 2.5f, 0);
						var position = _camera.WorldToScreenPoint(worldPosition);

						unitNameView.SetPosition(position);
					}).WithStructuralChanges().Run();
 */
		
		Entities
			.WithAll<UnitName_OnValueChangedEvent>()
			.ForEach((Entity entity,
					UnitNameView unitNameView,
					ref UnitName unitName
					) => {
						unitNameView.SetName(unitName.Value.ToString());
					}).WithStructuralChanges().Run();
	}
}
}