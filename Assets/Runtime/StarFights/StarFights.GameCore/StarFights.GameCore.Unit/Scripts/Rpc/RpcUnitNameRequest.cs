using Unity.Burst;
using Unity.NetCode;
using Unity.Networking.Transport;

namespace StarFights.GameCore.Unit {

[BurstCompile]
public struct RpcUnitNameRequest : IRpcCommand {

	public int GhostId;

}

}