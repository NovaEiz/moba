using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Unit {

[DisableAutoCreation]
public class UnitNameRequestClientSystem : ComponentSystem {
	
	private struct UnitName_InitializedTag : IComponentData {
		
	}
	
	private EntityQuery _entityQuery;

	protected override void OnCreate() {
		base.OnCreate();

		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();

		_entityQuery = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly<GhostComponent>(),
				ComponentType.ReadWrite<UnitName>()
			},
			None = new [] {
				ComponentType.ReadOnly<UnitName_InitializedTag>()
			}
		});
		//RequireForUpdate(_spawnEntityQuery);
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<UnitName_InitializedTag>()
			.ForEach((Entity entity,
					ref UnitName unitName,
					ref GhostComponent ghostComponent
					) => {
						SendRequestUnitName(ghostComponent.ghostId);

						EntityManager.AddComponentData(entity, new UnitName_InitializedTag());
					});
	}

	private void SendRequestUnitName(int ghostId) {
		var req = EntityManager.CreateEntity();
		EntityManager.AddComponentData(req, new RpcUnitNameRequest {
			GhostId = ghostId
		});
		EntityManager.AddComponentData(req, new SendRpcCommandRequestComponent());
	}
	
}
}