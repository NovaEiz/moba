using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Unit {

[DisableAutoCreation]
public class RpcUnitNameRequestServerSystem : ComponentSystem {
	
	private EntityQuery _entityQuery;

	protected override void OnCreate() {
		base.OnCreate();

		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();

		_entityQuery = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly<GhostComponent>(),
				ComponentType.ReadWrite<UnitName>()
			}
		});
		//RequireForUpdate(_spawnEntityQuery);
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<SendRpcCommandRequestComponent>()
			.ForEach((Entity reqEntity,
					ref RpcUnitNameRequest reqComponent,
					ref ReceiveRpcCommandRequestComponent receiveRpcCommand
					) => {
						var unitName = GetUnitName(reqComponent.GhostId);

						EntityManager.AddComponentData(reqEntity, new RpcUnitNameResult {
							GhostId = reqComponent.GhostId,
							UnitName = unitName
						});
						EntityManager.AddComponentData(reqEntity, new SendRpcCommandRequestComponent {
							TargetConnection = receiveRpcCommand.SourceConnection
						});
						EntityManager.RemoveComponent<ReceiveRpcCommandRequestComponent>(reqEntity);
			});
	}

	private FixedString64 GetUnitName(int ghostId) {
		var ghostComponentArray = _entityQuery.ToComponentDataArray<GhostComponent>(Allocator.TempJob);
		var unitNameArray = _entityQuery.ToComponentDataArray<UnitName>(Allocator.TempJob);
		var len = unitNameArray.Length;

		for (int i = 0; i < len; i++) {
			if (ghostComponentArray[i].ghostId == ghostId) {
				var resName = unitNameArray[i].Value;
				ghostComponentArray.Dispose();
				unitNameArray.Dispose();
				return resName;
			}
		}
		ghostComponentArray.Dispose();
		unitNameArray.Dispose();
		
		return new FixedString64();
	}
	
}
}