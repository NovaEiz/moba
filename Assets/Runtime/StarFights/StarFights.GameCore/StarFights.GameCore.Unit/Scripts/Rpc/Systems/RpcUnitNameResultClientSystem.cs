using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Unit {

[DisableAutoCreation]
public class RpcUnitNameResultClientSystem : ComponentSystem {
	
	private struct UnitName_InitializedTag : IComponentData {
		
	}

	
	private EntityQuery _entityQuery;

	protected override void OnCreate() {
		base.OnCreate();

		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();

		_entityQuery = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadOnly<GhostComponent>(),
				ComponentType.ReadWrite<UnitName>()
			},
			None = new [] {
				ComponentType.ReadOnly<UnitName_InitializedTag>()
			}
		});
		//RequireForUpdate(_spawnEntityQuery);
	}

	private void ClearEvents() {
		Entities
			.WithAll<UnitName_OnValueChangedEvent>()
			.ForEach((Entity entity
					) => {
						EntityManager.RemoveComponent<UnitName_OnValueChangedEvent>(entity);
					});
	}

	protected override void OnUpdate() {
		ClearEvents();
		
		Entities
			.WithNone<SendRpcCommandRequestComponent>()
			.ForEach((Entity reqEntity,
					ref RpcUnitNameResult reqComponent,
					ref ReceiveRpcCommandRequestComponent receiveRpcCommand
					) => {

						if (SetUnitName(reqComponent.GhostId, reqComponent.UnitName)) {
							EntityManager.DestroyEntity(reqEntity);
						}
					});
		
	}

	private bool SetUnitName(int ghostId, FixedString64 unitName) {
		var ghostComponentArray = _entityQuery.ToComponentDataArray<GhostComponent>(Allocator.TempJob);
		var entityArray = _entityQuery.ToEntityArray(Allocator.TempJob);
		var len = entityArray.Length;

		for (int i = 0; i < len; i++) {
			if (ghostComponentArray[i].ghostId == ghostId) {
				EntityManager.SetComponentData(entityArray[i], new UnitName {
					Value = unitName
				});
				
				EntityManager.AddComponentData(entityArray[i], new UnitName_OnValueChangedEvent());
				EntityManager.AddComponentData(entityArray[i], new UnitName_InitializedTag());
				
				ghostComponentArray.Dispose();
				entityArray.Dispose();
				return true;
			}
		}
		ghostComponentArray.Dispose();
		entityArray.Dispose();
		
		return false;
	}
	
}
}