using Unity.Burst;
using Unity.Collections;
using Unity.NetCode;
using Unity.Networking.Transport;

namespace StarFights.GameCore.Unit {

[BurstCompile]
public struct RpcUnitNameResult : IRpcCommand {

	public int GhostId;
	public FixedString64 UnitName;

}

}