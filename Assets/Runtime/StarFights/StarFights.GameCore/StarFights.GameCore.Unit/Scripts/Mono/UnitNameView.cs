using System;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StarFights.GameCore.Unit {
public class UnitNameView : MonoBehaviour {

	[SerializeField] private TextMeshProUGUI _nameText;

	public void SetName(string value) {
		_nameText.text = value;
	}

	public void SetPosition(Vector3 position) {
		transform.position = position;
	}

	private Transform _target;
	public void SetTarget(Transform value) {
		_target = value;
	}
	private Camera _camera;
	public void SetCamera(Camera value) {
		_camera = value;
		CinemachineCore.CameraUpdatedEvent.AddListener((cinemachineBrain) => {
			UpdatePosition();
		});
	}

	private void UpdatePosition() {
		var worldPosition = _target.position + new Vector3(0, 2.5f, 0);
		var position = _camera.WorldToScreenPoint(worldPosition);
		SetPosition(position);
	}

}
}