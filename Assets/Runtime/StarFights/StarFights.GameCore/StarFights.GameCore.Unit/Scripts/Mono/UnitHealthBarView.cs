using System;
using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StarFights.GameCore.Unit {
public class UnitHealthBarView : MonoBehaviour {

	[SerializeField] private Image _healthProgressImage;
	[SerializeField] private TextMeshProUGUI _healthText;

	public void SetFrameData(float value, int maxValue) {
		_healthProgressImage.fillAmount = value / maxValue;
		var valueInt = (int)value;
		_healthText.text = valueInt.ToString();
	}

	public void SetPosition(Vector3 position) {
		transform.position = position;
	}
	private Transform _target;
	public void SetTarget(Transform value) {
		_target = value;
	}
	private Camera _camera;
	public void SetCamera(Camera value) {
		_camera = value;
		CinemachineCore.CameraUpdatedEvent.AddListener((cinemachineBrain) => {
			UpdatePosition();
		});
	}

	private void UpdatePosition() {
		var worldPosition = _target.position + new Vector3(0, 2.5f, 0);
		var position = _camera.WorldToScreenPoint(worldPosition);
		SetPosition(position);
	}

	
}
}