using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Unit {

[GenerateAuthoringComponent]
public struct Unit : IComponentData {
	public int Id;
}
}