using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Unit {

public class UnitNameAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private string _name;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentData(entity, new UnitName {
			Value = _name
		});
	}
}

}