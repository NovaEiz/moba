using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Unit {

/*

public interface IStrengthUpdateGroup {
	void Update(Strength strength);
}

 */

[GenerateAuthoringComponent]
public struct Health : IComponentData {//, IStrengthUpdateGroup {

	private static int BaseDefaultValue = 200;

	[HideInInspector]
	[GhostField(Quantization=10)]
	public float Value;
	public float ValueP {
		get { return Value;  }
		set { Value = value; }
	}
	
	[HideInInspector]
	[GhostField]
	public int MaxValue;
	
	public int BaseValue;
	
	[HideInInspector]
	public float IncreaseValue;
	public float IncreaseValueP {
		get { return IncreaseValue; }
		set { IncreaseValue = value;  }
	}
	
	[HideInInspector]
	[GhostField(Quantization=10)]
	public float Regeneration;
	public float BaseRegeneration;
	[HideInInspector]
	public float IncreaseRegeneration;
	public float IncreaseRegenerationP {
		get { return IncreaseRegeneration; }
		set { IncreaseRegeneration = value;  }
	}

	private float IncreasePercentRegeneration;

	public float IncreasePercentRegenerationP {
		get { return IncreasePercentRegeneration; }
		set { IncreasePercentRegeneration = value;  }
	}
	 
	public void Init() {
		if (BaseValue == 0) {
			BaseValue = BaseDefaultValue;
		}

		MaxValue = BaseValue;
		Value = MaxValue;
	}
	
	/*
	public void Update(Strength strength) {
		var prevProgress = MaxValue == 0 ? 1 : Value / MaxValue;
		MaxValue     = BaseValue + strength.Value * 20;
		Value        = MaxValue * prevProgress;
		Regeneration = BaseRegeneration + IncreaseRegeneration + (MaxValue * IncreasePercentRegenerationP) + strength.Value * 0.1f;
	}
	 */
	
}
}