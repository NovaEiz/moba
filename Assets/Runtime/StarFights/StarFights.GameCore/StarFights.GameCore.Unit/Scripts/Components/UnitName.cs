using Unity.Collections;
using Unity.Entities;

namespace StarFights.GameCore.Unit {

public struct UnitName_OnValueChangedEvent : IComponentData {
		
}

public struct UnitName : IComponentData {
	
	public FixedString64 Value;
	
}
}