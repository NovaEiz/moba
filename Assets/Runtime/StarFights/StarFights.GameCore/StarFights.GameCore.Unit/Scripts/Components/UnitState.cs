using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Unit {

[GenerateAuthoringComponent]
public struct UnitState : IComponentData {

	public int ValuePrevious;
	
	[GhostField(Quantization=-1)] 
	public int Value;
	
	[GhostField(Quantization=-1)]
	public int ValueNext;

	public float angle;

	
}
}