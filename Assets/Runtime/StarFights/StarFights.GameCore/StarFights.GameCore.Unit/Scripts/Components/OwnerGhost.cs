using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.Unit {

/// <summary>
/// Владелец-призрак
/// </summary>
[GenerateAuthoringComponent]
public struct GhostProjectileOwner : IComponentData {
	public int GhostId;
	public Entity GhostEntity;
}
}