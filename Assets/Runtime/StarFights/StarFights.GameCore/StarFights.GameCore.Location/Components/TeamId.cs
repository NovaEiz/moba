using Unity.Entities;
using Unity.NetCode;

namespace StarFights.GameCore.Location {
[GenerateAuthoringComponent]
public struct TeamId : IComponentData {
	[GhostField]
	//[GhostDefaultField(-1)]
	public int Value;
	
	
}
}