using Unity.Entities;

namespace StarFights.GameCore.Location {
[GenerateAuthoringComponent]
public struct TurnedByTeamAngleComponent : IComponentData {
	public float Angle;
}
}