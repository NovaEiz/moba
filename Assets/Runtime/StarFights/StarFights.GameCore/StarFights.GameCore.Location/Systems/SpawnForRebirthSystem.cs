using StarFights.GameCore.Unit;
using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using ComponentSystem = Unity.Entities.ComponentSystem;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.Location {

[DisableAutoCreation]
[UpdateInGroup(typeof(ServerSimulationSystemGroup))]
[UpdateBefore(typeof(BuildPhysicsWorld))]
//[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class SpawnForRebirthSystem : ComponentSystem {

	public struct Timer : IComponentData {
		public float ElapsedTime;
		public float StartTime;
		public float DurationTime;
	}

	private struct InitializedTag : IComponentData {
	}

	private EntityQuery _teamSpawnsEntityQuery;

	protected override void OnCreate() {

		base.OnCreate();

		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();

		_teamSpawnsEntityQuery = GetEntityQuery(new EntityQueryDesc {
			All = new[] {
				ComponentType.ReadWrite<TeamSpawns>()
			}
		});
		RequireForUpdate(_teamSpawnsEntityQuery);
	}

	protected override void OnUpdate() {
		Entities
			.WithNone<Timer>()
			.WithAll<DeadState>()
			.ForEach((Entity entity
					) => {
						EntityManager.AddComponentData(entity, new Timer {
							StartTime = (float)Time.ElapsedTime,
							DurationTime = 3
						});
					});
		var deltaTime = Time.DeltaTime;
		Entities
			.WithAll<DeadState>()
			.ForEach((Entity entity,
					ref Translation translation,
					ref Rotation rotation,
					ref UnitState unitState,
					ref Timer timer,
					ref TeamId teamId
					) => {
				
						var elapsedTime = (float)Time.ElapsedTime - timer.StartTime;

						timer.ElapsedTime = elapsedTime;
						if (timer.ElapsedTime >= timer.DurationTime) {
							var teamSpawnsArray = _teamSpawnsEntityQuery.ToComponentDataArray<TeamSpawns>(Allocator.TempJob);
							var teamArrayLength = teamSpawnsArray.Length;
							if (teamArrayLength == 0) {
								teamSpawnsArray.Dispose();
								return;
							}
							var teamSpawnsEntityArray = _teamSpawnsEntityQuery.ToEntityArray(Allocator.TempJob);

							if (EntityManager.HasComponent<IndexPlayerInTeam>(entity)) {
								var indexPlayerInTeam = EntityManager.GetComponentData<IndexPlayerInTeam>(entity);
								Entity teamSpawnsEntity = Entity.Null;
								for (int i = 0; i < teamArrayLength; i++) {
									var team = teamSpawnsArray[i];
									if (team.Id == teamId.Value) {
										teamSpawnsEntity = teamSpawnsEntityArray[i];
										break;
									}
								}

								if (teamSpawnsEntity == Entity.Null) {
									teamSpawnsArray.Dispose();
									teamSpawnsEntityArray.Dispose();
									return;
								}

								DynamicBuffer<Child> childs = EntityManager.GetBuffer<Child>(teamSpawnsEntity);
								var child = childs[indexPlayerInTeam.Value];
								var spawnEntity = child.Value;
							
								var newPosition = EntityManager.GetComponentData<LocalToWorld>(spawnEntity).Position;
								var spawnRotation = EntityManager.GetComponentData<Rotation>(spawnEntity);
								
								translation.Value = newPosition;
								rotation.Value = spawnRotation.Value;
							}
							
							EntityManager.RemoveComponent<Timer>(entity);
							EntityManager.RemoveComponent<DeadState>(entity);

							/*
							EntityManager.SetComponentData(entity, new PhysicsDamping {
								Angular = 0,
								Linear = 0
							});
							*/

							var health = EntityManager.GetComponentData<Health>(entity);
							health.Value = health.MaxValue;
							EntityManager.SetComponentData(entity, health);
							//unitState.Value = 0;
							unitState.ValueNext = 0;

							teamSpawnsArray.Dispose();
							teamSpawnsEntityArray.Dispose();
						}
				
					});
	}

}

}