using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;

namespace StarFights.GameCore.CapturingCrystals {

[GenerateAuthoringComponent]
public struct CapturingCrystalsModeComponent : IComponentData {
	public int NeedCrystalsAmountForGameEnd;
	public int SecondsForEndGame;
	public float SecondsForEndGame_DurationOneSecond;
	public int SecondsForRessurection;
}
}