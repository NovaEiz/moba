using Unity.Entities;
using Unity.Mathematics;

namespace StarFights.GameCore.CapturingCrystals {

[GenerateAuthoringComponent]
public struct ThrowingCrystalsComponent : IComponentData {
	public float2 ThrowIntervalRange;
}
}