using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;

namespace StarFights.GameCore.CapturingCrystals {

[GenerateAuthoringComponent]
public struct CollectedCrystalsComponent : IComponentData {
	[GhostField]
	public int Amount;
}
}