using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;

namespace StarFights.GameCore.CapturingCrystals {

[GenerateAuthoringComponent]
public struct GameCountdownToEndComponent : IComponentData {
	[GhostField]
	public int TimeLeft;
	public float TimeLeftFloat;
	public float StartTime;
	public float DurationOneSecond;
	[GhostField]
	public float DurationTime;
	
	[GhostField]
	public int LeadingTeamId;
}
}