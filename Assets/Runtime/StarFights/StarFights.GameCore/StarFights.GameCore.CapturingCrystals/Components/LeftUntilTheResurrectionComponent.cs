using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;

namespace StarFights.GameCore.CapturingCrystals {

public struct LeftUntilTheResurrectionComponent : IComponentData {
	public int TimeLeft;
	public float TimeLeftFloat;
	public float StartTime;
	public float DurationTime;
}
}