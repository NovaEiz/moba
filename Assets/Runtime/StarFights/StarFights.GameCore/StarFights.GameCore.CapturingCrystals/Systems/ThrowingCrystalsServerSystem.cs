using System;
using System.Threading.Tasks;
using Nighday.General.PhysicsExtentions;
using Unity.Entities;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;
using StarFights.GameCore.Ghosts;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.NetCode;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;
using AnimationState = StarFights.GameCore.Animation.AnimationState;

namespace StarFights.GameCore.CapturingCrystals {

[UpdateInWorld(UpdateInWorld.TargetWorld.Server)]
[UpdateInGroup(typeof(SimulationSystemGroup))]
[UpdateBefore(typeof(Unity.Physics.Systems.BuildPhysicsWorld))]
[DisableAutoCreation]
public class ThrowingCrystalsServerSystem : ComponentSystem {
	
	private struct InitializedTag : IComponentData {
	}

	private struct WaitCrystalIsCreatedTag : IComponentData {
		public float3 StartPosition;
	}
	
	private struct DurationTime : IComponentData {
		public float Value;
	}
	private struct ElapsedTime : IComponentData {
		public float Value;
	}
	
	public struct ThrowCrystalComponent : IComponentData {
		public float ElapsedTime;
		public float DurationTime;
		public float Distance;
		public float DirectionAngle;
		public float3 StartPosition;
		public float StartScale;
	}

	private static uint randomSeed;
	private static Random random;

	private ThrowingCrystalsComponentAsset _throwingCrystalsComponentAsset;
	private AnimationCurveAsset _throwCrystalPathAnimationCurveAsset;
	private AnimationCurveAsset _throwCrystalScaleAnimationCurveAsset;
	
	private float ThrowImpulse {
		get {
			if (_throwingCrystalsComponentAsset == null) {
				return 1f;
			}

			return _throwingCrystalsComponentAsset.ThrowImpulse;
		}
	}

	public float GetRandom(ThrowingCrystalsComponent throwingCrystalsComponent) {
		return random.NextFloat(
			throwingCrystalsComponent.ThrowIntervalRange.x,
			throwingCrystalsComponent.ThrowIntervalRange.y
		);
	}
	public float GetRandomThrowInterval() {
		return random.NextFloat(
			_throwingCrystalsComponentAsset.ThrowIntervalRange.x,
			_throwingCrystalsComponentAsset.ThrowIntervalRange.y
		);
	}
	public float GetRandomThrowSpeed() {
		return random.NextFloat(
			_throwingCrystalsComponentAsset.DistanceRange.x,
			_throwingCrystalsComponentAsset.DistanceRange.y
		);
	}
	public float GetRandomAngle() {
		return random.NextFloat(
			0f,
			360f
		);
	}
	public float GetThrowDurationtime() {
		return _throwingCrystalsComponentAsset.ThrowDurationTime;
	}
	
	protected override void OnCreate() {
		randomSeed = 1;
		random = new Random(randomSeed);
		
		RequireSingletonForUpdate<GhostPrefabCollectionComponent>();
		RequireSingletonForUpdate<InitializedTag>();

		LoadThrowingCrystalsComponentAssetAsync();
	}

	private async Task LoadThrowingCrystalsComponentAssetAsync() {
		try {
			var asyncOperation = Addressables.LoadAssetAsync<ScriptableObject>("ThrowingCrystalsComponentAsset");
			var asyncOperation2 = Addressables.LoadAssetAsync<ScriptableObject>("ThrowCrystalScaleAnimationCurveAsset");
			var asyncOperation3 = Addressables.LoadAssetAsync<ScriptableObject>("ThrowCrystalPathAnimationCurveAsset");
			await Task.WhenAll(
				asyncOperation.Task,
				asyncOperation2.Task,
				asyncOperation3.Task
			);
			_throwingCrystalsComponentAsset
				= (ThrowingCrystalsComponentAsset)asyncOperation.Result;
			_throwCrystalScaleAnimationCurveAsset
				= (AnimationCurveAsset)asyncOperation2.Result;
			_throwCrystalPathAnimationCurveAsset
				= (AnimationCurveAsset)asyncOperation3.Result;

			EntityManager.CreateEntity(typeof(InitializedTag));
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
	}
	
	private struct DeadStateForOneThrowTag : IComponentData {}
	
	protected override void OnUpdate() {
		
		Entities
			.WithNone<DeadState>()
			.ForEach((Entity entity,
					ref CollectedCrystalsComponent collectedCrystalsComponent,
					ref UnitState unitState
					) => {
						if (Input.GetKeyDown(KeyCode.Space)) {
							unitState.ValueNext = (int)AnimationState.Dead;
						}
					});
		Entities
			.WithNone<DeadStateForOneThrowTag>()
			.WithAll<DeadState>()
			.ForEach((Entity entity,
					ref CollectedCrystalsComponent collectedCrystalsComponent,
					ref Translation translation
					) => {
				EntityManager.AddComponentData(entity, new DeadStateForOneThrowTag());

				var len = collectedCrystalsComponent.Amount;
				for (var i = 0; i < len; i++) {
					ThrowCrystal(translation.Value);
				}
				collectedCrystalsComponent.Amount = 0;
				
				
			});
		Entities
			.WithAll<DeadStateForOneThrowTag>()
			.WithNone<DeadState>()
			.ForEach((Entity entity,
					ref CollectedCrystalsComponent collectedCrystalsComponent
					) => {
				EntityManager.RemoveComponent<DeadStateForOneThrowTag>(entity);
			});
		
		var deltaTime = Time.DeltaTime;
		Entities
			.ForEach((Entity entity,
					ref ThrowCrystalComponent throwCrystalComponent,
					ref Translation translation,
					ref Rotation rotation,
					ref Scale scale
			) => {
				throwCrystalComponent.ElapsedTime += deltaTime;
				var progress = throwCrystalComponent.ElapsedTime / throwCrystalComponent.DurationTime;

				var yPositionInPath = _throwCrystalPathAnimationCurveAsset.AnimationCurve.Evaluate(progress);
				var positionInScale = _throwCrystalScaleAnimationCurveAsset.AnimationCurve.Evaluate(progress);

				var lengthTraversed = progress * throwCrystalComponent.Distance;
				
				var newPosition = new float3(
					lengthTraversed,
					yPositionInPath,
					0
				);

				var directionRotation = quaternion.Euler(new float3(0, throwCrystalComponent.DirectionAngle, 0));
				var pos = math.mul(directionRotation.value, new float3(0, 0, 1)) * lengthTraversed;
				pos.y = yPositionInPath;
				translation.Value = pos + throwCrystalComponent.StartPosition;
				scale.Value = positionInScale * throwCrystalComponent.StartScale;
				//scale.Value = float4x4.Scale(positionInScale,positionInScale,positionInScale);
				
				if (progress >= 1f) {
					EntityManager.RemoveComponent<ThrowCrystalComponent>(entity);
				}
			});
		Entities
			.ForEach((Entity reqEntity,
					ref CreateCrystalSystem.CreateRequest.Result createRequestResult,
					ref WaitCrystalIsCreatedTag waitCrystalIsCreatedTag
					) => {

				NativeArray<Child> bufferArray = default;
				if (!EntityManager.HasComponent<Child>(createRequestResult.Entity)) {
					EntityManager.AddBuffer<Child>(createRequestResult.Entity);
					return;
				} else {
					var buffer = EntityManager.GetBuffer<Child>(createRequestResult.Entity);
					if (buffer.Length == 0) {
						//return;
					}
					//bufferArray = buffer.ToNativeArray(Allocator.Temp);
				}

				EntityManager.AddComponentData(createRequestResult.Entity, new ThrowCrystalComponent {
					DurationTime = GetThrowDurationtime(),
					Distance = GetRandomThrowSpeed(),
					DirectionAngle = GetRandomAngle(),
					StartPosition = waitCrystalIsCreatedTag.StartPosition,
					StartScale = EntityManager.GetComponentData<Scale>(createRequestResult.Entity).Value
				});
				
				
				/*
				var child = bufferArray[0].Value;
				EntityManager.AddComponentData(child, new ScalePivot {
				});
				EntityManager.AddComponentData(child, new ScalePivotTranslation {
				});
				 */
				
						/*
						var physicsVelocity = EntityManager.GetComponentData<PhysicsVelocity>(createRequestResult.Entity);
						var physicsMass = EntityManager.GetComponentData<PhysicsMass>(createRequestResult.Entity);
						Debug.Log("ThrowImpulse = " + ThrowImpulse);
						var direction = new float3(0.5f, 0.5f, 0.5f) * ThrowImpulse;
						physicsVelocity.Linear = float3.zero;
						physicsVelocity.ApplyLinearImpulse(physicsMass, direction);
						EntityManager.SetComponentData(createRequestResult.Entity, physicsVelocity);
						EntityManager.SetComponentData(createRequestResult.Entity, physicsMass);
						*/
						EntityManager.DestroyEntity(reqEntity);
					});
		
						Entities
							.ForEach((Entity entity,
									ref ThrowingCrystalsComponent throwingCrystalsComponent,
									ref ElapsedTime elapsedTime,
									ref DurationTime durationTime,
									ref LocalToWorld localToWorld
									) => {
										elapsedTime.Value += deltaTime;

										if (elapsedTime.Value >= durationTime.Value) {
											durationTime.Value = GetRandomThrowInterval();
											elapsedTime.Value = 0;

											ThrowCrystal(localToWorld.Position);
										}
									});
		
						Entities
							.WithNone<ElapsedTime>()
							.ForEach((Entity entity,
									ref ThrowingCrystalsComponent throwingCrystalsComponent
									) => {
										EntityManager.AddComponentData(entity, new ElapsedTime());
										EntityManager.AddComponentData(entity, new DurationTime {
											Value = GetRandom(throwingCrystalsComponent)
										});
									});
					}

	
	private void ThrowCrystal(float3 startPosition = default) {
		var reqEntity = EntityManager.CreateEntity();
		EntityManager.AddComponentData(reqEntity, new CreateCrystalSystem.CreateRequest {
		});
		EntityManager.AddComponentData(reqEntity, new WaitCrystalIsCreatedTag {
			StartPosition = startPosition
		});
	}
	
}
}