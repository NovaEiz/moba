﻿using System;
using System.Threading.Tasks;
using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;

using Unity.Animation;
using Unity.Animation.Hybrid;
using Debug = UnityEngine.Debug;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
[UpdateInGroup(typeof(GhostPredictionSystemGroup))]
public class PickingUpCrystalsServerSystem : JobComponentSystem {

	private struct InitializedTag : IComponentData {
	}
	private struct CrystalCollected : IComponentData {
		public Entity Owner;
		
		public float ElapsedTime;
		public float DurationTime;
		public float StartScale;
		public float3 StartPosition;
		public float3 StartPositionOwnerEntity;
		public float TargetY;
	}

	private EndSimulationEntityCommandBufferSystem _endSimulationEntityCommandBufferSystem;

	private AnimationCurveAsset _pickingUpCrystalScaleAnimationCurveAsset;
	private AnimationCurveAsset _pickingUpCrystalPathAnimationCurveAsset;
	private PickingUpCrystalsComponentAsset _pickingUpCrystalsComponentAsset;
	
	private async Task LoadPickingUpCrystalsAssetsAsync() {
		try {
			var asyncOperation = Addressables.LoadAssetAsync<ScriptableObject>("PickingUpCrystalsComponentAsset");

			await Task.WhenAll(
				asyncOperation.Task
			);

			_pickingUpCrystalsComponentAsset
				= (PickingUpCrystalsComponentAsset)asyncOperation.Result;

			EntityManager.CreateEntity(typeof(InitializedTag));
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
	}
	
	protected override void OnCreate() {
		_endSimulationEntityCommandBufferSystem = World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
		
		RequireSingletonForUpdate<PickingUpCrystalPathAnimationCurveComponent>();
		RequireSingletonForUpdate<PickingUpCrystalScaleAnimationCurveComponent>();
		RequireSingletonForUpdate<InitializedTag>();

		LoadPickingUpCrystalsAssetsAsync();
	}
	
	protected override JobHandle OnUpdate(JobHandle inputDeps) {
		var PostUpdateCommands = _endSimulationEntityCommandBufferSystem.CreateCommandBuffer().AsParallelWriter();
		var crystalFromEntity = GetComponentDataFromEntity<Crystal.Crystal>();
		var physicsColliderFromEntity = GetComponentDataFromEntity<PhysicsCollider>();
		var localToWorldFromEntity = GetComponentDataFromEntity<LocalToWorld>();
		var scaleFromEntity = GetComponentDataFromEntity<Scale>();
		var throwCrystalComponentFromEntity = GetComponentDataFromEntity<ThrowingCrystalsServerSystem.ThrowCrystalComponent>();

		var pickingUpDurationTime = _pickingUpCrystalsComponentAsset.PickingUpDurationTime;
		var targetY = _pickingUpCrystalsComponentAsset.TargetY;
		
		var inputDeps2 = Entities
			.WithReadOnly(throwCrystalComponentFromEntity)
			.WithReadOnly(scaleFromEntity)
			.WithReadOnly(localToWorldFromEntity)
			.WithReadOnly(physicsColliderFromEntity)
			.WithReadOnly(crystalFromEntity)
			.WithNone<DeadState>()
			.ForEach((Entity entity,
					int entityInQueryIndex,
					DynamicBuffer<TriggerEventBufferElement> triggerEventBuffer,
					ref Triggerable triggerable,
					//ref Hero hero, // строка просто чтобы видеть, но не нужна тут
					ref CollectedCrystalsComponent collectedCrystalsComponent
					) => {

						if (triggerEventBuffer.Length > 0) {
							var len = triggerEventBuffer.Length;
							for (int i = 0; i < len; i++) {
								var triggerEventBufferElement = triggerEventBuffer[i];
								var triggerEntity = triggerEventBufferElement.WithEntity;
								if (crystalFromEntity.HasComponent(triggerEntity)) {
									if (physicsColliderFromEntity.HasComponent(triggerEntity)) {
										collectedCrystalsComponent.Amount++;

										if (throwCrystalComponentFromEntity.HasComponent(triggerEntity)) {
											PostUpdateCommands.RemoveComponent<ThrowingCrystalsServerSystem.ThrowCrystalComponent>(entityInQueryIndex, triggerEntity);
										}
										PostUpdateCommands.AddComponent(entityInQueryIndex, triggerEntity,
																		new CrystalCollected {
																			Owner = entity,
																			StartScale = scaleFromEntity[triggerEntity].Value,
																			StartPosition = localToWorldFromEntity[triggerEntity].Position,
																			StartPositionOwnerEntity = localToWorldFromEntity[entity].Position,
																			DurationTime = pickingUpDurationTime,
																			TargetY = targetY
																		});
									
										//EntityManager.RemoveComponent<Triggerable>(triggerable.TriggerEntity);
										PostUpdateCommands.RemoveComponent<Triggerable>(
											entityInQueryIndex,
											triggerEntity);
										PostUpdateCommands.RemoveComponent<Collideable>(
											entityInQueryIndex,
											triggerEntity);
										PostUpdateCommands.RemoveComponent<PhysicsCollider>(
											entityInQueryIndex,
											triggerEntity);
									}
									
									break;

									//EntityManager.DestroyEntity(triggerable.TriggerEntity);
									//EntityManager.RemoveComponent<GhostComponent>(triggerable.TriggerEntity);
								}
							}
						}
					}).Schedule(inputDeps);

		var animationCurvePathComponent = EntityManager.GetComponentData<AnimationCurveComponent>(GetSingletonEntity<PickingUpCrystalPathAnimationCurveComponent>());
		var animationCurveScaleComponent = EntityManager.GetComponentData<AnimationCurveComponent>(GetSingletonEntity<PickingUpCrystalScaleAnimationCurveComponent>());

		var deltaTime = Time.DeltaTime;
		var moveSpeed = 5f;
		var step = moveSpeed * deltaTime;
		var inputDeps3 = Entities
						//.WithReadOnly(PostUpdateCommands)
						.WithReadOnly(localToWorldFromEntity)
			.ForEach((Entity crystalEntity,
								int entityInQueryIndex,
								ref CrystalCollected crystalCollected,
								ref Translation crystalTranslation,
								ref Scale scale
								) => {
						
									crystalCollected.ElapsedTime += deltaTime;
									var progress = crystalCollected.ElapsedTime / crystalCollected.DurationTime;
									
									var yPositionInPath = AnimationCurveEvaluator.Evaluate(progress, animationCurvePathComponent.AnimationCurve);
									var positionInScale = AnimationCurveEvaluator.Evaluate(progress, animationCurveScaleComponent.AnimationCurve);

									//===

									var crystalLocalToWorld = localToWorldFromEntity[crystalEntity];

									var ownerLocalToWorld = localToWorldFromEntity[crystalCollected.Owner];
									var ownerPositionFromStartOffset =
										ownerLocalToWorld.Position - crystalCollected.StartPositionOwnerEntity;

									var toOwnerPositionOffset = ownerLocalToWorld.Position - crystalLocalToWorld.Position;
									var toOwnerDirection = math.mul(quaternion.LookRotation(toOwnerPositionOffset, new float3(0,1,0)),
																	new float3(0,0,1));

									var offsetByProgress = math.lerp(
										crystalCollected.StartPosition,
										crystalCollected.StartPositionOwnerEntity,
										progress
									);
									var currentPositionY = math.lerp(
										crystalCollected.StartPosition,
										crystalCollected.StartPositionOwnerEntity,
										progress
									);
									
									var resultPosition = new float3(
										offsetByProgress.x + ownerPositionFromStartOffset.x,
										yPositionInPath * crystalCollected.TargetY + crystalCollected.StartPosition.y,
										offsetByProgress.z + ownerPositionFromStartOffset.z
									);
							
									crystalTranslation.Value = resultPosition;

									scale.Value = positionInScale * crystalCollected.StartScale;
									
									if (progress >= 1f) {
										PostUpdateCommands.DestroyEntity(entityInQueryIndex, crystalEntity);
									} else {
									}

									/*
									
								return;
								
								//var ownerLocalToWorld = localToWorldFromEntity[crystalCollected.Owner];
								//var crystalLocalToWorld = localToWorldFromEntity[crystalEntity];
										
										var offset = ownerLocalToWorld.Position - crystalLocalToWorld.Position;
										var distance = math.length(offset);
	
										if (distance < 0.1f) {
											PostUpdateCommands.DestroyEntity(entityInQueryIndex, crystalEntity);
										} else {
											var translateOffset = math.normalize(offset) * step;
											crystalTranslation.Value = new float3(
												crystalLocalToWorld.Position + translateOffset);
										}
									*/
								}).Schedule(inputDeps2);

		inputDeps3.Complete();
		return inputDeps3;
	}
		
}
}