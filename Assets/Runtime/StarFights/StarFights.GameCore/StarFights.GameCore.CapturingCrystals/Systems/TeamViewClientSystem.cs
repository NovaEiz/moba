﻿using StarFights.GameCore.Location;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
public class TeamViewClientSystem : SystemBase {

	protected override void OnCreate() {
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<CapturingCrystalsModeComponent>();
	}

	protected override void OnUpdate() {
		var targetEntity = GetSingleton<CommandTargetComponent>().targetEntity;
		if (!EntityManager.Exists(targetEntity)) {
			return;
		}

		var teamId = EntityManager.GetComponentData<TeamId>(targetEntity).Value;
		var capturingCrystalsModeComponent = GetSingleton<CapturingCrystalsModeComponent>();
		Entities
			.ForEach((Entity entity,
					TeamView teamView
					) => {
				var generalCollectedCrystalsInTeam = 0;
				if (teamId == 2 && teamView.TeamId == 1) {
					generalCollectedCrystalsInTeam = GetGeneralCollectedCrystalsInTeam(2);
				} else if (teamId == 2 && teamView.TeamId == 2) {
					generalCollectedCrystalsInTeam = GetGeneralCollectedCrystalsInTeam(1);
				} else {
					generalCollectedCrystalsInTeam = GetGeneralCollectedCrystalsInTeam(teamView.TeamId);
				}
				
				teamView.SetProgress(generalCollectedCrystalsInTeam, capturingCrystalsModeComponent.NeedCrystalsAmountForGameEnd);
			}).WithStructuralChanges().Run();
	}

	private int GetGeneralCollectedCrystalsInTeam(int teamId_) {
		var generalCollectedCrystals = 0;
		Entities
			.ForEach((Entity entity,
					ref TeamId teamId,
					ref CollectedCrystalsComponent collectedCrystalsComponent
					) => {
				if (teamId.Value == teamId_) {
					generalCollectedCrystals += collectedCrystalsComponent.Amount;
				}
					}).WithStructuralChanges().Run();
		return generalCollectedCrystals;
	}
		
}
}