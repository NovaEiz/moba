﻿using System;
using System.Collections;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Matchmaking.General;
using StarFights.GameCore.Unit;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using UnityEngine.AddressableAssets;
using ComponentSystem = Unity.Entities.ComponentSystem;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
public class LeftUntilTheResurrectionClientSystem : ComponentSystem {
	
	public struct NotificationOfTheRemainingTimeComponent : IComponentData {
		public int RemainingSeconds;
	}
	
	private struct LeftUntilTheResurrectionClientComponent : IComponentData {
		public int LastSecondsLeft;
	}
	
	private struct InitializedTag : IComponentData {}

	protected override void OnCreate() {
		RequireSingletonForUpdate<InitializedTag>();
		RequireSingletonForUpdate<CapturingCrystalsModeComponent>();

		LoadLeftUntilTheResurrectionView();
	}

	private Transform _viewContainer;
	private LeftUntilTheResurrectionView _leftUntilTheResurrectionViewPrefab;
	private LeftUntilTheResurrectionView _leftUntilTheResurrectionView;

	private LeftUntilTheResurrectionView LeftUntilTheResurrectionView {
		get {
			if (_leftUntilTheResurrectionView == null) {
				_leftUntilTheResurrectionView = GameObject.Instantiate(_leftUntilTheResurrectionViewPrefab, _viewContainer);
			}

			return _leftUntilTheResurrectionView;
		}
	}

	private async Task LoadLeftUntilTheResurrectionView() {
		try {
			var asyncOperation = Addressables.LoadAssetAsync<GameObject>("LeftUntilTheResurrectionView");
			await asyncOperation.Task;
			_leftUntilTheResurrectionViewPrefab = asyncOperation.Result.GetComponent<LeftUntilTheResurrectionView>();

			var gameCanvas = App.World.DataManager.GetGameObject("Canvas - Game");

			_viewContainer = gameCanvas.transform;//gameCanvas.transform.Find("GameViewContainer");

			EntityManager.CreateEntity(typeof(InitializedTag));
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
	}

	protected override void OnUpdate() {
		if (!HasSingleton<LeftUntilTheResurrectionComponent>()
			&&
			HasSingleton<LeftUntilTheResurrectionClientComponent>()
		) {
			LeftUntilTheResurrectionView.gameObject.SetActive(false);
			EntityManager.DestroyEntity(GetSingletonEntity<LeftUntilTheResurrectionClientComponent>());
			return;
		}


		var targetEntity = GetSingleton<CommandTargetComponent>().targetEntity;
		if (EntityManager.Exists(targetEntity)) {
			if (EntityManager.HasComponent<DeadState>(targetEntity)) {
				if (!HasSingleton<LeftUntilTheResurrectionComponent>()) {
					var eventEntity = EntityManager.CreateEntity(typeof(LeftUntilTheResurrectionComponent));
					EntityManager.SetComponentData(eventEntity, new LeftUntilTheResurrectionComponent {
						StartTime = (float)Time.ElapsedTime,
						DurationTime = GetSingleton<CapturingCrystalsModeComponent>().SecondsForRessurection
					});
				}
			} else {
				if (HasSingleton<LeftUntilTheResurrectionComponent>()) {
					EntityManager.DestroyEntity(GetSingletonEntity<LeftUntilTheResurrectionComponent>());
				}
			}
		}
		
		if (!HasSingleton<LeftUntilTheResurrectionClientComponent>()) {
			Entities
				.ForEach((Entity entity,
						ref LeftUntilTheResurrectionComponent gameCountdownToEndComponent
						) => {
							EntityManager.CreateEntity(typeof(LeftUntilTheResurrectionClientComponent));
							LeftUntilTheResurrectionView.gameObject.SetActive(true);
						});
			return;
		}
		var gameCountdownToEndClientComponent = GetSingleton<LeftUntilTheResurrectionClientComponent>();

		Entities
			.ForEach((Entity entity,
					ref LeftUntilTheResurrectionComponent leftUntilTheResurrectionComponent
			) => {
						
						var elapsedTime = (float)Time.ElapsedTime - leftUntilTheResurrectionComponent.StartTime;
				
						leftUntilTheResurrectionComponent.TimeLeftFloat =
							leftUntilTheResurrectionComponent.DurationTime - elapsedTime;
						leftUntilTheResurrectionComponent.TimeLeft = Mathf.FloorToInt(leftUntilTheResurrectionComponent.TimeLeftFloat) + 1;

						
						var lastSecondsLeft = leftUntilTheResurrectionComponent.TimeLeft;
						
						if (lastSecondsLeft != gameCountdownToEndClientComponent.LastSecondsLeft) {

							gameCountdownToEndClientComponent.LastSecondsLeft = lastSecondsLeft;
							SetSingleton(gameCountdownToEndClientComponent);	
							
							var eventEntity = EntityManager.CreateEntity(typeof(NotificationOfTheRemainingTimeComponent));
							EntityManager.SetComponentData(eventEntity, new NotificationOfTheRemainingTimeComponent {
								RemainingSeconds = lastSecondsLeft
							});
						}
					});
		Entities
			.ForEach((Entity entity,
					ref NotificationOfTheRemainingTimeComponent notificationOfTheRemainingTimeComponent
					) => {
				if (notificationOfTheRemainingTimeComponent.RemainingSeconds == 0) {
					_leftUntilTheResurrectionView.gameObject.SetActive(false);
				}
				LeftUntilTheResurrectionView.SetRemainingSeconds(notificationOfTheRemainingTimeComponent.RemainingSeconds);
				EntityManager.DestroyEntity(entity);
			});
		
	}
	
}
}