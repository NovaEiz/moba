﻿using Unity.Core;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
public class GameCountdownToEndServerSystem : ComponentSystem {

	public struct CreateGameCountdownToEndRequest : IComponentData {
		public int LeadingTeamId;
	}

	protected override void OnCreate() {
		RequireSingletonForUpdate<CapturingCrystalsModeComponent>();
	}

	protected override void OnUpdate() {
		Entities
			.ForEach((Entity reqEntity,
					ref CreateGameCountdownToEndRequest createGameCountdownToEndRequest
					) => {
						EntityManager.DestroyEntity(reqEntity);

						CreateGameCountdownToEndGhost(createGameCountdownToEndRequest.LeadingTeamId);
					});
		Entities
			.WithNone<GameEnded>()
			.ForEach((Entity entity,
					ref GameCountdownToEndComponent gameCountdownToEndComponent
					) => {
				var elapsedTime = (float)Time.ElapsedTime - gameCountdownToEndComponent.StartTime;
				//elapsedTime /= gameCountdownToEndComponent.DurationOneSecond;
				
						gameCountdownToEndComponent.TimeLeftFloat =
							((gameCountdownToEndComponent.DurationTime * gameCountdownToEndComponent.DurationOneSecond)
						- elapsedTime) / gameCountdownToEndComponent.DurationOneSecond;
						
						gameCountdownToEndComponent.TimeLeft = Mathf.FloorToInt(gameCountdownToEndComponent.TimeLeftFloat) + 1;

						if (gameCountdownToEndComponent.TimeLeftFloat < 0) {
							EntityManager.AddComponentData(entity, new GameEnded());
							return;
						}});
	}

	public void CreateGameCountdownToEndGhost(int leadingTeamId) {
		var prefabGhostEntity = Entity.Null;
		
		var ghostCollectionEntity = GetSingletonEntity<GhostPrefabCollectionComponent>();
		var serverPrefabs = EntityManager.GetBuffer<GhostPrefabBuffer>(ghostCollectionEntity);
		for (int ghostId = 0; ghostId < serverPrefabs.Length; ++ghostId) {
			if (EntityManager.HasComponent<GameCountdownToEndComponent>(serverPrefabs[ghostId].Value)) {
				prefabGhostEntity = serverPrefabs[ghostId].Value;
				break;
			}
		}
		var instanceEntity = EntityManager.Instantiate(prefabGhostEntity);
		//var instanceEntity = GhostCollectionSystem.CreatePredictedSpawnPrefab(EntityManager, prefabGhostEntity);

		var capturingCrystalsModeComponent = GetSingleton<CapturingCrystalsModeComponent>();
		
		EntityManager.SetComponentData(instanceEntity, new GhostOwnerComponent { NetworkId = -1});
		EntityManager.SetComponentData(instanceEntity, new GameCountdownToEndComponent {
			StartTime = (float)Time.ElapsedTime,
			DurationTime = capturingCrystalsModeComponent.SecondsForEndGame,
			DurationOneSecond = capturingCrystalsModeComponent.SecondsForEndGame_DurationOneSecond,
			LeadingTeamId = leadingTeamId
		});
	}
		
}
}