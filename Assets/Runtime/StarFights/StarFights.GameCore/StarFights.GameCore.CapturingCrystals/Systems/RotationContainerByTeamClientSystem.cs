﻿using Nighday.GameCamera;
using StarFights.GameCore.Location;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
public class RotationContainerByTeamClientSystem : SystemBase {

	private struct NeedRotate : IComponentData { }

	protected override void OnCreate() {
		RequireSingletonForUpdate<NeedRotate>();
		RequireSingletonForUpdate<RotationContainerByTeamComponent>();
		RequireSingletonForUpdate<CommandTargetComponent>();
		RequireSingletonForUpdate<TrackingCamera>();

		EntityManager.CreateEntity(typeof(NeedRotate));
	}

	protected override void OnUpdate() {
		var commandTargetComponent = GetSingleton<CommandTargetComponent>();
		var targetEntity = commandTargetComponent.targetEntity;
		if (!EntityManager.Exists(targetEntity)) {
			return;
		}

		var teamId = EntityManager.GetComponentData<TeamId>(targetEntity).Value;
		
		
		var trackingCamera = GetSingleton<TrackingCamera>();

		Entities
			.ForEach((Entity entity,
					Transform transform,
					ref RotationContainerByTeamComponent rotationContainerByTeamComponent,
					ref Rotation rotation
					) => {
				var angle = 0f;
				if (teamId == 1) {
					angle = 0f;
				} else {
					angle = 180f;
					
					EntityManager.AddComponentData(targetEntity, new TurnedByTeamAngleComponent {
						Angle = angle
					});
				}
				
				rotation.Value = math.mul(rotation.Value, quaternion.Euler(
											new float3(0, 
														math.radians(angle), 
														0)
										));
				
				EntityManager.AddComponentData(entity, new CopyTransformToGameObject());
				
				EntityManager.DestroyEntity(GetSingletonEntity<NeedRotate>());

				trackingCamera.RotationAngles.y = angle;
				SetSingleton(trackingCamera);
			}).WithStructuralChanges().Run();
	}
		
}
}