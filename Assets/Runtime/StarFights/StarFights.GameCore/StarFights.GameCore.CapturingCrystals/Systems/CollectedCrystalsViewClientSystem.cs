﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Matchmaking.General;
using StarFights.GameCore.Location;
using StarFights.GameCore.Unit;
using StarFights.General;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AddressableAssets;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
[UpdateInGroup(typeof(ClientSimulationSystemGroup))]
[UpdateBefore(typeof(BuildPhysicsWorld))]
public class CollectedCrystalsViewClientSystem : SystemBase {

	private struct Creating : IComponentData {
		
	}

	protected override void OnCreate() {
		RequireSingletonForUpdate<CapturingCrystalsModeComponent>();
	}

	private Camera _camera;
	
	private IEnumerator LoadCollectedCrystalsView(Entity unitEntity) {
		if (_camera == null) {
			_camera = EntityManager.GetComponentObject<Camera>(GetSingletonEntity<GameCameraComponent>());
		}

		var asyncOperation = Addressables.LoadAssetAsync<GameObject>("CollectedCrystalsView");

		var gameCanvasObj = Nighday.Application.App.World.DataManager.GetGameObject("Canvas - Game");
		var gameCanvasTr = gameCanvasObj.GetComponent<Transform>();
		var gameViewTr = gameCanvasTr.Find("GameViewContainer/GameView");
		
		yield return asyncOperation;
		
		var collectedCrystalsView = GameObject.Instantiate(asyncOperation.Result.GetComponent<CollectedCrystalsView>(), gameViewTr);
		EntityManager.AddComponentObject(unitEntity, collectedCrystalsView);
		EntityManager.RemoveComponent<Creating>(unitEntity);

		while (!EntityManager.HasComponent<Child>(unitEntity)) {
			yield return null;
		}
		
		var childBuffer = EntityManager.GetBuffer<Child>(unitEntity);
		var childArray = childBuffer.ToNativeArray(Allocator.Temp);
		foreach (var child in childArray) {
			if (EntityManager.HasComponent<Transform>(child.Value)) {
				collectedCrystalsView.SetTarget(EntityManager.GetComponentObject<Transform>(child.Value));
				collectedCrystalsView.SetCamera(_camera);
				break;
			}
		}
		
		EntityManager.RemoveComponent<IsLoadingTag>(unitEntity);
		EntityManager.AddComponentObject(unitEntity, collectedCrystalsView);
		collectedCrystalsView.gameObject.SetActive(false);
	}

	private struct IsLoadingTag : IComponentData {}

	protected override void OnUpdate() {

		Entities
			.WithNone<IsLoadingTag>()
			.WithNone<CollectedCrystalsView>()
			.ForEach((Entity entity,
					ref CollectedCrystalsComponent collectedCrystalsComponent
					) => {
						EntityManager.AddComponentData(entity, new IsLoadingTag());
				
						MainMonoBehaviour.Instance.StartCoroutine(LoadCollectedCrystalsView(entity));
			}).WithStructuralChanges().Run();
		
		Entities
			.ForEach((Entity entity,
					CollectedCrystalsView collectedCrystalsView,
					ref CollectedCrystalsComponent collectedCrystalsComponent,
					ref LocalToWorld localToWorld
					) => {
						if (collectedCrystalsComponent.Amount > 0) {
							collectedCrystalsView.gameObject.SetActive(true);
							collectedCrystalsView.SetAmount(collectedCrystalsComponent.Amount);
						} else {
							collectedCrystalsView.gameObject.SetActive(false);
						}
					}).WithStructuralChanges().Run();
	}
		
}
}