﻿using System;
using System.Collections.Generic;
using Nighday.Application;
using Nighday.General.PhysicsExtentions;
using StarFights.GameCore.Location;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Physics;
using Unity.Transforms;
using UnityEngine;
using ComponentSystem = Unity.Entities.ComponentSystem;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
public class DeterminingCrystalsInTeamForGameEndServerSystem : ComponentSystem {

	private Dictionary<TeamId, int> _collectedCrystalsInTeam = new Dictionary<TeamId, int>();

	protected override void OnCreate() {
		RequireSingletonForUpdate<CapturingCrystalsModeComponent>();
	}

	protected override void OnUpdate() {
		var capturingCrystalsModeComponent = GetSingleton<CapturingCrystalsModeComponent>();
		Entities
			.ForEach((Entity entity,
					ref TeamId teamId,
					ref CollectedCrystalsComponent characterCollectedCoins
					) => {
						if (_collectedCrystalsInTeam.TryGetValue(teamId, out int amount)) {
							amount += characterCollectedCoins.Amount;
						} else {
							_collectedCrystalsInTeam.Add(teamId, characterCollectedCoins.Amount);
						}
					});

		foreach (var itemPair in _collectedCrystalsInTeam) {
			if (itemPair.Value >= capturingCrystalsModeComponent.NeedCrystalsAmountForGameEnd) {
				if (!HasSingleton<GameCountdownToEndComponent>() && !HasSingleton<GameCountdownToEndServerSystem.CreateGameCountdownToEndRequest>()) {
					var eventEntity = EntityManager.CreateEntity(typeof(GameCountdownToEndServerSystem.CreateGameCountdownToEndRequest));
					EntityManager.AddComponentData(eventEntity, new GameCountdownToEndServerSystem.CreateGameCountdownToEndRequest {
						LeadingTeamId = itemPair.Key.Value
					});
				}
				_collectedCrystalsInTeam.Clear();
				return;
			}
		}

		if (HasSingleton<GameCountdownToEndComponent>()) {
			EntityManager.DestroyEntity(GetSingletonEntity<GameCountdownToEndComponent>());
		}
		
		_collectedCrystalsInTeam.Clear();
	}
		
}
}