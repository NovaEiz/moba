﻿using System;
using System.Collections;
using System.Threading.Tasks;
using Nighday.Application;
using Nighday.Matchmaking.General;
using StarFights.GameCore.Location;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using UnityEngine.AddressableAssets;
using ComponentSystem = Unity.Entities.ComponentSystem;
using IComponentData = Unity.Entities.IComponentData;

namespace StarFights.GameCore.CapturingCrystals {

[DisableAutoCreation]
public class GameCountdownToEndClientSystem : ComponentSystem {
	
	public struct NotificationOfTheRemainingTimeComponent : IComponentData {
		public int RemainingSeconds;
		public float DurationTime;
	}
	
	private struct GameCountdownToEndClientComponent : IComponentData {
		public int LastSecondsLeft;
	}
	
	private struct InitializedTag : IComponentData {}

	protected override void OnCreate() {
		RequireSingletonForUpdate<InitializedTag>();
		RequireSingletonForUpdate<CapturingCrystalsModeComponent>();

		LoadGameCountdownToEndView();
	}

	private Transform _viewContainer;
	private GameCountdownToEndView _gameCountdownToEndViewPrefab;
	private GameCountdownToEndView _gameCountdownToEndView;

	private GameCountdownToEndView GameCountdownToEndView {
		get {
			if (_gameCountdownToEndView == null) {
				_gameCountdownToEndView = GameObject.Instantiate(_gameCountdownToEndViewPrefab, _viewContainer);
			}

			return _gameCountdownToEndView;
		}
	}

	private async Task LoadGameCountdownToEndView() {
		try {
			var asyncOperation = Addressables.LoadAssetAsync<GameObject>("GameCountdownToEndView");
			await asyncOperation.Task;
			_gameCountdownToEndViewPrefab = asyncOperation.Result.GetComponent<GameCountdownToEndView>();

			var gameCanvas = App.World.DataManager.GetGameObject("Canvas - Game");

			_viewContainer = gameCanvas.transform;//gameCanvas.transform.Find("GameViewContainer");

			EntityManager.CreateEntity(typeof(InitializedTag));
		}
		catch (Exception e) {
			Debug.LogError(e);
		}
	}

	protected override void OnUpdate() {
		if (!HasSingleton<GameCountdownToEndComponent>()
			&&
			HasSingleton<GameCountdownToEndClientComponent>()
		) {
			GameCountdownToEndView.gameObject.SetActive(false);
			EntityManager.DestroyEntity(GetSingletonEntity<GameCountdownToEndClientComponent>());
			return;
		}

		if (!HasSingleton<GameCountdownToEndClientComponent>()) {
			Entities
				.ForEach((Entity entity,
						ref GameCountdownToEndComponent gameCountdownToEndComponent
						) => {
							EntityManager.CreateEntity(typeof(GameCountdownToEndClientComponent));
							GameCountdownToEndView.gameObject.SetActive(true);


							var targetEntity = GetSingleton<CommandTargetComponent>().targetEntity;
							var teamId = EntityManager.GetComponentData<TeamId>(targetEntity);
							var leadingTeamId = gameCountdownToEndComponent.LeadingTeamId;
							if (teamId.Value == 2) {
								if (leadingTeamId == 1) {
									leadingTeamId = 2;
								} else if (leadingTeamId == 2) {
									leadingTeamId = 1;
								}
							}
							GameCountdownToEndView.SetLeadingTeam(leadingTeamId);

						});
			return;
		}
		var gameCountdownToEndClientComponent = GetSingleton<GameCountdownToEndClientComponent>();

		Entities
			.ForEach((Entity entity,
					ref GameCountdownToEndComponent gameCountdownToEndComponent
			) => {
						var lastSecondsLeft = gameCountdownToEndComponent.TimeLeft;
						
						if (lastSecondsLeft != gameCountdownToEndClientComponent.LastSecondsLeft) {

							gameCountdownToEndClientComponent.LastSecondsLeft = lastSecondsLeft;
							SetSingleton(gameCountdownToEndClientComponent);	
							
							var eventEntity = EntityManager.CreateEntity(typeof(NotificationOfTheRemainingTimeComponent));
							EntityManager.SetComponentData(eventEntity, new NotificationOfTheRemainingTimeComponent {
								RemainingSeconds = lastSecondsLeft,
								DurationTime = gameCountdownToEndComponent.DurationTime
							});
						}
					});
		Entities
			.ForEach((Entity entity,
					ref NotificationOfTheRemainingTimeComponent notificationOfTheRemainingTimeComponent
					) => {
				if (notificationOfTheRemainingTimeComponent.RemainingSeconds == 0) {
					var gameViewContainer = App.World.DataManager.GetGameObject("GameViewContainer");
					if (gameViewContainer != null) {
						gameViewContainer.SetActive(false);
					}
					MainMonoBehaviour.Instance.StartCoroutine(WaitEndToLobbyWithBattleResultWindowIe());
				}
				GameCountdownToEndView.SetRemainingSeconds(notificationOfTheRemainingTimeComponent.RemainingSeconds, notificationOfTheRemainingTimeComponent.DurationTime);
				EntityManager.DestroyEntity(entity);
			});
	}
	
	private IEnumerator WaitEndToLobbyWithBattleResultWindowIe() {
		yield return new WaitForSeconds(2.5f);
		EntityManager.CreateEntity(typeof(GameEnded));
	}
	
	private float RoundDown(float number, int p) {
		return Mathf.Round(number - number % Mathf.Pow(10, p));
	}
		
}
}