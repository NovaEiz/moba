using Unity.Mathematics;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {
[CreateAssetMenu(fileName = "ThrowingCrystalsComponentAsset", menuName = "StarFights/ThrowingCrystalsComponentAsset", order = 0)]
public class ThrowingCrystalsComponentAsset : ScriptableObject {

	public float ThrowImpulse = 1f;
	public float ThrowDurationTime = 1f;
	public float ThrowingSpeed = 1f;
	public float2 DistanceRange;
	public float2 ThrowIntervalRange;
	
}
}