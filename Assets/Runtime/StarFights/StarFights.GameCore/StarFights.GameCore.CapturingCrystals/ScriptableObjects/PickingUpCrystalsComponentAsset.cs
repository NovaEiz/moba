using Unity.Mathematics;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {
[CreateAssetMenu(fileName = "PickingUpCrystalsComponentAsset", menuName = "StarFights/PickingUpCrystalsComponentAsset", order = 0)]
public class PickingUpCrystalsComponentAsset : ScriptableObject {

	public float PickingUpDurationTime = 1f;
	public float TargetY = 1f;
	
}
}