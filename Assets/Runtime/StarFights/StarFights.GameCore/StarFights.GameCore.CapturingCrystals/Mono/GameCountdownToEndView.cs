using System;
using TMPro;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {
public class GameCountdownToEndView : MonoBehaviour {

	[SerializeField] private Animator _animator;
	[SerializeField] private TextMeshProUGUI _text;

	private string _startText;

	private void OnEnable() {
		_text.text = "";
	}

	public void SetRemainingSeconds(int value, float maxValue) {
		if (maxValue - value < 2) {
			_text.text = _startText + "\nCOUNTDOWN";
		} else if (value == 0) {
			_text.text = "The fight is over";
			_animator.Play("Notification", 0, 0);
		} else {
			_text.text = value.ToString();
			_animator.Play("Notification", 0, 0);
		}
	}
	public void SetLeadingTeam(int leadingTeamId) {
		if (leadingTeamId == 1) {
			_startText = "<color=#36E4F7>Blue team</color>";
		} else if (leadingTeamId == 2) {
			_startText = "<color=#FF0000>Red team</color>";
		}
	}

	
}
}