using System;
using TMPro;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {
public class LeftUntilTheResurrectionView : MonoBehaviour {

	[SerializeField] private Animator _animator;
	[SerializeField] private TextMeshProUGUI _text;

	private void OnEnable() {
		_animator.Play("Notification", 0, 0);
	}

	public void SetRemainingSeconds(int value) {
		if (value == 0) {
			_text.text = "";
		} else {
			_text.text = "Return via\n" + value.ToString();
		}
	}

}
}