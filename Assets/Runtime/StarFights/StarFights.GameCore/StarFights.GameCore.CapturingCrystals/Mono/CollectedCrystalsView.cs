using Cinemachine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StarFights.GameCore.CapturingCrystals {

public class CollectedCrystalsView : MonoBehaviour {

	[SerializeField] private TextMeshProUGUI _amountText;

	public void SetAmount(int amount) {
		_amountText.text = amount.ToString();
	}

	public void SetPosition(Vector3 position) {
		transform.position = position;
	}
	
	private Transform _target;
	public void SetTarget(Transform value) {
		_target = value;
	}
	private Camera _camera;
	public void SetCamera(Camera value) {
		_camera = value;
		CinemachineCore.CameraUpdatedEvent.AddListener((cinemachineBrain) => {
			UpdatePosition();
		});
	}

	private void UpdatePosition() {
		var worldPosition = _target.position + new Vector3(0, 2.5f, 0);
		var position = _camera.WorldToScreenPoint(worldPosition);
		SetPosition(position);
	}
}
}