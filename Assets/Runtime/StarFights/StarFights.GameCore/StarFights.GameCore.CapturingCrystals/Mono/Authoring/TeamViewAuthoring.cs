using Unity.Entities;
using UnityEngine;

namespace StarFights.GameCore.CapturingCrystals {

public class TeamViewAuthoring : MonoBehaviour, IConvertGameObjectToEntity {

	[SerializeField] private TeamView _team;

	public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem) {
		dstManager.AddComponentObject(entity, _team);
	}
}

}