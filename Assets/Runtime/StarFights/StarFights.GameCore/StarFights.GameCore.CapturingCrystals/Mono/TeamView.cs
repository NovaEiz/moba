using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace StarFights.GameCore.CapturingCrystals {

public class TeamView : MonoBehaviour {

	[SerializeField] private int _teamId;
	
	[SerializeField] private TextMeshProUGUI _collectedCrystalsText;
	[SerializeField] private Image _progressImage;

	public int TeamId => _teamId;

	public void SetProgress(int amount, int maxAmount) {
		_collectedCrystalsText.text = amount.ToString();
		_progressImage.fillAmount = (float)amount / maxAmount - 0.01f;
	}

}
}